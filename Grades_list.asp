<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-0BEF4380
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Grades
Dim Header
Dim Footer
Dim GradesSearch
Dim Grades1
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "Grades_list.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "Grades_list.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Authenticate User @1-3A3D5059
CCSecurityRedirect "3", Empty
'End Authenticate User

'Initialize Objects @1-EEE4F4CD
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Grades = New clsGridGrades
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Footer = New clsFooter
Set Footer.Attributes = Attributes
Footer.Initialize "Footer", ""
Set GradesSearch = new clsRecordGradesSearch
Set Grades1 = new clsRecordGrades1
Grades.Initialize DBFusionHO
Grades1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Grades_list_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-BF4A3202
Header.Operations
Footer.Operations
GradesSearch.Operation
Grades1.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-64E91E80
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Grades, Header, Footer, GradesSearch, Grades1))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-A1105212
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set Grades = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Footer.UnloadPage
    Set Footer = Nothing
    Set GradesSearch = Nothing
    Set Grades1 = Nothing
End Sub
'End UnloadPage Sub

'DEL      Private Sub Class_Initialize()
'DEL  
'DEL          Visible = False
'DEL          Set Errors = New clsErrors
'DEL          Set CCSEvents = CreateObject("Scripting.Dictionary")
'DEL          Set Attributes = New clsAttributes
'DEL          InsertAllowed = False
'DEL          UpdateAllowed = False
'DEL          DeleteAllowed = False
'DEL          ReadAllowed = True
'DEL          Dim Method
'DEL          Dim OperationMode
'DEL          OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
'DEL          If UBound(OperationMode) > -1 Then 
'DEL              FormSubmitted = (OperationMode(0) = "GradesSearch")
'DEL          End If
'DEL          If UBound(OperationMode) > 0 Then 
'DEL              EditMode = (OperationMode(1) = "Edit")
'DEL          End If
'DEL          ComponentName = "GradesSearch"
'DEL          Method = IIf(FormSubmitted, ccsPost, ccsGet)
'DEL          Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
'DEL          Set s_keyword = CCCreateControl(ccsTextBox, "s_keyword", Empty, ccsMemo, Empty, CCGetRequestParam("s_keyword", Method))
'DEL          Set ValidatingControls = new clsControls
'DEL          ValidatingControls.addControls Array(s_keyword)
'DEL      End Sub



Class clsGridGrades 'Grades Class @6-A0A925CF

'Grades Variables @6-71E7FA9C

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Grades_Insert
    Dim Sorter_grade_id
    Dim grade_id
    Dim grade_type_name
    Dim Navigator
    Dim grade_name
'End Grades Variables

'Grades Class_Initialize Event @6-15C9CA13
    Private Sub Class_Initialize()
        ComponentName = "Grades"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsGradesDataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 20 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("GradesOrder", Empty)
        SortingDirection = CCGetParam("GradesDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Grades_Insert = CCCreateControl(ccsLink, "Grades_Insert", Empty, ccsText, Empty, CCGetRequestParam("Grades_Insert", ccsGet))
        Set Sorter_grade_id = CCCreateSorter("Sorter_grade_id", Me, FileName)
        Set grade_id = CCCreateControl(ccsLink, "grade_id", Empty, ccsInteger, Empty, CCGetRequestParam("grade_id", ccsGet))
        Set grade_type_name = CCCreateControl(ccsLabel, "grade_type_name", Empty, ccsMemo, Empty, CCGetRequestParam("grade_type_name", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpSimple)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        Set grade_name = CCCreateControl(ccsLabel, "grade_name", Empty, ccsText, Empty, CCGetRequestParam("grade_name", ccsGet))
    IsDSEmpty = True
    End Sub
'End Grades Class_Initialize Event

'Grades Initialize Method @6-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Grades Initialize Method

'Grades Class_Terminate Event @6-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Grades Class_Terminate Event

'Grades Show Method @6-752B4738
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urls_keyword") = CCGetRequestParam("s_keyword", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Grades_Insert, Sorter_grade_id, Navigator))
            
            Grades_Insert.Parameters = CCGetQueryString("QueryString", Array("grade_id", "ccsForm"))
            Grades_Insert.Parameters = CCAddParam(Grades_Insert.Parameters, "var", 1)
            Grades_Insert.Page = "Grades_list.asp"
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(grade_id, grade_type_name, grade_name))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Grades:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    grade_id.Value = Recordset.Fields("grade_id")
                    grade_id.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
                    grade_id.Parameters = CCAddParam(grade_id.Parameters, "grade_id", Recordset.Fields("grade_id_param1"))
                    grade_id.Page = "Grades_list.asp"
                    grade_type_name.Value = Recordset.Fields("grade_type_name")
                    grade_name.Value = Recordset.Fields("grade_name")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Grades:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Grades:"
            StaticControls.Show
        End If

    End Sub
'End Grades Show Method

'Grades PageSize Property Let @6-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Grades PageSize Property Let

'Grades PageSize Property Get @6-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Grades PageSize Property Get

'Grades RowNumber Property Get @6-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Grades RowNumber Property Get

'Grades HasNextRow Function @6-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Grades HasNextRow Function

End Class 'End Grades Class @6-A61BA892

Class clsGradesDataSource 'GradesDataSource Class @6-FDCCC8A7

'DataSource Variables @6-D7A155A9
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public grade_id
    Public grade_id_param1
    Public grade_type_name
    Public grade_name
'End DataSource Variables

'DataSource Class_Initialize Event @6-C592C01E
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set grade_id = CCCreateField("grade_id", "grade_id", ccsInteger, Empty, Recordset)
        Set grade_id_param1 = CCCreateField("grade_id_param1", "grade_id", ccsText, Empty, Recordset)
        Set grade_type_name = CCCreateField("grade_type_name", "grade_type_name", ccsMemo, Empty, Recordset)
        Set grade_name = CCCreateField("grade_name", "grade_name", ccsText, Empty, Recordset)
        Fields.AddFields Array(grade_id, grade_id_param1, grade_type_name, grade_name)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_grade_id", "grade_id", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} Grades.grade_id, grade_type_name, grade_name  " & vbLf & _
        "FROM Grades LEFT JOIN Grade_type ON " & vbLf & _
        "Grades.grade_type_id = Grade_type.grade_type_id {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM Grades LEFT JOIN Grade_type ON " & vbLf & _
        "Grades.grade_type_id = Grade_type.grade_type_id"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @6-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @6-44F4389C
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urls_keyword", ccsMemo, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opContains, False, "[Grades].grade_name", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @6-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @6-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End GradesDataSource Class @6-A61BA892

Class clsRecordGradesSearch 'GradesSearch Class @28-CE4F30B5

'GradesSearch Variables @28-779C7885

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim s_keyword
    Dim Button_DoSearch1
'End GradesSearch Variables

'GradesSearch Class_Initialize Event @28-BD720752
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "GradesSearch")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "GradesSearch"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set s_keyword = CCCreateControl(ccsTextBox, "s_keyword", Empty, ccsMemo, Empty, CCGetRequestParam("s_keyword", Method))
        Set Button_DoSearch1 = CCCreateButton("Button_DoSearch1", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_keyword)
    End Sub
'End GradesSearch Class_Initialize Event

'GradesSearch Class_Terminate Event @28-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End GradesSearch Class_Terminate Event

'GradesSearch Validate Method @28-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End GradesSearch Validate Method

'GradesSearch Operation Method @28-584EFFC9
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch1"
            If Button_DoSearch1.Pressed Then
                PressedButton = "Button_DoSearch1"
            End If
        End If
        Redirect = "Grades_list.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch1" Then
                If NOT Button_DoSearch1.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "Grades_list.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch1.x", "Button_DoSearch1.y", "Button_DoSearch1"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End GradesSearch Operation Method

'GradesSearch Show Method @28-A650638B
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "GradesSearch" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_keyword, Button_DoSearch1))
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_keyword.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "GradesSearch" & ":"
            Controls.Show
        End If
    End Sub
'End GradesSearch Show Method

End Class 'End GradesSearch Class @28-A61BA892

Class clsRecordGrades1 'Grades1 Class @2-6DA2CB9E

'Grades1 Variables @2-3E6E2EBE

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim grade_name
    Dim grade_type_id
    Dim grade_id
    Dim Button_Insert1
    Dim Button_Update
    Dim Button_Delete
    Dim Button_Cancel
'End Grades1 Variables

'Grades1 Class_Initialize Event @2-DC7DF486
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsGrades1DataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Grades1")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Grades1"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set grade_name = CCCreateControl(ccsTextBox, "grade_name", "Name", ccsMemo, Empty, CCGetRequestParam("grade_name", Method))
        grade_name.Required = True
        Set grade_type_id = CCCreateList(ccsListBox, "grade_type_id", "Type Id", ccsInteger, CCGetRequestParam("grade_type_id", Method), Empty)
        grade_type_id.BoundColumn = "grade_type_id"
        grade_type_id.TextColumn = "grade_type_name"
        Set grade_type_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Grade_type {SQL_Where} {SQL_OrderBy}", "", ""))
        grade_type_id.Required = True
        Set grade_id = CCCreateControl(ccsTextBox, "grade_id", Empty, ccsText, Empty, CCGetRequestParam("grade_id", Method))
        grade_id.Required = True
        Set Button_Insert1 = CCCreateButton("Button_Insert1", Method)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Delete = CCCreateButton("Button_Delete", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(grade_name, grade_type_id, grade_id)
    End Sub
'End Grades1 Class_Initialize Event

'Grades1 Initialize Method @2-10254A92
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
            .Parameters("urlgrade_id") = CCGetRequestParam("grade_id", ccsGET)
        End With
    End Sub
'End Grades1 Initialize Method

'Grades1 Class_Terminate Event @2-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Grades1 Class_Terminate Event

'Grades1 Validate Method @2-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Grades1 Validate Method

'Grades1 Operation Method @2-E5E38A18
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Insert1")
            If Button_Insert1.Pressed Then
                PressedButton = "Button_Insert1"
            ElseIf Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Delete.Pressed Then
                PressedButton = "Button_Delete"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = "Grades_list.asp"
        If PressedButton = "Button_Delete" Then
            If NOT Button_Delete.OnClick OR NOT DeleteRow() Then
                Redirect = ""
            End If
        ElseIf PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Insert1" Then
                If NOT Button_Insert1.OnClick() OR NOT InsertRow() Then
                    Redirect = ""
                End If
            ElseIf PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Grades1 Operation Method

'Grades1 InsertRow Method @2-4C4AA7A2
    Function InsertRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInsert", Me)
        If NOT InsertAllowed Then InsertRow = False : Exit Function
        DataSource.grade_name.Value = grade_name.Value
        DataSource.grade_type_id.Value = grade_type_id.Value
        DataSource.grade_id.Value = grade_id.Value
        DataSource.Insert(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInsert", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        InsertRow = (Errors.Count = 0)
    End Function
'End Grades1 InsertRow Method

'Grades1 UpdateRow Method @2-AC6D9CEC
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        DataSource.grade_name.Value = grade_name.Value
        DataSource.grade_type_id.Value = grade_type_id.Value
        DataSource.grade_id.Value = grade_id.Value
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End Grades1 UpdateRow Method

'Grades1 DeleteRow Method @2-D5C1DF24
    Function DeleteRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeDelete", Me)
        If NOT DeleteAllowed Then DeleteRow = False : Exit Function
        DataSource.Delete(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterDelete", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        DeleteRow = (Errors.Count = 0)
    End Function
'End Grades1 DeleteRow Method

'Grades1 Show Method @2-616EAD55
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Grades1" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(grade_id, grade_name, grade_type_id, Button_Insert1, Button_Update, Button_Delete, Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        grade_name.Value = Recordset.Fields("grade_name")
                        grade_type_id.Value = Recordset.Fields("grade_type_id")
                        grade_id.Value = Recordset.Fields("grade_id")
                    End If
                Else
                    EditMode = False
                End If
            End If
        End If
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors grade_name.Errors
            Errors.AddErrors grade_type_id.Errors
            Errors.AddErrors grade_id.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Insert1.Visible = NOT EditMode AND InsertAllowed
        Button_Update.Visible = EditMode AND UpdateAllowed
        Button_Delete.Visible = EditMode AND DeleteAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Grades1" & ":"
            Controls.Show
        End If
    End Sub
'End Grades1 Show Method

End Class 'End Grades1 Class @2-A61BA892

Class clsGrades1DataSource 'Grades1DataSource Class @2-E3E5247B

'DataSource Variables @2-2C745668
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public grade_name
    Public grade_type_id
    Public grade_id
'End DataSource Variables

'DataSource Class_Initialize Event @2-FA12D140
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set grade_name = CCCreateField("grade_name", "grade_name", ccsMemo, Empty, Recordset)
        Set grade_type_id = CCCreateField("grade_type_id", "grade_type_id", ccsInteger, Empty, Recordset)
        Set grade_id = CCCreateField("grade_id", "grade_id", ccsText, Empty, Recordset)
        Fields.AddFields Array(grade_name, grade_type_id, grade_id)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "grade_name", True
        InsertOmitIfEmpty.Add "grade_type_id", True
        InsertOmitIfEmpty.Add "grade_id", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "grade_name", True
        UpdateOmitIfEmpty.Add "grade_type_id", True
        UpdateOmitIfEmpty.Add "grade_id", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT *  " & vbLf & _
        "FROM Grades {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @2-029C89C2
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlgrade_id", ccsInteger, Empty, Empty, Empty, False
            AllParamsSet = .AllParamsSet
            .Criterion(1) = .Operation(opEqual, False, "grade_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @2-48A2DA7D
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @2-420EC1EF
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM [Grades]" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @2-FD0E0F5B
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Cmd.Prepared = True
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_grade_name : IsDef_grade_name = CCIsDefined("grade_name", "Form")
        Dim IsDef_grade_type_id : IsDef_grade_type_id = CCIsDefined("grade_type_id", "Form")
        Dim IsDef_grade_id : IsDef_grade_id = CCIsDefined("grade_id", "Form")
        If Not UpdateOmitIfEmpty("grade_name") Or IsDef_grade_name Then Cmd.AddSQLStrings "grade_name=" & "?", Empty
        If Not UpdateOmitIfEmpty("grade_type_id") Or IsDef_grade_type_id Then Cmd.AddSQLStrings "grade_type_id=" & Connection.ToSQL(grade_type_id, grade_type_id.DataType), Empty
        If Not UpdateOmitIfEmpty("grade_id") Or IsDef_grade_id Then Cmd.AddSQLStrings "grade_id=" & Connection.ToSQL(grade_id, grade_id.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "[Grades]", Where)
        Cmd.CommandParameters = Array( _
            IIF(IsDef_grade_name,Array("grade_name", adLongVarChar, adParamInput, 2147483647, grade_name.Value), Empty))
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @2-24A6A66E
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Cmd.Prepared = True
        Dim IsDef_grade_name : IsDef_grade_name = CCIsDefined("grade_name", "Form")
        Dim IsDef_grade_type_id : IsDef_grade_type_id = CCIsDefined("grade_type_id", "Form")
        Dim IsDef_grade_id : IsDef_grade_id = CCIsDefined("grade_id", "Form")
        If Not InsertOmitIfEmpty("grade_name") Or IsDef_grade_name Then Cmd.AddSQLStrings "grade_name", "?"
        If Not InsertOmitIfEmpty("grade_type_id") Or IsDef_grade_type_id Then Cmd.AddSQLStrings "grade_type_id", Connection.ToSQL(grade_type_id, grade_type_id.DataType)
        If Not InsertOmitIfEmpty("grade_id") Or IsDef_grade_id Then Cmd.AddSQLStrings "grade_id", Connection.ToSQL(grade_id, grade_id.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "[Grades]", Empty)
        Cmd.CommandParameters = Array( _
            IIF(IsDef_grade_name,Array("grade_name", adLongVarChar, adParamInput,2147483647, grade_name.Value), Empty) _
        )
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End Grades1DataSource Class @2-A61BA892

'Include Page Implementation @21-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation

'Include Page Implementation @22-79649078
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Footer.asp" -->
<%
'End Include Page Implementation


%>
