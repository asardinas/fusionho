<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-60B531B0
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Header
Dim Sites_price_change_status
Dim Grid2
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "change_price.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "change_price.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-F8F8B8A2
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Sites_price_change_status = New clsGridSites_price_change_status
Set Grid2 = New clsGridGrid2
Sites_price_change_status.Initialize DBFusionHO
Grid2.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/change_price_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-D491FF66
Header.Operations
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-A094483D
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Header, Sites_price_change_status, Grid2))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-8115D955
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Set Sites_price_change_status = Nothing
    Set Grid2 = Nothing
End Sub
'End UnloadPage Sub

Class clsGridSites_price_change_status 'Sites_price_change_status Class @104-2C16F321

'Sites_price_change_status Variables @104-24C13674

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_nuevo
    Dim Sorter_SiteName
    Dim nuevo
    Dim SiteName
    Dim Navigator
'End Sites_price_change_status Variables

'Sites_price_change_status Class_Initialize Event @104-3CB0848F
    Private Sub Class_Initialize()
        ComponentName = "Sites_price_change_status"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsSites_price_change_statusDataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Sites_price_change_statusOrder", Empty)
        SortingDirection = CCGetParam("Sites_price_change_statusDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_nuevo = CCCreateSorter("Sorter_nuevo", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set nuevo = CCCreateControl(ccsLabel, "nuevo", Empty, ccsText, Empty, CCGetRequestParam("nuevo", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Sites_price_change_status Class_Initialize Event

'Sites_price_change_status Initialize Method @104-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Sites_price_change_status Initialize Method

'Sites_price_change_status Class_Terminate Event @104-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites_price_change_status Class_Terminate Event

'Sites_price_change_status Show Method @104-F2BA2308
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock


        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_nuevo, Sorter_SiteName, Navigator))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(nuevo, SiteName))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Sites_price_change_status:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    nuevo.Value = Recordset.Fields("nuevo")
                    SiteName.Value = Recordset.Fields("SiteName")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Sites_price_change_status:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Sites_price_change_status:"
            StaticControls.Show
        End If

    End Sub
'End Sites_price_change_status Show Method

'Sites_price_change_status PageSize Property Let @104-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Sites_price_change_status PageSize Property Let

'Sites_price_change_status PageSize Property Get @104-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Sites_price_change_status PageSize Property Get

'Sites_price_change_status RowNumber Property Get @104-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Sites_price_change_status RowNumber Property Get

'Sites_price_change_status HasNextRow Function @104-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Sites_price_change_status HasNextRow Function

End Class 'End Sites_price_change_status Class @104-A61BA892

Class clsSites_price_change_statusDataSource 'Sites_price_change_statusDataSource Class @104-4855D41F

'DataSource Variables @104-F895D67E
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public nuevo
    Public SiteName
'End DataSource Variables

'DataSource Class_Initialize Event @104-AECED74D
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set nuevo = CCCreateField("nuevo", "nuevo", ccsText, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Fields.AddFields Array(nuevo, SiteName)
        Orders = Array( _ 
            Array("Sorter_nuevo", "nuevo", ""), _
            Array("Sorter_SiteName", "SiteName", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} (Cast (aplication_date as varchar(10))  + '  ' + aplication_time)  AS nuevo, SiteName  " & vbLf & _
        "FROM Sites INNER JOIN price_change_status ON " & vbLf & _
        "Sites.ss_id = price_change_status.ss_id {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM Sites INNER JOIN price_change_status ON " & vbLf & _
        "Sites.ss_id = price_change_status.ss_id"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @104-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @104-98E5A92F
    Public Sub BuildTableWhere()
    End Sub
'End BuildTableWhere Method

'Open Method @104-6EA306C4
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @104-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Sites_price_change_statusDataSource Class @104-A61BA892

Class clsGridGrid2 'Grid2 Class @39-57B1525A

'Grid2 Variables @39-9B1560CA

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_price_change_id
    Dim Sorter_SiteName
    Dim Sorter_Aplication_Date
    Dim Sorter_Aplication_Time
    Dim Sorter_Reception_Date
    Dim Sorter_Reception_time
    Dim Sorter_File_Delivery_Date
    Dim Sorter_File_Delivery_Time
    Dim Sorter_Status
    Dim Sorter_Status_Description
    Dim price_change_id
    Dim SiteName
    Dim Aplication_Date
    Dim Aplication_Time
    Dim Reception_Date
    Dim Reception_time
    Dim File_Delivery_Date
    Dim File_Delivery_Time
    Dim Status
    Dim Status_Description
    Dim Navigator
    Dim Link1
'End Grid2 Variables

'Grid2 Class_Initialize Event @39-50EC9CE8
    Private Sub Class_Initialize()
        ComponentName = "Grid2"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsGrid2DataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Grid2Order", Empty)
        SortingDirection = CCGetParam("Grid2Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_price_change_id = CCCreateSorter("Sorter_price_change_id", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Aplication_Date = CCCreateSorter("Sorter_Aplication_Date", Me, FileName)
        Set Sorter_Aplication_Time = CCCreateSorter("Sorter_Aplication_Time", Me, FileName)
        Set Sorter_Reception_Date = CCCreateSorter("Sorter_Reception_Date", Me, FileName)
        Set Sorter_Reception_time = CCCreateSorter("Sorter_Reception_time", Me, FileName)
        Set Sorter_File_Delivery_Date = CCCreateSorter("Sorter_File_Delivery_Date", Me, FileName)
        Set Sorter_File_Delivery_Time = CCCreateSorter("Sorter_File_Delivery_Time", Me, FileName)
        Set Sorter_Status = CCCreateSorter("Sorter_Status", Me, FileName)
        Set Sorter_Status_Description = CCCreateSorter("Sorter_Status_Description", Me, FileName)
        Set price_change_id = CCCreateControl(ccsLink, "price_change_id", Empty, ccsInteger, Empty, CCGetRequestParam("price_change_id", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Aplication_Date = CCCreateControl(ccsLabel, "Aplication_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Aplication_Date", ccsGet))
        Set Aplication_Time = CCCreateControl(ccsLabel, "Aplication_Time", Empty, ccsText, Empty, CCGetRequestParam("Aplication_Time", ccsGet))
        Set Reception_Date = CCCreateControl(ccsLabel, "Reception_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Reception_Date", ccsGet))
        Set Reception_time = CCCreateControl(ccsLabel, "Reception_time", Empty, ccsText, Empty, CCGetRequestParam("Reception_time", ccsGet))
        Set File_Delivery_Date = CCCreateControl(ccsLabel, "File_Delivery_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("File_Delivery_Date", ccsGet))
        Set File_Delivery_Time = CCCreateControl(ccsLabel, "File_Delivery_Time", Empty, ccsText, Empty, CCGetRequestParam("File_Delivery_Time", ccsGet))
        Set Status = CCCreateControl(ccsLabel, "Status", Empty, ccsInteger, Empty, CCGetRequestParam("Status", ccsGet))
        Set Status_Description = CCCreateControl(ccsLabel, "Status_Description", Empty, ccsText, Empty, CCGetRequestParam("Status_Description", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        Set Link1 = CCCreateControl(ccsLink, "Link1", Empty, ccsText, Empty, CCGetRequestParam("Link1", ccsGet))
    IsDSEmpty = True
    End Sub
'End Grid2 Class_Initialize Event

'Grid2 Initialize Method @39-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Grid2 Initialize Method

'Grid2 Class_Terminate Event @39-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Grid2 Class_Terminate Event

'Grid2 Show Method @39-5717F83C
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urlss_id") = CCGetRequestParam("ss_id", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_price_change_id, Sorter_SiteName, Sorter_Aplication_Date, Sorter_Aplication_Time, Sorter_Reception_Date, Sorter_Reception_time, Sorter_File_Delivery_Date, Sorter_File_Delivery_Time, Sorter_Status, Sorter_Status_Description, Navigator, Link1))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
            
            Link1.Parameters = CCGetQueryString("QueryString", Array("price_change_id", "aplication_date", "aplication_time", "ccsForm"))
            Link1.Parameters = CCAddParam(Link1.Parameters, "var", 1)
            Link1.Page = "send_price.asp"
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(price_change_id, SiteName, Aplication_Date, Aplication_Time, Reception_Date, Reception_time, File_Delivery_Date, File_Delivery_Time, Status, Status_Description))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Grid2:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    price_change_id.Value = Recordset.Fields("price_change_id")
                    price_change_id.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
                    price_change_id.Parameters = CCAddParam(price_change_id.Parameters, "price_change_id", Recordset.Fields("price_change_id_param1"))
                    price_change_id.Page = "send_price.asp"
                    SiteName.Value = Recordset.Fields("SiteName")
                    Aplication_Date.Value = Recordset.Fields("Aplication_Date")
                    Aplication_Time.Value = Recordset.Fields("Aplication_Time")
                    Reception_Date.Value = Recordset.Fields("Reception_Date")
                    Reception_time.Value = Recordset.Fields("Reception_time")
                    File_Delivery_Date.Value = Recordset.Fields("File_Delivery_Date")
                    File_Delivery_Time.Value = Recordset.Fields("File_Delivery_Time")
                    Status.Value = Recordset.Fields("Status")
                    Status_Description.Value = Recordset.Fields("Status_Description")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Grid2:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Grid2:"
            StaticControls.Show
        End If

    End Sub
'End Grid2 Show Method

'Grid2 PageSize Property Let @39-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Grid2 PageSize Property Let

'Grid2 PageSize Property Get @39-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Grid2 PageSize Property Get

'Grid2 RowNumber Property Get @39-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Grid2 RowNumber Property Get

'Grid2 HasNextRow Function @39-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Grid2 HasNextRow Function

End Class 'End Grid2 Class @39-A61BA892

Class clsGrid2DataSource 'Grid2DataSource Class @39-441BB062

'DataSource Variables @39-CA8C2A28
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public price_change_id
    Public price_change_id_param1
    Public SiteName
    Public Aplication_Date
    Public Aplication_Time
    Public Reception_Date
    Public Reception_time
    Public File_Delivery_Date
    Public File_Delivery_Time
    Public Status
    Public Status_Description
'End DataSource Variables

'DataSource Class_Initialize Event @39-C316B127
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Set price_change_id_param1 = CCCreateField("price_change_id_param1", "price_change_id", ccsText, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Aplication_Date = CCCreateField("Aplication_Date", "Aplication_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Aplication_Time = CCCreateField("Aplication_Time", "Aplication_Time", ccsText, Empty, Recordset)
        Set Reception_Date = CCCreateField("Reception_Date", "Reception_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Reception_time = CCCreateField("Reception_time", "Reception_time", ccsText, Empty, Recordset)
        Set File_Delivery_Date = CCCreateField("File_Delivery_Date", "File_Delivery_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set File_Delivery_Time = CCCreateField("File_Delivery_Time", "File_Delivery_Time", ccsText, Empty, Recordset)
        Set Status = CCCreateField("Status", "Status", ccsInteger, Empty, Recordset)
        Set Status_Description = CCCreateField("Status_Description", "Status_Description", ccsText, Empty, Recordset)
        Fields.AddFields Array(price_change_id,  price_change_id_param1,  SiteName,  Aplication_Date,  Aplication_Time,  Reception_Date,  Reception_time, _
             File_Delivery_Date,  File_Delivery_Time,  Status,  Status_Description)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_price_change_id", "price_change_id", ""), _
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Aplication_Date", "Aplication_Date", ""), _
            Array("Sorter_Aplication_Time", "Aplication_Time", ""), _
            Array("Sorter_Reception_Date", "Reception_Date", ""), _
            Array("Sorter_Reception_time", "Reception_time", ""), _
            Array("Sorter_File_Delivery_Date", "File_Delivery_Date", ""), _
            Array("Sorter_File_Delivery_Time", "File_Delivery_Time", ""), _
            Array("Sorter_Status", "Status", ""), _
            Array("Sorter_Status_Description", "Status_Description", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} price_change_id, SiteName, (Cast (aplication_date as varchar(10))  + '  ' + aplication_time) AS Aplication_Date, aplication_time AS Aplication_Time, " & vbLf & _
        "reception_date AS Reception_Date, reception_time AS Reception_Time, file_delivery_date AS File_Delivery_Date, file_delivery_time AS File_Delivery_Time, " & vbLf & _
        "status_code AS Status, status_description AS Status_Description  " & vbLf & _
        "FROM Sites INNER JOIN price_change_status ON " & vbLf & _
        "Sites.ss_id = price_change_status.ss_id {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM Sites INNER JOIN price_change_status ON " & vbLf & _
        "Sites.ss_id = price_change_status.ss_id"
        Where = ""
        Order = "price_change_id desc"
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @39-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @39-5064D9D8
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlss_id", ccsInteger, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opEqual, False, "price_change_status.ss_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @39-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @39-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Grid2DataSource Class @39-A61BA892

'Include Page Implementation @103-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
