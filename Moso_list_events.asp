<%
'BindEvents Method @1-71549A37
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Moso.Navigator.CCSEvents("BeforeShow") = GetRef("Moso_Navigator_BeforeShow")
        Set Moso1.CCSEvents("BeforeShow") = GetRef("Moso1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Moso_Navigator_BeforeShow(Sender) 'Moso_Navigator_BeforeShow @17-F5D3340A

'Hide-Show Component @18-91217FD4
    Dim TotalPages_18_1 : TotalPages_18_1 = CCSConverter.VBSConvert(ccsInteger, Moso.DataSource.Recordset.PageCount)
    Dim Param2_18_2 : Param2_18_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_18_1) And Not IsEmpty(Param2_18_2) And TotalPages_18_1 < Param2_18_2) Then _
        Moso.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Moso_Navigator_BeforeShow @17-54C34B28

Function Moso1_BeforeShow(Sender) 'Moso1_BeforeShow @23-7DC8F477

'Custom Code @31-73254650
' -------------------------
  	If Moso1.Recordset.EOF Then
		Moso1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Moso1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Moso1_BeforeShow @23-54C34B28


%>
