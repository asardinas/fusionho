<%
'BindEvents Method @1-578BA2FD
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Brand.Navigator.CCSEvents("BeforeShow") = GetRef("Brand_Navigator_BeforeShow")
        Set Brand1.CCSEvents("BeforeShow") = GetRef("Brand1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Brand_Navigator_BeforeShow(Sender) 'Brand_Navigator_BeforeShow @17-A9C0D950

'Hide-Show Component @18-A6B91C2C
    Dim TotalPages_18_1 : TotalPages_18_1 = CCSConverter.VBSConvert(ccsInteger, Brand.DataSource.Recordset.PageCount)
    Dim Param2_18_2 : Param2_18_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_18_1) And Not IsEmpty(Param2_18_2) And TotalPages_18_1 < Param2_18_2) Then _
        Brand.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Brand_Navigator_BeforeShow @17-54C34B28

Function Brand1_BeforeShow(Sender) 'Brand1_BeforeShow @22-3BBDB09E

'Custom Code @28-73254650
' -------------------------
  	If Brand1.Recordset.EOF Then
		Brand1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Brand1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Brand1_BeforeShow @22-54C34B28


%>
