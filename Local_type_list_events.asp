<%
'BindEvents Method @1-49632EB6
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Local_type.Navigator.CCSEvents("BeforeShow") = GetRef("Local_type_Navigator_BeforeShow")
        Set Local_type1.CCSEvents("BeforeShow") = GetRef("Local_type1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Local_type_Navigator_BeforeShow(Sender) 'Local_type_Navigator_BeforeShow @17-27D02CA0

'Hide-Show Component @18-331C1551
    Dim TotalPages_18_1 : TotalPages_18_1 = CCSConverter.VBSConvert(ccsInteger, Local_type.DataSource.Recordset.PageCount)
    Dim Param2_18_2 : Param2_18_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_18_1) And Not IsEmpty(Param2_18_2) And TotalPages_18_1 < Param2_18_2) Then _
        Local_type.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Local_type_Navigator_BeforeShow @17-54C34B28

Function Local_type1_BeforeShow(Sender) 'Local_type1_BeforeShow @21-4BC6F983

'Custom Code @29-73254650
' -------------------------
  	If Local_type1.Recordset.EOF Then
		Local_type1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Local_type1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Local_type1_BeforeShow @21-54C34B28


%>
