<Page id="1" templateExtension="html" relativePath=".." fullRelativePath=".\services" secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="True" cachingEnabled="False" cachingDuration="1 minutes" needGeneration="0">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" name="Region" connection="FusionHO" dataSource="Region" pageSizeLimit="100" wizardCaption="List of Region ">
<Components>
<Label id="59" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Region_id" fieldSource="Region_id">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="60" fieldSourceType="DBColumn" dataType="Text" html="False" name="Region_name" fieldSource="Region_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
</Components>
<Events/>
<TableParameters>
<TableParameter id="58" conditionType="Parameter" useIsNull="True" field="Country_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" parameterSource="keyword"/>
</TableParameters>
<JoinTables/>
<JoinLinks/>
<Fields/>
<SPParameters/>
<SQLParameters/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Grid>
</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="Sites_maint_Sites_Region_id_PTDependentListBox1.asp" forShow="True" url="Sites_maint_Sites_Region_id_PTDependentListBox1.asp" comment="'" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
