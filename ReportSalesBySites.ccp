<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" pasteActions="pasteActions" needGeneration="0">
	<Components>
		<Report id="2" secured="False" enablePrint="False" showMode="Web" sourceType="Table" returnValueType="Number" linesPerWebPage="40" linesPerPhysicalPage="50" connection="FusionHO" dataSource="pump_sales, Sites" groupBy="SiteName, DAY_DATE" name="pump_sales_Sites" orderBy="SiteName" pageSizeLimit="100" wizardCaption="{res:CCS_ReportFormPrefix} {res:pump_salesSites} {res:CCS_ReportFormSuffix}" wizardLayoutType="GroupLeftAbove" activeCollection="TableParameters">
			<Components>
				<Section id="17" visible="True" lines="0" name="Report_Header" wizardSectionType="ReportHeader">
					<Components>
						<ReportLabel id="26" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Report_TotalRecords" function="Count" wizardUseTemplateBlock="False" PathID="pump_sales_SitesReport_HeaderReport_TotalRecords">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="18" visible="True" lines="1" name="Page_Header" wizardSectionType="PageHeader">
					<Components>
						<Sorter id="41" visible="True" name="Sorter_Sum_Money" column="Sum_Money" wizardCaption="{res:Sum_Money}" wizardSortingType="SimpleDir" wizardControl="Sum_Money">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="43" visible="True" name="Sorter_Sum_Volume" column="Sum_Volume" wizardCaption="{res:Sum_Volume}" wizardSortingType="SimpleDir" wizardControl="Sum_Volume">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="45" visible="True" name="Sorter_Count_Sales" column="Count_Sales" wizardCaption="{res:Count_Sales}" wizardSortingType="SimpleDir" wizardControl="Count_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="20" visible="True" lines="1" name="Sale_Date_Header">
					<Components>
						<ReportLabel id="27" fieldSourceType="DBColumn" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Sale_Date" fieldSource="Sale_Date" wizardCaption="Sale_Date" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="pump_sales_SitesSale_Date_HeaderSale_Date">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="21" visible="True" lines="1" name="Detail">
					<Components>
						<ReportLabel id="40" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Site_Name" fieldSource="Site_Name" wizardCaption="Site_Name" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="pump_sales_SitesDetailSite_Name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="42" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Sum_Money" fieldSource="Sum_Money" wizardCaption="Sum_Money" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailSum_Money" format="$0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="44" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Sum_Volume" fieldSource="Sum_Volume" wizardCaption="Sum_Volume" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailSum_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="46" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="Count_Sales" fieldSource="Count_Sales" wizardCaption="Count_Sales" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailCount_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Panel id="47" visible="True" name="Separator">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="22" visible="True" lines="1" name="Sale_Date_Footer">
					<Components>
						<ReportLabel id="28" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Sale_Date" name="Sum_Sum_Money" fieldSource="Sum_Money" summarised="True" function="Sum" wizardCaption="{res:Sum_Money}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesSale_Date_FooterSum_Sum_Money" format="$0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="29" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Sale_Date" name="Sum_Sum_Volume" fieldSource="Sum_Volume" summarised="True" function="Sum" wizardCaption="{res:Sum_Volume}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesSale_Date_FooterSum_Sum_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="30" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Sale_Date" name="Sum_Count_Sales" fieldSource="Count_Sales" summarised="True" function="Sum" wizardCaption="{res:Count_Sales}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesSale_Date_FooterSum_Count_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="23" visible="True" lines="1" name="Report_Footer" wizardSectionType="ReportFooter">
					<Components>
						<Panel id="24" visible="True" name="NoRecords" wizardNoRecords="{res:CCS_NoRecords}">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
						<ReportLabel id="34" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_Sum_Money" fieldSource="Sum_Money" summarised="True" function="Sum" wizardCaption="{res:Sum_Money}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesReport_FooterTotalSum_Sum_Money" format="$0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="35" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_Sum_Volume" fieldSource="Sum_Volume" summarised="True" function="Sum" wizardCaption="{res:Sum_Volume}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesReport_FooterTotalSum_Sum_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="36" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_Count_Sales" fieldSource="Count_Sales" summarised="True" function="Sum" wizardCaption="{res:Count_Sales}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesReport_FooterTotalSum_Count_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="25" visible="True" lines="1" name="Page_Footer" wizardSectionType="PageFooter" pageBreakAfter="True">
					<Components>
						<ReportLabel id="31" fieldSourceType="SpecialValue" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Report_CurrentDate" fieldSource="CurrentDate" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardInsertToDateTD="True" PathID="pump_sales_SitesPage_FooterReport_CurrentDate">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Navigator id="32" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardImagesScheme="Fusionho1">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Hide-Show Component" actionCategory="General" id="33" action="Hide" conditionType="Parameter" dataType="Integer" condition="LessThan" name1="TotalPages" sourceType1="SpecialValue" name2="2" sourceType2="Expression"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Navigator>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
			</Components>
			<Events>
				<Event name="BeforeBuildSelect" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="52"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="37" conditionType="Parameter" useIsNull="False" field="SiteName" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1" leftBrackets="0" parameterSource="s_Site_Name"/>
				<TableParameter id="55" conditionType="Parameter" useIsNull="False" field="DAY_DATE" dataType="Date" searchConditionType="GreaterThanOrEqual" parameterType="Expression" logicOperator="And" DBFormat="mm/dd/yyyy" format="mm/dd/yyyy" leftBrackets="0" defaultValue="dateadd(&quot;d&quot;, -10,date())" parameterSource="sites_pump_sales.s_Sale_Date0.value"/>
				<TableParameter id="56" conditionType="Parameter" useIsNull="False" field="DAY_DATE" dataType="Date" searchConditionType="LessThanOrEqual" parameterType="Expression" logicOperator="And" DBFormat="mm/dd/yyyy" format="mm/dd/yyyy" rightBrackets="0" defaultValue="date()" parameterSource="sites_pump_sales.s_Sale_Date.value"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="3" tableName="pump_sales" schemaName="dbo" posLeft="10" posTop="10" posWidth="117" posHeight="180"/>
				<JoinTable id="4" tableName="Sites" schemaName="dbo" posLeft="148" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="5" tableLeft="Sites" tableRight="pump_sales" fieldLeft="Sites.ss_id" fieldRight="pump_sales.ss_id" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="6" tableName="pump_sales" fieldName="DAY_DATE" isExpression="False" alias="Sale_Date"/>
				<Field id="7" tableName="Sites" fieldName="SiteName" isExpression="False" alias="Site_Name"/>
				<Field id="8" fieldName="sum(money)" isExpression="True" alias="Sum_Money" tableName="pump_sales"/>
				<Field id="9" fieldName="Sum(volume)" isExpression="True" alias="Sum_Volume"/>
				<Field id="10" tableName="pump_sales" fieldName="count(sale_id)" isExpression="True" alias="Count_Sales"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<ReportGroups>
				<ReportGroup id="19" name="Sale_Date" field="Sale_Date" sqlField="pump_sales.DAY_DATE" sortOrder="asc"/>
			</ReportGroups>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Report>
		<Record id="11" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Sites_pump_sales" wizardCaption="{res:CCS_SearchFormPrefix} {res:Sites_pump_sales} {res:CCS_SearchFormSuffix}" wizardOrientation="Vertical" wizardFormMethod="post" returnPage="ReportSalesBySites.ccp" PathID="Sites_pump_sales">
			<Components>
				<Link id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="ReportSalesBySites.ccp" removeParameters="s_Site_Name;s_Sale_Date;s_Sale_Date0" wizardThemeItem="SorterLink" wizardDefaultValue="{res:CCS_Clear}" PathID="Sites_pump_salesClearParameters">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="13" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}" wizardThemeItem="FooterIMG" wizardButtonImage="ButtonSearchOn" PathID="Sites_pump_salesButton_DoSearch">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<ListBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_Site_Name" wizardCaption="{res:Site_Name}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" PathID="Sites_pump_saless_Site_Name" required="False" sourceType="Table" connection="FusionHO" dataSource="Sites" boundColumn="SiteName" textColumn="SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="58" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="59" tableName="Sites" fieldName="SiteName"/>
					</Fields>
				</ListBox>
				<TextBox id="15" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_Sale_Date" wizardCaption="{res:Sale_Date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" PathID="Sites_pump_saless_Sale_Date" format="mm/dd/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="16" name="DatePicker_s_Sale_Date" control="s_Sale_Date" wizardSatellite="True" wizardControl="s_Sale_Date" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css" PathID="Sites_pump_salesDatePicker_s_Sale_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="53" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_Sale_Date0" PathID="Sites_pump_saless_Sale_Date0" format="mm/dd/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="54" name="DatePicker_s_Sale_Date0" PathID="Sites_pump_salesDatePicker_s_Sale_Date0" control="s_Sale_Date0" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="50"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="48" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="ASPTemplates" name="ReportSalesBySites_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
		<CodeFile id="Code" language="ASPTemplates" name="ReportSalesBySites.asp" forShow="True" url="ReportSalesBySites.asp" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
