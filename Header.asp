<%
Class clsHeader 'Header Class @1-56552C31

'Page Variables @1-CE20DE76
    Dim Redirect
    Dim IsService
    Dim Tpl, HTMLTemplate
    Dim TemplateFileName
    Dim ComponentName
    Dim PathToCurrentPage
    Dim Attributes

    ' Events
    Dim CCSEvents
    Dim CCSEventResult

    ' Page controls
    Dim Logout
    Dim Menu1
    Dim usuario
    Dim ChildControls
    Public Visible
    Public Page
    Public Name
    Public CacheAction
    Private TemplatePathValue
'End Page Variables

'Page Class_Initialize Event @1-ACE2B259
    Private Sub Class_Initialize()
        Visible = True
        Set Page = Me
        Set ParentPage = Me
        TemplatePathValue = ""
        IsService = False
        Redirect = ""
        TemplateFileName = "Header.html"
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        PathToCurrentPage = "./"
    End Sub
'End Page Class_Initialize Event

'Page Unload_Page Event @1-47502A19
    Public Sub UnloadPage()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Me)
        Set Page = Nothing
        If NOT Visible Then _
            Exit Sub
        Set CCSEvents = Nothing
        Set Menu1 = Nothing
    End Sub
'End Page Unload_Page Event

'Page Class_Terminate Event @1-61494ECF
    Private Sub Class_Terminate()
    End Sub
'End Page Class_Terminate Event

'Page BindEvents Method @1-E04E4CE5
    Sub BindEvents()
        Set usuario.CCSEvents("BeforeShow") = GetRef("Header_usuario_BeforeShow")
        Set CCSEvents("BeforeShow") = GetRef("Header_BeforeShow")
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Me)
    End Sub
'End Page BindEvents Method

'Operations Method @1-E8B9371E
    Function Operations()
        If NOT Visible Then _ 
            Exit Function
        Operations = Redirect
    End Function
'End Operations Method

'Initialize Method @1-0C252C07
    Sub Initialize(Name, Path)
        Me.Name = Name
        TemplatePathValue = Path
        If NOT Visible Then _
            Exit Sub

        ' Create Components
        Set Logout = CCCreateControl(ccsImageLink, "Logout", Empty, ccsText, Empty, CCGetRequestParam("Logout", ccsGet))
        Set Menu1 = New clsMenuHeaderMenu1
        Menu1.InitializeItems(TemplatePathValue)
        Set Menu1.Page = Me
        Set usuario = CCCreateControl(ccsLabel, "usuario", Empty, ccsText, Empty, CCGetRequestParam("usuario", ccsGet))
        BindEvents
        Set HTMLTemplate = new clsTemplate
        Set HTMLTemplate.Cache = TemplatesRepository
        HTMLTemplate.LoadTemplate TemplateFilePath & TemplatePathValue & TemplateFileName
        HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
        Set Tpl = HTMLTemplate.Block("main")
        
        Logout.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Logout.Parameters = CCAddParam(Logout.Parameters, "Logout", "True")
        Logout.Page = Page.TemplateURL & "Login.asp"
        
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Me)
    End Sub
'End Initialize Method

'Page Show Method @1-49005FD1
    Sub Show(MainTpl)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then _
            Exit Sub
        Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
            Array(Logout, Menu1, usuario))
        ChildControls.Show
        Attributes.Show HTMLTemplate, "page:"
        HTMLTemplate.Parse "main", False
        MainTpl.Variable(Name) = HTMLTemplate.GetVar("main")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Me)
    End Sub
'End Page Show Method

'Let TemplatePath Property @1-520E3E1A
    Property Let TemplatePath(NewTemplatePath)
        TemplatePathValue = NewTemplatePath
    End Property
'End Let TemplatePath Property

'Get TemplatePath Property @1-9428206A
    Property Get TemplatePath
        TemplatePath = TemplatePathValue
    End Property
'End Get TemplatePath Property

'TemplateURL Property @1-CFFB06B3
    Property Get TemplateURL
        TemplateURL = Replace(TemplatePathValue, "\", "/")
    End Property
'End TemplateURL Property

End Class 'End Header Class @1-A61BA892

'Include Event File @1-CA045B9B
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header_events.asp" -->
<%
'End Include Event File

Class clsMenuHeaderMenu1 'Menu1 Class @2-BC090CF4

'Menu1 Variables @2-1AC8260D

    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public Page
    Public Recordset

    Private CCSEventResult

    ' Menu Controls
    Public StaticControls, RowControls
    Dim ItemLink
    Dim RSMenu
    Dim MenuArray
    Dim Rows
'End Menu1 Variables

'Menu1 Class_Initialize Event @2-13961E7E
    Private Sub Class_Initialize()
        ComponentName = "Menu1"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors

        Set ItemLink = CCCreateControl(ccsLink, "ItemLink", Empty, ccsText, Empty, CCGetRequestParam("ItemLink", ccsGet))
        IsDSEmpty = True
    End Sub
'End Menu1 Class_Initialize Event

'Menu1 InitializeItems Method @2-44DC3ACB
    Sub InitializeItems(Path)
        Set DataSource = New clsHeaderMenu1DataSource
        ReDim Rows(5, 50)
        Rows(0, 0) = "MenuItem1" : Rows(1, 0) = "" : Rows(2, 0) = CCSLocales.GetText("Configuration", Empty) : Rows(3, 0) = "" : Rows(4, 0) = "" : Rows(5, 0) = CCSLocales.GetText("Configuration", Empty)
        Rows(0, 1) = "MenuItem1Item3" : Rows(1, 1) = "MenuItem1" : Rows(2, 1) = CCSLocales.GetText("City", Empty) : Rows(3, 1) = Path & "City_list.asp" : Rows(4, 1) = "_self" : Rows(5, 1) = CCSLocales.GetText("City", Empty)
        Rows(0, 2) = "MenuItem1Item4" : Rows(1, 2) = "MenuItem1" : Rows(2, 2) = CCSLocales.GetText("Brand", Empty) : Rows(3, 2) = Path & "Brand_list.asp" : Rows(4, 2) = "_self" : Rows(5, 2) = CCSLocales.GetText("Brand", Empty)
        Rows(0, 3) = "MenuItem1Item5" : Rows(1, 3) = "MenuItem1" : Rows(2, 3) = CCSLocales.GetText("Country", Empty) : Rows(3, 3) = Path & "Country_list.asp" : Rows(4, 3) = "_self" : Rows(5, 3) = CCSLocales.GetText("Country", Empty)
        Rows(0, 4) = "MenuItem1Item6" : Rows(1, 4) = "MenuItem1" : Rows(2, 4) = CCSLocales.GetText("Grades", Empty) : Rows(3, 4) = Path & "Grades_list.asp" : Rows(4, 4) = "_self" : Rows(5, 4) = CCSLocales.GetText("Grades", Empty)
        Rows(0, 5) = "MenuItem1Item7" : Rows(1, 5) = "MenuItem1" : Rows(2, 5) = CCSLocales.GetText("Grade_type", Empty) : Rows(3, 5) = Path & "Grade_type_list.asp" : Rows(4, 5) = "_self" : Rows(5, 5) = CCSLocales.GetText("Grade_type", Empty)
        Rows(0, 6) = "MenuItem1Item8" : Rows(1, 6) = "MenuItem1" : Rows(2, 6) = CCSLocales.GetText("Local_type", Empty) : Rows(3, 6) = Path & "Local_type_list.asp" : Rows(4, 6) = "_self" : Rows(5, 6) = CCSLocales.GetText("Local_type", Empty)
        Rows(0, 7) = "MenuItem1Item9" : Rows(1, 7) = "MenuItem1" : Rows(2, 7) = CCSLocales.GetText("Moso", Empty) : Rows(3, 7) = Path & "Moso_list.asp" : Rows(4, 7) = "_self" : Rows(5, 7) = CCSLocales.GetText("Moso", Empty)
        Rows(0, 8) = "MenuItem1Item10" : Rows(1, 8) = "MenuItem1" : Rows(2, 8) = CCSLocales.GetText("Region", Empty) : Rows(3, 8) = Path & "Region_list.asp" : Rows(4, 8) = "_self" : Rows(5, 8) = CCSLocales.GetText("Region", Empty)
        Rows(0, 9) = "MenuItem1Item11" : Rows(1, 9) = "MenuItem1" : Rows(2, 9) = CCSLocales.GetText("Service", Empty) : Rows(3, 9) = Path & "Service_list.asp" : Rows(4, 9) = "_self" : Rows(5, 9) = CCSLocales.GetText("Service", Empty)
        Rows(0, 10) = "MenuItem1Item12" : Rows(1, 10) = "MenuItem1" : Rows(2, 10) = CCSLocales.GetText("Sites", Empty) : Rows(3, 10) = Path & "Sites_list.asp" : Rows(4, 10) = "_self" : Rows(5, 10) = CCSLocales.GetText("Sites", Empty)
        Rows(0, 11) = "MenuItem1Item13" : Rows(1, 11) = "MenuItem1" : Rows(2, 11) = CCSLocales.GetText("Territory Manager", Empty) : Rows(3, 11) = Path & "Tm_list.asp" : Rows(4, 11) = "_self" : Rows(5, 11) = CCSLocales.GetText("", Empty)
        Rows(0, 12) = "MenuItem2" : Rows(1, 12) = "" : Rows(2, 12) = CCSLocales.GetText("Reports", Empty) : Rows(3, 12) = "" : Rows(4, 12) = "" : Rows(5, 12) = CCSLocales.GetText("Reports", Empty)
        Rows(0, 13) = "MenuItem2Item1" : Rows(1, 13) = "MenuItem2" : Rows(2, 13) = CCSLocales.GetText("Admin_Report", Empty) : Rows(3, 13) = "" : Rows(4, 13) = "" : Rows(5, 13) = CCSLocales.GetText("Admin_Report", Empty)
        Rows(0, 14) = "MenuItem2Item1Item1" : Rows(1, 14) = "MenuItem2Item1" : Rows(2, 14) = CCSLocales.GetText("Grade_Report", Empty) : Rows(3, 14) = Path & "Report_Grade.asp" : Rows(4, 14) = "_self" : Rows(5, 14) = CCSLocales.GetText("", Empty)
        Rows(0, 15) = "MenuItem2Item2" : Rows(1, 15) = "MenuItem2" : Rows(2, 15) = CCSLocales.GetText("Sales_Reports", Empty) : Rows(3, 15) = "" : Rows(4, 15) = "" : Rows(5, 15) = CCSLocales.GetText("Sales_Reports", Empty)
        Rows(0, 16) = "MenuItem2Item2Item1" : Rows(1, 16) = "MenuItem2Item2" : Rows(2, 16) = CCSLocales.GetText("Sales_By_Sites", Empty) : Rows(3, 16) = Path & "ReportSalesBySites.asp" : Rows(4, 16) = "_self" : Rows(5, 16) = CCSLocales.GetText("Sales_By_Sites", Empty)
        Rows(0, 17) = "MenuItem2Item2Item2" : Rows(1, 17) = "MenuItem2Item2" : Rows(2, 17) = CCSLocales.GetText("Sales_By_Grades", Empty) : Rows(3, 17) = Path & "ReportSalesByGrades.asp" : Rows(4, 17) = "_self" : Rows(5, 17) = CCSLocales.GetText("Sales_By_Grades", Empty)
        Rows(0, 18) = "MenuItem2Item2Item5" : Rows(1, 18) = "MenuItem2Item2" : Rows(2, 18) = CCSLocales.GetText("Sales_By_Payments", Empty) : Rows(3, 18) = Path & "ReportSalesByPay.asp" : Rows(4, 18) = "_self" : Rows(5, 18) = CCSLocales.GetText("Sales_By_Payments", Empty)
        Rows(0, 19) = "MenuItem2Item2Item6" : Rows(1, 19) = "MenuItem2Item2" : Rows(2, 19) = CCSLocales.GetText("Sales_By_Time", Empty) : Rows(3, 19) = Path & "ReportSalesByTime.asp" : Rows(4, 19) = "_self" : Rows(5, 19) = CCSLocales.GetText("Sales_By_Time", Empty)
        Rows(0, 20) = "MenuItem2Item3" : Rows(1, 20) = "MenuItem2" : Rows(2, 20) = CCSLocales.GetText("Tank_Reports", Empty) : Rows(3, 20) = "" : Rows(4, 20) = "" : Rows(5, 20) = CCSLocales.GetText("Tank_Reports", Empty)
        Rows(0, 21) = "MenuItem2Item3Item1" : Rows(1, 21) = "MenuItem2Item3" : Rows(2, 21) = CCSLocales.GetText("Stock_By_Tank", Empty) : Rows(3, 21) = "" : Rows(4, 21) = "" : Rows(5, 21) = CCSLocales.GetText("Stock_By_Tank", Empty)
        Rows(0, 22) = "MenuItem2Item3Item1Item1" : Rows(1, 22) = "MenuItem2Item3Item1" : Rows(2, 22) = CCSLocales.GetText("Stock_By_Date", Empty) : Rows(3, 22) = Path & "ReportStockByTanks.asp" : Rows(4, 22) = "_self" : Rows(5, 22) = CCSLocales.GetText("Stock_By_Date", Empty)
        Rows(0, 23) = "MenuItem2Item3Item1Item2" : Rows(1, 23) = "MenuItem2Item3Item1" : Rows(2, 23) = CCSLocales.GetText("Stock_By_Range_Date", Empty) : Rows(3, 23) = Path & "ReportStockTankRangeDate.asp" : Rows(4, 23) = "_self" : Rows(5, 23) = CCSLocales.GetText("Stock_By_Range_Date", Empty)
        Rows(0, 24) = "MenuItem2Item3Item2" : Rows(1, 24) = "MenuItem2Item3" : Rows(2, 24) = CCSLocales.GetText("Stock_By_Product", Empty) : Rows(3, 24) = "" : Rows(4, 24) = "" : Rows(5, 24) = CCSLocales.GetText("Stock_By_Product", Empty)
        Rows(0, 25) = "MenuItem2Item3Item2Item1" : Rows(1, 25) = "MenuItem2Item3Item2" : Rows(2, 25) = CCSLocales.GetText("Stock_ By_Date", Empty) : Rows(3, 25) = Path & "ReportStockbyGrade.asp" : Rows(4, 25) = "_self" : Rows(5, 25) = CCSLocales.GetText("Stock_By_Date", Empty)
        Rows(0, 26) = "MenuItem2Item3Item2Item2" : Rows(1, 26) = "MenuItem2Item3Item2" : Rows(2, 26) = CCSLocales.GetText("Stock_ By_Range_Date", Empty) : Rows(3, 26) = Path & "ReportStockByGradeRangeDate.asp" : Rows(4, 26) = "" : Rows(5, 26) = CCSLocales.GetText("Stock_By_Range_Date", Empty)
        Rows(0, 27) = "MenuItem2Item3Item3" : Rows(1, 27) = "MenuItem2Item3" : Rows(2, 27) = CCSLocales.GetText("Alarm_History", Empty) : Rows(3, 27) = Path & "ReportByAlarms.asp" : Rows(4, 27) = "_self" : Rows(5, 27) = CCSLocales.GetText("Alarm_History", Empty)
        Rows(0, 28) = "MenuItem2Item3Item4" : Rows(1, 28) = "MenuItem2Item3" : Rows(2, 28) = CCSLocales.GetText("Alarm_Deliveries", Empty) : Rows(3, 28) = Path & "ReportByDelivery.asp" : Rows(4, 28) = "_self" : Rows(5, 28) = CCSLocales.GetText("Alarm_Deliveries", Empty)
        Rows(0, 29) = "MenuItem2Item4" : Rows(1, 29) = "MenuItem2" : Rows(2, 29) = CCSLocales.GetText("Graph", Empty) : Rows(3, 29) = "" : Rows(4, 29) = "" : Rows(5, 29) = CCSLocales.GetText("Graph", Empty)
        Rows(0, 30) = "MenuItem2Item4Item1" : Rows(1, 30) = "MenuItem2Item4" : Rows(2, 30) = CCSLocales.GetText("Stock_By_Tank", Empty) : Rows(3, 30) = "" : Rows(4, 30) = "" : Rows(5, 30) = CCSLocales.GetText("Stock_By_Tank", Empty)
        Rows(0, 31) = "MenuItem2Item4Item1Item1" : Rows(1, 31) = "MenuItem2Item4Item1" : Rows(2, 31) = CCSLocales.GetText("Stock By Date", Empty) : Rows(3, 31) = Path & "ChartByTank.asp" : Rows(4, 31) = "_self" : Rows(5, 31) = CCSLocales.GetText("Stock_By_Date", Empty)
        Rows(0, 32) = "MenuItem2Item4Item1Item2" : Rows(1, 32) = "MenuItem2Item4Item1" : Rows(2, 32) = CCSLocales.GetText("Stock By Range Date", Empty) : Rows(3, 32) = Path & "ChartByTankRangeDate.asp" : Rows(4, 32) = "_self" : Rows(5, 32) = CCSLocales.GetText("Stock_By_Range_Date", Empty)
        Rows(0, 33) = "MenuItem2Item4Item2" : Rows(1, 33) = "MenuItem2Item4" : Rows(2, 33) = CCSLocales.GetText("Stock_By_Product", Empty) : Rows(3, 33) = "" : Rows(4, 33) = "" : Rows(5, 33) = CCSLocales.GetText("Stock_By_Product", Empty)
        Rows(0, 34) = "MenuItem2Item4Item2Item1" : Rows(1, 34) = "MenuItem2Item4Item2" : Rows(2, 34) = CCSLocales.GetText("Stock By Date", Empty) : Rows(3, 34) = Path & "ChartByProd.asp" : Rows(4, 34) = "_self" : Rows(5, 34) = CCSLocales.GetText("Stock_By_Date", Empty)
        Rows(0, 35) = "MenuItem2Item4Item2Item2" : Rows(1, 35) = "MenuItem2Item4Item2" : Rows(2, 35) = CCSLocales.GetText("Stock By Range Date", Empty) : Rows(3, 35) = Path & "ChartByGradeRangeDate.asp" : Rows(4, 35) = "_self" : Rows(5, 35) = CCSLocales.GetText("Stock_By_Range_Date", Empty)
        Rows(0, 36) = "MenuItem2Item4Item3" : Rows(1, 36) = "MenuItem2Item4" : Rows(2, 36) = CCSLocales.GetText("Sales_By_Time", Empty) : Rows(3, 36) = Path & "ChartByTime.asp" : Rows(4, 36) = "_self" : Rows(5, 36) = CCSLocales.GetText("Sales_By_Time", Empty)
        Rows(0, 37) = "MenuItem2Item4Item4" : Rows(1, 37) = "MenuItem2Item4" : Rows(2, 37) = CCSLocales.GetText("Sales_By_Grades", Empty) : Rows(3, 37) = Path & "ChartSalesByGrades.asp" : Rows(4, 37) = "_self" : Rows(5, 37) = CCSLocales.GetText("Sales_By_Grades", Empty)
        Rows(0, 38) = "MenuItem2Item5" : Rows(1, 38) = "MenuItem2" : Rows(2, 38) = CCSLocales.GetText("Log", Empty) : Rows(3, 38) = "" : Rows(4, 38) = "" : Rows(5, 38) = CCSLocales.GetText("Log", Empty)
        Rows(0, 39) = "MenuItem2Item5Item1" : Rows(1, 39) = "MenuItem2Item5" : Rows(2, 39) = CCSLocales.GetText("Addin Flow Control Data", Empty) : Rows(3, 39) = Path & "ReportByLogFLW.asp" : Rows(4, 39) = "_self" : Rows(5, 39) = CCSLocales.GetText("", Empty)
        Rows(0, 40) = "MenuItem2Item5Item2" : Rows(1, 40) = "MenuItem2Item5" : Rows(2, 40) = CCSLocales.GetText("Addin Payment Data", Empty) : Rows(3, 40) = Path & "ReportByLogPAY.asp" : Rows(4, 40) = "_self" : Rows(5, 40) = CCSLocales.GetText("", Empty)
        Rows(0, 41) = "MenuItem2Item5Item3" : Rows(1, 41) = "MenuItem2Item5" : Rows(2, 41) = CCSLocales.GetText("Pump_Sales", Empty) : Rows(3, 41) = Path & "ReportByLogSLS.asp" : Rows(4, 41) = "_self" : Rows(5, 41) = CCSLocales.GetText("", Empty)
        Rows(0, 42) = "MenuItem2Item5Item4" : Rows(1, 42) = "MenuItem2Item5" : Rows(2, 42) = CCSLocales.GetText("Tank Alarms History", Empty) : Rows(3, 42) = Path & "ReportByLogTAH.asp" : Rows(4, 42) = "_self" : Rows(5, 42) = CCSLocales.GetText("", Empty)
        Rows(0, 43) = "MenuItem2Item5Item5" : Rows(1, 43) = "MenuItem2Item5" : Rows(2, 43) = CCSLocales.GetText("Tank Actual Info", Empty) : Rows(3, 43) = Path & "ReportByLogTAI.asp" : Rows(4, 43) = "_self" : Rows(5, 43) = CCSLocales.GetText("", Empty)
        Rows(0, 44) = "MenuItem2Item5Item6" : Rows(1, 44) = "MenuItem2Item5" : Rows(2, 44) = CCSLocales.GetText("Tank Deliveries", Empty) : Rows(3, 44) = Path & "ReportByLogTDD.asp" : Rows(4, 44) = "_self" : Rows(5, 44) = CCSLocales.GetText("", Empty)
        Rows(0, 45) = "MenuItem2Item5Item7" : Rows(1, 45) = "MenuItem2Item5" : Rows(2, 45) = CCSLocales.GetText("Tank Day Sales", Empty) : Rows(3, 45) = Path & "ReportByLogTDS.asp" : Rows(4, 45) = "_self" : Rows(5, 45) = CCSLocales.GetText("", Empty)
        Rows(0, 46) = "MenuItem4" : Rows(1, 46) = "" : Rows(2, 46) = CCSLocales.GetText("Prices", Empty) : Rows(3, 46) = "" : Rows(4, 46) = "" : Rows(5, 46) = CCSLocales.GetText("Prices", Empty)
        Rows(0, 47) = "MenuItem4Item1" : Rows(1, 47) = "MenuItem4" : Rows(2, 47) = CCSLocales.GetText("price_send", Empty) : Rows(3, 47) = Path & "Price_send.asp" : Rows(4, 47) = "_self" : Rows(5, 47) = CCSLocales.GetText("price_send", Empty)
        Rows(0, 48) = "MenuItem3" : Rows(1, 48) = "" : Rows(2, 48) = CCSLocales.GetText("Security", Empty) : Rows(3, 48) = "" : Rows(4, 48) = "" : Rows(5, 48) = CCSLocales.GetText("Security", Empty)
        Rows(0, 49) = "MenuItem3Item1" : Rows(1, 49) = "MenuItem3" : Rows(2, 49) = CCSLocales.GetText("Users", Empty) : Rows(3, 49) = Path & "Users_list.asp" : Rows(4, 49) = "_self" : Rows(5, 49) = CCSLocales.GetText("Users", Empty)
        Rows(0, 50) = "MenuItem3Item2" : Rows(1, 50) = "MenuItem3" : Rows(2, 50) = CCSLocales.GetText("Change_password", Empty) : Rows(3, 50) = Path & "change.asp" : Rows(4, 50) = "_self" : Rows(5, 50) = CCSLocales.GetText("Change_password", Empty)
        DataSource.AbsolutePage = 1
    End Sub
'End Menu1 InitializeItems Method

'Menu1 Initialize Method @2-88A28545
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub
    End Sub
'End Menu1 Initialize Method

'Menu1 Class_Terminate Event @2-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Menu1 Class_Terminate Event

'Menu1 Show Method @2-4B5F06F9
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim ItemBlock, OpenLevelBlock, CloseItemBlock, CloseLevelBlock


        Set TemplateBlock = Tpl.Block("Menu " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set ItemBlock = TemplateBlock.Block("Item")
        Set OpenLevelBlock = ItemBlock.Block("OpenLevel")
        Set CloseItemBlock = ItemBlock.Block("CloseItem")
        Set CloseLevelBlock = ItemBlock.Block("CloseLevel")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        IsDSEmpty = False
        Set RowControls = CCCreateCollection(ItemBlock, Null, ccsParseAccumulate, _
            Array(ItemLink))

        'Create Array from datasource
        Dim f,i
        Dim indexParentID
        Dim indexID
        indexParentID = 1
        indexID = 0
        Set MenuArray = New clsDynamicArray
        If Not IsDSEmpty Then 
            For i = 0 to UBound(Rows,2)
            If IsNull(Rows(indexParentID, i)) or Rows(indexParentID, i)="" Then
                MenuArray.Add(CCCreateMenuNode(Rows(indexParentID, i), Rows(indexID, i),Rows, i))
            Else
                AddNode MenuArray, CCCreateMenuNode(Rows(indexParentID, i), Rows(indexID, i),Rows, i), 2
            End If
            Next
        End If

        'Create new Recordset
        Set rsMenu = CreateObject("ADODB.Recordset")
        With rsMenu
            .Fields.Append "item_id",  adVarChar, 200
            .Fields.Append "item_id_parent",  adVarChar, 200
            .Fields.Append "item_caption", adVarChar, 200
            .Fields.Append "item_url", adVarChar, 200
            .Fields.Append "target", adVarChar, 20
            .Fields.Append "title", adVarChar, 200
            .Fields.Append "ItemLink",adBSTR
            .Fields.Append "CCS_Level", adInteger

            .CursorType = adOpenStatic
            .LockType = adLockOptimistic
            .ActiveConnection = Nothing
            .CursorLocation = aduseclient
            .Open
        End With
        Set Recordset = DataSource.Recordset
        Set Recordset.Recordset = rsMenu
        TransformToFlat MenuArray, 1, Recordset.Recordset.Fields
        Attributes("MenuType") = "menu_htb"
        If Not IsDSEmpty Then Recordset.Recordset.MoveFirst

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Menu " & ComponentName, Errors)
        Else
            Dim  PrevLevel, CurrentLevel, NextLevel
            PrevLevel = 1
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                CurrentLevel= RecordSet.RecordSet("CCS_Level")
                Attributes("Item_Level") = CurrentLevel
                Attributes("rowNumber") = ShownRecords + 1
                Attributes("Target") = RecordSet.RecordSet("target")
                Attributes("Title") = RecordSet.RecordSet("title")
                If Not OpenLevelBlock is Nothing Then
                    OpenLevelBlock.Clear
                    If Recordset.Recordset("CCS_Level") - PrevLevel > 0 Then
                        Attributes.Show OpenLevelBlock, "Menu1:"
                        OpenLevelBlock.Show
                    End If
                    prevLevel = CurrentLevel
                End If
                If HasNext Then
                    ItemLink.Value = Recordset.Fields("ItemLink")
                    ItemLink.Link = ""
                    ItemLink.Page = Recordset.Fields("item_url")
                    ItemLink.LinkParameters = CCGetQueryString("QueryString", Array("ccsForm"))
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                If HasNext Then Recordset.MoveNext
                HasNext = HasNextRow()
                If HasNext Then
                    NextLevel = RecordSet.RecordSet("CCS_Level")
                    Attributes("Submenu") = IIF(NextLevel-CurrentLevel > 0 , "submenu", "")
                Else
                    NextLevel = 1
                    Attributes("Submenu") = ""
                End If
                If Not CloseLevelBlock is Nothing Then
                    CloseLevelBlock.Clear
                    If  NextLevel < CurrentLevel  Then
                        For i = 1 To   CurrentLevel - NextLevel
                            CloseLevelBlock.Parse True
                        Next
                    End If
                End If
                If Not CloseItemBlock is Nothing Then
                    CloseItemBlock.Clear
                    If  NextLevel <= CurrentLevel  Then
                        CloseItemBlock.Show
                    End If
                End If
                ShownRecords = ShownRecords + 1
                Attributes.Show ItemBlock, "Menu1:"
                RowControls.Show
            Loop
            Attributes.Show TemplateBlock, "Menu1:"
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End Menu1 Show Method

'Menu1 HasNextRow Function @2-5E30721E
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF
    End Function
'End Menu1 HasNextRow Function

'Menu1 AddNode Function @2-6D16DC2A
    Private Function  AddNode(MenuArray, Node, Level)
        Dim i
        For i=0 To MenuArray.Count-1
            If MenuArray.Item(i).Id = Node.ParentId Then
                If  MenuArray.Item(i).CCS_Children is Nothing Then _
                    Set MenuArray.Item(i).CCS_Children = New clsDynamicArray
                Node.Level = Level
                MenuArray.Item(i).CCS_Children.Add(Node)
            ElseIf Not MenuArray.Item(i).CCS_Children is Nothing Then
                AddNode MenuArray.Item(i).CCS_Children, Node, Level+1
            End If
        Next
    End Function
'End Menu1 AddNode Function

'Menu1 TransformToFlat Function @2-C96522E2
    Private Function TransformToFlat(MenuArray, Level, Fields)
        Dim i, c, Rec
        For i = 0 To MenuArray.Count-1
            Rec = MenuArray.Item(i).Record
            rsMenu.AddNew
            For c = 0 To  UBound(Rec)
                rsMenu(Fields(c).name) = Rec(c)
            Next
            rsMenu("CCS_Level") = Level
            rsMenu.Update
            If Not MenuArray.Item(i).CCS_Children is Nothing Then _
                 TransformToFlat MenuArray.Item(i).CCS_Children, Level + 1, Fields
        Next
    End Function
'End Menu1 TransformToFlat Function

End Class 'End Menu1 Class @2-A61BA892

Class clsHeaderMenu1DataSource 'Menu1DataSource Class @2-2D1886A6

'Menu1 Variables @2-7C72AA16
    Public Errors,  CCSEvents
    Public Recordset
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Public AllParamsSet
    Public CmdExecution
    Public Connection
    Private CCSEventResult
    ' Datasource fields
    Public ItemLink
'End Menu1 Variables

'Menu1 Class_Initialize Event @2-68131C25
    Private Sub Class_Initialize()
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set ItemLink = CCCreateField("ItemLink", "item_caption", ccsText, Empty, Recordset)
        Fields.AddFields Array(ItemLink)
        Set Recordset.FieldsCollection = Fields
    End Sub
'End Menu1 Class_Initialize Event

'Menu1 Class_Terminate Event @2-D57D8194
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set Errors = Nothing
    End Sub
'End Menu1 Class_Terminate Event

End Class 'End Menu1DataSource Class @2-A61BA892


%>
