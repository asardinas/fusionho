<%@ CodePage=1252 %>
<%
'Include Common Files @1-81072387
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<%
'End Include Common Files

'Initialize Page @1-1E7DDC5B
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim tank_deliveries
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "delivery.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "delivery.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-C3220BCC
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set tank_deliveries = New clsGridtank_deliveries
tank_deliveries.Initialize DBFusionHO

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-F273545D
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(tank_deliveries))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-81985511
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set tank_deliveries = Nothing
End Sub
'End UnloadPage Sub

Class clsGridtank_deliveries 'tank_deliveries Class @2-32156B10

'tank_deliveries Variables @2-4C652527

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_ss_id
    Dim Sorter_tank_id
    Dim Sorter_date_start
    Dim Sorter_time_start
    Dim Sorter_vol_total
    Dim Sorter_date_end
    Dim Sorter_time_end
    Dim Sorter_vol_start
    Dim Sorter_vol_end
    Dim Sorter_height_start
    Dim Sorter_height_end
    Dim Sorter_temp_start
    Dim Sorter_temp_end
    Dim Sorter_transactions_volume
    Dim Sorter_water_start
    Dim Sorter_water_end
    Dim Sorter_water_height_start
    Dim Sorter_water_height_end
    Dim Sorter_delivery_id
    Dim Sorter_delivery_note_id
    Dim Sorter_vol_tc_total
    Dim Sorter_vol_tc_start
    Dim Sorter_vol_tc_end
    Dim ss_id
    Dim tank_id
    Dim date_start
    Dim time_start
    Dim vol_total
    Dim date_end
    Dim time_end
    Dim vol_start
    Dim vol_end
    Dim height_start
    Dim height_end
    Dim temp_start
    Dim temp_end
    Dim transactions_volume
    Dim water_start
    Dim water_end
    Dim water_height_start
    Dim water_height_end
    Dim delivery_id
    Dim delivery_note_id
    Dim vol_tc_total
    Dim vol_tc_start
    Dim vol_tc_end
    Dim Navigator
'End tank_deliveries Variables

'tank_deliveries Class_Initialize Event @2-6729452E
    Private Sub Class_Initialize()
        ComponentName = "tank_deliveries"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clstank_deliveriesDataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("tank_deliveriesOrder", Empty)
        SortingDirection = CCGetParam("tank_deliveriesDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_ss_id = CCCreateSorter("Sorter_ss_id", Me, FileName)
        Set Sorter_tank_id = CCCreateSorter("Sorter_tank_id", Me, FileName)
        Set Sorter_date_start = CCCreateSorter("Sorter_date_start", Me, FileName)
        Set Sorter_time_start = CCCreateSorter("Sorter_time_start", Me, FileName)
        Set Sorter_vol_total = CCCreateSorter("Sorter_vol_total", Me, FileName)
        Set Sorter_date_end = CCCreateSorter("Sorter_date_end", Me, FileName)
        Set Sorter_time_end = CCCreateSorter("Sorter_time_end", Me, FileName)
        Set Sorter_vol_start = CCCreateSorter("Sorter_vol_start", Me, FileName)
        Set Sorter_vol_end = CCCreateSorter("Sorter_vol_end", Me, FileName)
        Set Sorter_height_start = CCCreateSorter("Sorter_height_start", Me, FileName)
        Set Sorter_height_end = CCCreateSorter("Sorter_height_end", Me, FileName)
        Set Sorter_temp_start = CCCreateSorter("Sorter_temp_start", Me, FileName)
        Set Sorter_temp_end = CCCreateSorter("Sorter_temp_end", Me, FileName)
        Set Sorter_transactions_volume = CCCreateSorter("Sorter_transactions_volume", Me, FileName)
        Set Sorter_water_start = CCCreateSorter("Sorter_water_start", Me, FileName)
        Set Sorter_water_end = CCCreateSorter("Sorter_water_end", Me, FileName)
        Set Sorter_water_height_start = CCCreateSorter("Sorter_water_height_start", Me, FileName)
        Set Sorter_water_height_end = CCCreateSorter("Sorter_water_height_end", Me, FileName)
        Set Sorter_delivery_id = CCCreateSorter("Sorter_delivery_id", Me, FileName)
        Set Sorter_delivery_note_id = CCCreateSorter("Sorter_delivery_note_id", Me, FileName)
        Set Sorter_vol_tc_total = CCCreateSorter("Sorter_vol_tc_total", Me, FileName)
        Set Sorter_vol_tc_start = CCCreateSorter("Sorter_vol_tc_start", Me, FileName)
        Set Sorter_vol_tc_end = CCCreateSorter("Sorter_vol_tc_end", Me, FileName)
        Set ss_id = CCCreateControl(ccsLabel, "ss_id", Empty, ccsInteger, Empty, CCGetRequestParam("ss_id", ccsGet))
        Set tank_id = CCCreateControl(ccsLabel, "tank_id", Empty, ccsInteger, Empty, CCGetRequestParam("tank_id", ccsGet))
        Set date_start = CCCreateControl(ccsLabel, "date_start", Empty, ccsText, Empty, CCGetRequestParam("date_start", ccsGet))
        Set time_start = CCCreateControl(ccsLabel, "time_start", Empty, ccsText, Empty, CCGetRequestParam("time_start", ccsGet))
        Set vol_total = CCCreateControl(ccsLabel, "vol_total", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("vol_total", ccsGet))
        Set date_end = CCCreateControl(ccsLabel, "date_end", Empty, ccsText, Empty, CCGetRequestParam("date_end", ccsGet))
        Set time_end = CCCreateControl(ccsLabel, "time_end", Empty, ccsText, Empty, CCGetRequestParam("time_end", ccsGet))
        Set vol_start = CCCreateControl(ccsLabel, "vol_start", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("vol_start", ccsGet))
        Set vol_end = CCCreateControl(ccsLabel, "vol_end", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("vol_end", ccsGet))
        Set height_start = CCCreateControl(ccsLabel, "height_start", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("height_start", ccsGet))
        Set height_end = CCCreateControl(ccsLabel, "height_end", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("height_end", ccsGet))
        Set temp_start = CCCreateControl(ccsLabel, "temp_start", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("temp_start", ccsGet))
        Set temp_end = CCCreateControl(ccsLabel, "temp_end", Empty, ccsFloat, Empty, CCGetRequestParam("temp_end", ccsGet))
        Set transactions_volume = CCCreateControl(ccsLabel, "transactions_volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("transactions_volume", ccsGet))
        Set water_start = CCCreateControl(ccsLabel, "water_start", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("water_start", ccsGet))
        Set water_end = CCCreateControl(ccsLabel, "water_end", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("water_end", ccsGet))
        Set water_height_start = CCCreateControl(ccsLabel, "water_height_start", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("water_height_start", ccsGet))
        Set water_height_end = CCCreateControl(ccsLabel, "water_height_end", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("water_height_end", ccsGet))
        Set delivery_id = CCCreateControl(ccsLabel, "delivery_id", Empty, ccsInteger, Empty, CCGetRequestParam("delivery_id", ccsGet))
        Set delivery_note_id = CCCreateControl(ccsLabel, "delivery_note_id", Empty, ccsInteger, Empty, CCGetRequestParam("delivery_note_id", ccsGet))
        Set vol_tc_total = CCCreateControl(ccsLabel, "vol_tc_total", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("vol_tc_total", ccsGet))
        Set vol_tc_start = CCCreateControl(ccsLabel, "vol_tc_start", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("vol_tc_start", ccsGet))
        Set vol_tc_end = CCCreateControl(ccsLabel, "vol_tc_end", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("vol_tc_end", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End tank_deliveries Class_Initialize Event

'tank_deliveries Initialize Method @2-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End tank_deliveries Initialize Method

'tank_deliveries Class_Terminate Event @2-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End tank_deliveries Class_Terminate Event

'tank_deliveries Show Method @2-87BD077D
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urldelivery_id") = CCGetRequestParam("delivery_id", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_ss_id, Sorter_tank_id, Sorter_date_start, Sorter_time_start, Sorter_vol_total, Sorter_date_end, Sorter_time_end, Sorter_vol_start, Sorter_vol_end, Sorter_height_start, Sorter_height_end, Sorter_temp_start, Sorter_temp_end, Sorter_transactions_volume, Sorter_water_start, Sorter_water_end, Sorter_water_height_start, Sorter_water_height_end, Sorter_delivery_id, Sorter_delivery_note_id, Sorter_vol_tc_total, Sorter_vol_tc_start, Sorter_vol_tc_end, Navigator))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(ss_id, tank_id, date_start, time_start, vol_total, date_end, time_end, vol_start, vol_end, height_start, height_end, temp_start, temp_end, transactions_volume, water_start, water_end, water_height_start, water_height_end, delivery_id, delivery_note_id, vol_tc_total, vol_tc_start, vol_tc_end))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "tank_deliveries:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    ss_id.Value = Recordset.Fields("ss_id")
                    tank_id.Value = Recordset.Fields("tank_id")
                    date_start.Value = Recordset.Fields("date_start")
                    time_start.Value = Recordset.Fields("time_start")
                    vol_total.Value = Recordset.Fields("vol_total")
                    date_end.Value = Recordset.Fields("date_end")
                    time_end.Value = Recordset.Fields("time_end")
                    vol_start.Value = Recordset.Fields("vol_start")
                    vol_end.Value = Recordset.Fields("vol_end")
                    height_start.Value = Recordset.Fields("height_start")
                    height_end.Value = Recordset.Fields("height_end")
                    temp_start.Value = Recordset.Fields("temp_start")
                    temp_end.Value = Recordset.Fields("temp_end")
                    transactions_volume.Value = Recordset.Fields("transactions_volume")
                    water_start.Value = Recordset.Fields("water_start")
                    water_end.Value = Recordset.Fields("water_end")
                    water_height_start.Value = Recordset.Fields("water_height_start")
                    water_height_end.Value = Recordset.Fields("water_height_end")
                    delivery_id.Value = Recordset.Fields("delivery_id")
                    delivery_note_id.Value = Recordset.Fields("delivery_note_id")
                    vol_tc_total.Value = Recordset.Fields("vol_tc_total")
                    vol_tc_start.Value = Recordset.Fields("vol_tc_start")
                    vol_tc_end.Value = Recordset.Fields("vol_tc_end")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "tank_deliveries:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "tank_deliveries:"
            StaticControls.Show
        End If

    End Sub
'End tank_deliveries Show Method

'tank_deliveries PageSize Property Let @2-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End tank_deliveries PageSize Property Let

'tank_deliveries PageSize Property Get @2-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End tank_deliveries PageSize Property Get

'tank_deliveries RowNumber Property Get @2-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End tank_deliveries RowNumber Property Get

'tank_deliveries HasNextRow Function @2-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End tank_deliveries HasNextRow Function

End Class 'End tank_deliveries Class @2-A61BA892

Class clstank_deliveriesDataSource 'tank_deliveriesDataSource Class @2-FD077DFE

'DataSource Variables @2-0BDFD330
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public ss_id
    Public tank_id
    Public date_start
    Public time_start
    Public vol_total
    Public date_end
    Public time_end
    Public vol_start
    Public vol_end
    Public height_start
    Public height_end
    Public temp_start
    Public temp_end
    Public transactions_volume
    Public water_start
    Public water_end
    Public water_height_start
    Public water_height_end
    Public delivery_id
    Public delivery_note_id
    Public vol_tc_total
    Public vol_tc_start
    Public vol_tc_end
'End DataSource Variables

'DataSource Class_Initialize Event @2-BF1DEB20
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set tank_id = CCCreateField("tank_id", "tank_id", ccsInteger, Empty, Recordset)
        Set date_start = CCCreateField("date_start", "date_start", ccsText, Empty, Recordset)
        Set time_start = CCCreateField("time_start", "time_start", ccsText, Empty, Recordset)
        Set vol_total = CCCreateField("vol_total", "vol_total", ccsFloat, Empty, Recordset)
        Set date_end = CCCreateField("date_end", "date_end", ccsText, Empty, Recordset)
        Set time_end = CCCreateField("time_end", "time_end", ccsText, Empty, Recordset)
        Set vol_start = CCCreateField("vol_start", "vol_start", ccsFloat, Empty, Recordset)
        Set vol_end = CCCreateField("vol_end", "vol_end", ccsFloat, Empty, Recordset)
        Set height_start = CCCreateField("height_start", "height_start", ccsFloat, Empty, Recordset)
        Set height_end = CCCreateField("height_end", "height_end", ccsFloat, Empty, Recordset)
        Set temp_start = CCCreateField("temp_start", "temp_start", ccsFloat, Empty, Recordset)
        Set temp_end = CCCreateField("temp_end", "temp_end", ccsFloat, Empty, Recordset)
        Set transactions_volume = CCCreateField("transactions_volume", "transactions_volume", ccsFloat, Empty, Recordset)
        Set water_start = CCCreateField("water_start", "water_start", ccsFloat, Empty, Recordset)
        Set water_end = CCCreateField("water_end", "water_end", ccsFloat, Empty, Recordset)
        Set water_height_start = CCCreateField("water_height_start", "water_height_start", ccsFloat, Empty, Recordset)
        Set water_height_end = CCCreateField("water_height_end", "water_height_end", ccsFloat, Empty, Recordset)
        Set delivery_id = CCCreateField("delivery_id", "delivery_id", ccsInteger, Empty, Recordset)
        Set delivery_note_id = CCCreateField("delivery_note_id", "delivery_note_id", ccsInteger, Empty, Recordset)
        Set vol_tc_total = CCCreateField("vol_tc_total", "vol_tc_total", ccsFloat, Empty, Recordset)
        Set vol_tc_start = CCCreateField("vol_tc_start", "vol_tc_start", ccsFloat, Empty, Recordset)
        Set vol_tc_end = CCCreateField("vol_tc_end", "vol_tc_end", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(ss_id,  tank_id,  date_start,  time_start,  vol_total,  date_end,  time_end, _
             vol_start,  vol_end,  height_start,  height_end,  temp_start,  temp_end,  transactions_volume,  water_start, _
             water_end,  water_height_start,  water_height_end,  delivery_id,  delivery_note_id,  vol_tc_total,  vol_tc_start,  vol_tc_end)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_ss_id", "ss_id", ""), _
            Array("Sorter_tank_id", "tank_id", ""), _
            Array("Sorter_date_start", "date_start", ""), _
            Array("Sorter_time_start", "time_start", ""), _
            Array("Sorter_vol_total", "vol_total", ""), _
            Array("Sorter_date_end", "date_end", ""), _
            Array("Sorter_time_end", "time_end", ""), _
            Array("Sorter_vol_start", "vol_start", ""), _
            Array("Sorter_vol_end", "vol_end", ""), _
            Array("Sorter_height_start", "height_start", ""), _
            Array("Sorter_height_end", "height_end", ""), _
            Array("Sorter_temp_start", "temp_start", ""), _
            Array("Sorter_temp_end", "temp_end", ""), _
            Array("Sorter_transactions_volume", "transactions_volume", ""), _
            Array("Sorter_water_start", "water_start", ""), _
            Array("Sorter_water_end", "water_end", ""), _
            Array("Sorter_water_height_start", "water_height_start", ""), _
            Array("Sorter_water_height_end", "water_height_end", ""), _
            Array("Sorter_delivery_id", "delivery_id", ""), _
            Array("Sorter_delivery_note_id", "delivery_note_id", ""), _
            Array("Sorter_vol_tc_total", "vol_tc_total", ""), _
            Array("Sorter_vol_tc_start", "vol_tc_start", ""), _
            Array("Sorter_vol_tc_end", "vol_tc_end", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} *  " & vbLf & _
        "FROM tank_deliveries {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM tank_deliveries"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @2-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @2-A69B9494
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urldelivery_id", ccsInteger, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opEqual, False, "delivery_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @2-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End tank_deliveriesDataSource Class @2-A61BA892


%>
