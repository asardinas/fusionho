<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-29CE7463
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Header
Dim addin_payments_data_pump
Dim Sites_addin_payments_data
Dim Report_Print
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "ReportSalesByPay.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "ReportSalesByPay.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-481C87AF
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set addin_payments_data_pump = New clsReportaddin_payments_data_pump
Set Sites_addin_payments_data = new clsRecordSites_addin_payments_data
Set Report_Print = CCCreateControl(ccsLink, "Report_Print", Empty, ccsText, Empty, CCGetRequestParam("Report_Print", ccsGet))

Report_Print.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
Report_Print.Parameters = CCAddParam(Report_Print.Parameters, "ViewMode", "Print")
Report_Print.Page = "ReportSalesByPay.asp"
addin_payments_data_pump.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/ReportSalesByPay_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-6C2D27B9
Header.Operations
Sites_addin_payments_data.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-0B368B70
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Header, addin_payments_data_pump, Sites_addin_payments_data, Report_Print))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-6E8CD361
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Set addin_payments_data_pump = Nothing
    Set Sites_addin_payments_data = Nothing
End Sub
'End UnloadPage Sub

'addin_payments_data_pump clsReportGroup @3-905DA7A7
Class clsReportGroupaddin_payments_data_pump
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public Site_Name
    Public Pay_Type
    Public Sum_Money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_Money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_Money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public Site_NameTotalIndex

    Public Sub SetControls()
        Me.Site_Name = addin_payments_data_pump.Site_Name.Value
        Me.Pay_Type = addin_payments_data_pump.Pay_Type.Value
        Me.Sum_Money = addin_payments_data_pump.Sum_Money.Value
        Me.Sum_Volume = addin_payments_data_pump.Sum_Volume.Value
        Me.Count_Sales = addin_payments_data_pump.Count_Sales.Value
        Me.Report_CurrentDate = addin_payments_data_pump.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.Site_Name = HeaderGrp.Site_Name
        addin_payments_data_pump.Site_Name.ChangeValue(Me.Site_Name)
        Me.Pay_Type = HeaderGrp.Pay_Type
        addin_payments_data_pump.Pay_Type.ChangeValue(Me.Pay_Type)
        Me.Sum_Money = HeaderGrp.Sum_Money
        addin_payments_data_pump.Sum_Money.ChangeValue(Me.Sum_Money)
        Me.Sum_Volume = HeaderGrp.Sum_Volume
        addin_payments_data_pump.Sum_Volume.ChangeValue(Me.Sum_Volume)
        Me.Count_Sales = HeaderGrp.Count_Sales
        addin_payments_data_pump.Count_Sales.ChangeValue(Me.Count_Sales)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        addin_payments_data_pump.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = addin_payments_data_pump.Report_TotalRecords.GetTotalValue(isCalculate)
        Me.Sum_Sum_Money = addin_payments_data_pump.Sum_Sum_Money.GetTotalValue(isCalculate)
        Me.Sum_Sum_Volume = addin_payments_data_pump.Sum_Sum_Volume.GetTotalValue(isCalculate)
        Me.Sum_Count_Sales = addin_payments_data_pump.Sum_Count_Sales.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_Money = addin_payments_data_pump.TotalSum_Sum_Money.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_Volume = addin_payments_data_pump.TotalSum_Sum_Volume.GetTotalValue(isCalculate)
        Me.TotalSum_Count_Sales = addin_payments_data_pump.TotalSum_Count_Sales.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = addin_payments_data_pump.Report_TotalRecords.Value
        Me.Sum_Sum_Money = addin_payments_data_pump.Sum_Sum_Money.Value
        Me.Sum_Sum_Volume = addin_payments_data_pump.Sum_Sum_Volume.Value
        Me.Sum_Count_Sales = addin_payments_data_pump.Sum_Count_Sales.Value
        Me.TotalSum_Sum_Money = addin_payments_data_pump.TotalSum_Sum_Money.Value
        Me.TotalSum_Sum_Volume = addin_payments_data_pump.TotalSum_Sum_Volume.Value
        Me.TotalSum_Count_Sales = addin_payments_data_pump.TotalSum_Count_Sales.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End addin_payments_data_pump clsReportGroup

'clsaddin_payments_data_pumpGroupsCollection @3-F63A765D
Class clsaddin_payments_data_pumpGroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSite_NameCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSite_NameCurrentHeaderIndex = 2
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGroupaddin_payments_data_pump
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.Site_NameTotalIndex = mSite_NameCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + addin_payments_data_pump.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(addin_payments_data_pump.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + addin_payments_data_pump.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(addin_payments_data_pump.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "Site_Name" Then
            If PageSize > 0 And addin_payments_data_pump.Site_Name_Header.Visible And CurrentPageSize + addin_payments_data_pump.Page_Footer.Height + addin_payments_data_pump.Site_Name_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + addin_payments_data_pump.Site_Name_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(addin_payments_data_pump.CCSEvents, "Site_Name_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mSite_NameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Site_Name"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(addin_payments_data_pump.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And addin_payments_data_pump.Report_Footer.Visible And CurrentPageSize + addin_payments_data_pump.Page_Footer.Height + addin_payments_data_pump.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + addin_payments_data_pump.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(addin_payments_data_pump.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And addin_payments_data_pump.Site_Name_Footer.Visible And CurrentPageSize + addin_payments_data_pump.Page_Footer.Height + addin_payments_data_pump.Site_Name_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + addin_payments_data_pump.Site_Name_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSite_NameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(addin_payments_data_pump.CCSEvents, "Site_Name_Footer_OnCalculate", Me)
        addin_payments_data_pump.Sum_Sum_Money.Reset()
        addin_payments_data_pump.Sum_Sum_Volume.Reset()
        addin_payments_data_pump.Sum_Count_Sales.Reset()
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Site_Name"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        addin_payments_data_pump.Report_TotalRecords.Value = addin_payments_data_pump.Report_TotalRecords.InitialValue
        addin_payments_data_pump.Site_Name.Value = addin_payments_data_pump.Site_Name.InitialValue
        addin_payments_data_pump.Pay_Type.Value = addin_payments_data_pump.Pay_Type.InitialValue
        addin_payments_data_pump.Sum_Money.Value = addin_payments_data_pump.Sum_Money.InitialValue
        addin_payments_data_pump.Sum_Volume.Value = addin_payments_data_pump.Sum_Volume.InitialValue
        addin_payments_data_pump.Count_Sales.Value = addin_payments_data_pump.Count_Sales.InitialValue
        addin_payments_data_pump.Sum_Sum_Money.Value = addin_payments_data_pump.Sum_Sum_Money.InitialValue
        addin_payments_data_pump.Sum_Sum_Volume.Value = addin_payments_data_pump.Sum_Sum_Volume.InitialValue
        addin_payments_data_pump.Sum_Count_Sales.Value = addin_payments_data_pump.Sum_Count_Sales.InitialValue
        addin_payments_data_pump.TotalSum_Sum_Money.Value = addin_payments_data_pump.TotalSum_Sum_Money.InitialValue
        addin_payments_data_pump.TotalSum_Sum_Volume.Value = addin_payments_data_pump.TotalSum_Sum_Volume.InitialValue
        addin_payments_data_pump.TotalSum_Count_Sales.Value = addin_payments_data_pump.TotalSum_Count_Sales.InitialValue
        addin_payments_data_pump.Report_CurrentDate.Value = addin_payments_data_pump.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And addin_payments_data_pump.Detail.Visible And CurrentPageSize + addin_payments_data_pump.Page_Footer.Height + addin_payments_data_pump.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + addin_payments_data_pump.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(addin_payments_data_pump.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clsaddin_payments_data_pumpGroupsCollection

Class clsReportaddin_payments_data_pump 'addin_payments_data_pump Class @3-80171C85

'addin_payments_data_pump Variables @3-0CFF53A0

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public Site_Name_HeaderBlock, Site_Name_Header
    Public Site_Name_FooterBlock, Site_Name_Footer
    Public ActiveSorter, SortingDirection
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public Site_Name_HeaderControls, Site_Name_FooterControls
    Dim Report_TotalRecords
    Dim Sorter_Pay_Type
    Dim Sorter_Sum_Money
    Dim Sorter_Sum_Volume
    Dim Sorter_Count_Sales
    Dim Site_Name
    Dim Pay_Type
    Dim Sum_Money
    Dim Sum_Volume
    Dim Count_Sales
    Dim Sum_Sum_Money
    Dim Sum_Sum_Volume
    Dim Sum_Count_Sales
    Dim NoRecords
    Dim TotalSum_Sum_Money
    Dim TotalSum_Sum_Volume
    Dim TotalSum_Count_Sales
    Dim PageBreak
    Dim Report_CurrentDate
    Dim Navigator
'End addin_payments_data_pump Variables

'addin_payments_data_pump Class_Initialize Event @3-DC622D14
    Private Sub Class_Initialize()
        ComponentName = "addin_payments_data_pump"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 2
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set Site_Name_Footer = new clsSection
        Site_Name_Footer.Visible = True
        Site_Name_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Footer.Height)
        Set Site_Name_Header = new clsSection
        Site_Name_Header.Visible = True
        Site_Name_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clsaddin_payments_data_pumpDataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If
        ActiveSorter = CCGetParam("addin_payments_data_pumpOrder", Empty)
        SortingDirection = CCGetParam("addin_payments_data_pumpDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Sorter_Pay_Type = CCCreateSorter("Sorter_Pay_Type", Me, FileName)
        Set Sorter_Sum_Money = CCCreateSorter("Sorter_Sum_Money", Me, FileName)
        Set Sorter_Sum_Volume = CCCreateSorter("Sorter_Sum_Volume", Me, FileName)
        Set Sorter_Count_Sales = CCCreateSorter("Sorter_Count_Sales", Me, FileName)
        Set Site_Name = CCCreateReportLabel( "Site_Name", Empty, ccsText, Empty, CCGetRequestParam("Site_Name", ccsGet), "",  False, False,"")
        Set Pay_Type = CCCreateReportLabel( "Pay_Type", Empty, ccsText, Empty, CCGetRequestParam("Pay_Type", ccsGet), "",  False, False,"")
        Set Sum_Money = CCCreateReportLabel( "Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Money", ccsGet), "",  False, False,"")
        Set Sum_Volume = CCCreateReportLabel( "Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Volume", ccsGet), "",  False, False,"")
        Set Count_Sales = CCCreateReportLabel( "Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Count_Sales", ccsGet), "",  False, False,"")
        Set Sum_Sum_Money = CCCreateReportLabel( "Sum_Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Money", ccsGet), "Sum",  False, False,"")
        Set Sum_Sum_Volume = CCCreateReportLabel( "Sum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set Sum_Count_Sales = CCCreateReportLabel( "Sum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Sum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set TotalSum_Sum_Money = CCCreateReportLabel( "TotalSum_Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_Money", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Sum_Volume = CCCreateReportLabel( "TotalSum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Count_Sales = CCCreateReportLabel( "TotalSum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("TotalSum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set PageBreak = CCCreatePanel("PageBreak")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End addin_payments_data_pump Class_Initialize Event

'addin_payments_data_pump Initialize Method @3-BD78A2B0
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.SetOrder ActiveSorter, SortingDirection
    End Sub
'End addin_payments_data_pump Initialize Method

'addin_payments_data_pump Class_Terminate Event @3-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End addin_payments_data_pump Class_Terminate Event

'addin_payments_data_pump Show Method @3-8F8F4E3B
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("urls_Site_Name") = CCGetRequestParam("s_Site_Name", ccsGET)
            .Parameters("expr59") = Sites_addin_payments_data.s_Sale_Date0.value
            .Parameters("expr60") = Sites_addin_payments_data.s_Sale_Date.value
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set Site_Name_HeaderBlock = TemplateBlock.Block("Section Site_Name_Header")
        Set Site_Name_FooterBlock = TemplateBlock.Block("Section Site_Name_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(Pay_Type, Sum_Money, Sum_Volume, Count_Sales))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords, TotalSum_Sum_Money, TotalSum_Sum_Volume, TotalSum_Count_Sales))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(PageBreak, Report_CurrentDate, Navigator))
        Set Page_HeaderControls = CCCreateCollection(Page_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sorter_Pay_Type, Sorter_Sum_Money, Sorter_Sum_Volume, Sorter_Count_Sales))
        Set Site_Name_HeaderControls = CCCreateCollection(Site_Name_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Site_Name))
        Set Site_Name_FooterControls = CCCreateCollection(Site_Name_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sum_Sum_Money, Sum_Sum_Volume, Sum_Count_Sales))
        Dim Site_NameKey
        Dim Groups
        Set Groups = New clsaddin_payments_data_pumpGroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report addin_payments_data_pump", Errors)
        Else
            Do While Not Recordset.EOF
                Site_Name.Value = Recordset.Fields("Site_Name")
                Pay_Type.Value = Recordset.Fields("Pay_Type")
                Sum_Money.Value = Recordset.Fields("Sum_Money")
                Sum_Volume.Value = Recordset.Fields("Sum_Volume")
                Count_Sales.Value = Recordset.Fields("Count_Sales")
                Sum_Sum_Money.Value = Recordset.Fields("Sum_Sum_Money")
                Sum_Sum_Volume.Value = Recordset.Fields("Sum_Sum_Volume")
                Sum_Count_Sales.Value = Recordset.Fields("Sum_Count_Sales")
                TotalSum_Sum_Money.Value = Recordset.Fields("TotalSum_Sum_Money")
                TotalSum_Sum_Volume.Value = Recordset.Fields("TotalSum_Sum_Volume")
                TotalSum_Count_Sales.Value = Recordset.Fields("TotalSum_Count_Sales")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or Site_NameKey <> Recordset.Fields("Site_Name") Then
                    Groups.OpenGroup "Site_Name"
                End If
                Groups.AddItem 
                Site_NameKey = Recordset.Fields("Site_Name")
                Recordset.MoveNext
                If Site_NameKey <> Recordset.Fields("Site_Name") Or Recordset.EOF Then
                    Groups.CloseGroup "Site_Name"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        Pay_Type.Value = items(i).Pay_Type
                        Sum_Money.Value = items(i).Sum_Money
                        Sum_Volume.Value = items(i).Sum_Volume
                        Count_Sales.Value = items(i).Count_Sales
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        TotalSum_Sum_Money.Value = items(i).TotalSum_Sum_Money
                        TotalSum_Sum_Volume.Value = items(i).TotalSum_Sum_Volume
                        TotalSum_Count_Sales.Value = items(i).TotalSum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                PageBreak.Visible = (i < EndItem-1 And ViewMode <> "Web")
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        Navigator.PageSize = PageSize
                        Navigator.PagesCount = Groups.TotalPages
                        Navigator.PageNumber = items(i).PageNumber
                        Navigator.Visible = ("Web" = ViewMode)
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderControls.Show
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "Site_Name"
                        Site_Name.Value = items(i).Site_Name
                        Sum_Sum_Money.Value = items(i).Sum_Sum_Money
                        Sum_Sum_Volume.Value = items(i).Sum_Sum_Volume
                        Sum_Count_Sales.Value = items(i).Sum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Header_BeforeShow", Me)
                            If Site_Name_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_HeaderBlock, AttributePrefix
                                Site_Name_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Footer_BeforeShow", Me)
                            If Site_Name_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_FooterBlock, AttributePrefix
                                Site_Name_FooterControls.Show
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End addin_payments_data_pump Show Method

'addin_payments_data_pump PageSize Property Let @3-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End addin_payments_data_pump PageSize Property Let

'addin_payments_data_pump PageSize Property Get @3-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End addin_payments_data_pump PageSize Property Get

End Class 'End addin_payments_data_pump Class @3-A61BA892

Class clsaddin_payments_data_pumpDataSource 'addin_payments_data_pumpDataSource Class @3-A46FC83F

'DataSource Variables @3-89C0C425
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Site_Name
    Public Pay_Type
    Public Sum_Money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_Money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_Money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
'End DataSource Variables

'DataSource Class_Initialize Event @3-86AA7215
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Site_Name = CCCreateField("Site_Name", "Site_Name", ccsText, Empty, Recordset)
        Set Pay_Type = CCCreateField("Pay_Type", "Pay_Type", ccsText, Empty, Recordset)
        Set Sum_Money = CCCreateField("Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set Sum_Volume = CCCreateField("Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Count_Sales = CCCreateField("Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set Sum_Sum_Money = CCCreateField("Sum_Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set Sum_Sum_Volume = CCCreateField("Sum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Sum_Count_Sales = CCCreateField("Sum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set TotalSum_Sum_Money = CCCreateField("TotalSum_Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set TotalSum_Sum_Volume = CCCreateField("TotalSum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set TotalSum_Count_Sales = CCCreateField("TotalSum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(Site_Name,  Pay_Type,  Sum_Money,  Sum_Volume,  Count_Sales,  Sum_Sum_Money,  Sum_Sum_Volume, _
             Sum_Count_Sales,  TotalSum_Sum_Money,  TotalSum_Sum_Volume,  TotalSum_Count_Sales)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_Pay_Type", "pay_payment_type", ""), _
            Array("Sorter_Sum_Money", "Sum_Money", ""), _
            Array("Sorter_Sum_Volume", "Sum_Volume", ""), _
            Array("Sorter_Count_Sales", "Count_Sales", ""))

        SQL = "SELECT SiteName AS Site_Name, pay_payment_type AS Pay_Type, Sum(pump_sales.money) AS Sum_Money, sum(pump_sales.volume) AS Sum_Volume, " & vbLf & _
        "Count(pump_sales.sale_id) AS Count_Sales  " & vbLf & _
        "FROM (sites inner join  addin_payments_data  ON " & vbLf & _
        "sites.ss_id = addin_payments_data.ss_id) left JOIN pump_sales ON " & vbLf & _
        "addin_payments_data.pay_sale_id = pump_sales.sale_id " & vbLf & _
        "WHERE addin_payments_data.ss_id = pump_sales.ss_id " & vbLf & _
        "and SiteName LIKE '%{s_Site_Name}%' " & vbLf & _
        "AND addin_payments_data.DAY_DATE >= '{Expr0}' " & vbLf & _
        "AND addin_payments_data.DAY_DATE <= '{Expr1}' " & vbLf & _
        "GROUP BY SiteName, pay_payment_type  {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) FROM (SELECT SiteName AS Site_Name, pay_payment_type AS Pay_Type, Sum(pump_sales.money) AS Sum_Money, sum(pump_sales.volume) AS Sum_Volume, " & vbLf & _
        "Count(pump_sales.sale_id) AS Count_Sales  " & vbLf & _
        "FROM (sites inner join  addin_payments_data  ON " & vbLf & _
        "sites.ss_id = addin_payments_data.ss_id) left JOIN pump_sales ON " & vbLf & _
        "addin_payments_data.pay_sale_id = pump_sales.sale_id " & vbLf & _
        "WHERE addin_payments_data.ss_id = pump_sales.ss_id " & vbLf & _
        "and SiteName LIKE '%{s_Site_Name}%' " & vbLf & _
        "AND addin_payments_data.DAY_DATE >= '{Expr0}' " & vbLf & _
        "AND addin_payments_data.DAY_DATE <= '{Expr1}' " & vbLf & _
        "GROUP BY SiteName, pay_payment_type) cnt"
        Where = ""
        Order = "pay_payment_type"
        StaticOrder = "Sites.SiteName asc"
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @3-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @3-C624D748
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "s_Site_Name", "urls_Site_Name", ccsText, Empty, Empty, Empty, False
            .AddParameter "Expr0", "expr59", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), dateadd("d", -10,date()), False
            .AddParameter "Expr1", "expr60", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), date(), False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @3-486FAD2E
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @3-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End addin_payments_data_pumpDataSource Class @3-A61BA892

Class clsRecordSites_addin_payments_data 'Sites_addin_payments_data Class @14-21F12275

'Sites_addin_payments_data Variables @14-77D261C1

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim ClearParameters
    Dim Button_DoSearch
    Dim s_Site_Name
    Dim s_Sale_Date0
    Dim DatePicker_s_Sale_Date0
    Dim s_Sale_Date
    Dim DatePicker_s_Sale_Date1
'End Sites_addin_payments_data Variables

'Sites_addin_payments_data Class_Initialize Event @14-2FEEA36D
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Sites_addin_payments_data")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Sites_addin_payments_data"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set s_Site_Name = CCCreateList(ccsListBox, "s_Site_Name", Empty, ccsText, CCGetRequestParam("s_Site_Name", Method), Empty)
        s_Site_Name.BoundColumn = "SiteName"
        s_Site_Name.TextColumn = "SiteName"
        Set s_Site_Name.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT SiteName  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_Sale_Date0 = CCCreateControl(ccsTextBox, "s_Sale_Date0", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_Sale_Date0", Method))
        Set DatePicker_s_Sale_Date0 = CCCreateDatePicker("DatePicker_s_Sale_Date0", "Sites_addin_payments_data", "s_Sale_Date0")
        Set s_Sale_Date = CCCreateControl(ccsTextBox, "s_Sale_Date", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_Sale_Date", Method))
        Set DatePicker_s_Sale_Date1 = CCCreateDatePicker("DatePicker_s_Sale_Date1", "Sites_addin_payments_data", "s_Sale_Date")
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_Site_Name, s_Sale_Date0, s_Sale_Date)
    End Sub
'End Sites_addin_payments_data Class_Initialize Event

'Sites_addin_payments_data Class_Terminate Event @14-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites_addin_payments_data Class_Terminate Event

'Sites_addin_payments_data Validate Method @14-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Sites_addin_payments_data Validate Method

'Sites_addin_payments_data Operation Method @14-A938B2C9
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "ReportSalesByPay.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "ReportSalesByPay.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Sites_addin_payments_data Operation Method

'Sites_addin_payments_data Show Method @14-3C7483FB
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Sites_addin_payments_data" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_Site_Name, s_Sale_Date0, DatePicker_s_Sale_Date0, s_Sale_Date, DatePicker_s_Sale_Date1, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("s_Site_Name", "s_Sale_Date", "s_Sale_Date0", "ccsForm"))
        ClearParameters.Page = "ReportSalesByPay.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_Site_Name.Errors
            Errors.AddErrors s_Sale_Date0.Errors
            Errors.AddErrors s_Sale_Date.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Sites_addin_payments_data" & ":"
            Controls.Show
        End If
    End Sub
'End Sites_addin_payments_data Show Method

End Class 'End Sites_addin_payments_data Class @14-A61BA892

'Include Page Implementation @2-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
