<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-67D366CE
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Header
Dim Report2
Dim Report1
Dim Report_Print
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "ReportStockByTanks.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "ReportStockByTanks.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-A77925F6
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Report2 = new clsRecordReport2
Set Report1 = New clsReportReport1
Set Report_Print = CCCreateControl(ccsLink, "Report_Print", Empty, ccsText, Empty, CCGetRequestParam("Report_Print", ccsGet))

Report_Print.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
Report_Print.Parameters = CCAddParam(Report_Print.Parameters, "ViewMode", "Print")
Report_Print.Page = "ReportStockByTanks.asp"
Report1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/ReportStockByTanks_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-BEAC6822
Header.Operations
Report2.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-10086C76
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Header, Report2, Report1, Report_Print))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-B8979B3A
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Set Report2 = Nothing
    Set Report1 = Nothing
End Sub
'End UnloadPage Sub



Class clsRecordReport2 'Report2 Class @60-0337016F

'Report2 Variables @60-78D29077

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim ClearParameters
    Dim Button_DoSearch
    Dim s_DAY_DATE
    Dim DatePicker_s_DAY_DATE
    Dim s_DAY_TIME
    Dim s_tank_id
    Dim s_Site_Name
    Dim s_DAY_TIME1
'End Report2 Variables

'Report2 Class_Initialize Event @60-D0528B85
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Report2")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Report2"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set s_DAY_DATE = CCCreateControl(ccsTextBox, "s_DAY_DATE", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_DAY_DATE", Method))
        Set DatePicker_s_DAY_DATE = CCCreateDatePicker("DatePicker_s_DAY_DATE", "Report2", "s_DAY_DATE")
        Set s_DAY_TIME = CCCreateList(ccsListBox, "s_DAY_TIME", Empty, ccsInteger, CCGetRequestParam("s_DAY_TIME", Method), Empty)
        Set s_DAY_TIME.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"), _
            Array("0:00Hs", "1:00 Hs", "2:00 Hs", "3:00 Hs", "4:00 Hs", "5:00 Hs", "6:00 Hs", "7:00 Hs", "8:00 Hs", "9:00 Hs", "10:00 Hs", "11:00 Hs", "12:00 Hs", "13:00 Hs", "14:00 Hs", "15:00 Hs", "16:00 Hs", "17:00 Hs", "18:00 Hs", "19:00 Hs", "20:00 Hs", "21:00 Hs", "22:00 Hs", "23:00 Hs")))
        Set s_tank_id = CCCreateList(ccsListBox, "s_tank_id", Empty, ccsInteger, CCGetRequestParam("s_tank_id", Method), Empty)
        s_tank_id.BoundColumn = "tank_id"
        s_tank_id.TextColumn = "tank_id"
        Set s_tank_id.DataSource = CCCreateDataSource(dsSQL, DBFusionHO, "SELECT distinct(tank_id)  " & _
"FROM tank_actual_info")
        Set s_Site_Name = CCCreateList(ccsListBox, "s_Site_Name", Empty, ccsText, CCGetRequestParam("s_Site_Name", Method), Empty)
        s_Site_Name.BoundColumn = "SiteName"
        s_Site_Name.TextColumn = "SiteName"
        Set s_Site_Name.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_DAY_TIME1 = CCCreateList(ccsListBox, "s_DAY_TIME1", Empty, ccsInteger, CCGetRequestParam("s_DAY_TIME1", Method), Empty)
        Set s_DAY_TIME1.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"), _
            Array("0:00Hs", "1:00 Hs", "2:00 Hs", "3:00 Hs", "4:00 Hs", "5:00 Hs", "6:00 Hs", "7:00 Hs", "8:00 Hs", "9:00 Hs", "10:00 Hs", "11:00 Hs", "12:00 Hs", "13:00 Hs", "14:00 Hs", "15:00 Hs", "16:00 Hs", "17:00 Hs", "18:00 Hs", "19:00 Hs", "20:00 Hs", "21:00 Hs", "22:00 Hs", "23:00 Hs")))
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_DAY_DATE, s_DAY_TIME, s_tank_id, s_Site_Name, s_DAY_TIME1)
    End Sub
'End Report2 Class_Initialize Event

'Report2 Class_Terminate Event @60-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Report2 Class_Terminate Event

'Report2 Validate Method @60-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Report2 Validate Method

'Report2 Operation Method @60-3A990EB3
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "ReportStockByTanks.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "ReportStockByTanks.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Report2 Operation Method

'Report2 Show Method @60-2AC9745B
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Report2" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_Site_Name, s_tank_id, s_DAY_DATE, DatePicker_s_DAY_DATE, s_DAY_TIME, s_DAY_TIME1, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("s_DAY_DATE", "s_DAY_TIME", "s_DAY_TIME1", "s_Site_Name", "s_tank_id", "ccsForm"))
        ClearParameters.Page = "ReportStockByTanks.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_DAY_DATE.Errors
            Errors.AddErrors s_DAY_TIME.Errors
            Errors.AddErrors s_tank_id.Errors
            Errors.AddErrors s_Site_Name.Errors
            Errors.AddErrors s_DAY_TIME1.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Report2" & ":"
            Controls.Show
        End If
    End Sub
'End Report2 Show Method

End Class 'End Report2 Class @60-A61BA892

'Report1 clsReportGroup @75-AE496B08
Class clsReportGroupReport1
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public Site_Name
    Public Tank
    Public Hour1
    Public Volume
    Public Max_Volume
    Public Min_Volume
    Public Product_Height
    Public Water_Volume
    Public Water_Height
    Public Temperature
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public Site_NameTotalIndex
    Public TankTotalIndex

    Public Sub SetControls()
        Me.Site_Name = Report1.Site_Name.Value
        Me.Tank = Report1.Tank.Value
        Me.Hour1 = Report1.Hour1.Value
        Me.Volume = Report1.Volume.Value
        Me.Max_Volume = Report1.Max_Volume.Value
        Me.Min_Volume = Report1.Min_Volume.Value
        Me.Product_Height = Report1.Product_Height.Value
        Me.Water_Volume = Report1.Water_Volume.Value
        Me.Water_Height = Report1.Water_Height.Value
        Me.Temperature = Report1.Temperature.Value
        Me.Report_CurrentDate = Report1.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.Site_Name = HeaderGrp.Site_Name
        Report1.Site_Name.ChangeValue(Me.Site_Name)
        Me.Tank = HeaderGrp.Tank
        Report1.Tank.ChangeValue(Me.Tank)
        Me.Hour1 = HeaderGrp.Hour1
        Report1.Hour1.ChangeValue(Me.Hour1)
        Me.Volume = HeaderGrp.Volume
        Report1.Volume.ChangeValue(Me.Volume)
        Me.Max_Volume = HeaderGrp.Max_Volume
        Report1.Max_Volume.ChangeValue(Me.Max_Volume)
        Me.Min_Volume = HeaderGrp.Min_Volume
        Report1.Min_Volume.ChangeValue(Me.Min_Volume)
        Me.Product_Height = HeaderGrp.Product_Height
        Report1.Product_Height.ChangeValue(Me.Product_Height)
        Me.Water_Volume = HeaderGrp.Water_Volume
        Report1.Water_Volume.ChangeValue(Me.Water_Volume)
        Me.Water_Height = HeaderGrp.Water_Height
        Report1.Water_Height.ChangeValue(Me.Water_Height)
        Me.Temperature = HeaderGrp.Temperature
        Report1.Temperature.ChangeValue(Me.Temperature)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        Report1.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = Report1.Report_TotalRecords.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = Report1.Report_TotalRecords.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End Report1 clsReportGroup

'clsReport1GroupsCollection @75-2ED82D4F
Class clsReport1GroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSite_NameCurrentHeaderIndex
    Private mTankCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSite_NameCurrentHeaderIndex = 2
        mTankCurrentHeaderIndex = 3
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGroupReport1
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.Site_NameTotalIndex = mSite_NameCurrentHeaderIndex
        group.TankTotalIndex = mTankCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + Report1.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + Report1.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "Site_Name" Then
            If PageSize > 0 And Report1.Site_Name_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Site_Name_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            OpenFlag = True
            mSite_NameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Site_Name"
            Groups.Add Groups.Count,Group
        End If
        If groupName = "Tank" Or OpenFlag Then
            If PageSize > 0 And Report1.Tank_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Tank_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Tank_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Tank_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mTankCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Tank"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And Report1.Report_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And Report1.Tank_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Tank_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Tank_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mTankCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Tank_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Tank"
        Groups.Add Groups.Count,Group
        If groupName = "Tank" Then Exit Sub
        If PageSize > 0 And Report1.Site_Name_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Site_Name_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSite_NameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Site_Name"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        Report1.Report_TotalRecords.Value = Report1.Report_TotalRecords.InitialValue
        Report1.Site_Name.Value = Report1.Site_Name.InitialValue
        Report1.Tank.Value = Report1.Tank.InitialValue
        Report1.Hour1.Value = Report1.Hour1.InitialValue
        Report1.Volume.Value = Report1.Volume.InitialValue
        Report1.Max_Volume.Value = Report1.Max_Volume.InitialValue
        Report1.Min_Volume.Value = Report1.Min_Volume.InitialValue
        Report1.Product_Height.Value = Report1.Product_Height.InitialValue
        Report1.Water_Volume.Value = Report1.Water_Volume.InitialValue
        Report1.Water_Height.Value = Report1.Water_Height.InitialValue
        Report1.Temperature.Value = Report1.Temperature.InitialValue
        Report1.Report_CurrentDate.Value = Report1.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And Report1.Detail.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clsReport1GroupsCollection

Class clsReportReport1 'Report1 Class @75-D191C334

'Report1 Variables @75-00A08ED5

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public Site_Name_HeaderBlock, Site_Name_Header
    Public Site_Name_FooterBlock, Site_Name_Footer
    Public Tank_HeaderBlock, Tank_Header
    Public Tank_FooterBlock, Tank_Footer
    Public ActiveSorter, SortingDirection
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public Site_Name_HeaderControls, Site_Name_FooterControls
    Public Tank_HeaderControls, Tank_FooterControls
    Dim Report_TotalRecords
    Dim Sorter_Hour
    Dim Sorter_Volume
    Dim Sorter_Max_Volume
    Dim Sorter_Min_Volume
    Dim Sorter_Product_Height
    Dim Sorter_Water_Volume
    Dim Sorter_Water_Height
    Dim Sorter_Temperature
    Dim Site_Name
    Dim Tank
    Dim Hour1
    Dim Volume
    Dim Max_Volume
    Dim Min_Volume
    Dim Product_Height
    Dim Water_Volume
    Dim Water_Height
    Dim Temperature
    Dim NoRecords
    Dim PageBreak
    Dim Report_CurrentDate
    Dim Navigator
'End Report1 Variables

'Report1 Class_Initialize Event @75-CAFF838F
    Private Sub Class_Initialize()
        ComponentName = "Report1"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 2
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set Site_Name_Footer = new clsSection
        Site_Name_Footer.Visible = True
        Site_Name_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Footer.Height)
        Set Site_Name_Header = new clsSection
        Site_Name_Header.Visible = True
        Site_Name_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Header.Height)
        Set Tank_Footer = new clsSection
        Tank_Footer.Visible = True
        Tank_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Tank_Footer.Height)
        Set Tank_Header = new clsSection
        Tank_Header.Visible = True
        Tank_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Tank_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clsReport1DataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If
        ActiveSorter = CCGetParam("Report1Order", Empty)
        SortingDirection = CCGetParam("Report1Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Sorter_Hour = CCCreateSorter("Sorter_Hour", Me, FileName)
        Set Sorter_Volume = CCCreateSorter("Sorter_Volume", Me, FileName)
        Set Sorter_Max_Volume = CCCreateSorter("Sorter_Max_Volume", Me, FileName)
        Set Sorter_Min_Volume = CCCreateSorter("Sorter_Min_Volume", Me, FileName)
        Set Sorter_Product_Height = CCCreateSorter("Sorter_Product_Height", Me, FileName)
        Set Sorter_Water_Volume = CCCreateSorter("Sorter_Water_Volume", Me, FileName)
        Set Sorter_Water_Height = CCCreateSorter("Sorter_Water_Height", Me, FileName)
        Set Sorter_Temperature = CCCreateSorter("Sorter_Temperature", Me, FileName)
        Set Site_Name = CCCreateReportLabel( "Site_Name", Empty, ccsText, Empty, CCGetRequestParam("Site_Name", ccsGet), "",  False, False,"")
        Set Tank = CCCreateReportLabel( "Tank", Empty, ccsInteger, Empty, CCGetRequestParam("Tank", ccsGet), "",  False, False,"")
        Set Hour1 = CCCreateReportLabel( "Hour1", Empty, ccsInteger, Empty, CCGetRequestParam("Hour1", ccsGet), "",  False, False,"")
        Set Volume = CCCreateReportLabel( "Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Volume", ccsGet), "",  False, False,"")
        Set Max_Volume = CCCreateReportLabel( "Max_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Max_Volume", ccsGet), "",  False, False,"")
        Set Min_Volume = CCCreateReportLabel( "Min_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Min_Volume", ccsGet), "",  False, False,"")
        Set Product_Height = CCCreateReportLabel( "Product_Height", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Product_Height", ccsGet), "",  False, False,"")
        Set Water_Volume = CCCreateReportLabel( "Water_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Water_Volume", ccsGet), "",  False, False,"")
        Set Water_Height = CCCreateReportLabel( "Water_Height", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Water_Height", ccsGet), "",  False, False,"")
        Set Temperature = CCCreateReportLabel( "Temperature", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Temperature", ccsGet), "",  False, False,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set PageBreak = CCCreatePanel("PageBreak")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End Report1 Class_Initialize Event

'Report1 Initialize Method @75-BD78A2B0
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.SetOrder ActiveSorter, SortingDirection
    End Sub
'End Report1 Initialize Method

'Report1 Class_Terminate Event @75-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End Report1 Class_Terminate Event

'Report1 Show Method @75-8CD36246
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("urls_Site_Name") = CCGetRequestParam("s_Site_Name", ccsGET)
            .Parameters("urls_tank_id") = CCGetRequestParam("s_tank_id", ccsGET)
            .Parameters("urls_DAY_TIME") = CCGetRequestParam("s_DAY_TIME", ccsGET)
            .Parameters("urls_DAY_TIME1") = CCGetRequestParam("s_DAY_TIME1", ccsGET)
            .Parameters("expr118") = Report2.s_DAY_DATE.value
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set Site_Name_HeaderBlock = TemplateBlock.Block("Section Site_Name_Header")
        Set Site_Name_FooterBlock = TemplateBlock.Block("Section Site_Name_Footer")
        Set Tank_HeaderBlock = TemplateBlock.Block("Section Tank_Header")
        Set Tank_FooterBlock = TemplateBlock.Block("Section Tank_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(Hour1, Volume, Max_Volume, Min_Volume, Product_Height, Water_Volume, Water_Height, Temperature))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(PageBreak, Report_CurrentDate, Navigator))
        Set Page_HeaderControls = CCCreateCollection(Page_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sorter_Hour, Sorter_Volume, Sorter_Max_Volume, Sorter_Min_Volume, Sorter_Product_Height, Sorter_Water_Volume, Sorter_Water_Height, Sorter_Temperature))
        Set Site_Name_HeaderControls = CCCreateCollection(Site_Name_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Site_Name))
        Set Site_Name_FooterControls = CCCreateCollection(Site_Name_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array())
        Set Tank_HeaderControls = CCCreateCollection(Tank_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Tank))
        Set Tank_FooterControls = CCCreateCollection(Tank_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array())
        Dim Site_NameKey
        Dim TankKey
        Dim Groups
        Set Groups = New clsReport1GroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report Report1", Errors)
        Else
            Do While Not Recordset.EOF
                Site_Name.Value = Recordset.Fields("Site_Name")
                Tank.Value = Recordset.Fields("Tank")
                Hour1.Value = Recordset.Fields("Hour1")
                Volume.Value = Recordset.Fields("Volume")
                Max_Volume.Value = Recordset.Fields("Max_Volume")
                Min_Volume.Value = Recordset.Fields("Min_Volume")
                Product_Height.Value = Recordset.Fields("Product_Height")
                Water_Volume.Value = Recordset.Fields("Water_Volume")
                Water_Height.Value = Recordset.Fields("Water_Height")
                Temperature.Value = Recordset.Fields("Temperature")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or Site_NameKey <> Recordset.Fields("Site_Name") Then
                    Groups.OpenGroup "Site_Name"
                ElseIf TankKey <> Recordset.Fields("Tank") Then
                    Groups.OpenGroup "Tank"
                End If
                Groups.AddItem 
                Site_NameKey = Recordset.Fields("Site_Name")
                TankKey = Recordset.Fields("Tank")
                Recordset.MoveNext
                If Site_NameKey <> Recordset.Fields("Site_Name") Or Recordset.EOF Then
                    Groups.CloseGroup "Site_Name"
                ElseIf TankKey <> Recordset.Fields("Tank") Then
                    Groups.CloseGroup "Tank"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        Hour1.Value = items(i).Hour1
                        Volume.Value = items(i).Volume
                        Max_Volume.Value = items(i).Max_Volume
                        Min_Volume.Value = items(i).Min_Volume
                        Product_Height.Value = items(i).Product_Height
                        Water_Volume.Value = items(i).Water_Volume
                        Water_Height.Value = items(i).Water_Height
                        Temperature.Value = items(i).Temperature
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                PageBreak.Visible = (i < EndItem-1 And ViewMode <> "Web")
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        Navigator.PageSize = PageSize
                        Navigator.PagesCount = Groups.TotalPages
                        Navigator.PageNumber = items(i).PageNumber
                        Navigator.Visible = ("Web" = ViewMode)
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderControls.Show
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "Site_Name"
                        Site_Name.Value = items(i).Site_Name
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Header_BeforeShow", Me)
                            If Site_Name_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_HeaderBlock, AttributePrefix
                                Site_Name_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Footer_BeforeShow", Me)
                            If Site_Name_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_FooterBlock, AttributePrefix
                                Site_Name_FooterBlock.ParseTo ccsParseAccumulate, DetailBlock
                            End If
                        End If
                    Case "Tank"
                        Tank.Value = items(i).Tank
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Tank_Header_BeforeShow", Me)
                            If Tank_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Tank_HeaderBlock, AttributePrefix
                                Tank_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Tank_Footer_BeforeShow", Me)
                            If Tank_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Tank_FooterBlock, AttributePrefix
                                Tank_FooterBlock.ParseTo ccsParseAccumulate, DetailBlock
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End Report1 Show Method

'Report1 PageSize Property Let @75-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Report1 PageSize Property Let

'Report1 PageSize Property Get @75-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Report1 PageSize Property Get

End Class 'End Report1 Class @75-A61BA892

Class clsReport1DataSource 'Report1DataSource Class @75-1489A126

'DataSource Variables @75-1D48A38B
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Site_Name
    Public Tank
    Public Hour1
    Public Volume
    Public Max_Volume
    Public Min_Volume
    Public Product_Height
    Public Water_Volume
    Public Water_Height
    Public Temperature
'End DataSource Variables

'DataSource Class_Initialize Event @75-D304DC1D
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Site_Name = CCCreateField("Site_Name", "Site_Name", ccsText, Empty, Recordset)
        Set Tank = CCCreateField("Tank", "Tank", ccsInteger, Empty, Recordset)
        Set Hour1 = CCCreateField("Hour1", "Hour", ccsInteger, Empty, Recordset)
        Set Volume = CCCreateField("Volume", "Volume", ccsFloat, Empty, Recordset)
        Set Max_Volume = CCCreateField("Max_Volume", "Max_Volume", ccsFloat, Empty, Recordset)
        Set Min_Volume = CCCreateField("Min_Volume", "Min_Volume", ccsFloat, Empty, Recordset)
        Set Product_Height = CCCreateField("Product_Height", "Product_Height", ccsFloat, Empty, Recordset)
        Set Water_Volume = CCCreateField("Water_Volume", "Water_Volume", ccsFloat, Empty, Recordset)
        Set Water_Height = CCCreateField("Water_Height", "Water_Height", ccsFloat, Empty, Recordset)
        Set Temperature = CCCreateField("Temperature", "Temperature", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(Site_Name,  Tank,  Hour1,  Volume,  Max_Volume,  Min_Volume,  Product_Height, _
             Water_Volume,  Water_Height,  Temperature)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_Hour", "Hour", ""), _
            Array("Sorter_Volume", "Volume", ""), _
            Array("Sorter_Max_Volume", "Max_Volume", ""), _
            Array("Sorter_Min_Volume", "Min_Volume", ""), _
            Array("Sorter_Product_Height", "Product_Height", ""), _
            Array("Sorter_Water_Volume", "Water_Volume", ""), _
            Array("Sorter_Water_Height", "Water_Height", ""), _
            Array("Sorter_Temperature", "Temperature", ""))

        SQL = "if '{s_tank_id}'  = 0  " & vbLf & _
        "	begin " & vbLf & _
        "		select t.Site_Name, t.s_tank as Tank, t.Tank_Time as Hour,  t.Volume,t.Max_Volume, t.Min_Volume, t.Product_Height, t.Water_Volume, t.Water_Height, t.Temperature " & vbLf & _
        "		from tank_table as t  " & vbLf & _
        "		where t.Site_name like '%{s_Site_Name}%' and t.Tank_Date = '{s_DAY_DATE}' and t.tank_time >= '{s_DAY_TIME}' and t.tank_time <= '{s_DAY_TIME1}' " & vbLf & _
        "		order by t.tank_date " & vbLf & _
        "    end " & vbLf & _
        "else " & vbLf & _
        "	begin " & vbLf & _
        "		select t.Site_Name, t.s_tank as Tank, t.Tank_Time as Hour,  t.Volume,t.Max_Volume, t.Min_Volume, t.Product_Height, t.Water_Volume, t.Water_Height, t.Temperature " & vbLf & _
        "		from tank_table as t  " & vbLf & _
        "		where t.Site_name like '%{s_Site_Name}%' and t.Tank_Date = '{s_DAY_DATE}' and t.tank_time >= '{s_DAY_TIME}' and t.tank_time <= '{s_DAY_TIME1}' and t.s_tank = '{s_tank_id}' " & vbLf & _
        "		 {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) FROM (if '{s_tank_id}'  = 0  " & vbLf & _
        "	begin " & vbLf & _
        "		select t.Site_Name, t.s_tank as Tank, t.Tank_Time as Hour,  t.Volume,t.Max_Volume, t.Min_Volume, t.Product_Height, t.Water_Volume, t.Water_Height, t.Temperature " & vbLf & _
        "		from tank_table as t  " & vbLf & _
        "		where t.Site_name like '%{s_Site_Name}%' and t.Tank_Date = '{s_DAY_DATE}' and t.tank_time >= '{s_DAY_TIME}' and t.tank_time <= '{s_DAY_TIME1}' " & vbLf & _
        "		order by t.tank_date " & vbLf & _
        "    end " & vbLf & _
        "else " & vbLf & _
        "	begin " & vbLf & _
        "		select t.Site_Name, t.s_tank as Tank, t.Tank_Time as Hour,  t.Volume,t.Max_Volume, t.Min_Volume, t.Product_Height, t.Water_Volume, t.Water_Height, t.Temperature " & vbLf & _
        "		from tank_table as t  " & vbLf & _
        "		where t.Site_name like '%{s_Site_Name}%' and t.Tank_Date = '{s_DAY_DATE}' and t.tank_time >= '{s_DAY_TIME}' and t.tank_time <= '{s_DAY_TIME1}' and t.s_tank = '{s_tank_id}') cnt"
        Where = ""
        Order = "t.tank_date	end"
        StaticOrder = "Site_Name asc,Tank asc"
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @75-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @75-D44946F6
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "s_Site_Name", "urls_Site_Name", ccsText, Empty, Empty, Empty, False
            .AddParameter "s_tank_id", "urls_tank_id", ccsInteger, Empty, Empty, 0, False
            .AddParameter "s_DAY_TIME", "urls_DAY_TIME", ccsInteger, Empty, Empty, 0, False
            .AddParameter "s_DAY_TIME1", "urls_DAY_TIME1", ccsInteger, Empty, Empty, 23, False
            .AddParameter "s_DAY_DATE", "expr118", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), date(), False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @75-486FAD2E
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @75-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Report1DataSource Class @75-A61BA892

'Include Page Implementation @58-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
