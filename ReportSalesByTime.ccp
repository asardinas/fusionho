<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" pasteActions="pasteActions" needGeneration="0">
	<Components>
		<IncludePage id="60" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Report id="61" secured="False" enablePrint="True" showMode="Web" sourceType="Table" returnValueType="Number" linesPerWebPage="40" linesPerPhysicalPage="50" connection="FusionHO" dataSource="pump_sales, Sites" orderBy="DAY_TIME" groupBy="SiteName, DAY_TIME" name="pump_sales_Sites" pageSizeLimit="100" wizardCaption="{res:CCS_ReportFormPrefix} {res:pump_salesSites} {res:CCS_ReportFormSuffix}" wizardLayoutType="GroupLeftAbove" activeCollection="TableParameters">
			<Components>
				<Section id="81" visible="True" lines="0" name="Report_Header" wizardSectionType="ReportHeader">
					<Components>
						<ReportLabel id="90" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Report_TotalRecords" function="Count" wizardUseTemplateBlock="False" PathID="pump_sales_SitesReport_HeaderReport_TotalRecords">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="82" visible="True" lines="1" name="Page_Header" wizardSectionType="PageHeader">
					<Components>
						<Sorter id="104" visible="True" name="Sorter_Sale_Time" column="DAY_TIME" wizardCaption="{res:Sale_Time}" wizardSortingType="SimpleDir" wizardControl="Sale_Time">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="106" visible="True" name="Sorter_Average_Money" column="Average_Money" wizardCaption="{res:Average_Money}" wizardSortingType="SimpleDir" wizardControl="Average_Money">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="108" visible="True" name="Sorter_Average_Volume" column="Average_Volume" wizardCaption="{res:Average_Volume}" wizardSortingType="SimpleDir" wizardControl="Average_Volume">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="112" visible="True" name="Sorter_Sum_Volume" column="Sum_Volume" wizardCaption="{res:Sum_Volume}" wizardSortingType="SimpleDir" wizardControl="Sum_Volume">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="114" visible="True" name="Sorter_Count_Sales" column="Count_Sales" wizardCaption="{res:Count_Sales}" wizardSortingType="SimpleDir" wizardControl="Count_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="110" visible="True" name="Sorter_Sum_Money" column="Sum_Money">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="84" visible="True" lines="1" name="Site_Name_Header">
					<Components>
						<ReportLabel id="91" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Site_Name" fieldSource="Site_Name" wizardCaption="Site_Name" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="pump_sales_SitesSite_Name_HeaderSite_Name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="85" visible="True" lines="1" name="Detail">
					<Components>
						<ReportLabel id="105" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="Sale_Time" fieldSource="Sale_Time" wizardCaption="Sale_Time" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailSale_Time">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="107" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Average_Money" fieldSource="Average_Money" wizardCaption="Average_Money" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailAverage_Money" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="109" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Average_Volume" fieldSource="Average_Volume" wizardCaption="Average_Volume" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailAverage_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="111" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Sum_Money" fieldSource="Sum_Money" wizardCaption="Sum_Money" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailSum_Money" format="$0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="113" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Sum_Volume" fieldSource="Sum_Volume" wizardCaption="Sum_Volume" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailSum_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="115" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="Count_Sales" fieldSource="Count_Sales" wizardCaption="Count_Sales" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="pump_sales_SitesDetailCount_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="86" visible="True" lines="1" name="Site_Name_Footer">
					<Components>
						<ReportLabel id="92" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Site_Name" name="Sum_Sum_Money" fieldSource="Sum_Money" summarised="True" function="Sum" wizardCaption="{res:Sum_Money}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesSite_Name_FooterSum_Sum_Money" format="$0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="93" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Site_Name" name="Sum_Sum_Volume" fieldSource="Sum_Volume" summarised="True" function="Sum" wizardCaption="{res:Sum_Volume}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesSite_Name_FooterSum_Sum_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="94" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Site_Name" name="Sum_Count_Sales" fieldSource="Count_Sales" summarised="True" function="Sum" wizardCaption="{res:Count_Sales}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesSite_Name_FooterSum_Count_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="87" visible="True" lines="1" name="Report_Footer" wizardSectionType="ReportFooter">
					<Components>
						<Panel id="88" visible="True" name="NoRecords" wizardNoRecords="{res:CCS_NoRecords}">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
						<ReportLabel id="99" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_Sum_Money" fieldSource="Sum_Money" summarised="True" function="Sum" wizardCaption="{res:Sum_Money}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesReport_FooterTotalSum_Sum_Money" format="$0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="100" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_Sum_Volume" fieldSource="Sum_Volume" summarised="True" function="Sum" wizardCaption="{res:Sum_Volume}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesReport_FooterTotalSum_Sum_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="101" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_Count_Sales" fieldSource="Count_Sales" summarised="True" function="Sum" wizardCaption="{res:Count_Sales}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="pump_sales_SitesReport_FooterTotalSum_Count_Sales">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="89" visible="True" lines="2" name="Page_Footer" wizardSectionType="PageFooter" pageBreakAfter="True">
					<Components>
						<Panel id="95" visible="True" name="PageBreak">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
						<ReportLabel id="96" fieldSourceType="SpecialValue" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Report_CurrentDate" fieldSource="CurrentDate" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardInsertToDateTD="True" PathID="pump_sales_SitesPage_FooterReport_CurrentDate">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Navigator id="97" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardImagesScheme="Fusionho1">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Hide-Show Component" actionCategory="General" id="98" action="Hide" conditionType="Parameter" dataType="Integer" condition="LessThan" name1="TotalPages" sourceType1="SpecialValue" name2="2" sourceType2="Expression"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Navigator>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="102" conditionType="Parameter" useIsNull="False" field="SiteName" parameterSource="s_Site_Name" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
				<TableParameter id="103" conditionType="Parameter" useIsNull="False" field="DAY_DATE" dataType="Date" logicOperator="And" searchConditionType="GreaterThanOrEqual" parameterType="Expression" orderNumber="2" defaultValue="dateadd(&quot;d&quot;, -10,date())" DBFormat="mm/dd/yyyy" format="mm/dd/yyyy" parameterSource="Sites_pump_sales.s_Sale_Date0.value"/>
				<TableParameter id="119" conditionType="Parameter" useIsNull="False" field="DAY_DATE" dataType="Date" searchConditionType="LessThanOrEqual" parameterType="Expression" logicOperator="And" DBFormat="mm/dd/yyyy" format="mm/dd/yyyy" defaultValue="date()" parameterSource="Sites_pump_sales.s_Sale_Date.value"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="62" tableName="pump_sales" schemaName="dbo" posLeft="10" posTop="10" posWidth="117" posHeight="180"/>
				<JoinTable id="63" tableName="Sites" schemaName="dbo" posLeft="148" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="64" tableLeft="Sites" tableRight="pump_sales" fieldLeft="Sites.ss_id" fieldRight="pump_sales.ss_id" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="65" tableName="Sites" fieldName="SiteName" isExpression="False" alias="Site_Name"/>
				<Field id="66" tableName="pump_sales" fieldName="DAY_TIME" isExpression="False" alias="Sale_Time"/>
				<Field id="67" fieldName="Avg(money)" isExpression="True" alias="Average_Money"/>
				<Field id="68" fieldName="avg(volume)" isExpression="True" alias="Average_Volume"/>
				<Field id="69" fieldName="Sum(money)" isExpression="True" alias="Sum_Money"/>
				<Field id="70" fieldName="sum(volume)" isExpression="True" alias="Sum_Volume"/>
				<Field id="71" fieldName="Count(sale_id)" isExpression="True" alias="Count_Sales"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<ReportGroups>
				<ReportGroup id="83" name="Site_Name" field="Site_Name" sqlField="Sites.SiteName" sortOrder="asc"/>
			</ReportGroups>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Report>
		<Record id="72" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Sites_pump_sales" wizardCaption="{res:CCS_SearchFormPrefix} {res:Sites_pump_sales} {res:CCS_SearchFormSuffix}" wizardOrientation="Vertical" wizardFormMethod="post" returnPage="ReportSalesByTime.ccp" PathID="Sites_pump_sales">
			<Components>
				<Link id="73" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="ReportSalesByTime.ccp" removeParameters="s_Site_Name;s_Sale_Date;s_Sale_Date0" wizardThemeItem="SorterLink" wizardDefaultValue="{res:CCS_Clear}" PathID="Sites_pump_salesClearParameters">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="74" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}" wizardThemeItem="FooterIMG" wizardButtonImage="ButtonSearchOn" PathID="Sites_pump_salesButton_DoSearch">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<ListBox id="75" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_Site_Name" wizardCaption="{res:Site_Name}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" PathID="Sites_pump_saless_Site_Name" sourceType="Table" connection="FusionHO" dataSource="Sites" boundColumn="SiteName" textColumn="SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="120" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="121" tableName="Sites" fieldName="SiteName"/>
					</Fields>
				</ListBox>
				<TextBox id="76" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_Sale_Date0" wizardCaption="{res:Sale_Time}" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" PathID="Sites_pump_saless_Sale_Date0" format="mm/dd/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="116" name="DatePicker_s_Sale_Date1" PathID="Sites_pump_salesDatePicker_s_Sale_Date1" control="s_Sale_Date0" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="117" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_Sale_Date" PathID="Sites_pump_saless_Sale_Date" format="mm/dd/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="118" name="DatePicker_s_Sale_Date2" PathID="Sites_pump_salesDatePicker_s_Sale_Date2" control="s_Sale_Date" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Hide-Show Component" actionCategory="General" id="80" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" parameter1="Print" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Link id="77" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Report_Print" hrefSource="ReportSalesByTime.ccp" wizardTheme="Fusionho1" wizardThemeType="File" wizardDefaultValue="{res:CCS_ReportPrintLink}" wizardUseTemplateBlock="True" wizardBeforeHTML="&lt;p align=&quot;right&quot;&gt;" wizardAfterHTML="&lt;/p&gt;" wizardLinkTarget="_blank" PathID="Report_Print">
			<Components/>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Hide-Show Component" actionCategory="General" id="79" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" parameter1="Print" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
					</Actions>
				</Event>
			</Events>
			<LinkParameters>
				<LinkParameter id="78" sourceType="Expression" format="yyyy-mm-dd" name="ViewMode" source="&quot;Print&quot;"/>
			</LinkParameters>
			<Attributes/>
			<Features/>
		</Link>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="ReportSalesByTime.asp" forShow="True" url="ReportSalesByTime.asp" comment="'" codePage="windows-1252"/>
		<CodeFile id="Events" language="ASPTemplates" name="ReportSalesByTime_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
