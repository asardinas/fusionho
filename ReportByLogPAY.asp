<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-FC4958B7
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Report1
Dim Header
Dim Report2
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "ReportByLogPAY.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "ReportByLogPAY.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-E690D317
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Report1 = New clsReportReport1
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Report2 = new clsRecordReport2
Report1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/ReportByLogPAY_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-BEAC6822
Header.Operations
Report2.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-07190FCB
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Report1, Header, Report2))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-2B419BB8
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set Report1 = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Set Report2 = Nothing
End Sub
'End UnloadPage Sub

'Report1 clsReportGroup @2-B179B505
Class clsReportGroupReport1
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public SiteName
    Public File_Date
    Public File_Time
    Public Type_State
    Public Record_Date
    Public Record_Time
    Public File_Path
    Public ReportLabel1
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public SiteNameTotalIndex

    Public Sub SetControls()
        Me.SiteName = Report1.SiteName.Value
        Me.File_Date = Report1.File_Date.Value
        Me.File_Time = Report1.File_Time.Value
        Me.Type_State = Report1.Type_State.Value
        Me.Record_Date = Report1.Record_Date.Value
        Me.Record_Time = Report1.Record_Time.Value
        Me.File_Path = Report1.File_Path.Value
        Me.ReportLabel1 = Report1.ReportLabel1.Value
        Me.Report_CurrentDate = Report1.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.SiteName = HeaderGrp.SiteName
        Report1.SiteName.ChangeValue(Me.SiteName)
        Me.File_Date = HeaderGrp.File_Date
        Report1.File_Date.ChangeValue(Me.File_Date)
        Me.File_Time = HeaderGrp.File_Time
        Report1.File_Time.ChangeValue(Me.File_Time)
        Me.Type_State = HeaderGrp.Type_State
        Report1.Type_State.ChangeValue(Me.Type_State)
        Me.Record_Date = HeaderGrp.Record_Date
        Report1.Record_Date.ChangeValue(Me.Record_Date)
        Me.Record_Time = HeaderGrp.Record_Time
        Report1.Record_Time.ChangeValue(Me.Record_Time)
        Me.File_Path = HeaderGrp.File_Path
        Report1.File_Path.ChangeValue(Me.File_Path)
        Me.ReportLabel1 = HeaderGrp.ReportLabel1
        Report1.ReportLabel1.ChangeValue(Me.ReportLabel1)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        Report1.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = Report1.Report_TotalRecords.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = Report1.Report_TotalRecords.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End Report1 clsReportGroup

'clsReport1GroupsCollection @2-BBE024BE
Class clsReport1GroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSiteNameCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSiteNameCurrentHeaderIndex = 2
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGroupReport1
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.SiteNameTotalIndex = mSiteNameCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + Report1.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + Report1.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "SiteName" Then
            If PageSize > 0 And Report1.SiteName_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.SiteName_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.SiteName_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "SiteName_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mSiteNameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="SiteName"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And Report1.Report_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And Report1.SiteName_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.SiteName_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.SiteName_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSiteNameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "SiteName_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="SiteName"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        Report1.Report_TotalRecords.Value = Report1.Report_TotalRecords.InitialValue
        Report1.SiteName.Value = Report1.SiteName.InitialValue
        Report1.File_Date.Value = Report1.File_Date.InitialValue
        Report1.File_Time.Value = Report1.File_Time.InitialValue
        Report1.Type_State.Value = Report1.Type_State.InitialValue
        Report1.Record_Date.Value = Report1.Record_Date.InitialValue
        Report1.Record_Time.Value = Report1.Record_Time.InitialValue
        Report1.File_Path.Value = Report1.File_Path.InitialValue
        Report1.ReportLabel1.Value = Report1.ReportLabel1.InitialValue
        Report1.Report_CurrentDate.Value = Report1.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And Report1.Detail.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clsReport1GroupsCollection

Class clsReportReport1 'Report1 Class @2-D191C334

'Report1 Variables @2-7538F0FC

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public SiteName_HeaderBlock, SiteName_Header
    Public SiteName_FooterBlock, SiteName_Footer
    Public ActiveSorter, SortingDirection
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public SiteName_HeaderControls, SiteName_FooterControls
    Dim Report_TotalRecords
    Dim Sorter_File_Path
    Dim Sorter_File_Date
    Dim Sorter_File_Time
    Dim Sorter_Type_State
    Dim Sorter_Record_Date
    Dim Sorter_Record_Time
    Dim SiteName
    Dim File_Date
    Dim File_Time
    Dim Type_State
    Dim Record_Date
    Dim Record_Time
    Dim File_Path
    Dim ReportLabel1
    Dim NoRecords
    Dim Report_CurrentDate
    Dim Navigator
'End Report1 Variables

'Report1 Class_Initialize Event @2-70562A07
    Private Sub Class_Initialize()
        ComponentName = "Report1"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 1
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set SiteName_Footer = new clsSection
        SiteName_Footer.Visible = True
        SiteName_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, SiteName_Footer.Height)
        Set SiteName_Header = new clsSection
        SiteName_Header.Visible = True
        SiteName_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, SiteName_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clsReport1DataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If
        ActiveSorter = CCGetParam("Report1Order", Empty)
        SortingDirection = CCGetParam("Report1Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Sorter_File_Path = CCCreateSorter("Sorter_File_Path", Me, FileName)
        Set Sorter_File_Date = CCCreateSorter("Sorter_File_Date", Me, FileName)
        Set Sorter_File_Time = CCCreateSorter("Sorter_File_Time", Me, FileName)
        Set Sorter_Type_State = CCCreateSorter("Sorter_Type_State", Me, FileName)
        Set Sorter_Record_Date = CCCreateSorter("Sorter_Record_Date", Me, FileName)
        Set Sorter_Record_Time = CCCreateSorter("Sorter_Record_Time", Me, FileName)
        Set SiteName = CCCreateReportLabel( "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet), "",  False, False,"")
        Set File_Date = CCCreateReportLabel( "File_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("File_Date", ccsGet), "",  False, False,"")
        Set File_Time = CCCreateReportLabel( "File_Time", Empty, ccsText, Empty, CCGetRequestParam("File_Time", ccsGet), "",  False, False,"")
        Set Type_State = CCCreateReportLabel( "Type_State", Empty, ccsText, Empty, CCGetRequestParam("Type_State", ccsGet), "",  False, False,"")
        Set Record_Date = CCCreateReportLabel( "Record_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Record_Date", ccsGet), "",  False, False,"")
        Set Record_Time = CCCreateReportLabel( "Record_Time", Empty, ccsText, Empty, CCGetRequestParam("Record_Time", ccsGet), "",  False, False,"")
        Set File_Path = CCCreateReportLabel( "File_Path", Empty, ccsText, Empty, CCGetRequestParam("File_Path", ccsGet), "",  False, False,"")
        Set ReportLabel1 = CCCreateReportLabel( "ReportLabel1", Empty, ccsText, Empty, CCGetRequestParam("ReportLabel1", ccsGet), "",  False, True,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End Report1 Class_Initialize Event

'Report1 Initialize Method @2-BD78A2B0
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.SetOrder ActiveSorter, SortingDirection
    End Sub
'End Report1 Initialize Method

'Report1 Class_Terminate Event @2-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End Report1 Class_Terminate Event

'Report1 Show Method @2-4ED164FB
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("urlsite") = CCGetRequestParam("site", ccsGET)
            .Parameters("urls_type_state") = CCGetRequestParam("s_type_state", ccsGET)
            .Parameters("expr43") = Report2.s_file_date.value
            .Parameters("expr44") = Report2.s_file_date1.value
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set SiteName_HeaderBlock = TemplateBlock.Block("Section SiteName_Header")
        Set SiteName_FooterBlock = TemplateBlock.Block("Section SiteName_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(File_Date, File_Time, Type_State, Record_Date, Record_Time, File_Path, ReportLabel1))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_CurrentDate, Navigator))
        Set Page_HeaderControls = CCCreateCollection(Page_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sorter_File_Path, Sorter_File_Date, Sorter_File_Time, Sorter_Type_State, Sorter_Record_Date, Sorter_Record_Time))
        Set SiteName_HeaderControls = CCCreateCollection(SiteName_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(SiteName))
        Set SiteName_FooterControls = CCCreateCollection(SiteName_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array())
        Dim SiteNameKey
        Dim Groups
        Set Groups = New clsReport1GroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report Report1", Errors)
        Else
            Do While Not Recordset.EOF
                SiteName.Value = Recordset.Fields("SiteName")
                File_Date.Value = Recordset.Fields("File_Date")
                File_Time.Value = Recordset.Fields("File_Time")
                Type_State.Value = Recordset.Fields("Type_State")
                Record_Date.Value = Recordset.Fields("Record_Date")
                Record_Time.Value = Recordset.Fields("Record_Time")
                File_Path.Value = Recordset.Fields("File_Path")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                ReportLabel1.Value = Empty
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or SiteNameKey <> Recordset.Fields("SiteName") Then
                    Groups.OpenGroup "SiteName"
                End If
                Groups.AddItem 
                SiteNameKey = Recordset.Fields("SiteName")
                Recordset.MoveNext
                If SiteNameKey <> Recordset.Fields("SiteName") Or Recordset.EOF Then
                    Groups.CloseGroup "SiteName"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        File_Date.Value = items(i).File_Date
                        File_Time.Value = items(i).File_Time
                        Type_State.Value = items(i).Type_State
                        Record_Date.Value = items(i).Record_Date
                        Record_Time.Value = items(i).Record_Time
                        File_Path.Value = items(i).File_Path
                        ReportLabel1.Value = items(i).ReportLabel1
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        Navigator.PageSize = PageSize
                        Navigator.PagesCount = Groups.TotalPages
                        Navigator.PageNumber = items(i).PageNumber
                        Navigator.Visible = ("Web" = ViewMode)
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderControls.Show
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "SiteName"
                        SiteName.Value = items(i).SiteName
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "SiteName_Header_BeforeShow", Me)
                            If SiteName_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show SiteName_HeaderBlock, AttributePrefix
                                SiteName_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "SiteName_Footer_BeforeShow", Me)
                            If SiteName_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show SiteName_FooterBlock, AttributePrefix
                                SiteName_FooterBlock.ParseTo ccsParseAccumulate, DetailBlock
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End Report1 Show Method

'Report1 PageSize Property Let @2-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Report1 PageSize Property Let

'Report1 PageSize Property Get @2-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Report1 PageSize Property Get

End Class 'End Report1 Class @2-A61BA892

Class clsReport1DataSource 'Report1DataSource Class @2-1489A126

'DataSource Variables @2-9EBA803A
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public SiteName
    Public File_Date
    Public File_Time
    Public Type_State
    Public Record_Date
    Public Record_Time
    Public File_Path
'End DataSource Variables

'DataSource Class_Initialize Event @2-43486DB1
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set File_Date = CCCreateField("File_Date", "File_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set File_Time = CCCreateField("File_Time", "File_Time", ccsText, Empty, Recordset)
        Set Type_State = CCCreateField("Type_State", "Type_State", ccsText, Empty, Recordset)
        Set Record_Date = CCCreateField("Record_Date", "Record_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Record_Time = CCCreateField("Record_Time", "Record_Time", ccsText, Empty, Recordset)
        Set File_Path = CCCreateField("File_Path", "File_Path", ccsText, Empty, Recordset)
        Fields.AddFields Array(SiteName, File_Date, File_Time, Type_State, Record_Date, Record_Time, File_Path)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_File_Path", "File_Path", ""), _
            Array("Sorter_File_Date", "File_Date", ""), _
            Array("Sorter_File_Time", "File_Time", ""), _
            Array("Sorter_Type_State", "Type_State", ""), _
            Array("Sorter_Record_Date", "Record_Date", ""), _
            Array("Sorter_Record_Time", "Record_Time", ""))

        SQL = " " & vbLf & _
        "(select  SiteName, t.File_Date, t.File_Time , t.error_description as Type_State, t.Record_Date, t.Record_Time, t.path_source as File_Path from PAY_error as t inner join Sites on t.ss_id = Sites.ss_id " & vbLf & _
        "where t.record_time = (select max(p.record_time) from PAY_error as p where  p.path_source = t.path_source ) " & vbLf & _
        "and SiteName like '%{site}%' and File_Date >= '{s_file_date}' and File_Date <= '{s_file_date1}' and error_description like '%{s_type_state}%') " & vbLf & _
        "union " & vbLf & _
        "(select SiteName, File_Date, File_Time, Type_State, Record_Date, Record_Time, File_Path  from log_PAY inner join sites on log_PAY.ss_id = sites.ss_id " & vbLf & _
        "where SiteName like '%{site}%' and File_Date >= '{s_file_date}' and File_Date <= '{s_file_date1}' and Type_State like '%{s_type_state}%') " & vbLf & _
        ""
        CountSQL = "SELECT COUNT(*) FROM ( " & vbLf & _
        "(select  SiteName, t.File_Date, t.File_Time , t.error_description as Type_State, t.Record_Date, t.Record_Time, t.path_source as File_Path from PAY_error as t inner join Sites on t.ss_id = Sites.ss_id " & vbLf & _
        "where t.record_time = (select max(p.record_time) from PAY_error as p where  p.path_source = t.path_source ) " & vbLf & _
        "and SiteName like '%{site}%' and File_Date >= '{s_file_date}' and File_Date <= '{s_file_date1}' and error_description like '%{s_type_state}%') " & vbLf & _
        "union " & vbLf & _
        "(select SiteName, File_Date, File_Time, Type_State, Record_Date, Record_Time, File_Path  from log_PAY inner join sites on log_PAY.ss_id = sites.ss_id " & vbLf & _
        "where SiteName like '%{site}%' and File_Date >= '{s_file_date}' and File_Date <= '{s_file_date1}' and Type_State like '%{s_type_state}%') " & vbLf & _
        ") cnt"
        Where = ""
        Order = ""
        StaticOrder = "SiteName asc"
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @2-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @2-C998766C
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "site", "urlsite", ccsText, Empty, Empty, Empty, False
            .AddParameter "s_type_state", "urls_type_state", ccsText, Empty, Empty, Empty, False
            .AddParameter "s_file_date", "expr43", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), dateadd("d", -10,date()), False
            .AddParameter "s_file_date1", "expr44", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), date(), False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @2-486FAD2E
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Report1DataSource Class @2-A61BA892

Class clsRecordReport2 'Report2 Class @30-0337016F

'Report2 Variables @30-FDFEFBC1

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim ClearParameters
    Dim Button_DoSearch
    Dim s_file_date
    Dim DatePicker_s_file_date
    Dim s_type_state
    Dim site
    Dim s_file_date1
    Dim DatePicker_s_file_date1
'End Report2 Variables

'Report2 Class_Initialize Event @30-C5C58551
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Report2")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Report2"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set s_file_date = CCCreateControl(ccsTextBox, "s_file_date", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_file_date", Method))
        Set DatePicker_s_file_date = CCCreateDatePicker("DatePicker_s_file_date", "Report2", "s_file_date")
        Set s_type_state = CCCreateList(ccsListBox, "s_type_state", Empty, ccsText, CCGetRequestParam("s_type_state", Method), Empty)
        Set s_type_state.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("Success", "Primary Key Duplicate", "Site Not Exists", "Site Inactive", "Unknown error"), _
            Array("Success", "Primary Key Duplicate", "Site Not Exists", "Site Inactive", "Unknown error")))
        Set site = CCCreateList(ccsListBox, "site", Empty, ccsText, CCGetRequestParam("site", Method), Empty)
        site.BoundColumn = "SiteName"
        site.TextColumn = "SiteName"
        Set site.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT SiteName  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_file_date1 = CCCreateControl(ccsTextBox, "s_file_date1", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_file_date1", Method))
        Set DatePicker_s_file_date1 = CCCreateDatePicker("DatePicker_s_file_date1", "Report2", "s_file_date1")
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_file_date, s_type_state, site, s_file_date1)
    End Sub
'End Report2 Class_Initialize Event

'Report2 Class_Terminate Event @30-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Report2 Class_Terminate Event

'Report2 Validate Method @30-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Report2 Validate Method

'Report2 Operation Method @30-E8823FFA
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "ReportByLogPAY.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "ReportByLogPAY.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Report2 Operation Method

'Report2 Show Method @30-BDF30FB5
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Report2" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(site, s_file_date, DatePicker_s_file_date, s_file_date1, DatePicker_s_file_date1, s_type_state, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("site", "s_file_date1", "s_file_date", "s_type_state", "ccsForm"))
        ClearParameters.Page = "ReportByLogPAY.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_file_date.Errors
            Errors.AddErrors s_type_state.Errors
            Errors.AddErrors site.Errors
            Errors.AddErrors s_file_date1.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Report2" & ":"
            Controls.Show
        End If
    End Sub
'End Report2 Show Method

End Class 'End Report2 Class @30-A61BA892

'Include Page Implementation @29-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
