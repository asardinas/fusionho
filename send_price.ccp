<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" pasteActions="pasteActions" needGeneration="0">
	<Components>
		<IncludePage id="2" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Grid id="3" secured="False" sourceType="SQL" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="SELECT ss_id , SiteName, Address, Moso_id as Moso, Latitud, Longitud, Tm_id as Tm_Name, Zip, Phone, Mail, Valid as State, Region_id as Region,Country_id as Country,City_id as City,Local_type_id as Local,Service_id as Service,Brand_id as Brand FROM Sites 
where SiteName like '%{s_SiteName}%'" name="Grid1" orderBy="ss_id" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Grid1} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" activeCollection="SQLParameters" parameterTypeListName="ParameterTypeList">
			<Components>
				<Sorter id="4" visible="True" name="Sorter_ss_id" column="ss_id" wizardCaption="{res:ss_id}" wizardSortingType="SimpleDir" wizardControl="ss_id" wizardAddNbsp="False" PathID="Grid1Sorter_ss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="5" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Grid1Sorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="6" visible="True" name="Sorter_Address" column="Address" wizardCaption="{res:Address}" wizardSortingType="SimpleDir" wizardControl="Address" wizardAddNbsp="False" PathID="Grid1Sorter_Address">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="7" visible="True" name="Sorter_Latitud" column="Latitud" wizardCaption="{res:Latitud}" wizardSortingType="SimpleDir" wizardControl="Latitud" wizardAddNbsp="False" PathID="Grid1Sorter_Latitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="8" visible="True" name="Sorter_Longitud" column="Longitud" wizardCaption="{res:Longitud}" wizardSortingType="SimpleDir" wizardControl="Longitud" wizardAddNbsp="False" PathID="Grid1Sorter_Longitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="9" visible="True" name="Sorter_Country" column="Country" wizardCaption="{res:Country}" wizardSortingType="SimpleDir" wizardControl="Country" wizardAddNbsp="False" PathID="Grid1Sorter_Country">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="10" visible="True" name="Sorter_Region" column="Region" wizardCaption="{res:Region}" wizardSortingType="SimpleDir" wizardControl="Region" wizardAddNbsp="False" PathID="Grid1Sorter_Region">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="11" visible="True" name="Sorter_City" column="City" wizardCaption="{res:City}" wizardSortingType="SimpleDir" wizardControl="City" wizardAddNbsp="False" PathID="Grid1Sorter_City">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="12" visible="True" name="Sorter_Zip" column="Zip" wizardCaption="{res:Zip}" wizardSortingType="SimpleDir" wizardControl="Zip" wizardAddNbsp="False" PathID="Grid1Sorter_Zip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="13" visible="True" name="Sorter_Phone" column="Phone" wizardCaption="{res:Phone}" wizardSortingType="SimpleDir" wizardControl="Phone" wizardAddNbsp="False" PathID="Grid1Sorter_Phone">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="14" visible="True" name="Sorter_Moso" column="Moso" wizardCaption="{res:Moso}" wizardSortingType="SimpleDir" wizardControl="Moso" wizardAddNbsp="False" PathID="Grid1Sorter_Moso">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="15" visible="True" name="Sorter_Tm_Name" column="Tm_Name" wizardCaption="{res:Tm_Name}" wizardSortingType="SimpleDir" wizardControl="Tm_Name" wizardAddNbsp="False" PathID="Grid1Sorter_Tm_Name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="16" visible="True" name="Sorter_Mail" column="Mail" wizardCaption="{res:Mail}" wizardSortingType="SimpleDir" wizardControl="Mail" wizardAddNbsp="False" PathID="Grid1Sorter_Mail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="17" visible="True" name="Sorter_State" column="State" wizardCaption="{res:State}" wizardSortingType="SimpleDir" wizardControl="State" wizardAddNbsp="False" PathID="Grid1Sorter_State">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="18" visible="True" name="Sorter_Local" column="Local" wizardCaption="{res:Local}" wizardSortingType="SimpleDir" wizardControl="Local" wizardAddNbsp="False" PathID="Grid1Sorter_Local">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="19" visible="True" name="Sorter_Service" column="Service" wizardCaption="{res:Service}" wizardSortingType="SimpleDir" wizardControl="Service" wizardAddNbsp="False" PathID="Grid1Sorter_Service">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="20" visible="True" name="Sorter_Brand" column="Brand" wizardCaption="{res:Brand}" wizardSortingType="SimpleDir" wizardControl="Brand" wizardAddNbsp="False" PathID="Grid1Sorter_Brand">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="21" fieldSourceType="DBColumn" dataType="Integer" html="False" name="ss_id" fieldSource="ss_id" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1ss_id" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="send_price.ccp" removeParameters="price_change_id;aplication_date;aplication_time;var">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="61" sourceType="DataField" name="ss_id" source="ss_id"/>
						<LinkParameter id="150" sourceType="Expression" name="site" source="1"/>
					</LinkParameters>
				</Link>
				<Label id="22" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid1SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="23" fieldSourceType="DBColumn" dataType="Text" html="False" name="Address" fieldSource="Address" wizardCaption="{res:Address}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid1Address">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="24" fieldSourceType="DBColumn" dataType="Float" html="False" name="Latitud" fieldSource="Latitud" wizardCaption="{res:Latitud}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Latitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="25" fieldSourceType="DBColumn" dataType="Float" html="False" name="Longitud" fieldSource="Longitud" wizardCaption="{res:Longitud}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Longitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="26" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Country" fieldSource="Country" wizardCaption="{res:Country}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Country">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="27" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Region" fieldSource="Region" wizardCaption="{res:Region}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Region">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="28" fieldSourceType="DBColumn" dataType="Integer" html="False" name="City" fieldSource="City" wizardCaption="{res:City}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1City">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="29" fieldSourceType="DBColumn" dataType="Text" html="False" name="Zip" fieldSource="Zip" wizardCaption="{res:Zip}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid1Zip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="30" fieldSourceType="DBColumn" dataType="Text" html="False" name="Phone" fieldSource="Phone" wizardCaption="{res:Phone}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid1Phone">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="31" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Moso" fieldSource="Moso" wizardCaption="{res:Moso}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Moso">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="32" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Tm_Name" fieldSource="Tm_Name" wizardCaption="{res:Tm_Name}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Tm_Name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="33" fieldSourceType="DBColumn" dataType="Text" html="False" name="Mail" fieldSource="Mail" wizardCaption="{res:Mail}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid1Mail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="34" fieldSourceType="DBColumn" dataType="Boolean" html="False" name="State" fieldSource="State" wizardCaption="{res:State}" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid1State">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="35" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Local" fieldSource="Local" wizardCaption="{res:Local}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Local">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="36" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Service" fieldSource="Service" wizardCaption="{res:Service}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Service">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="37" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Brand" fieldSource="Brand" wizardCaption="{res:Brand}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid1Brand">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="38" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="157" variable="s_SiteName" parameterType="URL" dataType="Text" parameterSource="s_SiteName"/>
			</SQLParameters>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<Grid id="39" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="Sites, price_change_status" name="Grid2" orderBy="price_change_id desc" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Grid2} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" activeCollection="TableParameters" parameterTypeListName="ParameterTypeList">
			<Components>
				<Sorter id="40" visible="True" name="Sorter_price_change_id" column="price_change_id" wizardCaption="{res:price_change_id}" wizardSortingType="SimpleDir" wizardControl="price_change_id" wizardAddNbsp="False" PathID="Grid2Sorter_price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="41" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Grid2Sorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="42" visible="True" name="Sorter_Aplication_Date" column="Aplication_Date" wizardCaption="{res:Aplication_Date}" wizardSortingType="SimpleDir" wizardControl="Aplication_Date" wizardAddNbsp="False" PathID="Grid2Sorter_Aplication_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="43" visible="True" name="Sorter_Aplication_Time" column="Aplication_Time" wizardCaption="{res:Aplication_Time}" wizardSortingType="SimpleDir" wizardControl="Aplication_Time" wizardAddNbsp="False" PathID="Grid2Sorter_Aplication_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="44" visible="True" name="Sorter_Reception_Date" column="Reception_Date" wizardCaption="{res:Reception_Date}" wizardSortingType="SimpleDir" wizardControl="Reception_Date" wizardAddNbsp="False" PathID="Grid2Sorter_Reception_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="45" visible="True" name="Sorter_Reception_time" column="Reception_time" wizardCaption="{res:Reception_time}" wizardSortingType="SimpleDir" wizardControl="Reception_time" wizardAddNbsp="False" PathID="Grid2Sorter_Reception_time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="46" visible="True" name="Sorter_File_Delivery_Date" column="File_Delivery_Date" wizardCaption="{res:File_Delivery_Date}" wizardSortingType="SimpleDir" wizardControl="File_Delivery_Date" wizardAddNbsp="False" PathID="Grid2Sorter_File_Delivery_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="47" visible="True" name="Sorter_File_Delivery_Time" column="File_Delivery_Time" wizardCaption="{res:File_Delivery_Time}" wizardSortingType="SimpleDir" wizardControl="File_Delivery_Time" wizardAddNbsp="False" PathID="Grid2Sorter_File_Delivery_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="48" visible="True" name="Sorter_Status" column="Status" wizardCaption="{res:Status}" wizardSortingType="SimpleDir" wizardControl="Status" wizardAddNbsp="False" PathID="Grid2Sorter_Status">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="49" visible="True" name="Sorter_Status_Description" column="Status_Description" wizardCaption="{res:Status_Description}" wizardSortingType="SimpleDir" wizardControl="Status_Description" wizardAddNbsp="False" PathID="Grid2Sorter_Status_Description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="50" fieldSourceType="DBColumn" dataType="Integer" html="False" name="price_change_id" fieldSource="price_change_id" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid2price_change_id" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="send_price.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="63" sourceType="DataField" name="price_change_id" source="price_change_id"/>
					</LinkParameters>
				</Link>
				<Label id="51" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="52" fieldSourceType="DBColumn" dataType="Date" html="False" name="Aplication_Date" fieldSource="Aplication_Date" wizardCaption="{res:Aplication_Date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Aplication_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="53" fieldSourceType="DBColumn" dataType="Text" html="False" name="Aplication_Time" fieldSource="Aplication_Time" wizardCaption="{res:Aplication_Time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Aplication_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="54" fieldSourceType="DBColumn" dataType="Date" html="False" name="Reception_Date" fieldSource="Reception_Date" wizardCaption="{res:Reception_Date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Reception_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="55" fieldSourceType="DBColumn" dataType="Text" html="False" name="Reception_time" fieldSource="Reception_time" wizardCaption="{res:Reception_time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Reception_time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="56" fieldSourceType="DBColumn" dataType="Date" html="False" name="File_Delivery_Date" fieldSource="File_Delivery_Date" wizardCaption="{res:File_Delivery_Date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2File_Delivery_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="57" fieldSourceType="DBColumn" dataType="Text" html="False" name="File_Delivery_Time" fieldSource="File_Delivery_Time" wizardCaption="{res:File_Delivery_Time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2File_Delivery_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="58" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Status" fieldSource="Status" wizardCaption="{res:Status}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid2Status">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="59" fieldSourceType="DBColumn" dataType="Text" html="False" name="Status_Description" fieldSource="Status_Description" wizardCaption="{res:Status_Description}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Status_Description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="60" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
				<Link id="131" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" PathID="Grid2Link1" hrefSource="send_price.ccp" wizardUseTemplateBlock="False" removeParameters="price_change_id;aplication_date;aplication_time">
					<Components/>
					<Events/>
					<LinkParameters>
						<LinkParameter id="133" sourceType="Expression" name="var" source="1"/>
					</LinkParameters>
					<Attributes/>
					<Features/>
				</Link>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="149"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="148" conditionType="Parameter" useIsNull="False" field="price_change_status.ss_id" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="ss_id"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="134" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
				<JoinTable id="135" tableName="price_change_status" schemaName="dbo" posLeft="146" posTop="10" posWidth="140" posHeight="180"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="136" tableLeft="Sites" tableRight="price_change_status" fieldLeft="Sites.ss_id" fieldRight="price_change_status.ss_id" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="138" tableName="price_change_status" fieldName="price_change_id"/>
				<Field id="139" tableName="Sites" fieldName="SiteName"/>
				<Field id="140" tableName="price_change_status" fieldName="aplication_date" alias="Aplication_Date"/>
				<Field id="141" tableName="price_change_status" fieldName="aplication_time" alias="Aplication_Time"/>
				<Field id="142" tableName="price_change_status" fieldName="reception_date" alias="Reception_Date"/>
				<Field id="143" tableName="price_change_status" fieldName="reception_time" alias="Reception_Time"/>
				<Field id="144" tableName="price_change_status" fieldName="file_delivery_date" alias="File_Delivery_Date"/>
				<Field id="145" tableName="price_change_status" fieldName="file_delivery_time" alias="File_Delivery_Time"/>
				<Field id="146" tableName="price_change_status" fieldName="status_code" alias="Status"/>
				<Field id="147" tableName="price_change_status" fieldName="status_description" alias="Status_Description"/>
			</Fields>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="62" variable="ss_id" parameterType="URL" dataType="Text" parameterSource="ss_id"/>
			</SQLParameters>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<Record id="64" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="All" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="FusionHO" name="price_change_status" dataSource="price_change_status" errorSummator="Error" wizardCaption="{res:CCS_RecordFormPrefix} {res:price_change_status} {res:CCS_RecordFormSuffix}" wizardFormMethod="post" PathID="price_change_status" pasteAsReplace="pasteAsReplace" pasteActions="pasteActions">
			<Components>
				<Button id="65" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="{res:CCS_Insert}" PathID="price_change_statusButton_Insert">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="66" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="{res:CCS_Update}" PathID="price_change_statusButton_Update" removeParameters="price_change_id;aplication_date;aplication_time;var" returnPage="send_price.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="67" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="{res:CCS_Delete}" PathID="price_change_statusButton_Delete" removeParameters="price_change_id;aplication_date;aplication_time;var" returnPage="send_price.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="68" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="{res:CCS_Cancel}" PathID="price_change_statusButton_Cancel" removeParameters="price_change_id;aplication_date;aplication_time;var" returnPage="send_price.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<TextBox id="72" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="aplication_date" fieldSource="aplication_date" required="False" caption="{res:aplication_date}" wizardCaption="{res:aplication_date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusaplication_date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="73" name="DatePicker_aplication_date" control="aplication_date" wizardSatellite="True" wizardControl="aplication_date" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css" PathID="price_change_statusDatePicker_aplication_date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="74" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="aplication_time" fieldSource="aplication_time" required="False" caption="{res:aplication_time}" wizardCaption="{res:aplication_time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusaplication_time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="70" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="ss_id" fieldSource="ss_id" required="True" caption="{res:ss_id}" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Hidden id="71" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="price_change_id" fieldSource="price_change_id" required="True" caption="{res:price_change_id}" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusprice_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="121" eventType="Server"/>
					</Actions>
				</Event>
				<Event name="OnLoad" type="Client">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="132" eventType="Client"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="69" conditionType="Parameter" useIsNull="False" field="price_change_id" parameterSource="price_change_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="75" tableName="price_change_status" posLeft="10" posTop="10" posWidth="140" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<EditableGrid id="76" urlType="Relative" secured="False" emptyRows="1" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" sourceType="Table" defaultPageSize="10" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="FusionHO" dataSource="price_send" name="price_send" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:price_send} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAltRecord="False" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" PathID="price_send" deleteControl="CheckBox_Delete" activeCollection="TableParameters" pasteActions="pasteActions">
			<Components>
				<Sorter id="82" visible="True" name="Sorter_price_change_id" column="price_change_id" wizardCaption="{res:price_change_id}" wizardSortingType="SimpleDir" wizardControl="price_change_id" PathID="price_sendSorter_price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="83" visible="True" name="Sorter_ss_id" column="ss_id" wizardCaption="{res:ss_id}" wizardSortingType="SimpleDir" wizardControl="ss_id" PathID="price_sendSorter_ss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="84" visible="True" name="Sorter_grade_id" column="grade_id" wizardCaption="{res:grade_id}" wizardSortingType="SimpleDir" wizardControl="grade_id" PathID="price_sendSorter_grade_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="85" visible="True" name="Sorter_levels" column="levels" wizardCaption="{res:levels}" wizardSortingType="SimpleDir" wizardControl="levels" PathID="price_sendSorter_levels">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="86" visible="True" name="Sorter_ppu_actual" column="ppu_actual" wizardCaption="{res:ppu_actual}" wizardSortingType="SimpleDir" wizardControl="ppu_actual" PathID="price_sendSorter_ppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="87" visible="True" name="Sorter_price_change_type" column="price_change_type" wizardCaption="{res:price_change_type}" wizardSortingType="SimpleDir" wizardControl="price_change_type" PathID="price_sendSorter_price_change_type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="88" visible="True" name="Sorter_new_price" column="new_price" wizardCaption="{res:new_price}" wizardSortingType="SimpleDir" wizardControl="new_price" PathID="price_sendSorter_new_price">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<TextBox id="89" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="price_change_id" fieldSource="price_change_id" required="False" caption="{res:price_change_id}" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendprice_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="90" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="ss_id" fieldSource="ss_id" required="True" caption="{res:ss_id}" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="91" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="grade_id" fieldSource="grade_id" required="True" caption="{res:grade_id}" wizardCaption="{res:grade_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendgrade_id" sourceType="Table" connection="FusionHO" dataSource="Grades" boundColumn="grade_id" textColumn="grade_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<ListBox id="92" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="levels" fieldSource="levels" required="True" caption="{res:levels}" wizardCaption="{res:levels}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendlevels" sourceType="ListOfValues" dataSource="1;1;2;2;3;3;4;4;5;5;6;6;7;7">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="93" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="ppu_actual" fieldSource="ppu_actual" required="False" caption="{res:ppu_actual}" wizardCaption="{res:ppu_actual}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="94" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="price_change_type" fieldSource="price_change_type" required="True" caption="{res:price_change_type}" wizardCaption="{res:price_change_type}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendprice_change_type" sourceType="Table" connection="FusionHO" dataSource="price_type" boundColumn="price_change_type" textColumn="price_change_description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="95" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="new_price" fieldSource="new_price" required="False" caption="{res:new_price}" wizardCaption="{res:new_price}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendnew_price" format="0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<CheckBox id="96" visible="Dynamic" fieldSourceType="CodeExpression" dataType="Boolean" name="CheckBox_Delete" checkedValue="true" uncheckedValue="false" wizardCaption="{res:CCS_Delete}" wizardAddNbsp="True" PathID="price_sendCheckBox_Delete">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</CheckBox>
				<Navigator id="97" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
				<Button id="98" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Submit" operation="Submit" wizardCaption="{res:CCS_Update}" PathID="price_sendButton_Submit">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="99" urlType="Relative" enableValidation="False" isDefault="False" name="Cancel" operation="Cancel" wizardCaption="{res:CCS_Cancel}" PathID="price_sendCancel">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Label id="123" fieldSourceType="DBColumn" dataType="Text" html="False" editable="False" name="RowIDAttribute">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</Label>
				<Label id="125" fieldSourceType="DBColumn" dataType="Text" html="False" editable="False" name="RowNameAttribute">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="127" fieldSourceType="DBColumn" dataType="Text" html="True" editable="False" name="RowStyleAttribute">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="128" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="TextBox1" PathID="price_sendTextBox1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="120" eventType="Server"/>
					</Actions>
				</Event>
				<Event name="BeforeShowRow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="129" eventType="Server"/>
					</Actions>
				</Event>
				<Event name="OnLoad" type="Client">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="130" eventType="Client"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="117" conditionType="Parameter" useIsNull="False" field="price_change_id" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="price_change_id"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="116" tableName="price_send" posLeft="10" posTop="10" posWidth="143" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<PKFields>
				<PKField id="77" tableName="price_send" fieldName="price_send_id" dataType="Integer"/>
				<PKField id="78" tableName="price_send" fieldName="ss_id" dataType="Integer"/>
				<PKField id="79" tableName="price_send" fieldName="grade_id" dataType="Integer"/>
				<PKField id="80" tableName="price_send" fieldName="levels" dataType="Integer"/>
				<PKField id="81" tableName="price_send" fieldName="price_change_type" dataType="Integer"/>
			</PKFields>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</EditableGrid>
		<Grid id="100" secured="False" sourceType="SQL" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="select p.price_change_id, s.SiteName, g.grade_name as Grade, p.levels, p.ppu_actual, t.price_change_description as change_type, p.new_price
from (((price_send as p inner join sites as s on p.ss_id = s.ss_id) inner join grades as g on p.grade_id = g.grade_id) inner join price_type as t on p.price_change_type = t.price_change_type)
where p.price_change_id = '{price_change_id}'" name="Grid3" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Grid3} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" activeCollection="SQLParameters" parameterTypeListName="ParameterTypeList">
			<Components>
				<Sorter id="101" visible="True" name="Sorter_price_change_id" column="price_change_id" wizardCaption="{res:price_change_id}" wizardSortingType="SimpleDir" wizardControl="price_change_id" wizardAddNbsp="False" PathID="Grid3Sorter_price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="102" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Grid3Sorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="103" visible="True" name="Sorter_Grade" column="Grade" wizardCaption="{res:Grade}" wizardSortingType="SimpleDir" wizardControl="Grade" wizardAddNbsp="False" PathID="Grid3Sorter_Grade">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="104" visible="True" name="Sorter_levels" column="levels" wizardCaption="{res:levels}" wizardSortingType="SimpleDir" wizardControl="levels" wizardAddNbsp="False" PathID="Grid3Sorter_levels">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="105" visible="True" name="Sorter_ppu_actual" column="ppu_actual" wizardCaption="{res:ppu_actual}" wizardSortingType="SimpleDir" wizardControl="ppu_actual" wizardAddNbsp="False" PathID="Grid3Sorter_ppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="106" visible="True" name="Sorter_change_type" column="change_type" wizardCaption="{res:change_type}" wizardSortingType="SimpleDir" wizardControl="change_type" wizardAddNbsp="False" PathID="Grid3Sorter_change_type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="107" visible="True" name="Sorter_new_price" column="new_price" wizardCaption="{res:new_price}" wizardSortingType="SimpleDir" wizardControl="new_price" wizardAddNbsp="False" PathID="Grid3Sorter_new_price">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="108" fieldSourceType="DBColumn" dataType="Integer" html="False" name="price_change_id" fieldSource="price_change_id" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="109" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid3SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="110" fieldSourceType="DBColumn" dataType="Text" html="False" name="Grade" fieldSource="Grade" wizardCaption="{res:Grade}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid3Grade">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="111" fieldSourceType="DBColumn" dataType="Integer" html="False" name="levels" fieldSource="levels" wizardCaption="{res:levels}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3levels">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="112" fieldSourceType="DBColumn" dataType="Float" html="False" name="ppu_actual" fieldSource="ppu_actual" wizardCaption="{res:ppu_actual}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3ppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="113" fieldSourceType="DBColumn" dataType="Text" html="False" name="change_type" fieldSource="change_type" wizardCaption="{res:change_type}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid3change_type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="114" fieldSourceType="DBColumn" dataType="Float" html="False" name="new_price" fieldSource="new_price" wizardCaption="{res:new_price}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3new_price">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="115" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="119" eventType="Server"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="122" variable="price_change_id" parameterType="URL" dataType="Text" parameterSource="price_change_id"/>
			</SQLParameters>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<Record id="151" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Search_Sites" actionPage="send_price" errorSummator="Error" wizardFormMethod="post" PathID="Search_Sites" wizardCaption="{res:NewRecord1}" wizardOrientation="Vertical" connection="FusionHO" dataSource="Sites">
			<Components>
				<TextBox id="152" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_SiteName" PathID="Search_Sitess_SiteName" wizardCaption="{res:TextBox1}" features="(assigned)">
					<Components/>
					<Events/>
					<Attributes/>
					<Features>
						<PTAutocomplete id="161" enabled="True" sourceType="Table" name="PTAutocomplete1" servicePage="services/send_price_Search_Sites_s_SiteName_PTAutocomplete1.ccp" category="Prototype" searchField="SiteName" connection="FusionHO" featureNameChanged="No" dataSource="Sites">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables/>
							<JoinLinks/>
							<Fields/>
							<Features/>
						</PTAutocomplete>
					</Features>
				</TextBox>
				<Link id="154" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="send_price.ccp" wizardThemeItem="SorterLink" wizardDefaultValue="{res:CCS_Clear}" PathID="Search_SitesClearParameters">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="155" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" PathID="Search_SitesButton_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="156" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="send_price.asp" forShow="True" url="send_price.asp" comment="'" codePage="windows-1252"/>
		<CodeFile id="Events" language="ASPTemplates" name="send_price_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
		<CodeFile id="PTAutocomplete161" language="ASPTemplates" name="send_priceSearch_Sitess_SiteName_style.css" forShow="False" comment="/*" commentEnd="*/" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
