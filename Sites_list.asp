<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-F39B51FF
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim SitesSearch
Dim Sites
Dim Header
Dim Footer
Dim Sites1
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "Sites_list.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "Sites_list.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Authenticate User @1-78185724
CCSecurityRedirect "1", Empty
'End Authenticate User

'Initialize Objects @1-BD8F098F
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set SitesSearch = new clsRecordSitesSearch
Set Sites = New clsGridSites
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Footer = New clsFooter
Set Footer.Attributes = Attributes
Footer.Initialize "Footer", ""
Set Sites1 = new clsRecordSites1
Sites.Initialize DBFusionHO
Sites1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sites_list_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-D959493D
SitesSearch.Operation
Header.Operations
Footer.Operations
Sites1.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-0293088D
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(SitesSearch, Sites, Header, Footer, Sites1))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-BD9DE439
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set SitesSearch = Nothing
    Set Sites = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Footer.UnloadPage
    Set Footer = Nothing
    Set Sites1 = Nothing
End Sub
'End UnloadPage Sub

Class clsRecordSitesSearch 'SitesSearch Class @2-B2E801CD

'SitesSearch Variables @2-779C7885

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim s_keyword
    Dim Button_DoSearch1
'End SitesSearch Variables

'SitesSearch Class_Initialize Event @2-DA5EA6A4
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "SitesSearch")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "SitesSearch"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set s_keyword = CCCreateControl(ccsTextBox, "s_keyword", Empty, ccsText, Empty, CCGetRequestParam("s_keyword", Method))
        Set Button_DoSearch1 = CCCreateButton("Button_DoSearch1", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_keyword)
    End Sub
'End SitesSearch Class_Initialize Event

'SitesSearch Class_Terminate Event @2-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End SitesSearch Class_Terminate Event

'SitesSearch Validate Method @2-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End SitesSearch Validate Method

'SitesSearch Operation Method @2-99AC20B9
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch1"
            If Button_DoSearch1.Pressed Then
                PressedButton = "Button_DoSearch1"
            End If
        End If
        Redirect = "Sites_list.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch1" Then
                If NOT Button_DoSearch1.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "Sites_list.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch1.x", "Button_DoSearch1.y", "Button_DoSearch1"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End SitesSearch Operation Method

'SitesSearch Show Method @2-D7F6F37C
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "SitesSearch" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_keyword, Button_DoSearch1))
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_keyword.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "SitesSearch" & ":"
            Controls.Show
        End If
    End Sub
'End SitesSearch Show Method

End Class 'End SitesSearch Class @2-A61BA892

Class clsGridSites 'Sites Class @6-49F7C3E2

'Sites Variables @6-B4501408

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sites_Insert
    Dim Sorter_ss_id
    Dim Sorter_SiteName
    Dim Sorter_Address
    Dim Sorter_Moso_name
    Dim Sorter_Latitud
    Dim Sorter_Longitud
    Dim Sorter_Tm_name
    Dim Sorter_Zip
    Dim Sorter_Phone
    Dim Sorter_Mail
    Dim Sorter_Valid
    Dim Sorter_Region_name
    Dim Sorter_Country_name
    Dim Sorter_City_name
    Dim ss_id
    Dim SiteName
    Dim Address
    Dim Moso_name
    Dim Latitud
    Dim Longitud
    Dim Tm_name
    Dim Zip
    Dim Phone
    Dim Mail
    Dim Valid
    Dim Region_name
    Dim Country_name
    Dim City_name
    Dim Navigator
'End Sites Variables

'Sites Class_Initialize Event @6-4F939DAB
    Private Sub Class_Initialize()
        ComponentName = "Sites"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsSitesDataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 20 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("SitesOrder", Empty)
        SortingDirection = CCGetParam("SitesDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sites_Insert = CCCreateControl(ccsLink, "Sites_Insert", Empty, ccsText, Empty, CCGetRequestParam("Sites_Insert", ccsGet))
        Set Sorter_ss_id = CCCreateSorter("Sorter_ss_id", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Address = CCCreateSorter("Sorter_Address", Me, FileName)
        Set Sorter_Moso_name = CCCreateSorter("Sorter_Moso_name", Me, FileName)
        Set Sorter_Latitud = CCCreateSorter("Sorter_Latitud", Me, FileName)
        Set Sorter_Longitud = CCCreateSorter("Sorter_Longitud", Me, FileName)
        Set Sorter_Tm_name = CCCreateSorter("Sorter_Tm_name", Me, FileName)
        Set Sorter_Zip = CCCreateSorter("Sorter_Zip", Me, FileName)
        Set Sorter_Phone = CCCreateSorter("Sorter_Phone", Me, FileName)
        Set Sorter_Mail = CCCreateSorter("Sorter_Mail", Me, FileName)
        Set Sorter_Valid = CCCreateSorter("Sorter_Valid", Me, FileName)
        Set Sorter_Region_name = CCCreateSorter("Sorter_Region_name", Me, FileName)
        Set Sorter_Country_name = CCCreateSorter("Sorter_Country_name", Me, FileName)
        Set Sorter_City_name = CCCreateSorter("Sorter_City_name", Me, FileName)
        Set ss_id = CCCreateControl(ccsLink, "ss_id", Empty, ccsInteger, Empty, CCGetRequestParam("ss_id", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Address = CCCreateControl(ccsLabel, "Address", Empty, ccsText, Empty, CCGetRequestParam("Address", ccsGet))
        Set Moso_name = CCCreateControl(ccsLabel, "Moso_name", Empty, ccsText, Empty, CCGetRequestParam("Moso_name", ccsGet))
        Set Latitud = CCCreateControl(ccsLabel, "Latitud", Empty, ccsFloat, Empty, CCGetRequestParam("Latitud", ccsGet))
        Set Longitud = CCCreateControl(ccsLabel, "Longitud", Empty, ccsFloat, Empty, CCGetRequestParam("Longitud", ccsGet))
        Set Tm_name = CCCreateControl(ccsLabel, "Tm_name", Empty, ccsText, Empty, CCGetRequestParam("Tm_name", ccsGet))
        Set Zip = CCCreateControl(ccsLabel, "Zip", Empty, ccsText, Empty, CCGetRequestParam("Zip", ccsGet))
        Set Phone = CCCreateControl(ccsLabel, "Phone", Empty, ccsText, Empty, CCGetRequestParam("Phone", ccsGet))
        Set Mail = CCCreateControl(ccsLabel, "Mail", Empty, ccsText, Empty, CCGetRequestParam("Mail", ccsGet))
        Set Valid = CCCreateControl(ccsLabel, "Valid", Empty, ccsBoolean, DefaultBooleanFormat, CCGetRequestParam("Valid", ccsGet))
        Set Region_name = CCCreateControl(ccsLabel, "Region_name", Empty, ccsText, Empty, CCGetRequestParam("Region_name", ccsGet))
        Set Country_name = CCCreateControl(ccsLabel, "Country_name", Empty, ccsText, Empty, CCGetRequestParam("Country_name", ccsGet))
        Set City_name = CCCreateControl(ccsLabel, "City_name", Empty, ccsText, Empty, CCGetRequestParam("City_name", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpSimple)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Sites Class_Initialize Event

'Sites Initialize Method @6-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Sites Initialize Method

'Sites Class_Terminate Event @6-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites Class_Terminate Event

'Sites Show Method @6-07023919
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urls_keyword") = CCGetRequestParam("s_keyword", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sites_Insert, Sorter_ss_id, Sorter_SiteName, Sorter_Address, Sorter_Moso_name, Sorter_Latitud, Sorter_Longitud, Sorter_Tm_name, Sorter_Zip, Sorter_Phone, Sorter_Mail, Sorter_Valid, Sorter_Region_name, Sorter_Country_name, Sorter_City_name, Navigator))
            
            Sites_Insert.Parameters = CCGetQueryString("QueryString", Array("ss_id", "ccsForm"))
            Sites_Insert.Parameters = CCAddParam(Sites_Insert.Parameters, "var", 1)
            Sites_Insert.Page = "Sites_list.asp"
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(ss_id, SiteName, Address, Moso_name, Latitud, Longitud, Tm_name, Zip, Phone, Mail, Valid, Region_name, Country_name, City_name))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Sites:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    ss_id.Value = Recordset.Fields("ss_id")
                    ss_id.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
                    ss_id.Parameters = CCAddParam(ss_id.Parameters, "ss_id", Recordset.Fields("ss_id_param1"))
                    ss_id.Page = "Sites_list.asp"
                    SiteName.Value = Recordset.Fields("SiteName")
                    Address.Value = Recordset.Fields("Address")
                    Moso_name.Value = Recordset.Fields("Moso_name")
                    Latitud.Value = Recordset.Fields("Latitud")
                    Longitud.Value = Recordset.Fields("Longitud")
                    Tm_name.Value = Recordset.Fields("Tm_name")
                    Zip.Value = Recordset.Fields("Zip")
                    Phone.Value = Recordset.Fields("Phone")
                    Mail.Value = Recordset.Fields("Mail")
                    Valid.Value = Recordset.Fields("Valid")
                    Region_name.Value = Recordset.Fields("Region_name")
                    Country_name.Value = Recordset.Fields("Country_name")
                    City_name.Value = Recordset.Fields("City_name")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Sites:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Sites:"
            StaticControls.Show
        End If

    End Sub
'End Sites Show Method

'Sites PageSize Property Let @6-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Sites PageSize Property Let

'Sites PageSize Property Get @6-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Sites PageSize Property Get

'Sites RowNumber Property Get @6-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Sites RowNumber Property Get

'Sites HasNextRow Function @6-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Sites HasNextRow Function

End Class 'End Sites Class @6-A61BA892

Class clsSitesDataSource 'SitesDataSource Class @6-DCC7012B

'DataSource Variables @6-0E95E122
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public ss_id
    Public ss_id_param1
    Public SiteName
    Public Address
    Public Moso_name
    Public Latitud
    Public Longitud
    Public Tm_name
    Public Zip
    Public Phone
    Public Mail
    Public Valid
    Public Region_name
    Public Country_name
    Public City_name
'End DataSource Variables

'DataSource Class_Initialize Event @6-D30F18A5
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set ss_id_param1 = CCCreateField("ss_id_param1", "ss_id", ccsText, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Address = CCCreateField("Address", "Expr1", ccsText, Empty, Recordset)
        Set Moso_name = CCCreateField("Moso_name", "Moso_name", ccsText, Empty, Recordset)
        Set Latitud = CCCreateField("Latitud", "Latitud", ccsFloat, Empty, Recordset)
        Set Longitud = CCCreateField("Longitud", "Longitud", ccsFloat, Empty, Recordset)
        Set Tm_name = CCCreateField("Tm_name", "Tm_name", ccsText, Empty, Recordset)
        Set Zip = CCCreateField("Zip", "Zip", ccsText, Empty, Recordset)
        Set Phone = CCCreateField("Phone", "Phone", ccsText, Empty, Recordset)
        Set Mail = CCCreateField("Mail", "Mail", ccsText, Empty, Recordset)
        Set Valid = CCCreateField("Valid", "Sites_Valid", ccsBoolean, Array(1, 0, Empty), Recordset)
        Set Region_name = CCCreateField("Region_name", "Region_name", ccsText, Empty, Recordset)
        Set Country_name = CCCreateField("Country_name", "Country_name", ccsText, Empty, Recordset)
        Set City_name = CCCreateField("City_name", "City_name", ccsText, Empty, Recordset)
        Fields.AddFields Array(ss_id,  ss_id_param1,  SiteName,  Address,  Moso_name,  Latitud,  Longitud, _
             Tm_name,  Zip,  Phone,  Mail,  Valid,  Region_name,  Country_name,  City_name)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_ss_id", "ss_id", ""), _
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Address", "Address", ""), _
            Array("Sorter_Moso_name", "Moso_name", ""), _
            Array("Sorter_Latitud", "Latitud", ""), _
            Array("Sorter_Longitud", "Longitud", ""), _
            Array("Sorter_Tm_name", "Tm_name", ""), _
            Array("Sorter_Zip", "Zip", ""), _
            Array("Sorter_Phone", "Phone", ""), _
            Array("Sorter_Mail", "Mail", ""), _
            Array("Sorter_Valid", "Sites.Valid", ""), _
            Array("Sorter_Region_name", "Region_name", ""), _
            Array("Sorter_Country_name", "Country_name", ""), _
            Array("Sorter_City_name", "City_name", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} ss_id, SiteName, Address AS Expr1, Moso_name, Latitud, Longitud, Tm_name, Zip, Phone, Sites.Mail, Sites.Valid AS Sites_Valid, " & vbLf & _
        "Region.Region_name, Country.Country_name, City.City_name  " & vbLf & _
        "FROM ((((Sites LEFT JOIN Moso ON " & vbLf & _
        "Sites.Moso_id = Moso.Moso_id) LEFT JOIN Tm ON " & vbLf & _
        "Sites.Tm_id = Tm.Tm_id) LEFT JOIN Region ON " & vbLf & _
        "Sites.Region_id = Region.Region_id) LEFT JOIN Country ON " & vbLf & _
        "Sites.Country_id = Country.Country_id) LEFT JOIN City ON " & vbLf & _
        "Sites.City_id = City.City_id {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM ((((Sites LEFT JOIN Moso ON " & vbLf & _
        "Sites.Moso_id = Moso.Moso_id) LEFT JOIN Tm ON " & vbLf & _
        "Sites.Tm_id = Tm.Tm_id) LEFT JOIN Region ON " & vbLf & _
        "Sites.Region_id = Region.Region_id) LEFT JOIN Country ON " & vbLf & _
        "Sites.Country_id = Country.Country_id) LEFT JOIN City ON " & vbLf & _
        "Sites.City_id = City.City_id"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @6-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @6-7FCD8770
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urls_keyword", ccsText, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opContains, False, "[Sites].[SiteName]", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @6-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @6-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End SitesDataSource Class @6-A61BA892

Class clsRecordSites1 'Sites1 Class @73-1D7DBBED

'Sites1 Variables @73-9B264902

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim SiteName
    Dim Address
    Dim Latitud
    Dim Longitud
    Dim Tm_id
    Dim Zip
    Dim Phone
    Dim Mail
    Dim Valid
    Dim Country_id
    Dim City_id
    Dim ss_id
    Dim Service_id
    Dim Brand_id
    Dim Local_type_id
    Dim Region_id
    Dim Moso
    Dim Button_Insert1
    Dim Button_Update
    Dim Button_Delete
    Dim Button_Cancel
'End Sites1 Variables

'Sites1 Class_Initialize Event @73-26E21E01
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsSites1DataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Sites1")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Sites1"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set SiteName = CCCreateControl(ccsTextBox, "SiteName", "Name", ccsText, Empty, CCGetRequestParam("SiteName", Method))
        Set Address = CCCreateControl(ccsTextBox, "Address", "Address", ccsText, Empty, CCGetRequestParam("Address", Method))
        Set Latitud = CCCreateControl(ccsTextBox, "Latitud", "Latitud", ccsFloat, Empty, CCGetRequestParam("Latitud", Method))
        Set Longitud = CCCreateControl(ccsTextBox, "Longitud", "Longitud", ccsFloat, Empty, CCGetRequestParam("Longitud", Method))
        Set Tm_id = CCCreateList(ccsListBox, "Tm_id", "Tm Id", ccsInteger, CCGetRequestParam("Tm_id", Method), Empty)
        Tm_id.BoundColumn = "Tm_id"
        Tm_id.TextColumn = "Tm_name"
        Set Tm_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Tm {SQL_Where} {SQL_OrderBy}", "", ""))
        With Tm_id.DataSource.WhereParameters
            Set .ParameterSources = Server.CreateObject("Scripting.Dictionary")
            .ParameterSources("expr79") = True
            .AddParameter 1, "expr79", ccsBoolean, Array("True", "False", Empty), Array(1, 0, Empty), Empty, False
            .Criterion(1) = .Operation(opEqual, False, "[Valid]", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
        End With
        Tm_id.DataSource.Where = Tm_id.DataSource.WhereParameters.AssembledWhere
        Set Zip = CCCreateControl(ccsTextBox, "Zip", "Zip", ccsText, Empty, CCGetRequestParam("Zip", Method))
        Set Phone = CCCreateControl(ccsTextBox, "Phone", "Phone", ccsText, Empty, CCGetRequestParam("Phone", Method))
        Set Mail = CCCreateControl(ccsTextBox, "Mail", "Mail", ccsText, Empty, CCGetRequestParam("Mail", Method))
        Set Valid = CCCreateControl(ccsCheckBox, "Valid", Empty, ccsBoolean, DefaultBooleanFormat, CCGetRequestParam("Valid", Method))
        Set Country_id = CCCreateList(ccsListBox, "Country_id", "Country Id", ccsInteger, CCGetRequestParam("Country_id", Method), Empty)
        Country_id.BoundColumn = "Country_id"
        Country_id.TextColumn = "Country_name"
        Set Country_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Country {SQL_Where} {SQL_OrderBy}", "", ""))
        Set City_id = CCCreateList(ccsListBox, "City_id", "City Id", ccsInteger, CCGetRequestParam("City_id", Method), Empty)
        City_id.BoundColumn = "City_id"
        City_id.TextColumn = "City_name"
        Set City_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM City {SQL_Where} {SQL_OrderBy}", "", ""))
        Set ss_id = CCCreateControl(ccsTextBox, "ss_id", Empty, ccsText, Empty, CCGetRequestParam("ss_id", Method))
        Set Service_id = CCCreateList(ccsListBox, "Service_id", "Service Id", ccsInteger, CCGetRequestParam("Service_id", Method), Empty)
        Service_id.BoundColumn = "Service_id"
        Service_id.TextColumn = "Service_name"
        Set Service_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Service {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Brand_id = CCCreateList(ccsListBox, "Brand_id", CCSLocales.GetText("Brand", ""), ccsInteger, CCGetRequestParam("Brand_id", Method), Empty)
        Brand_id.BoundColumn = "Brand_id"
        Brand_id.TextColumn = "Brand_name"
        Set Brand_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Brand {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Local_type_id = CCCreateList(ccsListBox, "Local_type_id", "Local_type Id", ccsInteger, CCGetRequestParam("Local_type_id", Method), Empty)
        Local_type_id.BoundColumn = "Local_type_id"
        Local_type_id.TextColumn = "Local_type_name"
        Set Local_type_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Local_type {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Region_id = CCCreateList(ccsListBox, "Region_id", "Region Id", ccsInteger, CCGetRequestParam("Region_id", Method), Empty)
        Region_id.BoundColumn = "Region_id"
        Region_id.TextColumn = "Region_name"
        Set Region_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Region {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Moso = CCCreateList(ccsListBox, "Moso", Empty, ccsFloat, CCGetRequestParam("Moso", Method), Empty)
        Moso.BoundColumn = "Moso_id"
        Moso.TextColumn = "Moso_name"
        Set Moso.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Moso {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Button_Insert1 = CCCreateButton("Button_Insert1", Method)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Delete = CCCreateButton("Button_Delete", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(SiteName, Address, Latitud, Longitud, Tm_id, Zip, Phone, _
             Mail, Valid, Country_id, City_id, ss_id, Service_id, Brand_id, Local_type_id, Region_id, Moso)
    End Sub
'End Sites1 Class_Initialize Event

'Sites1 Initialize Method @73-AFFD1FD2
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
            .Parameters("urlss_id") = CCGetRequestParam("ss_id", ccsGET)
        End With
    End Sub
'End Sites1 Initialize Method

'Sites1 Class_Terminate Event @73-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites1 Class_Terminate Event

'Sites1 Validate Method @73-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Sites1 Validate Method

'Sites1 Operation Method @73-05A6D508
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Insert1")
            If Button_Insert1.Pressed Then
                PressedButton = "Button_Insert1"
            ElseIf Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Delete.Pressed Then
                PressedButton = "Button_Delete"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = "Sites_list.asp"
        If PressedButton = "Button_Delete" Then
            If NOT Button_Delete.OnClick OR NOT DeleteRow() Then
                Redirect = ""
            End If
        ElseIf PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Insert1" Then
                If NOT Button_Insert1.OnClick() OR NOT InsertRow() Then
                    Redirect = ""
                End If
            ElseIf PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Sites1 Operation Method

'Sites1 InsertRow Method @73-DFB28F46
    Function InsertRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInsert", Me)
        If NOT InsertAllowed Then InsertRow = False : Exit Function
        DataSource.SiteName.Value = SiteName.Value
        DataSource.Address.Value = Address.Value
        DataSource.Latitud.Value = Latitud.Value
        DataSource.Longitud.Value = Longitud.Value
        DataSource.Tm_id.Value = Tm_id.Value
        DataSource.Zip.Value = Zip.Value
        DataSource.Phone.Value = Phone.Value
        DataSource.Mail.Value = Mail.Value
        DataSource.Valid.Value = Valid.Value
        DataSource.Country_id.Value = Country_id.Value
        DataSource.City_id.Value = City_id.Value
        DataSource.ss_id.Value = ss_id.Value
        DataSource.Service_id.Value = Service_id.Value
        DataSource.Brand_id.Value = Brand_id.Value
        DataSource.Local_type_id.Value = Local_type_id.Value
        DataSource.Region_id.Value = Region_id.Value
        DataSource.Moso.Value = Moso.Value
        DataSource.Insert(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInsert", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        InsertRow = (Errors.Count = 0)
    End Function
'End Sites1 InsertRow Method

'Sites1 UpdateRow Method @73-6C1C5273
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        DataSource.SiteName.Value = SiteName.Value
        DataSource.Address.Value = Address.Value
        DataSource.Latitud.Value = Latitud.Value
        DataSource.Longitud.Value = Longitud.Value
        DataSource.Tm_id.Value = Tm_id.Value
        DataSource.Zip.Value = Zip.Value
        DataSource.Phone.Value = Phone.Value
        DataSource.Mail.Value = Mail.Value
        DataSource.Valid.Value = Valid.Value
        DataSource.Country_id.Value = Country_id.Value
        DataSource.City_id.Value = City_id.Value
        DataSource.ss_id.Value = ss_id.Value
        DataSource.Service_id.Value = Service_id.Value
        DataSource.Brand_id.Value = Brand_id.Value
        DataSource.Local_type_id.Value = Local_type_id.Value
        DataSource.Region_id.Value = Region_id.Value
        DataSource.Moso.Value = Moso.Value
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End Sites1 UpdateRow Method

'Sites1 DeleteRow Method @73-D5C1DF24
    Function DeleteRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeDelete", Me)
        If NOT DeleteAllowed Then DeleteRow = False : Exit Function
        DataSource.Delete(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterDelete", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        DeleteRow = (Errors.Count = 0)
    End Function
'End Sites1 DeleteRow Method

'Sites1 Show Method @73-27192701
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Sites1" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(ss_id,  SiteName,  Address,  Moso,  Latitud,  Longitud,  Tm_id, _
                 Zip,  Phone,  Mail,  Valid,  Local_type_id,  Brand_id,  Service_id,  Country_id, _
                 Region_id,  City_id,  Button_Insert1,  Button_Update,  Button_Delete,  Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        SiteName.Value = Recordset.Fields("SiteName")
                        Address.Value = Recordset.Fields("Address")
                        Latitud.Value = Recordset.Fields("Latitud")
                        Longitud.Value = Recordset.Fields("Longitud")
                        Tm_id.Value = Recordset.Fields("Tm_id")
                        Zip.Value = Recordset.Fields("Zip")
                        Phone.Value = Recordset.Fields("Phone")
                        Mail.Value = Recordset.Fields("Mail")
                        Valid.Value = Recordset.Fields("Valid")
                        Country_id.Value = Recordset.Fields("Country_id")
                        City_id.Value = Recordset.Fields("City_id")
                        ss_id.Value = Recordset.Fields("ss_id")
                        Service_id.Value = Recordset.Fields("Service_id")
                        Brand_id.Value = Recordset.Fields("Brand_id")
                        Local_type_id.Value = Recordset.Fields("Local_type_id")
                        Region_id.Value = Recordset.Fields("Region_id")
                        Moso.Value = Recordset.Fields("Moso")
                    End If
                Else
                    EditMode = False
                End If
            End If
        End If
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors SiteName.Errors
            Errors.AddErrors Address.Errors
            Errors.AddErrors Latitud.Errors
            Errors.AddErrors Longitud.Errors
            Errors.AddErrors Tm_id.Errors
            Errors.AddErrors Zip.Errors
            Errors.AddErrors Phone.Errors
            Errors.AddErrors Mail.Errors
            Errors.AddErrors Valid.Errors
            Errors.AddErrors Country_id.Errors
            Errors.AddErrors City_id.Errors
            Errors.AddErrors ss_id.Errors
            Errors.AddErrors Service_id.Errors
            Errors.AddErrors Brand_id.Errors
            Errors.AddErrors Local_type_id.Errors
            Errors.AddErrors Region_id.Errors
            Errors.AddErrors Moso.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Insert1.Visible = NOT EditMode AND InsertAllowed
        Button_Update.Visible = EditMode AND UpdateAllowed
        Button_Delete.Visible = EditMode AND DeleteAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Sites1" & ":"
            Controls.Show
        End If
    End Sub
'End Sites1 Show Method

End Class 'End Sites1 Class @73-A61BA892

Class clsSites1DataSource 'Sites1DataSource Class @73-07CAE0B9

'DataSource Variables @73-04B86469
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public SiteName
    Public Address
    Public Latitud
    Public Longitud
    Public Tm_id
    Public Zip
    Public Phone
    Public Mail
    Public Valid
    Public Country_id
    Public City_id
    Public ss_id
    Public Service_id
    Public Brand_id
    Public Local_type_id
    Public Region_id
    Public Moso
'End DataSource Variables

'DataSource Class_Initialize Event @73-F2A48ECE
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Address = CCCreateField("Address", "Address", ccsText, Empty, Recordset)
        Set Latitud = CCCreateField("Latitud", "Latitud", ccsFloat, Empty, Recordset)
        Set Longitud = CCCreateField("Longitud", "Longitud", ccsFloat, Empty, Recordset)
        Set Tm_id = CCCreateField("Tm_id", "Tm_id", ccsInteger, Empty, Recordset)
        Set Zip = CCCreateField("Zip", "Zip", ccsText, Empty, Recordset)
        Set Phone = CCCreateField("Phone", "Phone", ccsText, Empty, Recordset)
        Set Mail = CCCreateField("Mail", "Mail", ccsText, Empty, Recordset)
        Set Valid = CCCreateField("Valid", "Valid", ccsBoolean, Array(1, 0, Empty), Recordset)
        Set Country_id = CCCreateField("Country_id", "Country_id", ccsInteger, Empty, Recordset)
        Set City_id = CCCreateField("City_id", "City_id", ccsInteger, Empty, Recordset)
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsText, Empty, Recordset)
        Set Service_id = CCCreateField("Service_id", "Service_id", ccsInteger, Empty, Recordset)
        Set Brand_id = CCCreateField("Brand_id", "Brand_id", ccsInteger, Empty, Recordset)
        Set Local_type_id = CCCreateField("Local_type_id", "Local_type_id", ccsInteger, Empty, Recordset)
        Set Region_id = CCCreateField("Region_id", "Region_id", ccsInteger, Empty, Recordset)
        Set Moso = CCCreateField("Moso", "Moso_id", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(SiteName,  Address,  Latitud,  Longitud,  Tm_id,  Zip,  Phone, _
             Mail,  Valid,  Country_id,  City_id,  ss_id,  Service_id,  Brand_id,  Local_type_id,  Region_id,  Moso)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "SiteName", True
        InsertOmitIfEmpty.Add "Address", True
        InsertOmitIfEmpty.Add "Latitud", True
        InsertOmitIfEmpty.Add "Longitud", True
        InsertOmitIfEmpty.Add "Tm_id", True
        InsertOmitIfEmpty.Add "Zip", True
        InsertOmitIfEmpty.Add "Phone", True
        InsertOmitIfEmpty.Add "Mail", True
        InsertOmitIfEmpty.Add "Valid", False
        InsertOmitIfEmpty.Add "Country_id", True
        InsertOmitIfEmpty.Add "City_id", True
        InsertOmitIfEmpty.Add "ss_id", True
        InsertOmitIfEmpty.Add "Service_id", True
        InsertOmitIfEmpty.Add "Brand_id", True
        InsertOmitIfEmpty.Add "Local_type_id", True
        InsertOmitIfEmpty.Add "Region_id", True
        InsertOmitIfEmpty.Add "Moso", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "SiteName", True
        UpdateOmitIfEmpty.Add "Address", True
        UpdateOmitIfEmpty.Add "Latitud", True
        UpdateOmitIfEmpty.Add "Longitud", True
        UpdateOmitIfEmpty.Add "Tm_id", True
        UpdateOmitIfEmpty.Add "Zip", True
        UpdateOmitIfEmpty.Add "Phone", True
        UpdateOmitIfEmpty.Add "Mail", True
        UpdateOmitIfEmpty.Add "Valid", False
        UpdateOmitIfEmpty.Add "Country_id", True
        UpdateOmitIfEmpty.Add "City_id", True
        UpdateOmitIfEmpty.Add "ss_id", True
        UpdateOmitIfEmpty.Add "Service_id", True
        UpdateOmitIfEmpty.Add "Brand_id", True
        UpdateOmitIfEmpty.Add "Local_type_id", True
        UpdateOmitIfEmpty.Add "Region_id", True
        UpdateOmitIfEmpty.Add "Moso", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT *  " & vbLf & _
        "FROM Sites {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @73-A3278524
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlss_id", ccsInteger, Empty, Empty, Empty, False
            AllParamsSet = .AllParamsSet
            .Criterion(1) = .Operation(opEqual, False, "ss_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @73-48A2DA7D
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @73-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @73-5F624625
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM [Sites]" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @73-0855A908
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_SiteName : IsDef_SiteName = CCIsDefined("SiteName", "Form")
        Dim IsDef_Address : IsDef_Address = CCIsDefined("Address", "Form")
        Dim IsDef_Latitud : IsDef_Latitud = CCIsDefined("Latitud", "Form")
        Dim IsDef_Longitud : IsDef_Longitud = CCIsDefined("Longitud", "Form")
        Dim IsDef_Tm_id : IsDef_Tm_id = CCIsDefined("Tm_id", "Form")
        Dim IsDef_Zip : IsDef_Zip = CCIsDefined("Zip", "Form")
        Dim IsDef_Phone : IsDef_Phone = CCIsDefined("Phone", "Form")
        Dim IsDef_Mail : IsDef_Mail = CCIsDefined("Mail", "Form")
        Dim IsDef_Valid : IsDef_Valid = CCIsDefined("Valid", "Form")
        Dim IsDef_Country_id : IsDef_Country_id = CCIsDefined("Country_id", "Form")
        Dim IsDef_City_id : IsDef_City_id = CCIsDefined("City_id", "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id", "Form")
        Dim IsDef_Service_id : IsDef_Service_id = CCIsDefined("Service_id", "Form")
        Dim IsDef_Brand_id : IsDef_Brand_id = CCIsDefined("Brand_id", "Form")
        Dim IsDef_Local_type_id : IsDef_Local_type_id = CCIsDefined("Local_type_id", "Form")
        Dim IsDef_Region_id : IsDef_Region_id = CCIsDefined("Region_id", "Form")
        Dim IsDef_Moso : IsDef_Moso = CCIsDefined("Moso", "Form")
        If Not UpdateOmitIfEmpty("SiteName") Or IsDef_SiteName Then Cmd.AddSQLStrings "[SiteName]=" & Connection.ToSQL(SiteName, SiteName.DataType), Empty
        If Not UpdateOmitIfEmpty("Address") Or IsDef_Address Then Cmd.AddSQLStrings "[Address]=" & Connection.ToSQL(Address, Address.DataType), Empty
        If Not UpdateOmitIfEmpty("Latitud") Or IsDef_Latitud Then Cmd.AddSQLStrings "[Latitud]=" & Connection.ToSQL(Latitud, Latitud.DataType), Empty
        If Not UpdateOmitIfEmpty("Longitud") Or IsDef_Longitud Then Cmd.AddSQLStrings "[Longitud]=" & Connection.ToSQL(Longitud, Longitud.DataType), Empty
        If Not UpdateOmitIfEmpty("Tm_id") Or IsDef_Tm_id Then Cmd.AddSQLStrings "[Tm_id]=" & Connection.ToSQL(Tm_id, Tm_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Zip") Or IsDef_Zip Then Cmd.AddSQLStrings "[Zip]=" & Connection.ToSQL(Zip, Zip.DataType), Empty
        If Not UpdateOmitIfEmpty("Phone") Or IsDef_Phone Then Cmd.AddSQLStrings "[Phone]=" & Connection.ToSQL(Phone, Phone.DataType), Empty
        If Not UpdateOmitIfEmpty("Mail") Or IsDef_Mail Then Cmd.AddSQLStrings "[Mail]=" & Connection.ToSQL(Mail, Mail.DataType), Empty
        If Not UpdateOmitIfEmpty("Valid") Or IsDef_Valid Then Cmd.AddSQLStrings "[Valid]=" & Connection.ToSQL(Valid, Valid.DataType), Empty
        If Not UpdateOmitIfEmpty("Country_id") Or IsDef_Country_id Then Cmd.AddSQLStrings "[Country_id]=" & Connection.ToSQL(Country_id, Country_id.DataType), Empty
        If Not UpdateOmitIfEmpty("City_id") Or IsDef_City_id Then Cmd.AddSQLStrings "[City_id]=" & Connection.ToSQL(City_id, City_id.DataType), Empty
        If Not UpdateOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id=" & Connection.ToSQL(ss_id, ss_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Service_id") Or IsDef_Service_id Then Cmd.AddSQLStrings "[Service_id]=" & Connection.ToSQL(Service_id, Service_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Brand_id") Or IsDef_Brand_id Then Cmd.AddSQLStrings "[Brand_id]=" & Connection.ToSQL(Brand_id, Brand_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Local_type_id") Or IsDef_Local_type_id Then Cmd.AddSQLStrings "[Local_type_id]=" & Connection.ToSQL(Local_type_id, Local_type_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Region_id") Or IsDef_Region_id Then Cmd.AddSQLStrings "[Region_id]=" & Connection.ToSQL(Region_id, Region_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Moso") Or IsDef_Moso Then Cmd.AddSQLStrings "[Moso_id]=" & Connection.ToSQL(Moso, Moso.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "[Sites]", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @73-1875E5FC
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_SiteName : IsDef_SiteName = CCIsDefined("SiteName", "Form")
        Dim IsDef_Address : IsDef_Address = CCIsDefined("Address", "Form")
        Dim IsDef_Latitud : IsDef_Latitud = CCIsDefined("Latitud", "Form")
        Dim IsDef_Longitud : IsDef_Longitud = CCIsDefined("Longitud", "Form")
        Dim IsDef_Tm_id : IsDef_Tm_id = CCIsDefined("Tm_id", "Form")
        Dim IsDef_Zip : IsDef_Zip = CCIsDefined("Zip", "Form")
        Dim IsDef_Phone : IsDef_Phone = CCIsDefined("Phone", "Form")
        Dim IsDef_Mail : IsDef_Mail = CCIsDefined("Mail", "Form")
        Dim IsDef_Valid : IsDef_Valid = CCIsDefined("Valid", "Form")
        Dim IsDef_Country_id : IsDef_Country_id = CCIsDefined("Country_id", "Form")
        Dim IsDef_City_id : IsDef_City_id = CCIsDefined("City_id", "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id", "Form")
        Dim IsDef_Service_id : IsDef_Service_id = CCIsDefined("Service_id", "Form")
        Dim IsDef_Brand_id : IsDef_Brand_id = CCIsDefined("Brand_id", "Form")
        Dim IsDef_Local_type_id : IsDef_Local_type_id = CCIsDefined("Local_type_id", "Form")
        Dim IsDef_Region_id : IsDef_Region_id = CCIsDefined("Region_id", "Form")
        Dim IsDef_Moso : IsDef_Moso = CCIsDefined("Moso", "Form")
        If Not InsertOmitIfEmpty("SiteName") Or IsDef_SiteName Then Cmd.AddSQLStrings "[SiteName]", Connection.ToSQL(SiteName, SiteName.DataType)
        If Not InsertOmitIfEmpty("Address") Or IsDef_Address Then Cmd.AddSQLStrings "[Address]", Connection.ToSQL(Address, Address.DataType)
        If Not InsertOmitIfEmpty("Latitud") Or IsDef_Latitud Then Cmd.AddSQLStrings "[Latitud]", Connection.ToSQL(Latitud, Latitud.DataType)
        If Not InsertOmitIfEmpty("Longitud") Or IsDef_Longitud Then Cmd.AddSQLStrings "[Longitud]", Connection.ToSQL(Longitud, Longitud.DataType)
        If Not InsertOmitIfEmpty("Tm_id") Or IsDef_Tm_id Then Cmd.AddSQLStrings "[Tm_id]", Connection.ToSQL(Tm_id, Tm_id.DataType)
        If Not InsertOmitIfEmpty("Zip") Or IsDef_Zip Then Cmd.AddSQLStrings "[Zip]", Connection.ToSQL(Zip, Zip.DataType)
        If Not InsertOmitIfEmpty("Phone") Or IsDef_Phone Then Cmd.AddSQLStrings "[Phone]", Connection.ToSQL(Phone, Phone.DataType)
        If Not InsertOmitIfEmpty("Mail") Or IsDef_Mail Then Cmd.AddSQLStrings "[Mail]", Connection.ToSQL(Mail, Mail.DataType)
        If Not InsertOmitIfEmpty("Valid") Or IsDef_Valid Then Cmd.AddSQLStrings "[Valid]", Connection.ToSQL(Valid, Valid.DataType)
        If Not InsertOmitIfEmpty("Country_id") Or IsDef_Country_id Then Cmd.AddSQLStrings "[Country_id]", Connection.ToSQL(Country_id, Country_id.DataType)
        If Not InsertOmitIfEmpty("City_id") Or IsDef_City_id Then Cmd.AddSQLStrings "[City_id]", Connection.ToSQL(City_id, City_id.DataType)
        If Not InsertOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id", Connection.ToSQL(ss_id, ss_id.DataType)
        If Not InsertOmitIfEmpty("Service_id") Or IsDef_Service_id Then Cmd.AddSQLStrings "[Service_id]", Connection.ToSQL(Service_id, Service_id.DataType)
        If Not InsertOmitIfEmpty("Brand_id") Or IsDef_Brand_id Then Cmd.AddSQLStrings "[Brand_id]", Connection.ToSQL(Brand_id, Brand_id.DataType)
        If Not InsertOmitIfEmpty("Local_type_id") Or IsDef_Local_type_id Then Cmd.AddSQLStrings "[Local_type_id]", Connection.ToSQL(Local_type_id, Local_type_id.DataType)
        If Not InsertOmitIfEmpty("Region_id") Or IsDef_Region_id Then Cmd.AddSQLStrings "[Region_id]", Connection.ToSQL(Region_id, Region_id.DataType)
        If Not InsertOmitIfEmpty("Moso") Or IsDef_Moso Then Cmd.AddSQLStrings "[Moso_id]", Connection.ToSQL(Moso, Moso.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "[Sites]", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End Sites1DataSource Class @73-A61BA892

'Include Page Implementation @70-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation

'Include Page Implementation @71-79649078
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Footer.asp" -->
<%
'End Include Page Implementation


%>
