<%
'BindEvents Method @1-7C8C0B75
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeShow") = GetRef("Page_BeforeShow")
    Else
    End If
End Sub
'End BindEvents Method

Function Page_BeforeShow(Sender) 'Page_BeforeShow @1-A1547E8B

'Custom Code @2-73254650
' -------------------------
' Write your own code here.
' -------------------------
'End Custom Code
dim FusionHO
Dim SQL
Dim SQL2
Dim SQL3
Dim rsGroups
dim rGroups
Dim strGroups
Dim strXML

dim strCategories, strtank1, strtank2, strtank3, strtank4
dim i
dim var
dim var1
dim var2
dim b
dim fecha
dim k
dim cant_tanks
dim j
dim aux

b = false
strGroups = ""
strXML = ""
Set FusionHO = Server.CreateObject("ADODB.Connection") 
FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
var = ""
var = CCGetRequestParam("site", ccsGet)
var2 = CCGetRequestParam("prod", ccsGet)

var1 = CCGetRequestParam("date", ccsGet)
fecha = mid(var1,7,4)&"-"& mid(var1,1,2)&"-"& mid(var1,4,2)


SQL = "select  Grade as tank, Tank_Time as hour, round(sum(Volume),0,1) as vol from Grade_table where tank_DATE ='"&fecha&"' and Site_Name = '"&var&"' group by site_Name, Grade, tank_time"
SQL2 = "select count(distinct(g.grade_name)) as cantidad from (grades as g inner join tank_actual_info as t on g.grade_id= t.grade_id) inner join Sites as s on t.ss_id= s.ss_id where s.sitename = '"&var&"'"
SQL3 = "select distinct(g.grade_name) as productos from (grades as g inner join tank_actual_info as t on g.grade_id= t.grade_id) inner join Sites as s on t.ss_id= s.ss_id where s.sitename = '"&var&"'"


Set rGroups = FusionHO.Execute(SQL2)
While not rGroups.EOF
	cant_tanks = rGroups("cantidad")
rGroups.MoveNext 
wend
 

dim algo																																																												
dim vec()
Redim vec(cant_tanks)
dim grades()
Redim grades(cant_tanks)
dim color(16) 
color(1) = "AF00F8"
color(2) = "F6000F"
color(3) = "AFBBF8"
color(4) = "FBFF00"
color(5) = "C75045"
color(6) = "00CC00"
color(7) = "FF6600"
color(8) = "0066B3"
color(9) = "8FB200"
color(10) = "80FE80"
color(11) = "B24700"
color(12) = "0033CC"
color(13) = "007D47"
color(14) = "9CD4A2"
color(15) = "26C7E3"
color(16) = "FFB300"

k=1
Set rGroups = FusionHO.Execute(SQL3)
While not rGroups.EOF
	grades(k) = rGroups("productos")
	k = k + 1
rGroups.MoveNext 
wend



Dim arrData(24,5)
	
For i=0 to UBound(arrData)-1  
arrData(i,1) = i
arrData(i,2) = 0
arrData(i,3) = 0
arrData(i,4) = 0
arrData(i,5) = 0
next

 Set rsGroups = FusionHO.Execute(SQL)
While not rsGroups.EOF

				for j= 1 to cant_tanks
 			if rsGroups("tank") = grades(j) then
				arrData(rsGroups("hour"),j+1) = rsGroups("vol")
			end if
			next
  rsGroups.MoveNext

'Initialize <graph> element
strXML = "<graph caption='      Stock By Product in " & var & "' subcaption='"&fecha&"              ' hovercapbg='FFECAA' chartBottomMargin='0' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='2' showvalues='0' numdivlines='5' numVdivlines='0' yaxisminvalue='0' yaxismaxvalue='7000'  rotateNames='0' xAxisName='Hours' yAxisName='Volume'>"

'Initialize <categories> element - necessary to generate a multi-series chart
strCategories = "<categories>"


	 
'Initiate <dataset> elements
strtank1 = "<dataset seriesName=' Normal' color='AF00F8' >"
strtank2 = "<dataset seriesName=' Super' color='F6000F' >"
strtank3 = "<dataset seriesName=' Ultra' color='AFBBF8' >"
strtank4 = "<dataset seriesName=' Diesel' color='F6CC0F' >"
for j = 1 to cant_tanks
vec(j) = "<dataset seriesName=' Grade "
vec(j) = vec(j) & grades(j)
vec(j) = vec(j) & "' color='"
vec(j) = vec(j) & color(j)
vec(j) = vec(j) & "' >"
next
 
dim d
dim m
'//////// armo el xml llenando los campos vacios hacia atras
 For i=0 to UBound(arrData)-1

		strCategories = strCategories & "<category name='" & arrData(i,1) & "' />"

		if arrData(i,2) = 0 then
			arrData(i,2) =  arrData(i + 1,2)
			strtank1 = strtank1 & "<set value='" & arrData(i,2) & "' />"
		else 
			strtank1 = strtank1 & "<set value='" & arrData(i,2) & "' />"
		end if
		
		if arrData(i,3) = 0 then
			arrData(i,3) =  arrData(i + 1,3)
			strtank2 = strtank2 & "<set value='" & arrData(i,3) & "' />"
		else 
			strtank2 = strtank2 & "<set value='" & arrData(i,3) & "' />"
		end if
		
		if arrData(i,4) = 0 then
			arrData(i,4) =  arrData(i + 1,4)
			strtank3 = strtank3 & "<set value='" & arrData(i,4) & "' />"
		else 
			strtank3 = strtank3 & "<set value='" & arrData(i,4) & "' />"
			
		end if
		

		if arrData(i,5) = 0 then
			arrData(i,5) =  arrData(i + 1,5)
			strtank4 = strtank4 & "<set value='" & arrData(i,5) & "' />"
		else 
			strtank4 = strtank4 & "<set value='" & arrData(i,5) & "' />"
		end if
			for j = 1 to cant_tanks
			vec(j)= vec(j) &  "<set value='" & arrData(i,j+1) & "' />"
			next

   Next
 

  

Wend

   strCategories = strCategories & "</categories>"
   

   strtank1 = strtank1 & "</dataset>"
   strtank2 = strtank2 & "</dataset>"
   strtank3 = strtank3 & "</dataset>"
   strtank4 = strtank4 & "</dataset>"
   	  for j = 1 to cant_tanks
	  vec(j) = vec(j) & "</dataset>"
	  next

   'Assemble the entire XML now
'   ///////////// definir esto cuando capturo los datos del form ////////////

if var2 = "" then
	aux = ""
	for j = 1 to cant_tanks
    	aux = aux & vec(j)
	next
	strXML = strXML & strCategories & aux & "</graph>"'
else
	for j = 1 to cant_tanks
		if var2 = grades(j) then
			strXML = strXML & strCategories & vec(j) & "</graph>"'
		end if
	next
end if

Response.ContentType = "text/xml"
'Just write out the XML data
'NOTE THAT THIS PAGE DOESN'T CONTAIN ANY HTML TAG, WHATSOEVER
 Response.Write(strXML)
'end if
End Function 'Close Page_BeforeShow @1-54C34B28


%>
