<%
'BindEvents Method @1-6A380500
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Grades.Navigator.CCSEvents("BeforeShow") = GetRef("Grades_Navigator_BeforeShow")
        Set Grades1.CCSEvents("BeforeShow") = GetRef("Grades1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Grades_Navigator_BeforeShow(Sender) 'Grades_Navigator_BeforeShow @18-065F909D

'Hide-Show Component @19-21EA3E99
    Dim TotalPages_19_1 : TotalPages_19_1 = CCSConverter.VBSConvert(ccsInteger, Grades.DataSource.Recordset.PageCount)
    Dim Param2_19_2 : Param2_19_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_19_1) And Not IsEmpty(Param2_19_2) And TotalPages_19_1 < Param2_19_2) Then _
        Grades.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Grades_Navigator_BeforeShow @18-54C34B28

Function Grades1_BeforeShow(Sender) 'Grades1_BeforeShow @2-AB722975

'Custom Code @42-73254650
' -------------------------
  	If Grades1.recordset.EOF Then
		Grades1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Grades1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Grades1_BeforeShow @2-54C34B28




%>
