<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="tank_deliveries" name="tank_deliveries" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:tank_deliveries} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" activeCollection="TableParameters">
			<Components>
				<Sorter id="3" visible="True" name="Sorter_ss_id" column="ss_id" wizardCaption="{res:ss_id}" wizardSortingType="SimpleDir" wizardControl="ss_id" wizardAddNbsp="False" PathID="tank_deliveriesSorter_ss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="4" visible="True" name="Sorter_tank_id" column="tank_id" wizardCaption="{res:tank_id}" wizardSortingType="SimpleDir" wizardControl="tank_id" wizardAddNbsp="False" PathID="tank_deliveriesSorter_tank_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="5" visible="True" name="Sorter_date_start" column="date_start" wizardCaption="{res:date_start}" wizardSortingType="SimpleDir" wizardControl="date_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_date_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="6" visible="True" name="Sorter_time_start" column="time_start" wizardCaption="{res:time_start}" wizardSortingType="SimpleDir" wizardControl="time_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_time_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="7" visible="True" name="Sorter_vol_total" column="vol_total" wizardCaption="{res:vol_total}" wizardSortingType="SimpleDir" wizardControl="vol_total" wizardAddNbsp="False" PathID="tank_deliveriesSorter_vol_total">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="8" visible="True" name="Sorter_date_end" column="date_end" wizardCaption="{res:date_end}" wizardSortingType="SimpleDir" wizardControl="date_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_date_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="9" visible="True" name="Sorter_time_end" column="time_end" wizardCaption="{res:time_end}" wizardSortingType="SimpleDir" wizardControl="time_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_time_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="10" visible="True" name="Sorter_vol_start" column="vol_start" wizardCaption="{res:vol_start}" wizardSortingType="SimpleDir" wizardControl="vol_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_vol_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="11" visible="True" name="Sorter_vol_end" column="vol_end" wizardCaption="{res:vol_end}" wizardSortingType="SimpleDir" wizardControl="vol_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_vol_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="12" visible="True" name="Sorter_height_start" column="height_start" wizardCaption="{res:height_start}" wizardSortingType="SimpleDir" wizardControl="height_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_height_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="13" visible="True" name="Sorter_height_end" column="height_end" wizardCaption="{res:height_end}" wizardSortingType="SimpleDir" wizardControl="height_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_height_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="14" visible="True" name="Sorter_temp_start" column="temp_start" wizardCaption="{res:temp_start}" wizardSortingType="SimpleDir" wizardControl="temp_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_temp_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="15" visible="True" name="Sorter_temp_end" column="temp_end" wizardCaption="{res:temp_end}" wizardSortingType="SimpleDir" wizardControl="temp_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_temp_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="16" visible="True" name="Sorter_transactions_volume" column="transactions_volume" wizardCaption="{res:transactions_volume}" wizardSortingType="SimpleDir" wizardControl="transactions_volume" wizardAddNbsp="False" PathID="tank_deliveriesSorter_transactions_volume">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="17" visible="True" name="Sorter_water_start" column="water_start" wizardCaption="{res:water_start}" wizardSortingType="SimpleDir" wizardControl="water_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_water_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="18" visible="True" name="Sorter_water_end" column="water_end" wizardCaption="{res:water_end}" wizardSortingType="SimpleDir" wizardControl="water_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_water_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="19" visible="True" name="Sorter_water_height_start" column="water_height_start" wizardCaption="{res:water_height_start}" wizardSortingType="SimpleDir" wizardControl="water_height_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_water_height_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="20" visible="True" name="Sorter_water_height_end" column="water_height_end" wizardCaption="{res:water_height_end}" wizardSortingType="SimpleDir" wizardControl="water_height_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_water_height_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="21" visible="True" name="Sorter_delivery_id" column="delivery_id" wizardCaption="{res:delivery_id}" wizardSortingType="SimpleDir" wizardControl="delivery_id" wizardAddNbsp="False" PathID="tank_deliveriesSorter_delivery_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="22" visible="True" name="Sorter_delivery_note_id" column="delivery_note_id" wizardCaption="{res:delivery_note_id}" wizardSortingType="SimpleDir" wizardControl="delivery_note_id" wizardAddNbsp="False" PathID="tank_deliveriesSorter_delivery_note_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="23" visible="True" name="Sorter_vol_tc_total" column="vol_tc_total" wizardCaption="{res:vol_tc_total}" wizardSortingType="SimpleDir" wizardControl="vol_tc_total" wizardAddNbsp="False" PathID="tank_deliveriesSorter_vol_tc_total">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="24" visible="True" name="Sorter_vol_tc_start" column="vol_tc_start" wizardCaption="{res:vol_tc_start}" wizardSortingType="SimpleDir" wizardControl="vol_tc_start" wizardAddNbsp="False" PathID="tank_deliveriesSorter_vol_tc_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="25" visible="True" name="Sorter_vol_tc_end" column="vol_tc_end" wizardCaption="{res:vol_tc_end}" wizardSortingType="SimpleDir" wizardControl="vol_tc_end" wizardAddNbsp="False" PathID="tank_deliveriesSorter_vol_tc_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="26" fieldSourceType="DBColumn" dataType="Integer" html="False" name="ss_id" fieldSource="ss_id" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="27" fieldSourceType="DBColumn" dataType="Integer" html="False" name="tank_id" fieldSource="tank_id" wizardCaption="{res:tank_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriestank_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="28" fieldSourceType="DBColumn" dataType="Text" html="False" name="date_start" fieldSource="date_start" wizardCaption="{res:date_start}" wizardSize="9" wizardMaxLength="9" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="tank_deliveriesdate_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="29" fieldSourceType="DBColumn" dataType="Text" html="False" name="time_start" fieldSource="time_start" wizardCaption="{res:time_start}" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="tank_deliveriestime_start">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="30" fieldSourceType="DBColumn" dataType="Float" html="False" name="vol_total" fieldSource="vol_total" wizardCaption="{res:vol_total}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesvol_total" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="31" fieldSourceType="DBColumn" dataType="Text" html="False" name="date_end" fieldSource="date_end" wizardCaption="{res:date_end}" wizardSize="9" wizardMaxLength="9" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="tank_deliveriesdate_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="32" fieldSourceType="DBColumn" dataType="Text" html="False" name="time_end" fieldSource="time_end" wizardCaption="{res:time_end}" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="tank_deliveriestime_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="33" fieldSourceType="DBColumn" dataType="Float" html="False" name="vol_start" fieldSource="vol_start" wizardCaption="{res:vol_start}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesvol_start" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="34" fieldSourceType="DBColumn" dataType="Float" html="False" name="vol_end" fieldSource="vol_end" wizardCaption="{res:vol_end}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesvol_end" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="35" fieldSourceType="DBColumn" dataType="Float" html="False" name="height_start" fieldSource="height_start" wizardCaption="{res:height_start}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesheight_start" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="36" fieldSourceType="DBColumn" dataType="Float" html="False" name="height_end" fieldSource="height_end" wizardCaption="{res:height_end}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesheight_end" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="37" fieldSourceType="DBColumn" dataType="Float" html="False" name="temp_start" fieldSource="temp_start" wizardCaption="{res:temp_start}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriestemp_start" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="38" fieldSourceType="DBColumn" dataType="Float" html="False" name="temp_end" fieldSource="temp_end" wizardCaption="{res:temp_end}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriestemp_end">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="39" fieldSourceType="DBColumn" dataType="Float" html="False" name="transactions_volume" fieldSource="transactions_volume" wizardCaption="{res:transactions_volume}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriestransactions_volume" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="40" fieldSourceType="DBColumn" dataType="Float" html="False" name="water_start" fieldSource="water_start" wizardCaption="{res:water_start}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliverieswater_start" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="41" fieldSourceType="DBColumn" dataType="Float" html="False" name="water_end" fieldSource="water_end" wizardCaption="{res:water_end}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliverieswater_end" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="42" fieldSourceType="DBColumn" dataType="Float" html="False" name="water_height_start" fieldSource="water_height_start" wizardCaption="{res:water_height_start}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliverieswater_height_start" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="43" fieldSourceType="DBColumn" dataType="Float" html="False" name="water_height_end" fieldSource="water_height_end" wizardCaption="{res:water_height_end}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliverieswater_height_end" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="44" fieldSourceType="DBColumn" dataType="Integer" html="False" name="delivery_id" fieldSource="delivery_id" wizardCaption="{res:delivery_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesdelivery_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="45" fieldSourceType="DBColumn" dataType="Integer" html="False" name="delivery_note_id" fieldSource="delivery_note_id" wizardCaption="{res:delivery_note_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesdelivery_note_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="46" fieldSourceType="DBColumn" dataType="Float" html="False" name="vol_tc_total" fieldSource="vol_tc_total" wizardCaption="{res:vol_tc_total}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesvol_tc_total" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="47" fieldSourceType="DBColumn" dataType="Float" html="False" name="vol_tc_start" fieldSource="vol_tc_start" wizardCaption="{res:vol_tc_start}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesvol_tc_start" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="48" fieldSourceType="DBColumn" dataType="Float" html="False" name="vol_tc_end" fieldSource="vol_tc_end" wizardCaption="{res:vol_tc_end}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="tank_deliveriesvol_tc_end" format="#,##0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="49" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="51" conditionType="Parameter" useIsNull="False" field="delivery_id" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="delivery_id"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="50" tableName="tank_deliveries" posLeft="10" posTop="10" posWidth="156" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="delivery.asp" forShow="True" url="delivery.asp" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
