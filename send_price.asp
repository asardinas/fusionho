<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-DED34849
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Header
Dim Grid1
Dim Grid2
Dim price_change_status
Dim price_send
Dim Grid3
Dim Search_Sites
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "send_price.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "send_price.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-71EB5868
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Grid1 = New clsGridGrid1
Set Grid2 = New clsGridGrid2
Set price_change_status = new clsRecordprice_change_status
Set price_send = new clsEditableGridprice_send
Set Grid3 = New clsGridGrid3
Set Search_Sites = new clsRecordSearch_Sites
Grid1.Initialize DBFusionHO
Grid2.Initialize DBFusionHO
price_change_status.Initialize DBFusionHO
price_send.Initialize DBFusionHO
Grid3.Initialize DBFusionHO
Search_Sites.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/send_price_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-B666D5EE
Header.Operations
price_change_status.Operation
price_send.ProcessOperations
Search_Sites.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-07F95E66
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Header, Grid1, Grid2, price_change_status, price_send, Grid3, Search_Sites))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-6B10DBE1
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Set Grid1 = Nothing
    Set Grid2 = Nothing
    Set price_change_status = Nothing
    Set price_send = Nothing
    Set Grid3 = Nothing
    Set Search_Sites = Nothing
End Sub
'End UnloadPage Sub

Class clsGridGrid1 'Grid1 Class @3-CEB803E0

'Grid1 Variables @3-585F5DB7

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_ss_id
    Dim Sorter_SiteName
    Dim Sorter_Address
    Dim Sorter_Latitud
    Dim Sorter_Longitud
    Dim Sorter_Country
    Dim Sorter_Region
    Dim Sorter_City
    Dim Sorter_Zip
    Dim Sorter_Phone
    Dim Sorter_Moso
    Dim Sorter_Tm_Name
    Dim Sorter_Mail
    Dim Sorter_State
    Dim Sorter_Local
    Dim Sorter_Service
    Dim Sorter_Brand
    Dim ss_id
    Dim SiteName
    Dim Address
    Dim Latitud
    Dim Longitud
    Dim Country
    Dim Region
    Dim City
    Dim Zip
    Dim Phone
    Dim Moso
    Dim Tm_Name
    Dim Mail
    Dim State
    Dim Local
    Dim Service
    Dim Brand
    Dim Navigator
'End Grid1 Variables

'Grid1 Class_Initialize Event @3-FC0B5B55
    Private Sub Class_Initialize()
        ComponentName = "Grid1"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsGrid1DataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Grid1Order", Empty)
        SortingDirection = CCGetParam("Grid1Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_ss_id = CCCreateSorter("Sorter_ss_id", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Address = CCCreateSorter("Sorter_Address", Me, FileName)
        Set Sorter_Latitud = CCCreateSorter("Sorter_Latitud", Me, FileName)
        Set Sorter_Longitud = CCCreateSorter("Sorter_Longitud", Me, FileName)
        Set Sorter_Country = CCCreateSorter("Sorter_Country", Me, FileName)
        Set Sorter_Region = CCCreateSorter("Sorter_Region", Me, FileName)
        Set Sorter_City = CCCreateSorter("Sorter_City", Me, FileName)
        Set Sorter_Zip = CCCreateSorter("Sorter_Zip", Me, FileName)
        Set Sorter_Phone = CCCreateSorter("Sorter_Phone", Me, FileName)
        Set Sorter_Moso = CCCreateSorter("Sorter_Moso", Me, FileName)
        Set Sorter_Tm_Name = CCCreateSorter("Sorter_Tm_Name", Me, FileName)
        Set Sorter_Mail = CCCreateSorter("Sorter_Mail", Me, FileName)
        Set Sorter_State = CCCreateSorter("Sorter_State", Me, FileName)
        Set Sorter_Local = CCCreateSorter("Sorter_Local", Me, FileName)
        Set Sorter_Service = CCCreateSorter("Sorter_Service", Me, FileName)
        Set Sorter_Brand = CCCreateSorter("Sorter_Brand", Me, FileName)
        Set ss_id = CCCreateControl(ccsLink, "ss_id", Empty, ccsInteger, Empty, CCGetRequestParam("ss_id", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Address = CCCreateControl(ccsLabel, "Address", Empty, ccsText, Empty, CCGetRequestParam("Address", ccsGet))
        Set Latitud = CCCreateControl(ccsLabel, "Latitud", Empty, ccsFloat, Empty, CCGetRequestParam("Latitud", ccsGet))
        Set Longitud = CCCreateControl(ccsLabel, "Longitud", Empty, ccsFloat, Empty, CCGetRequestParam("Longitud", ccsGet))
        Set Country = CCCreateControl(ccsLabel, "Country", Empty, ccsInteger, Empty, CCGetRequestParam("Country", ccsGet))
        Set Region = CCCreateControl(ccsLabel, "Region", Empty, ccsInteger, Empty, CCGetRequestParam("Region", ccsGet))
        Set City = CCCreateControl(ccsLabel, "City", Empty, ccsInteger, Empty, CCGetRequestParam("City", ccsGet))
        Set Zip = CCCreateControl(ccsLabel, "Zip", Empty, ccsText, Empty, CCGetRequestParam("Zip", ccsGet))
        Set Phone = CCCreateControl(ccsLabel, "Phone", Empty, ccsText, Empty, CCGetRequestParam("Phone", ccsGet))
        Set Moso = CCCreateControl(ccsLabel, "Moso", Empty, ccsInteger, Empty, CCGetRequestParam("Moso", ccsGet))
        Set Tm_Name = CCCreateControl(ccsLabel, "Tm_Name", Empty, ccsInteger, Empty, CCGetRequestParam("Tm_Name", ccsGet))
        Set Mail = CCCreateControl(ccsLabel, "Mail", Empty, ccsText, Empty, CCGetRequestParam("Mail", ccsGet))
        Set State = CCCreateControl(ccsLabel, "State", Empty, ccsBoolean, DefaultBooleanFormat, CCGetRequestParam("State", ccsGet))
        Set Local = CCCreateControl(ccsLabel, "Local", Empty, ccsInteger, Empty, CCGetRequestParam("Local", ccsGet))
        Set Service = CCCreateControl(ccsLabel, "Service", Empty, ccsInteger, Empty, CCGetRequestParam("Service", ccsGet))
        Set Brand = CCCreateControl(ccsLabel, "Brand", Empty, ccsInteger, Empty, CCGetRequestParam("Brand", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Grid1 Class_Initialize Event

'Grid1 Initialize Method @3-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Grid1 Initialize Method

'Grid1 Class_Terminate Event @3-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Grid1 Class_Terminate Event

'Grid1 Show Method @3-637E3347
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urls_SiteName") = CCGetRequestParam("s_SiteName", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_ss_id, Sorter_SiteName, Sorter_Address, Sorter_Latitud, Sorter_Longitud, Sorter_Country, Sorter_Region, Sorter_City, Sorter_Zip, Sorter_Phone, Sorter_Moso, Sorter_Tm_Name, Sorter_Mail, Sorter_State, Sorter_Local, Sorter_Service, Sorter_Brand, Navigator))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(ss_id, SiteName, Address, Latitud, Longitud, Country, Region, City, Zip, Phone, Moso, Tm_Name, Mail, State, Local, Service, Brand))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Grid1:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    ss_id.Value = Recordset.Fields("ss_id")
                    ss_id.Parameters = CCGetQueryString("QueryString", Array("price_change_id", "aplication_date", "aplication_time", "var", "ccsForm"))
                    ss_id.Parameters = CCAddParam(ss_id.Parameters, "ss_id", Recordset.Fields("ss_id_param1"))
                    ss_id.Parameters = CCAddParam(ss_id.Parameters, "site", 1)
                    ss_id.Page = "send_price.asp"
                    SiteName.Value = Recordset.Fields("SiteName")
                    Address.Value = Recordset.Fields("Address")
                    Latitud.Value = Recordset.Fields("Latitud")
                    Longitud.Value = Recordset.Fields("Longitud")
                    Country.Value = Recordset.Fields("Country")
                    Region.Value = Recordset.Fields("Region")
                    City.Value = Recordset.Fields("City")
                    Zip.Value = Recordset.Fields("Zip")
                    Phone.Value = Recordset.Fields("Phone")
                    Moso.Value = Recordset.Fields("Moso")
                    Tm_Name.Value = Recordset.Fields("Tm_Name")
                    Mail.Value = Recordset.Fields("Mail")
                    State.Value = Recordset.Fields("State")
                    Local.Value = Recordset.Fields("Local")
                    Service.Value = Recordset.Fields("Service")
                    Brand.Value = Recordset.Fields("Brand")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Grid1:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Grid1:"
            StaticControls.Show
        End If

    End Sub
'End Grid1 Show Method

'Grid1 PageSize Property Let @3-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Grid1 PageSize Property Let

'Grid1 PageSize Property Get @3-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Grid1 PageSize Property Get

'Grid1 RowNumber Property Get @3-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Grid1 RowNumber Property Get

'Grid1 HasNextRow Function @3-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Grid1 HasNextRow Function

End Class 'End Grid1 Class @3-A61BA892

Class clsGrid1DataSource 'Grid1DataSource Class @3-DDF9D663

'DataSource Variables @3-9104183E
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public ss_id
    Public ss_id_param1
    Public ss_id_param2
    Public SiteName
    Public Address
    Public Latitud
    Public Longitud
    Public Country
    Public Region
    Public City
    Public Zip
    Public Phone
    Public Moso
    Public Tm_Name
    Public Mail
    Public State
    Public Local
    Public Service
    Public Brand
'End DataSource Variables

'DataSource Class_Initialize Event @3-B5A03582
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set ss_id_param1 = CCCreateField("ss_id_param1", "ss_id", ccsText, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Address = CCCreateField("Address", "Address", ccsText, Empty, Recordset)
        Set Latitud = CCCreateField("Latitud", "Latitud", ccsFloat, Empty, Recordset)
        Set Longitud = CCCreateField("Longitud", "Longitud", ccsFloat, Empty, Recordset)
        Set Country = CCCreateField("Country", "Country", ccsInteger, Empty, Recordset)
        Set Region = CCCreateField("Region", "Region", ccsInteger, Empty, Recordset)
        Set City = CCCreateField("City", "City", ccsInteger, Empty, Recordset)
        Set Zip = CCCreateField("Zip", "Zip", ccsText, Empty, Recordset)
        Set Phone = CCCreateField("Phone", "Phone", ccsText, Empty, Recordset)
        Set Moso = CCCreateField("Moso", "Moso", ccsInteger, Empty, Recordset)
        Set Tm_Name = CCCreateField("Tm_Name", "Tm_Name", ccsInteger, Empty, Recordset)
        Set Mail = CCCreateField("Mail", "Mail", ccsText, Empty, Recordset)
        Set State = CCCreateField("State", "State", ccsBoolean, Array(1, 0, Empty), Recordset)
        Set Local = CCCreateField("Local", "Local", ccsInteger, Empty, Recordset)
        Set Service = CCCreateField("Service", "Service", ccsInteger, Empty, Recordset)
        Set Brand = CCCreateField("Brand", "Brand", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(ss_id,  ss_id_param1,  SiteName,  Address,  Latitud,  Longitud,  Country, _
             Region,  City,  Zip,  Phone,  Moso,  Tm_Name,  Mail,  State, _
             Local,  Service,  Brand)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_ss_id", "ss_id", ""), _
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Address", "Address", ""), _
            Array("Sorter_Latitud", "Latitud", ""), _
            Array("Sorter_Longitud", "Longitud", ""), _
            Array("Sorter_Country", "Country", ""), _
            Array("Sorter_Region", "Region", ""), _
            Array("Sorter_City", "City", ""), _
            Array("Sorter_Zip", "Zip", ""), _
            Array("Sorter_Phone", "Phone", ""), _
            Array("Sorter_Moso", "Moso", ""), _
            Array("Sorter_Tm_Name", "Tm_Name", ""), _
            Array("Sorter_Mail", "Mail", ""), _
            Array("Sorter_State", "State", ""), _
            Array("Sorter_Local", "Local", ""), _
            Array("Sorter_Service", "Service", ""), _
            Array("Sorter_Brand", "Brand", ""))

        SQL = " " & vbLf & _
        "SELECT ss_id , SiteName, Address, Moso_id as Moso, Latitud, Longitud, Tm_id as Tm_Name, Zip, Phone, Mail, Valid as State, Region_id as Region,Country_id as Country,City_id as City,Local_type_id as Local,Service_id as Service,Brand_id as Brand FROM Sites  " & vbLf & _
        "where SiteName like '%{s_SiteName}%'"
        CountSQL = " " & vbLf & _
        "SELECT COUNT(*) FROM (SELECT ss_id , SiteName, Address, Moso_id as Moso, Latitud, Longitud, Tm_id as Tm_Name, Zip, Phone, Mail, Valid as State, Region_id as Region,Country_id as Country,City_id as City,Local_type_id as Local,Service_id as Service,Brand_id as Brand FROM Sites  " & vbLf & _
        "where SiteName like '%{s_SiteName}%') cnt"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @3-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @3-9EAF0845
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "s_SiteName", "urls_SiteName", ccsText, Empty, Empty, Empty, False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @3-CA87DA7C
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @3-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Grid1DataSource Class @3-A61BA892

Class clsGridGrid2 'Grid2 Class @39-57B1525A

'Grid2 Variables @39-9B1560CA

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_price_change_id
    Dim Sorter_SiteName
    Dim Sorter_Aplication_Date
    Dim Sorter_Aplication_Time
    Dim Sorter_Reception_Date
    Dim Sorter_Reception_time
    Dim Sorter_File_Delivery_Date
    Dim Sorter_File_Delivery_Time
    Dim Sorter_Status
    Dim Sorter_Status_Description
    Dim price_change_id
    Dim SiteName
    Dim Aplication_Date
    Dim Aplication_Time
    Dim Reception_Date
    Dim Reception_time
    Dim File_Delivery_Date
    Dim File_Delivery_Time
    Dim Status
    Dim Status_Description
    Dim Navigator
    Dim Link1
'End Grid2 Variables

'Grid2 Class_Initialize Event @39-50EC9CE8
    Private Sub Class_Initialize()
        ComponentName = "Grid2"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsGrid2DataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Grid2Order", Empty)
        SortingDirection = CCGetParam("Grid2Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_price_change_id = CCCreateSorter("Sorter_price_change_id", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Aplication_Date = CCCreateSorter("Sorter_Aplication_Date", Me, FileName)
        Set Sorter_Aplication_Time = CCCreateSorter("Sorter_Aplication_Time", Me, FileName)
        Set Sorter_Reception_Date = CCCreateSorter("Sorter_Reception_Date", Me, FileName)
        Set Sorter_Reception_time = CCCreateSorter("Sorter_Reception_time", Me, FileName)
        Set Sorter_File_Delivery_Date = CCCreateSorter("Sorter_File_Delivery_Date", Me, FileName)
        Set Sorter_File_Delivery_Time = CCCreateSorter("Sorter_File_Delivery_Time", Me, FileName)
        Set Sorter_Status = CCCreateSorter("Sorter_Status", Me, FileName)
        Set Sorter_Status_Description = CCCreateSorter("Sorter_Status_Description", Me, FileName)
        Set price_change_id = CCCreateControl(ccsLink, "price_change_id", Empty, ccsInteger, Empty, CCGetRequestParam("price_change_id", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Aplication_Date = CCCreateControl(ccsLabel, "Aplication_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Aplication_Date", ccsGet))
        Set Aplication_Time = CCCreateControl(ccsLabel, "Aplication_Time", Empty, ccsText, Empty, CCGetRequestParam("Aplication_Time", ccsGet))
        Set Reception_Date = CCCreateControl(ccsLabel, "Reception_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Reception_Date", ccsGet))
        Set Reception_time = CCCreateControl(ccsLabel, "Reception_time", Empty, ccsText, Empty, CCGetRequestParam("Reception_time", ccsGet))
        Set File_Delivery_Date = CCCreateControl(ccsLabel, "File_Delivery_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("File_Delivery_Date", ccsGet))
        Set File_Delivery_Time = CCCreateControl(ccsLabel, "File_Delivery_Time", Empty, ccsText, Empty, CCGetRequestParam("File_Delivery_Time", ccsGet))
        Set Status = CCCreateControl(ccsLabel, "Status", Empty, ccsInteger, Empty, CCGetRequestParam("Status", ccsGet))
        Set Status_Description = CCCreateControl(ccsLabel, "Status_Description", Empty, ccsText, Empty, CCGetRequestParam("Status_Description", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        Set Link1 = CCCreateControl(ccsLink, "Link1", Empty, ccsText, Empty, CCGetRequestParam("Link1", ccsGet))
    IsDSEmpty = True
    End Sub
'End Grid2 Class_Initialize Event

'Grid2 Initialize Method @39-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Grid2 Initialize Method

'Grid2 Class_Terminate Event @39-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Grid2 Class_Terminate Event

'Grid2 Show Method @39-5717F83C
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urlss_id") = CCGetRequestParam("ss_id", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_price_change_id, Sorter_SiteName, Sorter_Aplication_Date, Sorter_Aplication_Time, Sorter_Reception_Date, Sorter_Reception_time, Sorter_File_Delivery_Date, Sorter_File_Delivery_Time, Sorter_Status, Sorter_Status_Description, Navigator, Link1))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
            
            Link1.Parameters = CCGetQueryString("QueryString", Array("price_change_id", "aplication_date", "aplication_time", "ccsForm"))
            Link1.Parameters = CCAddParam(Link1.Parameters, "var", 1)
            Link1.Page = "send_price.asp"
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(price_change_id, SiteName, Aplication_Date, Aplication_Time, Reception_Date, Reception_time, File_Delivery_Date, File_Delivery_Time, Status, Status_Description))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Grid2:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    price_change_id.Value = Recordset.Fields("price_change_id")
                    price_change_id.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
                    price_change_id.Parameters = CCAddParam(price_change_id.Parameters, "price_change_id", Recordset.Fields("price_change_id_param1"))
                    price_change_id.Page = "send_price.asp"
                    SiteName.Value = Recordset.Fields("SiteName")
                    Aplication_Date.Value = Recordset.Fields("Aplication_Date")
                    Aplication_Time.Value = Recordset.Fields("Aplication_Time")
                    Reception_Date.Value = Recordset.Fields("Reception_Date")
                    Reception_time.Value = Recordset.Fields("Reception_time")
                    File_Delivery_Date.Value = Recordset.Fields("File_Delivery_Date")
                    File_Delivery_Time.Value = Recordset.Fields("File_Delivery_Time")
                    Status.Value = Recordset.Fields("Status")
                    Status_Description.Value = Recordset.Fields("Status_Description")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Grid2:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Grid2:"
            StaticControls.Show
        End If

    End Sub
'End Grid2 Show Method

'Grid2 PageSize Property Let @39-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Grid2 PageSize Property Let

'Grid2 PageSize Property Get @39-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Grid2 PageSize Property Get

'Grid2 RowNumber Property Get @39-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Grid2 RowNumber Property Get

'Grid2 HasNextRow Function @39-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Grid2 HasNextRow Function

End Class 'End Grid2 Class @39-A61BA892

Class clsGrid2DataSource 'Grid2DataSource Class @39-441BB062

'DataSource Variables @39-CA8C2A28
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public price_change_id
    Public price_change_id_param1
    Public SiteName
    Public Aplication_Date
    Public Aplication_Time
    Public Reception_Date
    Public Reception_time
    Public File_Delivery_Date
    Public File_Delivery_Time
    Public Status
    Public Status_Description
'End DataSource Variables

'DataSource Class_Initialize Event @39-B56A2D13
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Set price_change_id_param1 = CCCreateField("price_change_id_param1", "price_change_id", ccsText, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Aplication_Date = CCCreateField("Aplication_Date", "Aplication_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Aplication_Time = CCCreateField("Aplication_Time", "Aplication_Time", ccsText, Empty, Recordset)
        Set Reception_Date = CCCreateField("Reception_Date", "Reception_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Reception_time = CCCreateField("Reception_time", "Reception_time", ccsText, Empty, Recordset)
        Set File_Delivery_Date = CCCreateField("File_Delivery_Date", "File_Delivery_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set File_Delivery_Time = CCCreateField("File_Delivery_Time", "File_Delivery_Time", ccsText, Empty, Recordset)
        Set Status = CCCreateField("Status", "Status", ccsInteger, Empty, Recordset)
        Set Status_Description = CCCreateField("Status_Description", "Status_Description", ccsText, Empty, Recordset)
        Fields.AddFields Array(price_change_id,  price_change_id_param1,  SiteName,  Aplication_Date,  Aplication_Time,  Reception_Date,  Reception_time, _
             File_Delivery_Date,  File_Delivery_Time,  Status,  Status_Description)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_price_change_id", "price_change_id", ""), _
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Aplication_Date", "Aplication_Date", ""), _
            Array("Sorter_Aplication_Time", "Aplication_Time", ""), _
            Array("Sorter_Reception_Date", "Reception_Date", ""), _
            Array("Sorter_Reception_time", "Reception_time", ""), _
            Array("Sorter_File_Delivery_Date", "File_Delivery_Date", ""), _
            Array("Sorter_File_Delivery_Time", "File_Delivery_Time", ""), _
            Array("Sorter_Status", "Status", ""), _
            Array("Sorter_Status_Description", "Status_Description", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} price_change_id, SiteName, aplication_date AS Aplication_Date, aplication_time AS Aplication_Time, reception_date AS Reception_Date, " & vbLf & _
        "reception_time AS Reception_Time, file_delivery_date AS File_Delivery_Date, file_delivery_time AS File_Delivery_Time, status_code AS Status, " & vbLf & _
        "status_description AS Status_Description  " & vbLf & _
        "FROM Sites INNER JOIN price_change_status ON " & vbLf & _
        "Sites.ss_id = price_change_status.ss_id {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM Sites INNER JOIN price_change_status ON " & vbLf & _
        "Sites.ss_id = price_change_status.ss_id"
        Where = ""
        Order = "price_change_id desc"
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @39-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @39-5064D9D8
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlss_id", ccsInteger, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opEqual, False, "price_change_status.ss_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @39-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @39-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Grid2DataSource Class @39-A61BA892

Class clsRecordprice_change_status 'price_change_status Class @64-5918E2A9

'price_change_status Variables @64-AA5C05F8

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim Button_Insert
    Dim Button_Update
    Dim Button_Delete
    Dim Button_Cancel
    Dim aplication_date
    Dim DatePicker_aplication_date
    Dim aplication_time
    Dim ss_id
    Dim price_change_id
'End price_change_status Variables

'price_change_status Class_Initialize Event @64-ADFBE18F
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsprice_change_statusDataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "price_change_status")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "price_change_status"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set Button_Insert = CCCreateButton("Button_Insert", Method)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Delete = CCCreateButton("Button_Delete", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set aplication_date = CCCreateControl(ccsTextBox, "aplication_date", CCSLocales.GetText("aplication_date", ""), ccsDate, DefaultDateFormat, CCGetRequestParam("aplication_date", Method))
        Set DatePicker_aplication_date = CCCreateDatePicker("DatePicker_aplication_date", "price_change_status", "aplication_date")
        Set aplication_time = CCCreateControl(ccsTextBox, "aplication_time", CCSLocales.GetText("aplication_time", ""), ccsText, Empty, CCGetRequestParam("aplication_time", Method))
        Set ss_id = CCCreateControl(ccsHidden, "ss_id", Empty, ccsInteger, Empty, CCGetRequestParam("ss_id", Method))
        ss_id.Required = True
        Set price_change_id = CCCreateControl(ccsHidden, "price_change_id", Empty, ccsInteger, Empty, CCGetRequestParam("price_change_id", Method))
        price_change_id.Required = True
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(aplication_date, aplication_time, ss_id, price_change_id)
    End Sub
'End price_change_status Class_Initialize Event

'price_change_status Initialize Method @64-BD48B8DF
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
            .Parameters("urlprice_change_id") = CCGetRequestParam("price_change_id", ccsGET)
        End With
    End Sub
'End price_change_status Initialize Method

'price_change_status Class_Terminate Event @64-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End price_change_status Class_Terminate Event

'price_change_status Validate Method @64-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End price_change_status Validate Method

'price_change_status Operation Method @64-27FA2F7C
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Insert")
            If Button_Insert.Pressed Then
                PressedButton = "Button_Insert"
            ElseIf Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Delete.Pressed Then
                PressedButton = "Button_Delete"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = FileName & "?" & CCGetQueryString("All", Array("ccsForm", "Button_Insert.x", "Button_Insert.y", "Button_Insert", "Button_Update.x", "Button_Update.y", "Button_Update", "Button_Delete.x", "Button_Delete.y", "Button_Delete", "Button_Cancel.x", "Button_Cancel.y", "Button_Cancel"))
        If PressedButton = "Button_Delete" Then
            If NOT Button_Delete.OnClick OR NOT DeleteRow() Then
                Redirect = ""
            Else
                Redirect = "send_price.asp?" & CCGetQueryString("QueryString", Array("ccsForm", "price_change_id", "aplication_date", "aplication_time", "var"))
            End If
        ElseIf PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            Else
                Redirect = "send_price.asp?" & CCGetQueryString("QueryString", Array("ccsForm", "price_change_id", "aplication_date", "aplication_time", "var"))
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Insert" Then
                If NOT Button_Insert.OnClick() OR NOT InsertRow() Then
                    Redirect = ""
                End If
            ElseIf PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                Else
                    Redirect = "send_price.asp?" & CCGetQueryString("QueryString", Array("ccsForm", "price_change_id", "aplication_date", "aplication_time", "var"))
                                
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End price_change_status Operation Method

'price_change_status InsertRow Method @64-55646BB7
    Function InsertRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInsert", Me)
        If NOT InsertAllowed Then InsertRow = False : Exit Function
        DataSource.aplication_date.Value = aplication_date.Value
        DataSource.aplication_time.Value = aplication_time.Value
        DataSource.ss_id.Value = ss_id.Value
        DataSource.price_change_id.Value = price_change_id.Value
        DataSource.Insert(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInsert", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        InsertRow = (Errors.Count = 0)
    End Function
'End price_change_status InsertRow Method

'price_change_status UpdateRow Method @64-AB08B581
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        DataSource.aplication_date.Value = aplication_date.Value
        DataSource.aplication_time.Value = aplication_time.Value
        DataSource.ss_id.Value = ss_id.Value
        DataSource.price_change_id.Value = price_change_id.Value
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End price_change_status UpdateRow Method

'price_change_status DeleteRow Method @64-D5C1DF24
    Function DeleteRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeDelete", Me)
        If NOT DeleteAllowed Then DeleteRow = False : Exit Function
        DataSource.Delete(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterDelete", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        DeleteRow = (Errors.Count = 0)
    End Function
'End price_change_status DeleteRow Method

'price_change_status Show Method @64-81B83C67
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        If CCGetFromGet("ccsForm", Empty) = "" Then
            HTMLFormAction = FileName & "?" & CCAddParam(CCGetQueryString("All", Array("ccsForm", "Button_Insert.x", "Button_Insert.y", "Button_Insert", "Button_Update.x", "Button_Update.y", "Button_Update", "Button_Delete.x", "Button_Delete.y", "Button_Delete", "Button_Cancel.x", "Button_Cancel.y", "Button_Cancel")), "ccsForm", "price_change_status" & IIf(EditMode, ":Edit", ""))
        Else
            HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "price_change_status" & IIf(EditMode, ":Edit", ""))
        End If
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(aplication_date, DatePicker_aplication_date, ss_id, price_change_id, aplication_time, Button_Insert, Button_Update, Button_Delete, Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        aplication_date.Value = Recordset.Fields("aplication_date")
                        aplication_time.Value = Recordset.Fields("aplication_time")
                        ss_id.Value = Recordset.Fields("ss_id")
                        price_change_id.Value = Recordset.Fields("price_change_id")
                    End If
                Else
                    EditMode = False
                End If
            End If
        End If
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors aplication_date.Errors
            Errors.AddErrors aplication_time.Errors
            Errors.AddErrors ss_id.Errors
            Errors.AddErrors price_change_id.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Insert.Visible = NOT EditMode AND InsertAllowed
        Button_Update.Visible = EditMode AND UpdateAllowed
        Button_Delete.Visible = EditMode AND DeleteAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "price_change_status" & ":"
            Controls.Show
        End If
    End Sub
'End price_change_status Show Method

End Class 'End price_change_status Class @64-A61BA892

Class clsprice_change_statusDataSource 'price_change_statusDataSource Class @64-CF166079

'DataSource Variables @64-9F6AA127
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public aplication_date
    Public aplication_time
    Public ss_id
    Public price_change_id
'End DataSource Variables

'DataSource Class_Initialize Event @64-864F638E
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set aplication_date = CCCreateField("aplication_date", "aplication_date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set aplication_time = CCCreateField("aplication_time", "aplication_time", ccsText, Empty, Recordset)
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(aplication_date, aplication_time, ss_id, price_change_id)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "aplication_date", True
        InsertOmitIfEmpty.Add "aplication_time", True
        InsertOmitIfEmpty.Add "ss_id", True
        InsertOmitIfEmpty.Add "price_change_id", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "aplication_date", True
        UpdateOmitIfEmpty.Add "aplication_time", True
        UpdateOmitIfEmpty.Add "ss_id", True
        UpdateOmitIfEmpty.Add "price_change_id", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT *  " & vbLf & _
        "FROM price_change_status {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @64-1574D3EC
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlprice_change_id", ccsInteger, Empty, Empty, Empty, False
            AllParamsSet = .AllParamsSet
            .Criterion(1) = .Operation(opEqual, False, "price_change_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @64-48A2DA7D
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @64-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @64-63C8EEEA
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM price_change_status" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @64-17B66192
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_aplication_date : IsDef_aplication_date = CCIsDefined("aplication_date", "Form")
        Dim IsDef_aplication_time : IsDef_aplication_time = CCIsDefined("aplication_time", "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id", "Form")
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id", "Form")
        If Not UpdateOmitIfEmpty("aplication_date") Or IsDef_aplication_date Then Cmd.AddSQLStrings "aplication_date=" & Connection.ToSQL(aplication_date, aplication_date.DataType), Empty
        If Not UpdateOmitIfEmpty("aplication_time") Or IsDef_aplication_time Then Cmd.AddSQLStrings "aplication_time=" & Connection.ToSQL(aplication_time, aplication_time.DataType), Empty
        If Not UpdateOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id=" & Connection.ToSQL(ss_id, ss_id.DataType), Empty
        If Not UpdateOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id=" & Connection.ToSQL(price_change_id, price_change_id.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "price_change_status", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @64-CD3AC7EE
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_aplication_date : IsDef_aplication_date = CCIsDefined("aplication_date", "Form")
        Dim IsDef_aplication_time : IsDef_aplication_time = CCIsDefined("aplication_time", "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id", "Form")
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id", "Form")
        If Not InsertOmitIfEmpty("aplication_date") Or IsDef_aplication_date Then Cmd.AddSQLStrings "aplication_date", Connection.ToSQL(aplication_date, aplication_date.DataType)
        If Not InsertOmitIfEmpty("aplication_time") Or IsDef_aplication_time Then Cmd.AddSQLStrings "aplication_time", Connection.ToSQL(aplication_time, aplication_time.DataType)
        If Not InsertOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id", Connection.ToSQL(ss_id, ss_id.DataType)
        If Not InsertOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id", Connection.ToSQL(price_change_id, price_change_id.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "price_change_status", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End price_change_statusDataSource Class @64-A61BA892

Class clsEditableGridprice_send 'price_send Class @76-F6179667

'price_send Variables @76-3DA20FE7

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public HTMLFormMethod
    Public PressedButton
    Public Errors
    Public IsFormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public PageNumber
    Public IsDSEmpty
    Public RowNumber
    Public CachedColumns
    Public CachedColumnsNames
    Public CachedColumnsNumber
    Public SubmittedRows
    Public EmptyRows
    Public ErrorMessages
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public ActiveSorter, SortingDirection
    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls
    Public NoRecordsControls
    Private MaxCachedValues
    Private CachedValuesNumber
    Private NewEmptyRows
    Private ErrorControls

    ' Class variables
    Dim Sorter_price_change_id
    Dim Sorter_ss_id
    Dim Sorter_grade_id
    Dim Sorter_levels
    Dim Sorter_ppu_actual
    Dim Sorter_price_change_type
    Dim Sorter_new_price
    Dim price_change_id
    Dim ss_id
    Dim grade_id
    Dim levels
    Dim ppu_actual
    Dim price_change_type
    Dim new_price
    Dim CheckBox_Delete
    Dim Navigator
    Dim Button_Submit
    Dim Cancel
    Dim RowIDAttribute
    Dim RowNameAttribute
    Dim RowStyleAttribute
    Dim TextBox1
    Public Row
'End price_send Variables

'price_send Class_Initialize Event @76-F562532E
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set ErrorControls = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsprice_sendDataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        ComponentName = "price_send"

        ActiveSorter = CCGetParam("price_sendOrder", Empty)
        SortingDirection = CCGetParam("price_sendDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CInt(CCGetParam(ComponentName & "Page", 1))

        If CCGetFromGet("ccsForm", Empty) = ComponentName Then
            IsFormSubmitted = True
            EditMode = True
        Else
            IsFormSubmitted = False
            EditMode = False
        End If
        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        Set Sorter_price_change_id = CCCreateSorter("Sorter_price_change_id", Me, FileName)
        Set Sorter_ss_id = CCCreateSorter("Sorter_ss_id", Me, FileName)
        Set Sorter_grade_id = CCCreateSorter("Sorter_grade_id", Me, FileName)
        Set Sorter_levels = CCCreateSorter("Sorter_levels", Me, FileName)
        Set Sorter_ppu_actual = CCCreateSorter("Sorter_ppu_actual", Me, FileName)
        Set Sorter_price_change_type = CCCreateSorter("Sorter_price_change_type", Me, FileName)
        Set Sorter_new_price = CCCreateSorter("Sorter_new_price", Me, FileName)
        Set price_change_id = CCCreateControl(ccsTextBox, "price_change_id", CCSLocales.GetText("price_change_id", ""), ccsInteger, Empty, CCGetRequestParam("price_change_id", Method))
        Set ss_id = CCCreateControl(ccsTextBox, "ss_id", CCSLocales.GetText("ss_id", ""), ccsInteger, Empty, CCGetRequestParam("ss_id", Method))
        ss_id.Required = True
        Set grade_id = CCCreateList(ccsListBox, "grade_id", CCSLocales.GetText("grade_id", ""), ccsInteger, CCGetRequestParam("grade_id", Method), Empty)
        grade_id.BoundColumn = "grade_id"
        grade_id.TextColumn = "grade_name"
        Set grade_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Grades {SQL_Where} {SQL_OrderBy}", "", ""))
        grade_id.Required = True
        Set levels = CCCreateList(ccsListBox, "levels", CCSLocales.GetText("levels", ""), ccsInteger, CCGetRequestParam("levels", Method), Empty)
        Set levels.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("1", "2", "3", "4", "5", "6", "7"), _
            Array("1", "2", "3", "4", "5", "6", "7")))
        levels.Required = True
        Set ppu_actual = CCCreateControl(ccsTextBox, "ppu_actual", CCSLocales.GetText("ppu_actual", ""), ccsFloat, Empty, CCGetRequestParam("ppu_actual", Method))
        Set price_change_type = CCCreateList(ccsListBox, "price_change_type", CCSLocales.GetText("price_change_type", ""), ccsInteger, CCGetRequestParam("price_change_type", Method), Empty)
        price_change_type.BoundColumn = "price_change_type"
        price_change_type.TextColumn = "price_change_description"
        Set price_change_type.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM price_type {SQL_Where} {SQL_OrderBy}", "", ""))
        price_change_type.Required = True
        Set new_price = CCCreateControl(ccsTextBox, "new_price", CCSLocales.GetText("new_price", ""), ccsFloat, Array(False, 2, True, False, False, "", "", 1, True, ""), CCGetRequestParam("new_price", Method))
        Set CheckBox_Delete = CCCreateControl(ccsCheckBox, "CheckBox_Delete", Empty, ccsBoolean, DefaultBooleanFormat, CCGetRequestParam("CheckBox_Delete", Method))
        CheckBox_Delete.CheckedValue = true
        CheckBox_Delete.UncheckedValue = false
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        Set Button_Submit = CCCreateButton("Button_Submit", Method)
        Set Cancel = CCCreateButton("Cancel", Method)
        Set RowIDAttribute = CCCreateControl(ccsLabel, "RowIDAttribute", Empty, ccsText, Empty, CCGetRequestParam("RowIDAttribute", Method))
        Set RowNameAttribute = CCCreateControl(ccsLabel, "RowNameAttribute", Empty, ccsText, Empty, CCGetRequestParam("RowNameAttribute", Method))
        Set RowStyleAttribute = CCCreateControl(ccsLabel, "RowStyleAttribute", Empty, ccsText, Empty, CCGetRequestParam("RowStyleAttribute", Method))
        RowStyleAttribute.HTML = True
        Set TextBox1 = CCCreateControl(ccsTextBox, "TextBox1", Empty, ccsText, Empty, CCGetRequestParam("TextBox1", Method))
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(price_change_id, ss_id, grade_id, levels, ppu_actual, price_change_type, new_price, TextBox1)

        SubmittedRows = 0
        NewEmptyRows = 0
        EmptyRows = 1

        InitCachedColumns()

        IsDSEmpty = True
    End Sub
'End price_send Class_Initialize Event

'price_send InitCachedColumns Method @76-52628C1A
    Sub InitCachedColumns()
        Dim RetrievedNumber, i
        CachedColumnsNumber = 5
        ReDim CachedColumnsNames(CachedColumnsNumber)
        CachedColumnsNames(0) = "price_send_id"
        CachedColumnsNames(1) = "ss_id"
        CachedColumnsNames(2) = "grade_id"
        CachedColumnsNames(3) = "levels"
        CachedColumnsNames(4) = "price_change_type"

        RetrievedNumber = 0
        CachedColumns = GetCachedColumns()

        If IsArray(CachedColumns) Then
            RetrievedNumber = UBound(CachedColumns)
            If RetrievedNumber > 0 Then
                MaxCachedValues = CInt(RetrievedNumber / CachedColumnsNumber)
                If (RetrievedNumber Mod CachedColumnsNumber) > 0 Then
                    MaxCachedValues = MaxCachedValues + 1
                End If
                CachedValuesNumber = MaxCachedValues
            End If
        End If

        If RetrievedNumber = 0 Then
            MaxCachedValues = 50
            ReDim CachedColumns(MaxCachedValues * CachedColumnsNumber)
            CachedValuesNumber = 0
        End If 

        If SubmittedRows > 0 Or NewEmptyRows > 0 Then
            EmptyRows = NewEmptyRows
        End If

        DataSource.CachedColumns = CachedColumns
        DataSource.CachedColumnsNumber = CachedColumnsNumber
        ReDim ErrorMessages(SubmittedRows + EmptyRows)
    End Sub
'End price_send InitCachedColumns Method

'price_send Initialize Method @76-3D281C70
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        With DataSource

            Set .Connection = objConnection
            .PageSize = PageSize
            .SetOrder ActiveSorter, SortingDirection
            .AbsolutePage = PageNumber
            .Parameters("urlprice_change_id") = CCGetRequestParam("price_change_id", ccsGET)
        End With
    End Sub
'End price_send Initialize Method

'price_send Class_Terminate Event @76-80BE16BA
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End price_send Class_Terminate Event

'price_send Validate Method @76-E51056EF
    Function Validate()
        Dim Validation
        Dim i, InsertedRows, Method, IsDeleted, IsEmptyRow, IsNewRow,EGErrors
        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        Validation = True

        If SubmittedRows > 0 Then
            Set EGErrors = New clsErrors
            EGErrors.AddErrors(Errors)
            Errors.Clear
            For i = 1 To SubmittedRows
                IsDeleted = (Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0)
                IsEmptyRow = (Len(CCGetRequestParam("price_change_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ss_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("grade_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("levels_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ppu_actual_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("price_change_type_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("new_price_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("TextBox1_" & CStr(i), Method)) = 0)

                If (Not IsDeleted) And (Not IsEmptyRow Or (i < SubmittedRows - EmptyRows + 1)) Then
                    price_change_id.Errors.Clear
                    price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), Method)
                    ss_id.Errors.Clear
                    ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), Method)
                    grade_id.Errors.Clear
                    grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), Method)
                    levels.Errors.Clear
                    levels.Text = CCGetRequestParam("levels_" & CStr(i), Method)
                    ppu_actual.Errors.Clear
                    ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), Method)
                    price_change_type.Errors.Clear
                    price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), Method)
                    new_price.Errors.Clear
                    new_price.Text = CCGetRequestParam("new_price_" & CStr(i), Method)
                    TextBox1.Errors.Clear
                    TextBox1.Text = CCGetRequestParam("TextBox1_" & CStr(i), Method)
                    ValidatingControls.Validate
                    CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidateRow", Me)
                    If Not ValidatingControls.IsValid() or Errors.Count >0 Then
                        Errors.AddErrors price_change_id.Errors
                        Errors.AddErrors ss_id.Errors
                        Errors.AddErrors grade_id.Errors
                        Errors.AddErrors levels.Errors
                        Errors.AddErrors ppu_actual.Errors
                        Errors.AddErrors price_change_type.Errors
                        Errors.AddErrors new_price.Errors
                        Errors.AddErrors CheckBox_Delete.Errors
                        Errors.AddErrors TextBox1.Errors
                        ErrorMessages(i) = Errors.ToString()
                        Validation = False
                        Errors.Clear
                    End If
                End If
            Next
            Errors.AddErrors(EGErrors)
            Set EGErrors = Nothing
        End If

        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = Validation And (Errors.Count = 0)
    End Function
'End price_send Validate Method

'price_send ProcessOperations Method @76-1D386A1C
    Sub ProcessOperations()
        Dim TmpWhere: TmpWhere = Datasource.Where

        If Not ( Visible And IsFormSubmitted ) Then Exit Sub

        If IsFormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Submit", "Button_Submit")
            If Button_Submit.Pressed Then
                PressedButton = "Button_Submit"
            ElseIf Cancel.Pressed Then
                PressedButton = "Cancel"
            End If
        End If
        Redirect = FileName & "?" & CCGetQueryString("QueryString",Array("ccsForm", "Button_Submit.x", "Button_Submit", "Cancel.x", "Cancel"))

        If PressedButton = "Cancel" Then
            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSubmit", Me)
            If NOT Cancel.OnClick Then
                Redirect = ""
            End If
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterSubmit", Me)
        ElseIf Validate() Then
            If PressedButton = "Button_Submit" Then
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSubmit", Me)
                If NOT Button_Submit.OnClick() OR (NOT InsertRows() And InsertAllowed) OR (NOT UpdateRows() And UpdateAllowed) OR (NOT DeleteRows() And DeleteAllowed) Then
                    Redirect = ""
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "AfterSubmit", Me)
            End If
        Else
            Redirect = ""
        End If

        Datasource.Where = TmpWhere
    End Sub
'End price_send ProcessOperations Method

'price_send InsertRows Method @76-2C3BACB6
    Function InsertRows()
        If NOT InsertAllowed Then InsertRows = False : Exit Function

        Dim i, InsertedRows, Method, IsDeleted, IsEmptyRow, HasErrors

        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)

        If SubmittedRows > 0 Then
            i = SubmittedRows - EmptyRows
            For i = (SubmittedRows - EmptyRows + 1) To SubmittedRows
                IsDeleted = (Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0)
                IsEmptyRow = True
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("price_change_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ss_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("grade_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("levels_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ppu_actual_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("price_change_type_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("new_price_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("TextBox1_" & CStr(i), Method)) = 0)

                If (Not IsDeleted) And (Not IsEmptyRow) Then
                    price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), Method)
                    DataSource.price_change_id.Value = price_change_id.Value
                    ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), Method)
                    DataSource.ss_id.Value = ss_id.Value
                    grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), Method)
                    DataSource.grade_id.Value = grade_id.Value
                    levels.Text = CCGetRequestParam("levels_" & CStr(i), Method)
                    DataSource.levels.Value = levels.Value
                    ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), Method)
                    DataSource.ppu_actual.Value = ppu_actual.Value
                    price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), Method)
                    DataSource.price_change_type.Value = price_change_type.Value
                    new_price.Text = CCGetRequestParam("new_price_" & CStr(i), Method)
                    DataSource.new_price.Value = new_price.Value
                    DataSource.CurrentRow = i
                    DataSource.Insert(Command)


                    If DataSource.Errors.Count > 0 Then
                        HasErrors = True
                        ErrorMessages(i) = DataSource.Errors.ToString()
                        DataSource.Errors.Clear
                    End If
                End If
            Next
        End If

        InsertRows = Not(HasErrors)
    End Function
'End price_send InsertRows Method

'price_send UpdateRows Method @76-28F30A39
    Function UpdateRows()
        If NOT UpdateAllowed Then UpdateRows = False : Exit Function

        Dim i, InsertedRows, Method, IsDeleted, HasErrors
        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        If SubmittedRows > 0 Then
            For i = 1 To SubmittedRows - EmptyRows
                IsDeleted = (Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0)

                If Not IsDeleted Then
                    price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), Method)
                    DataSource.price_change_id.Value = price_change_id.Value
                    ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), Method)
                    DataSource.ss_id.Value = ss_id.Value
                    grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), Method)
                    DataSource.grade_id.Value = grade_id.Value
                    levels.Text = CCGetRequestParam("levels_" & CStr(i), Method)
                    DataSource.levels.Value = levels.Value
                    ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), Method)
                    DataSource.ppu_actual.Value = ppu_actual.Value
                    price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), Method)
                    DataSource.price_change_type.Value = price_change_type.Value
                    new_price.Text = CCGetRequestParam("new_price_" & CStr(i), Method)
                    DataSource.new_price.Value = new_price.Value
                    DataSource.CurrentRow = i
                    DataSource.Update(Command)


                    If DataSource.Errors.Count > 0 Then
                        HasErrors = True
                        ErrorMessages(i) = DataSource.Errors.ToString()
                        DataSource.Errors.Clear
                    End If
                End If
            Next
        End If
        UpdateRows = Not(HasErrors)
    End Function
'End price_send UpdateRows Method

'price_send DeleteRows Method @76-BF059AF4
    Function DeleteRows()
        Dim i, Method, HasErrors

        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        If NOT DeleteAllowed Then DeleteRows = False : Exit Function


        If SubmittedRows > 0 Then
            For i = 1 To SubmittedRows - EmptyRows
                If Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0 Then
                    DataSource.CurrentRow = i
                    DataSource.Delete(Command)


                    If DataSource.Errors.Count > 0 Then
                        HasErrors = True
                        ErrorMessages(i) = DataSource.Errors.ToString()
                        DataSource.Errors.Clear
                    End If
                End If
            Next
        End If

        DeleteRows = Not(HasErrors)
    End Function
'End price_send DeleteRows Method

'GetFormScript Method @76-C012DA2E
    Function GetFormScript(TotalRows)
        Dim script,i: script = ""
        script = script & vbNewLine & "<script language=""JavaScript"">" & vbNewLine & "<!--" & vbNewLine
        script = script & "var price_sendElements;" & vbNewLine
        script = script & "var price_sendEmptyRows = 1;" & vbNewLine
        script = script & "var " & ComponentName & "price_change_idID = 0;" & vbNewLine
        script = script & "var " & ComponentName & "ss_idID = 1;" & vbNewLine
        script = script & "var " & ComponentName & "grade_idID = 2;" & vbNewLine
        script = script & "var " & ComponentName & "levelsID = 3;" & vbNewLine
        script = script & "var " & ComponentName & "ppu_actualID = 4;" & vbNewLine
        script = script & "var " & ComponentName & "price_change_typeID = 5;" & vbNewLine
        script = script & "var " & ComponentName & "new_priceID = 6;" & vbNewLine
        script = script & "var " & ComponentName & "DeleteControl = 7;" & vbNewLine
        script = script & "var " & ComponentName & "TextBox1ID = 8;" & vbNewLine
        script = script & vbNewLine & "function initprice_sendElements() {" & vbNewLine
        script = script & vbTab & "var ED = document.forms[""price_send""];" & vbNewLine
        script = script & vbTab & "price_sendElements = new Array (" & vbNewLine
        For i = 1 To TotalRows
            script = script & vbTab & vbTab & "new Array(" & "ED.price_change_id_" & CStr(i) & ", " & "ED.ss_id_" & CStr(i) & ", " & "ED.grade_id_" & CStr(i) & ", " & "ED.levels_" & CStr(i) & ", " & "ED.ppu_actual_" & CStr(i) & ", " & "ED.price_change_type_" & CStr(i) & ", " & "ED.new_price_" & CStr(i) & ", " & "ED.CheckBox_Delete_" & CStr(i) & ", " & "ED.TextBox1_" & CStr(i) & ")"
            If(i <> TotalRows) Then script = script & "," & vbNewLine
        Next
        script = script & ");" & vbNewLine
        script = script & "}" & vbNewLine
        script = script & vbNewLine & "//-->" & vbNewLine & "</script>"
        GetFormScript = script
    End Function
'End GetFormScript Method

'GetFormState Method @76-8BEF9A95
    Function GetFormState
        Dim FormState, i, LastValueIndex, NewRows

        FormState = ""
        LastValueIndex = CachedValuesNumber * CachedColumnsNumber - 1

        If EditMode And LastValueIndex >= 0 Then

            For i = 1 To CachedColumnsNumber
                FormState = FormState & CachedColumnsNames(i-1)
                If i < CachedColumnsNumber Or LastValueIndex >= 0 Then FormState = FormState & ";"
            Next

            For i = 0 To LastValueIndex
                If IsNull(CachedColumns(i)) Then  CachedColumns(i) = ""
                FormState = FormState & CCToHTML(CCEscapeLOV(CachedColumns(i)))
                If i < LastValueIndex Then FormState = FormState & ";"
            Next
        End If

        NewRows = IIf(InsertAllowed, EmptyRows, 0)
        GetFormState = CStr(SubmittedRows - NewRows) & ";" & CStr(NewRows)
        If Len(FormState) > 0 Then GetFormState = GetFormState & ";" & FormState

    End Function
'End GetFormState Method

'GetCachedColumns Method @76-AA749F06
    Function GetCachedColumns
        Dim FormState, i, TotalValuesNumber, TempColumns, NewCachedColumns, TempValuesNumber
        Dim NewSubmittedRows : NewSubmittedRows = 0

        NewCachedColumns = Empty
        FormState = CCGetRequestParam("FormState", ccsPost)

        If CCGetFromGet("ccsForm", Empty) = ComponentName Then
            If Not IsNull(FormState) Then
                If InStr(FormState,"\;") > 0 Then _
                    FormState = Replace(FormState, "\;", "<!--semicolon-->")
                If InStr(FormState,";") > 0 Then 
                    TempColumns = Split(FormState,";")
                    If IsArray(TempColumns) Then 
                        TempValuesNumber = UBound(TempColumns) - 1
                        If TempValuesNumber >= 0 Then
                            NewSubmittedRows = TempColumns(0)
                            NewEmptyRows     = TempColumns(1)
                        End If
                        SubmittedRows = CLng(NewSubmittedRows) + CLng(NewEmptyRows)

                        If TempValuesNumber > 1 And TempValuesNumber >= CachedColumnsNumber Then
                            ReDim NewCachedColumns(TempValuesNumber - CachedColumnsNumber + 1)
                            For i = 0 To TempValuesNumber - CachedColumnsNumber - 1
                                NewCachedColumns(i) = Replace(CCUnEscapeLOV(TempColumns(i + CachedColumnsNumber + 2)),"<!--semicolon-->",";")
                            Next
                        End If
                    End If
                Else
                    SubmittedRows = FormState
                End If
            End If
        End If

        GetCachedColumns = NewCachedColumns
    End Function
'End GetCachedColumns Method

'price_send Show Method @76-2E84F1D9
    Sub Show(Tpl)
        Dim StaticControls,RowControls

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If Recordset.State = adStateOpen Then 
            EditMode = NOT Recordset.EOF 
        Else
            EditMode = False
        End If
        IsDSEmpty = NOT EditMode

        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "price_send")
        Set TemplateBlock = Tpl.Block("EditableGrid " & ComponentName)
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        TemplateBlock.Variable("HTMLFormProperties") = "action=""" & HTMLFormAction & """ method=""post"" " & "name=""" & ComponentName & """"
        TemplateBlock.Variable("HTMLFormEnctype") = "application/x-www-form-urlencoded"

        Dim NoRecordsBlock
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If

        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_price_change_id, Sorter_ss_id, Sorter_grade_id, Sorter_levels, Sorter_ppu_actual, Sorter_price_change_type, Sorter_new_price, _
                 Navigator, Button_Submit, Cancel))
        Set RowControls = CCCreateCollection(TemplateBlock.Block("Row"), Null, ccsParseAccumulate, _
            Array(price_change_id, ss_id, grade_id, levels, ppu_actual, price_change_type, new_price, _
                 CheckBox_Delete, RowIDAttribute, RowNameAttribute, RowStyleAttribute, TextBox1))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        If Not DeleteAllowed Then CheckBox_Delete.Visible = False

        Dim i, j
        i = 1 : j = 0
        If EditMode AND ReadAllowed Then
            If Recordset.Errors.Count > 0 Then
                With TemplateBlock.Block("Error")
                    .Variable("Error") = Recordset.Errors.ToString
                    .Parse False
                End With
            ElseIf Not Recordset.EOF Then
                While Not Recordset.EoF AND (i-1) < PageSize
                    RowNumber = i
                    Attributes("rowNumber") = i
                    
                    
                    

                    If Not IsFormSubmitted Then
                        price_change_id.Value = Recordset.Fields("price_change_id")
                    Else
                        price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        ss_id.Value = Recordset.Fields("ss_id")
                    Else
                        ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        grade_id.Value = Recordset.Fields("grade_id")
                    Else
                        grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        levels.Value = Recordset.Fields("levels")
                    Else
                        levels.Text = CCGetRequestParam("levels_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        ppu_actual.Value = Recordset.Fields("ppu_actual")
                    Else
                        ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        price_change_type.Value = Recordset.Fields("price_change_type")
                    Else
                        price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        new_price.Value = Recordset.Fields("new_price")
                    Else
                        new_price.Text = CCGetRequestParam("new_price_" & CStr(i), ccsPost)
                    End If
                    If IsFormSubmitted Then 
                        CheckBox_Delete.Value = CCGetRequestParam("CheckBox_Delete_" & CStr(i), ccsPost)
                    End If
                    If IsFormSubmitted Then 
                        TextBox1.Text = CCGetRequestParam("TextBox1_" & CStr(i), ccsPost)
                    End If
                    price_change_id.ExternalName = "price_change_id_" & CStr(i)
                    ss_id.ExternalName = "ss_id_" & CStr(i)
                    grade_id.ExternalName = "grade_id_" & CStr(i)
                    levels.ExternalName = "levels_" & CStr(i)
                    ppu_actual.ExternalName = "ppu_actual_" & CStr(i)
                    price_change_type.ExternalName = "price_change_type_" & CStr(i)
                    new_price.ExternalName = "new_price_" & CStr(i)
                    CheckBox_Delete.ExternalName = "CheckBox_Delete_" & CStr(i)
                    RowIDAttribute.ExternalName = "RowIDAttribute_" & CStr(i)
                    RowNameAttribute.ExternalName = "RowNameAttribute_" & CStr(i)
                    RowStyleAttribute.ExternalName = "RowStyleAttribute_" & CStr(i)
                    TextBox1.ExternalName = "TextBox1_" & CStr(i)

                    If j >= MaxCachedValues Then
                            MaxCachedValues = MaxCachedValues + 50
                            ReDim Preserve CachedColumns(MaxCachedValues*CachedColumnsNumber)
                    End If
                    CachedColumns(j * CachedColumnsNumber) = Recordset.Recordset.Fields("price_send_id")
                    CachedColumns(j * CachedColumnsNumber + 1) = Recordset.Recordset.Fields("ss_id")
                    CachedColumns(j * CachedColumnsNumber + 2) = Recordset.Recordset.Fields("grade_id")
                    CachedColumns(j * CachedColumnsNumber + 3) = Recordset.Recordset.Fields("levels")
                    CachedColumns(j * CachedColumnsNumber + 4) = Recordset.Recordset.Fields("price_change_type")
                    CachedValuesNumber = i

                    If IsFormSubmitted Then
                        If Len(ErrorMessages(i)) > 0 Then
                            With TemplateBlock.Block("Row").Block("RowError")
                                .Variable("Error") = ErrorMessages(i)
                                .Parse False
                            End With
                        Else
                            TemplateBlock.Block("Row").Block("RowError").Visible = False
                        End If
                    End If

                    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                    Attributes.Show TemplateBlock.Block("Row"), "price_send:"
                    RowControls.Show
                    i = i + 1: j = j + 1

                    Recordset.MoveNext
                Wend
            End If
            Attributes.Show TemplateBlock, "price_send:"
            
            
            
        ElseIf Not EditMode And (Not InsertAllowed Or EmptyRows=0)Then
            NoRecordsBlock.Parse ccsParseOverwrite
        End If

        If Not InsertAllowed And Not UpdateAllowed And Not DeleteAllowed Then
            Button_Submit.Visible = False
        End If

        CheckBox_Delete.Visible = False
        price_change_id.Value = ""
        ss_id.Value = ""
        grade_id.Value = ""
        levels.Value = ""
        ppu_actual.Value = ""
        price_change_type.Value = ""
        new_price.Value = ""
        RowIDAttribute.Value = ""
        RowNameAttribute.Value = ""
        RowStyleAttribute.Value = ""
        TextBox1.Value = ""

        Dim NewRows
        NewRows = IIf(InsertAllowed, EmptyRows, 0)
        For i = i To i + NewRows - 1
            Attributes("rowNumber") = i
            price_change_id.ExternalName = "price_change_id_" & CStr(i)
            ss_id.ExternalName = "ss_id_" & CStr(i)
            grade_id.ExternalName = "grade_id_" & CStr(i)
            levels.ExternalName = "levels_" & CStr(i)
            ppu_actual.ExternalName = "ppu_actual_" & CStr(i)
            price_change_type.ExternalName = "price_change_type_" & CStr(i)
            new_price.ExternalName = "new_price_" & CStr(i)
            RowIDAttribute.ExternalName = "RowIDAttribute_" & CStr(i)
            RowNameAttribute.ExternalName = "RowNameAttribute_" & CStr(i)
            RowStyleAttribute.ExternalName = "RowStyleAttribute_" & CStr(i)
            TextBox1.ExternalName = "TextBox1_" & CStr(i)

            If IsFormSubmitted Then 
                CheckBox_Delete.Value = CCGetRequestParam("CheckBox_Delete_" & CStr(i), ccsPost)
            End If

            If IsFormSubmitted Then
                price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), ccsPost)
                ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), ccsPost)
                grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), ccsPost)
                levels.Text = CCGetRequestParam("levels_" & CStr(i), ccsPost)
                ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), ccsPost)
                price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), ccsPost)
                new_price.Text = CCGetRequestParam("new_price_" & CStr(i), ccsPost)
                RowIDAttribute.Text = CCGetRequestParam("RowIDAttribute_" & CStr(i), ccsPost)
                RowNameAttribute.Text = CCGetRequestParam("RowNameAttribute_" & CStr(i), ccsPost)
                RowStyleAttribute.Text = CCGetRequestParam("RowStyleAttribute_" & CStr(i), ccsPost)
                TextBox1.Text = CCGetRequestParam("TextBox1_" & CStr(i), ccsPost)

                If Len(ErrorMessages(i)) > 0 Then
                    With TemplateBlock.Block("Row").Block("RowError")
                        .Variable("Error") = ErrorMessages(i)
                        .Parse False
                    End With
                Else
                    TemplateBlock.Block("Row").Block("RowError").Visible = False
                End If
            End If

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
            Attributes.Show TemplateBlock.Block("Row"), "price_send:"
            RowControls.Show
        Next

        SubmittedRows = i - 1
        TemplateBlock.Variable("FormScript") = GetFormScript(i - 1)
        TemplateBlock.Variable("FormState")  = GetFormState()

        If IsFormSubmitted Then
            If Errors.Count > 0 Or DataSource.Errors.Count > 0 Then
                Errors.addErrors DataSource.Errors
                With TemplateBlock.Block("Error")
                    .Variable("Error") = Errors.ToString
                    .Parse False
                End With
            End If
        End If

        Navigator.PageSize = PageSize
        Navigator.SetDataSource Recordset
        StaticControls.Show

    End Sub
'End price_send Show Method

'price_send PageSize Property Let @76-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End price_send PageSize Property Let

'price_send PageSize Property Get @76-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End price_send PageSize Property Get

End Class 'End price_send Class @76-A61BA892

Class clsprice_sendDataSource 'price_sendDataSource Class @76-132A83E7

'DataSource Variables @76-6F5F8522
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CachedColumns
    Public CachedColumnsNumber
    Public CurrentRow
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public price_change_id
    Public ss_id
    Public grade_id
    Public levels
    Public ppu_actual
    Public price_change_type
    Public new_price
'End DataSource Variables

'DataSource Class_Initialize Event @76-E260E3EC
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set grade_id = CCCreateField("grade_id", "grade_id", ccsInteger, Empty, Recordset)
        Set levels = CCCreateField("levels", "levels", ccsInteger, Empty, Recordset)
        Set ppu_actual = CCCreateField("ppu_actual", "ppu_actual", ccsFloat, Empty, Recordset)
        Set price_change_type = CCCreateField("price_change_type", "price_change_type", ccsInteger, Empty, Recordset)
        Set new_price = CCCreateField("new_price", "new_price", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(price_change_id, ss_id, grade_id, levels, ppu_actual, price_change_type, new_price)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "price_change_id", True
        InsertOmitIfEmpty.Add "ss_id", True
        InsertOmitIfEmpty.Add "grade_id", True
        InsertOmitIfEmpty.Add "levels", True
        InsertOmitIfEmpty.Add "ppu_actual", True
        InsertOmitIfEmpty.Add "price_change_type", True
        InsertOmitIfEmpty.Add "new_price", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "price_change_id", True
        UpdateOmitIfEmpty.Add "ss_id", True
        UpdateOmitIfEmpty.Add "grade_id", True
        UpdateOmitIfEmpty.Add "levels", True
        UpdateOmitIfEmpty.Add "ppu_actual", True
        UpdateOmitIfEmpty.Add "price_change_type", True
        UpdateOmitIfEmpty.Add "new_price", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_price_change_id", "price_change_id", ""), _
            Array("Sorter_ss_id", "ss_id", ""), _
            Array("Sorter_grade_id", "grade_id", ""), _
            Array("Sorter_levels", "levels", ""), _
            Array("Sorter_ppu_actual", "ppu_actual", ""), _
            Array("Sorter_price_change_type", "price_change_type", ""), _
            Array("Sorter_new_price", "new_price", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} *  " & vbLf & _
        "FROM price_send {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM price_send"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @76-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @76-5C2A6A53
    Public Sub BuildTableWhere()
        If CurrentRow > 0 Then
            Where = "price_send_id=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber), ccsInteger) & " AND ss_id=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 1), ccsInteger) & " AND grade_id=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 2), ccsInteger) & " AND levels=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 3), ccsInteger) & " AND price_change_type=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 4), ccsInteger)
        End If
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlprice_change_id", ccsInteger, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opEqual, False, "price_change_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @76-48D41146
    Function Open(Cmd)
        Errors.Clear
        CurrentRow = 0
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        Set WhereParameters = Nothing
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @76-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @76-48969D0E
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM price_send" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @76-2D74ACA3
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id_" & CurrentRow, "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id_" & CurrentRow, "Form")
        Dim IsDef_grade_id : IsDef_grade_id = CCIsDefined("grade_id_" & CurrentRow, "Form")
        Dim IsDef_levels : IsDef_levels = CCIsDefined("levels_" & CurrentRow, "Form")
        Dim IsDef_ppu_actual : IsDef_ppu_actual = CCIsDefined("ppu_actual_" & CurrentRow, "Form")
        Dim IsDef_price_change_type : IsDef_price_change_type = CCIsDefined("price_change_type_" & CurrentRow, "Form")
        Dim IsDef_new_price : IsDef_new_price = CCIsDefined("new_price_" & CurrentRow, "Form")
        If Not UpdateOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id=" & Connection.ToSQL(price_change_id, price_change_id.DataType), Empty
        If Not UpdateOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id=" & Connection.ToSQL(ss_id, ss_id.DataType), Empty
        If Not UpdateOmitIfEmpty("grade_id") Or IsDef_grade_id Then Cmd.AddSQLStrings "grade_id=" & Connection.ToSQL(grade_id, grade_id.DataType), Empty
        If Not UpdateOmitIfEmpty("levels") Or IsDef_levels Then Cmd.AddSQLStrings "levels=" & Connection.ToSQL(levels, levels.DataType), Empty
        If Not UpdateOmitIfEmpty("ppu_actual") Or IsDef_ppu_actual Then Cmd.AddSQLStrings "ppu_actual=" & Connection.ToSQL(ppu_actual, ppu_actual.DataType), Empty
        If Not UpdateOmitIfEmpty("price_change_type") Or IsDef_price_change_type Then Cmd.AddSQLStrings "price_change_type=" & Connection.ToSQL(price_change_type, price_change_type.DataType), Empty
        If Not UpdateOmitIfEmpty("new_price") Or IsDef_new_price Then Cmd.AddSQLStrings "new_price=" & Connection.ToSQL(new_price, new_price.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "price_send", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @76-07863575
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id_" & CurrentRow, "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id_" & CurrentRow, "Form")
        Dim IsDef_grade_id : IsDef_grade_id = CCIsDefined("grade_id_" & CurrentRow, "Form")
        Dim IsDef_levels : IsDef_levels = CCIsDefined("levels_" & CurrentRow, "Form")
        Dim IsDef_ppu_actual : IsDef_ppu_actual = CCIsDefined("ppu_actual_" & CurrentRow, "Form")
        Dim IsDef_price_change_type : IsDef_price_change_type = CCIsDefined("price_change_type_" & CurrentRow, "Form")
        Dim IsDef_new_price : IsDef_new_price = CCIsDefined("new_price_" & CurrentRow, "Form")
        If Not InsertOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id", Connection.ToSQL(price_change_id, price_change_id.DataType)
        If Not InsertOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id", Connection.ToSQL(ss_id, ss_id.DataType)
        If Not InsertOmitIfEmpty("grade_id") Or IsDef_grade_id Then Cmd.AddSQLStrings "grade_id", Connection.ToSQL(grade_id, grade_id.DataType)
        If Not InsertOmitIfEmpty("levels") Or IsDef_levels Then Cmd.AddSQLStrings "levels", Connection.ToSQL(levels, levels.DataType)
        If Not InsertOmitIfEmpty("ppu_actual") Or IsDef_ppu_actual Then Cmd.AddSQLStrings "ppu_actual", Connection.ToSQL(ppu_actual, ppu_actual.DataType)
        If Not InsertOmitIfEmpty("price_change_type") Or IsDef_price_change_type Then Cmd.AddSQLStrings "price_change_type", Connection.ToSQL(price_change_type, price_change_type.DataType)
        If Not InsertOmitIfEmpty("new_price") Or IsDef_new_price Then Cmd.AddSQLStrings "new_price", Connection.ToSQL(new_price, new_price.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "price_send", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End price_sendDataSource Class @76-A61BA892

Class clsGridGrid3 'Grid3 Class @100-20B662CC

'Grid3 Variables @100-3B295878

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_price_change_id
    Dim Sorter_SiteName
    Dim Sorter_Grade
    Dim Sorter_levels
    Dim Sorter_ppu_actual
    Dim Sorter_change_type
    Dim Sorter_new_price
    Dim price_change_id
    Dim SiteName
    Dim Grade
    Dim levels
    Dim ppu_actual
    Dim change_type
    Dim new_price
    Dim Navigator
'End Grid3 Variables

'Grid3 Class_Initialize Event @100-0E166C27
    Private Sub Class_Initialize()
        ComponentName = "Grid3"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsGrid3DataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Grid3Order", Empty)
        SortingDirection = CCGetParam("Grid3Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_price_change_id = CCCreateSorter("Sorter_price_change_id", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Grade = CCCreateSorter("Sorter_Grade", Me, FileName)
        Set Sorter_levels = CCCreateSorter("Sorter_levels", Me, FileName)
        Set Sorter_ppu_actual = CCCreateSorter("Sorter_ppu_actual", Me, FileName)
        Set Sorter_change_type = CCCreateSorter("Sorter_change_type", Me, FileName)
        Set Sorter_new_price = CCCreateSorter("Sorter_new_price", Me, FileName)
        Set price_change_id = CCCreateControl(ccsLabel, "price_change_id", Empty, ccsInteger, Empty, CCGetRequestParam("price_change_id", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Grade = CCCreateControl(ccsLabel, "Grade", Empty, ccsText, Empty, CCGetRequestParam("Grade", ccsGet))
        Set levels = CCCreateControl(ccsLabel, "levels", Empty, ccsInteger, Empty, CCGetRequestParam("levels", ccsGet))
        Set ppu_actual = CCCreateControl(ccsLabel, "ppu_actual", Empty, ccsFloat, Empty, CCGetRequestParam("ppu_actual", ccsGet))
        Set change_type = CCCreateControl(ccsLabel, "change_type", Empty, ccsText, Empty, CCGetRequestParam("change_type", ccsGet))
        Set new_price = CCCreateControl(ccsLabel, "new_price", Empty, ccsFloat, Empty, CCGetRequestParam("new_price", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Grid3 Class_Initialize Event

'Grid3 Initialize Method @100-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Grid3 Initialize Method

'Grid3 Class_Terminate Event @100-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Grid3 Class_Terminate Event

'Grid3 Show Method @100-21F4C68B
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urlprice_change_id") = CCGetRequestParam("price_change_id", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_price_change_id, Sorter_SiteName, Sorter_Grade, Sorter_levels, Sorter_ppu_actual, Sorter_change_type, Sorter_new_price, Navigator))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(price_change_id, SiteName, Grade, levels, ppu_actual, change_type, new_price))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Grid3:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    price_change_id.Value = Recordset.Fields("price_change_id")
                    SiteName.Value = Recordset.Fields("SiteName")
                    Grade.Value = Recordset.Fields("Grade")
                    levels.Value = Recordset.Fields("levels")
                    ppu_actual.Value = Recordset.Fields("ppu_actual")
                    change_type.Value = Recordset.Fields("change_type")
                    new_price.Value = Recordset.Fields("new_price")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Grid3:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Grid3:"
            StaticControls.Show
        End If

    End Sub
'End Grid3 Show Method

'Grid3 PageSize Property Let @100-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Grid3 PageSize Property Let

'Grid3 PageSize Property Get @100-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Grid3 PageSize Property Get

'Grid3 RowNumber Property Get @100-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Grid3 RowNumber Property Get

'Grid3 HasNextRow Function @100-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Grid3 HasNextRow Function

End Class 'End Grid3 Class @100-A61BA892

Class clsGrid3DataSource 'Grid3DataSource Class @100-85956FA2

'DataSource Variables @100-57953EF2
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public price_change_id
    Public SiteName
    Public Grade
    Public levels
    Public ppu_actual
    Public change_type
    Public new_price
'End DataSource Variables

'DataSource Class_Initialize Event @100-1A6117E1
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Grade = CCCreateField("Grade", "Grade", ccsText, Empty, Recordset)
        Set levels = CCCreateField("levels", "levels", ccsInteger, Empty, Recordset)
        Set ppu_actual = CCCreateField("ppu_actual", "ppu_actual", ccsFloat, Empty, Recordset)
        Set change_type = CCCreateField("change_type", "change_type", ccsText, Empty, Recordset)
        Set new_price = CCCreateField("new_price", "new_price", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(price_change_id, SiteName, Grade, levels, ppu_actual, change_type, new_price)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_price_change_id", "price_change_id", ""), _
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Grade", "Grade", ""), _
            Array("Sorter_levels", "levels", ""), _
            Array("Sorter_ppu_actual", "ppu_actual", ""), _
            Array("Sorter_change_type", "change_type", ""), _
            Array("Sorter_new_price", "new_price", ""))

        SQL = "select p.price_change_id, s.SiteName, g.grade_name as Grade, p.levels, p.ppu_actual, t.price_change_description as change_type, p.new_price " & vbLf & _
        "from (((price_send as p inner join sites as s on p.ss_id = s.ss_id) inner join grades as g on p.grade_id = g.grade_id) inner join price_type as t on p.price_change_type = t.price_change_type) " & vbLf & _
        "where p.price_change_id = '{price_change_id}'"
        CountSQL = "SELECT COUNT(*) FROM (select p.price_change_id, s.SiteName, g.grade_name as Grade, p.levels, p.ppu_actual, t.price_change_description as change_type, p.new_price " & vbLf & _
        "from (((price_send as p inner join sites as s on p.ss_id = s.ss_id) inner join grades as g on p.grade_id = g.grade_id) inner join price_type as t on p.price_change_type = t.price_change_type) " & vbLf & _
        "where p.price_change_id = '{price_change_id}') cnt"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @100-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @100-83EA6348
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "price_change_id", "urlprice_change_id", ccsText, Empty, Empty, Empty, False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @100-CA87DA7C
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @100-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Grid3DataSource Class @100-A61BA892

Class clsRecordSearch_Sites 'Search_Sites Class @151-7E557A5C

'Search_Sites Variables @151-E7B8D3F1

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim s_SiteName
    Dim ClearParameters
    Dim Button_DoSearch
'End Search_Sites Variables

'Search_Sites Class_Initialize Event @151-4E55C172
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsSearch_SitesDataSource
        Set Command = New clsCommand
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Search_Sites")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Search_Sites"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set s_SiteName = CCCreateControl(ccsTextBox, "s_SiteName", Empty, ccsText, Empty, CCGetRequestParam("s_SiteName", Method))
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_SiteName)
    End Sub
'End Search_Sites Class_Initialize Event

'Search_Sites Initialize Method @151-36D27969
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
        End With
    End Sub
'End Search_Sites Initialize Method

'Search_Sites Class_Terminate Event @151-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Search_Sites Class_Terminate Event

'Search_Sites Validate Method @151-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Search_Sites Validate Method

'Search_Sites Operation Method @151-B9CBAB19
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = FileName & ""
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = FileName & "?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Search_Sites Operation Method

'Search_Sites Show Method @151-1DE7CB63
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Search_Sites" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_SiteName, ClearParameters, Button_DoSearch))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        
                    End If
                Else
                    EditMode = False
                End If
            End If
            If EditMode Then
                
            End If
        End If
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        ClearParameters.Page = "send_price.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_SiteName.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Search_Sites" & ":"
            Controls.Show
        End If
    End Sub
'End Search_Sites Show Method

End Class 'End Search_Sites Class @151-A61BA892

Class clsSearch_SitesDataSource 'Search_SitesDataSource Class @151-9EB917DC

'DataSource Variables @151-51A7ED8F
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
'End DataSource Variables

'DataSource Class_Initialize Event @151-4365E4B8
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Fields.AddFields Array()

        SQL = "SELECT *  " & vbLf & _
        "FROM Sites {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @151-98E5A92F
    Public Sub BuildTableWhere()
    End Sub
'End BuildTableWhere Method

'Open Method @151-5DD0F01A
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @151-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Search_SitesDataSource Class @151-A61BA892





'Include Page Implementation @2-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
