<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-4E5B7776
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Report1
Dim Report_Print
Dim Report2
Dim Header
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "ReportByAlarms.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "ReportByAlarms.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-EB0F5D0E
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Report1 = New clsReportReport1
Set Report_Print = CCCreateControl(ccsLink, "Report_Print", Empty, ccsText, Empty, CCGetRequestParam("Report_Print", ccsGet))
Set Report2 = new clsRecordReport2
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""

Report_Print.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
Report_Print.Parameters = CCAddParam(Report_Print.Parameters, "ViewMode", "Print")
Report_Print.Page = "ReportByAlarms.asp"
Report1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/ReportByAlarms_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-34DA6FEB
Report2.Operation
Header.Operations
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-BCB1D4FA
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Report1, Report_Print, Report2, Header))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-320E5398
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set Report1 = Nothing
    Set Report2 = Nothing
    Header.UnloadPage
    Set Header = Nothing
End Sub
'End UnloadPage Sub

'Report1 clsReportGroup @2-065158D7
Class clsReportGroupReport1
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public Site_Name
    Public Alarm_ID
    Public Type1
    Public Status
    Public Tank
    Public Grade
    Public Date1
    Public Time1
    Public Users
    Public Source1
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public Site_NameTotalIndex

    Public Sub SetControls()
        Me.Site_Name = Report1.Site_Name.Value
        Me.Alarm_ID = Report1.Alarm_ID.Value
        Me.Type1 = Report1.Type1.Value
        Me.Status = Report1.Status.Value
        Me.Tank = Report1.Tank.Value
        Me.Grade = Report1.Grade.Value
        Me.Date1 = Report1.Date1.Value
        Me.Time1 = Report1.Time1.Value
        Me.Users = Report1.Users.Value
        Me.Source1 = Report1.Source1.Value
        Me.Report_CurrentDate = Report1.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.Site_Name = HeaderGrp.Site_Name
        Report1.Site_Name.ChangeValue(Me.Site_Name)
        Me.Alarm_ID = HeaderGrp.Alarm_ID
        Report1.Alarm_ID.ChangeValue(Me.Alarm_ID)
        Me.Type1 = HeaderGrp.Type1
        Report1.Type1.ChangeValue(Me.Type1)
        Me.Status = HeaderGrp.Status
        Report1.Status.ChangeValue(Me.Status)
        Me.Tank = HeaderGrp.Tank
        Report1.Tank.ChangeValue(Me.Tank)
        Me.Grade = HeaderGrp.Grade
        Report1.Grade.ChangeValue(Me.Grade)
        Me.Date1 = HeaderGrp.Date1
        Report1.Date1.ChangeValue(Me.Date1)
        Me.Time1 = HeaderGrp.Time1
        Report1.Time1.ChangeValue(Me.Time1)
        Me.Users = HeaderGrp.Users
        Report1.Users.ChangeValue(Me.Users)
        Me.Source1 = HeaderGrp.Source1
        Report1.Source1.ChangeValue(Me.Source1)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        Report1.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = Report1.Report_TotalRecords.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = Report1.Report_TotalRecords.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End Report1 clsReportGroup

'clsReport1GroupsCollection @2-541655F7
Class clsReport1GroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSite_NameCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSite_NameCurrentHeaderIndex = 2
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGroupReport1
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.Site_NameTotalIndex = mSite_NameCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + Report1.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + Report1.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "Site_Name" Then
            If PageSize > 0 And Report1.Site_Name_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Site_Name_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mSite_NameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Site_Name"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And Report1.Report_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And Report1.Site_Name_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Site_Name_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSite_NameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Site_Name"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        Report1.Report_TotalRecords.Value = Report1.Report_TotalRecords.InitialValue
        Report1.Site_Name.Value = Report1.Site_Name.InitialValue
        Report1.Alarm_ID.Value = Report1.Alarm_ID.InitialValue
        Report1.Type1.Value = Report1.Type1.InitialValue
        Report1.Status.Value = Report1.Status.InitialValue
        Report1.Tank.Value = Report1.Tank.InitialValue
        Report1.Grade.Value = Report1.Grade.InitialValue
        Report1.Date1.Value = Report1.Date1.InitialValue
        Report1.Time1.Value = Report1.Time1.InitialValue
        Report1.Users.Value = Report1.Users.InitialValue
        Report1.Source1.Value = Report1.Source1.InitialValue
        Report1.Report_CurrentDate.Value = Report1.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And Report1.Detail.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clsReport1GroupsCollection

Class clsReportReport1 'Report1 Class @2-D191C334

'Report1 Variables @2-FC5EB65A

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public Site_Name_HeaderBlock, Site_Name_Header
    Public Site_Name_FooterBlock, Site_Name_Footer
    Public ActiveSorter, SortingDirection
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public Site_Name_HeaderControls, Site_Name_FooterControls
    Dim Report_TotalRecords
    Dim Sorter_Alarm_ID
    Dim Sorter_Type
    Dim Sorter_Status
    Dim Sorter_Tank
    Dim Sorter_Grade
    Dim Sorter_Date
    Dim Sorter_Time
    Dim Sorter_Users
    Dim Sorter_Source
    Dim Site_Name
    Dim Alarm_ID
    Dim Type1
    Dim Status
    Dim Tank
    Dim Grade
    Dim Date1
    Dim Time1
    Dim Users
    Dim Source1
    Dim NoRecords
    Dim PageBreak
    Dim Report_CurrentDate
    Dim Navigator
'End Report1 Variables

'Report1 Class_Initialize Event @2-202CDBF5
    Private Sub Class_Initialize()
        ComponentName = "Report1"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 2
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set Site_Name_Footer = new clsSection
        Site_Name_Footer.Visible = True
        Site_Name_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Footer.Height)
        Set Site_Name_Header = new clsSection
        Site_Name_Header.Visible = True
        Site_Name_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clsReport1DataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If
        ActiveSorter = CCGetParam("Report1Order", Empty)
        SortingDirection = CCGetParam("Report1Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Sorter_Alarm_ID = CCCreateSorter("Sorter_Alarm_ID", Me, FileName)
        Set Sorter_Type = CCCreateSorter("Sorter_Type", Me, FileName)
        Set Sorter_Status = CCCreateSorter("Sorter_Status", Me, FileName)
        Set Sorter_Tank = CCCreateSorter("Sorter_Tank", Me, FileName)
        Set Sorter_Grade = CCCreateSorter("Sorter_Grade", Me, FileName)
        Set Sorter_Date = CCCreateSorter("Sorter_Date", Me, FileName)
        Set Sorter_Time = CCCreateSorter("Sorter_Time", Me, FileName)
        Set Sorter_Users = CCCreateSorter("Sorter_Users", Me, FileName)
        Set Sorter_Source = CCCreateSorter("Sorter_Source", Me, FileName)
        Set Site_Name = CCCreateReportLabel( "Site_Name", Empty, ccsText, Empty, CCGetRequestParam("Site_Name", ccsGet), "",  False, False,"")
        Set Alarm_ID = CCCreateReportLabel( "Alarm_ID", Empty, ccsInteger, Empty, CCGetRequestParam("Alarm_ID", ccsGet), "",  False, False,"")
        Set Type1 = CCCreateReportLabel( "Type1", Empty, ccsText, Empty, CCGetRequestParam("Type1", ccsGet), "",  False, False,"")
        Set Status = CCCreateReportLabel( "Status", Empty, ccsText, Empty, CCGetRequestParam("Status", ccsGet), "",  False, False,"")
        Set Tank = CCCreateReportLabel( "Tank", Empty, ccsInteger, Empty, CCGetRequestParam("Tank", ccsGet), "",  False, False,"")
        Set Grade = CCCreateReportLabel( "Grade", Empty, ccsText, Empty, CCGetRequestParam("Grade", ccsGet), "",  False, False,"")
        Set Date1 = CCCreateReportLabel( "Date1", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Date1", ccsGet), "",  False, False,"")
        Set Time1 = CCCreateReportLabel( "Time1", Empty, ccsText, Empty, CCGetRequestParam("Time1", ccsGet), "",  False, False,"")
        Set Users = CCCreateReportLabel( "Users", Empty, ccsText, Empty, CCGetRequestParam("Users", ccsGet), "",  False, False,"")
        Set Source1 = CCCreateReportLabel( "Source1", Empty, ccsText, Empty, CCGetRequestParam("Source1", ccsGet), "",  False, False,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set PageBreak = CCCreatePanel("PageBreak")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End Report1 Class_Initialize Event

'Report1 Initialize Method @2-BD78A2B0
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.SetOrder ActiveSorter, SortingDirection
    End Sub
'End Report1 Initialize Method

'Report1 Class_Terminate Event @2-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End Report1 Class_Terminate Event

'Report1 Show Method @2-9E24B4FA
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("urlsite_name") = CCGetRequestParam("site_name", ccsGET)
            .Parameters("urlgrade") = CCGetRequestParam("grade", ccsGET)
            .Parameters("expr56") = report2.s_DAY_DATE.value
            .Parameters("urlalarm_type") = CCGetRequestParam("alarm_type", ccsGET)
            .Parameters("expr64") = Report2.s_DAY_DATE1.value
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set Site_Name_HeaderBlock = TemplateBlock.Block("Section Site_Name_Header")
        Set Site_Name_FooterBlock = TemplateBlock.Block("Section Site_Name_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(Alarm_ID, Type1, Status, Tank, Grade, Date1, Time1, Users, Source1))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(PageBreak, Report_CurrentDate, Navigator))
        Set Page_HeaderControls = CCCreateCollection(Page_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sorter_Alarm_ID, Sorter_Type, Sorter_Status, Sorter_Tank, Sorter_Grade, Sorter_Date, Sorter_Time, Sorter_Users, Sorter_Source))
        Set Site_Name_HeaderControls = CCCreateCollection(Site_Name_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Site_Name))
        Set Site_Name_FooterControls = CCCreateCollection(Site_Name_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array())
        Dim Site_NameKey
        Dim Groups
        Set Groups = New clsReport1GroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report Report1", Errors)
        Else
            Do While Not Recordset.EOF
                Site_Name.Value = Recordset.Fields("Site_Name")
                Alarm_ID.Value = Recordset.Fields("Alarm_ID")
                Type1.Value = Recordset.Fields("Type1")
                Status.Value = Recordset.Fields("Status")
                Tank.Value = Recordset.Fields("Tank")
                Grade.Value = Recordset.Fields("Grade")
                Date1.Value = Recordset.Fields("Date1")
                Time1.Value = Recordset.Fields("Time1")
                Users.Value = Recordset.Fields("Users")
                Source1.Value = Recordset.Fields("Source1")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or Site_NameKey <> Recordset.Fields("Site_Name") Then
                    Groups.OpenGroup "Site_Name"
                End If
                Groups.AddItem 
                Site_NameKey = Recordset.Fields("Site_Name")
                Recordset.MoveNext
                If Site_NameKey <> Recordset.Fields("Site_Name") Or Recordset.EOF Then
                    Groups.CloseGroup "Site_Name"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        Alarm_ID.Value = items(i).Alarm_ID
                        Type1.Value = items(i).Type1
                        Status.Value = items(i).Status
                        Tank.Value = items(i).Tank
                        Grade.Value = items(i).Grade
                        Date1.Value = items(i).Date1
                        Time1.Value = items(i).Time1
                        Users.Value = items(i).Users
                        Source1.Value = items(i).Source1
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                PageBreak.Visible = (i < EndItem-1 And ViewMode <> "Web")
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        Navigator.PageSize = PageSize
                        Navigator.PagesCount = Groups.TotalPages
                        Navigator.PageNumber = items(i).PageNumber
                        Navigator.Visible = ("Web" = ViewMode)
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderControls.Show
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "Site_Name"
                        Site_Name.Value = items(i).Site_Name
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Header_BeforeShow", Me)
                            If Site_Name_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_HeaderBlock, AttributePrefix
                                Site_Name_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Footer_BeforeShow", Me)
                            If Site_Name_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_FooterBlock, AttributePrefix
                                Site_Name_FooterBlock.ParseTo ccsParseAccumulate, DetailBlock
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End Report1 Show Method

'Report1 PageSize Property Let @2-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Report1 PageSize Property Let

'Report1 PageSize Property Get @2-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Report1 PageSize Property Get

End Class 'End Report1 Class @2-A61BA892

Class clsReport1DataSource 'Report1DataSource Class @2-1489A126

'DataSource Variables @2-2E262673
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Site_Name
    Public Alarm_ID
    Public Type1
    Public Status
    Public Tank
    Public Grade
    Public Date1
    Public Time1
    Public Users
    Public Source1
'End DataSource Variables

'DataSource Class_Initialize Event @2-1E43D0F9
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Site_Name = CCCreateField("Site_Name", "Site_Name", ccsText, Empty, Recordset)
        Set Alarm_ID = CCCreateField("Alarm_ID", "Alarm_ID", ccsInteger, Empty, Recordset)
        Set Type1 = CCCreateField("Type1", "Type", ccsText, Empty, Recordset)
        Set Status = CCCreateField("Status", "Status", ccsText, Empty, Recordset)
        Set Tank = CCCreateField("Tank", "Tank", ccsInteger, Empty, Recordset)
        Set Grade = CCCreateField("Grade", "Grade", ccsText, Empty, Recordset)
        Set Date1 = CCCreateField("Date1", "Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Time1 = CCCreateField("Time1", "Time", ccsText, Empty, Recordset)
        Set Users = CCCreateField("Users", "Users", ccsText, Empty, Recordset)
        Set Source1 = CCCreateField("Source1", "Source", ccsText, Empty, Recordset)
        Fields.AddFields Array(Site_Name,  Alarm_ID,  Type1,  Status,  Tank,  Grade,  Date1, _
             Time1,  Users,  Source1)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_Alarm_ID", "Alarm_ID", ""), _
            Array("Sorter_Type", "Type", ""), _
            Array("Sorter_Status", "Status", ""), _
            Array("Sorter_Tank", "Tank", ""), _
            Array("Sorter_Grade", "Grade", ""), _
            Array("Sorter_Date", "Date", ""), _
            Array("Sorter_Time", "Time", ""), _
            Array("Sorter_Users", "Users", ""), _
            Array("Sorter_Source", "Source", ""))

        SQL = " " & vbLf & _
        "select s.SiteName as Site_Name, t.alarm_id as Alarm_ID, t.alarm_type as Type, t.alarm_status as Status, t.tank_id as Tank, g.grade_name as Grade, t.DAY_DATE as Date, t.DAY_TIME as Time, t.ack_user as Users, t.alr_source as Source  " & vbLf & _
        "from (sites as s join tank_alarms_history as t on s.ss_id = t.ss_id) join grades as g on t.grade_id = g.grade_id " & vbLf & _
        "where siteName like '%{site_name}%' and grade_name like '%{grade}%' and alarm_type like '%{alarm_type}%' and DAY_DATE >= '{s_DAY_DATE}' and DAY_DATE <= '{s_DAY_DATE1}'"
        CountSQL = "SELECT COUNT(*) FROM ( " & vbLf & _
        "select s.SiteName as Site_Name, t.alarm_id as Alarm_ID, t.alarm_type as Type, t.alarm_status as Status, t.tank_id as Tank, g.grade_name as Grade, t.DAY_DATE as Date, t.DAY_TIME as Time, t.ack_user as Users, t.alr_source as Source  " & vbLf & _
        "from (sites as s join tank_alarms_history as t on s.ss_id = t.ss_id) join grades as g on t.grade_id = g.grade_id " & vbLf & _
        "where siteName like '%{site_name}%' and grade_name like '%{grade}%' and alarm_type like '%{alarm_type}%' and DAY_DATE >= '{s_DAY_DATE}' and DAY_DATE <= '{s_DAY_DATE1}') cnt"
        Where = ""
        Order = ""
        StaticOrder = "Site_Name asc"
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @2-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @2-D9B60D8F
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "site_name", "urlsite_name", ccsText, Empty, Empty, Empty, False
            .AddParameter "grade", "urlgrade", ccsText, Empty, Empty, Empty, False
            .AddParameter "s_DAY_DATE", "expr56", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), dateadd("d", -10,date()), False
            .AddParameter "alarm_type", "urlalarm_type", ccsText, Empty, Empty, Empty, False
            .AddParameter "s_DAY_DATE1", "expr64", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), date(), False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @2-486FAD2E
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Report1DataSource Class @2-A61BA892

Class clsRecordReport2 'Report2 Class @39-0337016F

'Report2 Variables @39-E7091E06

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim ClearParameters
    Dim Button_DoSearch
    Dim site_name
    Dim grade
    Dim s_DAY_DATE
    Dim DatePicker_s_DAY_DATE
    Dim alarm_type
    Dim s_DAY_DATE1
    Dim DatePicker_s_DAY_DATE1
'End Report2 Variables

'Report2 Class_Initialize Event @39-8EFC2B52
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Report2")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Report2"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set site_name = CCCreateList(ccsListBox, "site_name", Empty, ccsText, CCGetRequestParam("site_name", Method), Empty)
        site_name.BoundColumn = "SiteName"
        site_name.TextColumn = "SiteName"
        Set site_name.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT SiteName  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
        Set grade = CCCreateList(ccsListBox, "grade", Empty, ccsText, CCGetRequestParam("grade", Method), Empty)
        grade.BoundColumn = "grade_name"
        grade.TextColumn = "grade_name"
        Set grade.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT grade_name  " & _
"FROM Grades {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_DAY_DATE = CCCreateControl(ccsTextBox, "s_DAY_DATE", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_DAY_DATE", Method))
        Set DatePicker_s_DAY_DATE = CCCreateDatePicker("DatePicker_s_DAY_DATE", "Report2", "s_DAY_DATE")
        Set alarm_type = CCCreateList(ccsListBox, "alarm_type", Empty, ccsText, CCGetRequestParam("alarm_type", Method), Empty)
        alarm_type.BoundColumn = "alarm_type"
        alarm_type.TextColumn = "alarm_type"
        Set alarm_type.DataSource = CCCreateDataSource(dsSQL, DBFusionHO, "select distinct(alarm_type) from tank_alarms_history")
        Set s_DAY_DATE1 = CCCreateControl(ccsTextBox, "s_DAY_DATE1", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_DAY_DATE1", Method))
        Set DatePicker_s_DAY_DATE1 = CCCreateDatePicker("DatePicker_s_DAY_DATE1", "Report2", "s_DAY_DATE1")
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(site_name, grade, s_DAY_DATE, alarm_type, s_DAY_DATE1)
    End Sub
'End Report2 Class_Initialize Event

'Report2 Class_Terminate Event @39-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Report2 Class_Terminate Event

'Report2 Validate Method @39-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Report2 Validate Method

'Report2 Operation Method @39-4C73D33A
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "ReportByAlarms.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "ReportByAlarms.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Report2 Operation Method

'Report2 Show Method @39-CB40AD33
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Report2" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(site_name, alarm_type, grade, s_DAY_DATE, DatePicker_s_DAY_DATE, s_DAY_DATE1, DatePicker_s_DAY_DATE1, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("site_name", "grade", "alarm_type", "s_DAY_DATE", "s_DAY_DATE1", "ccsForm"))
        ClearParameters.Page = "ReportByAlarms.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors site_name.Errors
            Errors.AddErrors grade.Errors
            Errors.AddErrors s_DAY_DATE.Errors
            Errors.AddErrors alarm_type.Errors
            Errors.AddErrors s_DAY_DATE1.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Report2" & ":"
            Controls.Show
        End If
    End Sub
'End Report2 Show Method

End Class 'End Report2 Class @39-A61BA892

'Include Page Implementation @57-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
