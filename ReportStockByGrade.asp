<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-0055FB6C
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Header
Dim Report1
Dim Report_Print
Dim Report2
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "ReportStockByGrade.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "ReportStockByGrade.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-2477ACA4
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Report1 = New clsReportReport1
Set Report_Print = CCCreateControl(ccsLink, "Report_Print", Empty, ccsText, Empty, CCGetRequestParam("Report_Print", ccsGet))
Set Report2 = new clsRecordReport2

Report_Print.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
Report_Print.Parameters = CCAddParam(Report_Print.Parameters, "ViewMode", "Print")
Report_Print.Page = "ReportStockByGrade.asp"
Report1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/ReportStockByGrade_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-BEAC6822
Header.Operations
Report2.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-57506F0A
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Header, Report1, Report_Print, Report2))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-F2FD4056
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Set Report1 = Nothing
    Set Report2 = Nothing
End Sub
'End UnloadPage Sub

'Report1 clsReportGroup @3-CC588FD0
Class clsReportGroupReport1
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public Site_Name
    Public Grade
    Public Hour1
    Public Grade_Volume
    Public Water_Volume
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public Site_NameTotalIndex
    Public GradeTotalIndex

    Public Sub SetControls()
        Me.Site_Name = Report1.Site_Name.Value
        Me.Grade = Report1.Grade.Value
        Me.Hour1 = Report1.Hour1.Value
        Me.Grade_Volume = Report1.Grade_Volume.Value
        Me.Water_Volume = Report1.Water_Volume.Value
        Me.Report_CurrentDate = Report1.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.Site_Name = HeaderGrp.Site_Name
        Report1.Site_Name.ChangeValue(Me.Site_Name)
        Me.Grade = HeaderGrp.Grade
        Report1.Grade.ChangeValue(Me.Grade)
        Me.Hour1 = HeaderGrp.Hour1
        Report1.Hour1.ChangeValue(Me.Hour1)
        Me.Grade_Volume = HeaderGrp.Grade_Volume
        Report1.Grade_Volume.ChangeValue(Me.Grade_Volume)
        Me.Water_Volume = HeaderGrp.Water_Volume
        Report1.Water_Volume.ChangeValue(Me.Water_Volume)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        Report1.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = Report1.Report_TotalRecords.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = Report1.Report_TotalRecords.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End Report1 clsReportGroup

'clsReport1GroupsCollection @3-3DB32A38
Class clsReport1GroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSite_NameCurrentHeaderIndex
    Private mGradeCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSite_NameCurrentHeaderIndex = 2
        mGradeCurrentHeaderIndex = 3
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGroupReport1
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.Site_NameTotalIndex = mSite_NameCurrentHeaderIndex
        group.GradeTotalIndex = mGradeCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + Report1.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + Report1.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "Site_Name" Then
            If PageSize > 0 And Report1.Site_Name_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Site_Name_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            OpenFlag = True
            mSite_NameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Site_Name"
            Groups.Add Groups.Count,Group
        End If
        If groupName = "Grade" Or OpenFlag Then
            If PageSize > 0 And Report1.Grade_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Grade_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Grade_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Grade_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mGradeCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Grade"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And Report1.Report_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And Report1.Grade_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Grade_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Grade_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mGradeCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Grade_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Grade"
        Groups.Add Groups.Count,Group
        If groupName = "Grade" Then Exit Sub
        If PageSize > 0 And Report1.Site_Name_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Site_Name_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSite_NameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Site_Name"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        Report1.Report_TotalRecords.Value = Report1.Report_TotalRecords.InitialValue
        Report1.Site_Name.Value = Report1.Site_Name.InitialValue
        Report1.Grade.Value = Report1.Grade.InitialValue
        Report1.Hour1.Value = Report1.Hour1.InitialValue
        Report1.Grade_Volume.Value = Report1.Grade_Volume.InitialValue
        Report1.Water_Volume.Value = Report1.Water_Volume.InitialValue
        Report1.Report_CurrentDate.Value = Report1.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And Report1.Detail.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clsReport1GroupsCollection

Class clsReportReport1 'Report1 Class @3-D191C334

'Report1 Variables @3-34581C44

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public Site_Name_HeaderBlock, Site_Name_Header
    Public Site_Name_FooterBlock, Site_Name_Footer
    Public Grade_HeaderBlock, Grade_Header
    Public Grade_FooterBlock, Grade_Footer
    Public ActiveSorter, SortingDirection
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public Site_Name_HeaderControls, Site_Name_FooterControls
    Public Grade_HeaderControls, Grade_FooterControls
    Dim Report_TotalRecords
    Dim Sorter_Hour
    Dim Sorter_Grade_Volume
    Dim Sorter_Water_Volume
    Dim Site_Name
    Dim Grade
    Dim Hour1
    Dim Grade_Volume
    Dim Water_Volume
    Dim NoRecords
    Dim PageBreak
    Dim Report_CurrentDate
    Dim Navigator
'End Report1 Variables

'Report1 Class_Initialize Event @3-EA19FB29
    Private Sub Class_Initialize()
        ComponentName = "Report1"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 2
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set Site_Name_Footer = new clsSection
        Site_Name_Footer.Visible = True
        Site_Name_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Footer.Height)
        Set Site_Name_Header = new clsSection
        Site_Name_Header.Visible = True
        Site_Name_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Header.Height)
        Set Grade_Footer = new clsSection
        Grade_Footer.Visible = True
        Grade_Footer.Height = 0
        MaxSectionSize = Max(MaxSectionSize, Grade_Footer.Height)
        Set Grade_Header = new clsSection
        Grade_Header.Visible = True
        Grade_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Grade_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clsReport1DataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If
        ActiveSorter = CCGetParam("Report1Order", Empty)
        SortingDirection = CCGetParam("Report1Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Sorter_Hour = CCCreateSorter("Sorter_Hour", Me, FileName)
        Set Sorter_Grade_Volume = CCCreateSorter("Sorter_Grade_Volume", Me, FileName)
        Set Sorter_Water_Volume = CCCreateSorter("Sorter_Water_Volume", Me, FileName)
        Set Site_Name = CCCreateReportLabel( "Site_Name", Empty, ccsText, Empty, CCGetRequestParam("Site_Name", ccsGet), "",  False, False,"")
        Set Grade = CCCreateReportLabel( "Grade", Empty, ccsText, Empty, CCGetRequestParam("Grade", ccsGet), "",  False, False,"")
        Set Hour1 = CCCreateReportLabel( "Hour1", Empty, ccsInteger, Empty, CCGetRequestParam("Hour1", ccsGet), "",  False, False,"")
        Set Grade_Volume = CCCreateReportLabel( "Grade_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Grade_Volume", ccsGet), "",  False, False,"")
        Set Water_Volume = CCCreateReportLabel( "Water_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Water_Volume", ccsGet), "",  False, False,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set PageBreak = CCCreatePanel("PageBreak")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End Report1 Class_Initialize Event

'Report1 Initialize Method @3-BD78A2B0
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.SetOrder ActiveSorter, SortingDirection
    End Sub
'End Report1 Initialize Method

'Report1 Class_Terminate Event @3-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End Report1 Class_Terminate Event

'Report1 Show Method @3-06D83FC9
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("expr43") = Report2.s_DAY_DATE.value
            .Parameters("urls_DAY_TIME") = CCGetRequestParam("s_DAY_TIME", ccsGET)
            .Parameters("urls_DAY_TIME1") = CCGetRequestParam("s_DAY_TIME1", ccsGET)
            .Parameters("urls_Site_Name") = CCGetRequestParam("s_Site_Name", ccsGET)
            .Parameters("urls_Grade_Name") = CCGetRequestParam("s_Grade_Name", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set Site_Name_HeaderBlock = TemplateBlock.Block("Section Site_Name_Header")
        Set Site_Name_FooterBlock = TemplateBlock.Block("Section Site_Name_Footer")
        Set Grade_HeaderBlock = TemplateBlock.Block("Section Grade_Header")
        Set Grade_FooterBlock = TemplateBlock.Block("Section Grade_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(Hour1, Grade_Volume, Water_Volume))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(PageBreak, Report_CurrentDate, Navigator))
        Set Page_HeaderControls = CCCreateCollection(Page_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sorter_Hour, Sorter_Grade_Volume, Sorter_Water_Volume))
        Set Site_Name_HeaderControls = CCCreateCollection(Site_Name_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Site_Name))
        Set Site_Name_FooterControls = CCCreateCollection(Site_Name_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array())
        Set Grade_HeaderControls = CCCreateCollection(Grade_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Grade))
        Set Grade_FooterControls = CCCreateCollection(Grade_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array())
        Dim Site_NameKey
        Dim GradeKey
        Dim Groups
        Set Groups = New clsReport1GroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report Report1", Errors)
        Else
            Do While Not Recordset.EOF
                Site_Name.Value = Recordset.Fields("Site_Name")
                Grade.Value = Recordset.Fields("Grade")
                Hour1.Value = Recordset.Fields("Hour1")
                Grade_Volume.Value = Recordset.Fields("Grade_Volume")
                Water_Volume.Value = Recordset.Fields("Water_Volume")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or Site_NameKey <> Recordset.Fields("Site_Name") Then
                    Groups.OpenGroup "Site_Name"
                ElseIf GradeKey <> Recordset.Fields("Grade") Then
                    Groups.OpenGroup "Grade"
                End If
                Groups.AddItem 
                Site_NameKey = Recordset.Fields("Site_Name")
                GradeKey = Recordset.Fields("Grade")
                Recordset.MoveNext
                If Site_NameKey <> Recordset.Fields("Site_Name") Or Recordset.EOF Then
                    Groups.CloseGroup "Site_Name"
                ElseIf GradeKey <> Recordset.Fields("Grade") Then
                    Groups.CloseGroup "Grade"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        Hour1.Value = items(i).Hour1
                        Grade_Volume.Value = items(i).Grade_Volume
                        Water_Volume.Value = items(i).Water_Volume
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                PageBreak.Visible = (i < EndItem-1 And ViewMode <> "Web")
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        Navigator.PageSize = PageSize
                        Navigator.PagesCount = Groups.TotalPages
                        Navigator.PageNumber = items(i).PageNumber
                        Navigator.Visible = ("Web" = ViewMode)
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderControls.Show
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "Site_Name"
                        Site_Name.Value = items(i).Site_Name
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Header_BeforeShow", Me)
                            If Site_Name_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_HeaderBlock, AttributePrefix
                                Site_Name_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Footer_BeforeShow", Me)
                            If Site_Name_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_FooterBlock, AttributePrefix
                                Site_Name_FooterBlock.ParseTo ccsParseAccumulate, DetailBlock
                            End If
                        End If
                    Case "Grade"
                        Grade.Value = items(i).Grade
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Grade_Header_BeforeShow", Me)
                            If Grade_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Grade_HeaderBlock, AttributePrefix
                                Grade_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Grade_Footer_BeforeShow", Me)
                            If Grade_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Grade_FooterBlock, AttributePrefix
                                Grade_FooterBlock.ParseTo ccsParseAccumulate, DetailBlock
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End Report1 Show Method

'Report1 PageSize Property Let @3-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Report1 PageSize Property Let

'Report1 PageSize Property Get @3-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Report1 PageSize Property Get

End Class 'End Report1 Class @3-A61BA892

Class clsReport1DataSource 'Report1DataSource Class @3-1489A126

'DataSource Variables @3-32AAF9C4
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Site_Name
    Public Grade
    Public Hour1
    Public Grade_Volume
    Public Water_Volume
'End DataSource Variables

'DataSource Class_Initialize Event @3-F7F4DDF1
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Site_Name = CCCreateField("Site_Name", "Site_Name", ccsText, Empty, Recordset)
        Set Grade = CCCreateField("Grade", "Grade", ccsText, Empty, Recordset)
        Set Hour1 = CCCreateField("Hour1", "Hour", ccsInteger, Empty, Recordset)
        Set Grade_Volume = CCCreateField("Grade_Volume", "Grade_Volume", ccsFloat, Empty, Recordset)
        Set Water_Volume = CCCreateField("Water_Volume", "Water_Volume", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(Site_Name, Grade, Hour1, Grade_Volume, Water_Volume)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_Hour", "Hour", ""), _
            Array("Sorter_Grade_Volume", "Grade_Volume", ""), _
            Array("Sorter_Water_Volume", "Water_Volume", ""))

        SQL = "select Site_Name, Grade, Tank_Time as Hour, sum(Volume) as Grade_Volume, sum(Water_Volume) as Water_Volume from Grade_table  " & vbLf & _
        "where tank_DATE ='{s_DAY_DATE}' and Grade like '%{s_Grade_Name}%' and Site_Name like '%{s_Site_Name}%' and tank_time >= '{s_DAY_TIME}' and tank_time <= '{s_DAY_TIME1}'  " & vbLf & _
        "group by site_Name, Grade, tank_time " & vbLf & _
        "		"
        CountSQL = "SELECT COUNT(*) FROM (select Site_Name, Grade, Tank_Time as Hour, sum(Volume) as Grade_Volume, sum(Water_Volume) as Water_Volume from Grade_table  " & vbLf & _
        "where tank_DATE ='{s_DAY_DATE}' and Grade like '%{s_Grade_Name}%' and Site_Name like '%{s_Site_Name}%' and tank_time >= '{s_DAY_TIME}' and tank_time <= '{s_DAY_TIME1}'  " & vbLf & _
        "group by site_Name, Grade, tank_time " & vbLf & _
        "		) cnt"
        Where = ""
        Order = ""
        StaticOrder = "Site_Name asc,Grade asc"
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @3-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @3-A53637B1
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "s_DAY_DATE", "expr43", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), date(), False
            .AddParameter "s_DAY_TIME", "urls_DAY_TIME", ccsInteger, Empty, Empty, 0, False
            .AddParameter "s_DAY_TIME1", "urls_DAY_TIME1", ccsInteger, Empty, Empty, 23, False
            .AddParameter "s_Site_Name", "urls_Site_Name", ccsText, Empty, Empty, Empty, False
            .AddParameter "s_Grade_Name", "urls_Grade_Name", ccsText, Empty, Empty, Empty, False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @3-486FAD2E
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @3-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Report1DataSource Class @3-A61BA892

Class clsRecordReport2 'Report2 Class @32-0337016F

'Report2 Variables @32-F3D79DC3

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim Button_DoSearch
    Dim s_DAY_DATE
    Dim DatePicker_s_DAY_DATE
    Dim s_DAY_TIME
    Dim s_Grade_Name
    Dim s_Site_Name
    Dim s_DAY_TIME1
    Dim ClearParameters
'End Report2 Variables

'Report2 Class_Initialize Event @32-EEDEB923
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Report2")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Report2"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set s_DAY_DATE = CCCreateControl(ccsTextBox, "s_DAY_DATE", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_DAY_DATE", Method))
        Set DatePicker_s_DAY_DATE = CCCreateDatePicker("DatePicker_s_DAY_DATE", "Report2", "s_DAY_DATE")
        Set s_DAY_TIME = CCCreateList(ccsListBox, "s_DAY_TIME", Empty, ccsInteger, CCGetRequestParam("s_DAY_TIME", Method), Empty)
        Set s_DAY_TIME.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"), _
            Array("0:00Hs", "1:00 Hs", "2:00 Hs", "3:00 Hs", "4:00 Hs", "5:00 Hs", "6:00 Hs", "7:00 Hs", "8:00 Hs", "9:00 Hs", "10:00 Hs", "11:00 Hs", "12:00 Hs", "13:00 Hs", "14:00 Hs", "15:00 Hs", "16:00 Hs", "17:00 Hs", "18:00 Hs", "19:00 Hs", "20:00 Hs", "21:00 Hs", "22:00 Hs", "23:00 Hs")))
        Set s_Grade_Name = CCCreateList(ccsListBox, "s_Grade_Name", Empty, ccsText, CCGetRequestParam("s_Grade_Name", Method), Empty)
        s_Grade_Name.BoundColumn = "grade_name"
        s_Grade_Name.TextColumn = "grade_name"
        Set s_Grade_Name.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Grades {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_Site_Name = CCCreateList(ccsListBox, "s_Site_Name", Empty, ccsText, CCGetRequestParam("s_Site_Name", Method), Empty)
        s_Site_Name.BoundColumn = "SiteName"
        s_Site_Name.TextColumn = "SiteName"
        Set s_Site_Name.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_DAY_TIME1 = CCCreateList(ccsListBox, "s_DAY_TIME1", Empty, ccsText, CCGetRequestParam("s_DAY_TIME1", Method), Empty)
        Set s_DAY_TIME1.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"), _
            Array("0:00Hs", "1:00 Hs", "2:00 Hs", "3:00 Hs", "4:00 Hs", "5:00 Hs", "6:00 Hs", "7:00 Hs", "8:00 Hs", "9:00 Hs", "10:00 Hs", "11:00 Hs", "12:00 Hs", "13:00 Hs", "14:00 Hs", "15:00 Hs", "16:00 Hs", "17:00 Hs", "18:00 Hs", "19:00 Hs", "20:00 Hs", "21:00 Hs", "22:00 Hs", "23:00 Hs")))
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_DAY_DATE, s_DAY_TIME, s_Grade_Name, s_Site_Name, s_DAY_TIME1)
    End Sub
'End Report2 Class_Initialize Event

'Report2 Class_Terminate Event @32-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Report2 Class_Terminate Event

'Report2 Validate Method @32-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Report2 Validate Method

'Report2 Operation Method @32-F0BE6328
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "ReportStockByGrade.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "ReportStockByGrade.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Report2 Operation Method

'Report2 Show Method @32-4EB99CCB
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Report2" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_Site_Name, s_Grade_Name, s_DAY_DATE, DatePicker_s_DAY_DATE, s_DAY_TIME, s_DAY_TIME1, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("s_DAY_DATE", "s_DAY_TIME", "s_DAY_TIME1", "s_Site_Name", "s_Grade_Name", "ccsForm"))
        ClearParameters.Page = "ReportStockByGrade.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_DAY_DATE.Errors
            Errors.AddErrors s_DAY_TIME.Errors
            Errors.AddErrors s_Grade_Name.Errors
            Errors.AddErrors s_Site_Name.Errors
            Errors.AddErrors s_DAY_TIME1.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Report2" & ":"
            Controls.Show
        End If
    End Sub
'End Report2 Show Method

End Class 'End Report2 Class @32-A61BA892

'Include Page Implementation @2-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
