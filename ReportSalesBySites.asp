<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-F4762C75
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim pump_sales_Sites
Dim Sites_pump_sales
Dim Header
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "ReportSalesBySites.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "ReportSalesBySites.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-25B99553
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set pump_sales_Sites = New clsReportpump_sales_Sites
Set Sites_pump_sales = new clsRecordSites_pump_sales
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
pump_sales_Sites.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/ReportSalesBySites_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-69CC762B
Sites_pump_sales.Operation
Header.Operations
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-DBD81F6C
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(pump_sales_Sites, Sites_pump_sales, Header))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-0F268B1C
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set pump_sales_Sites = Nothing
    Set Sites_pump_sales = Nothing
    Header.UnloadPage
    Set Header = Nothing
End Sub
'End UnloadPage Sub

'pump_sales_Sites clsReportGroup @2-43BC5A36
Class clsReportGrouppump_sales_Sites
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public Sale_Date
    Public Site_Name
    Public Sum_Money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_Money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_Money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public Sale_DateTotalIndex

    Public Sub SetControls()
        Me.Sale_Date = pump_sales_Sites.Sale_Date.Value
        Me.Site_Name = pump_sales_Sites.Site_Name.Value
        Me.Sum_Money = pump_sales_Sites.Sum_Money.Value
        Me.Sum_Volume = pump_sales_Sites.Sum_Volume.Value
        Me.Count_Sales = pump_sales_Sites.Count_Sales.Value
        Me.Report_CurrentDate = pump_sales_Sites.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.Sale_Date = HeaderGrp.Sale_Date
        pump_sales_Sites.Sale_Date.ChangeValue(Me.Sale_Date)
        Me.Site_Name = HeaderGrp.Site_Name
        pump_sales_Sites.Site_Name.ChangeValue(Me.Site_Name)
        Me.Sum_Money = HeaderGrp.Sum_Money
        pump_sales_Sites.Sum_Money.ChangeValue(Me.Sum_Money)
        Me.Sum_Volume = HeaderGrp.Sum_Volume
        pump_sales_Sites.Sum_Volume.ChangeValue(Me.Sum_Volume)
        Me.Count_Sales = HeaderGrp.Count_Sales
        pump_sales_Sites.Count_Sales.ChangeValue(Me.Count_Sales)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        pump_sales_Sites.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = pump_sales_Sites.Report_TotalRecords.GetTotalValue(isCalculate)
        Me.Sum_Sum_Money = pump_sales_Sites.Sum_Sum_Money.GetTotalValue(isCalculate)
        Me.Sum_Sum_Volume = pump_sales_Sites.Sum_Sum_Volume.GetTotalValue(isCalculate)
        Me.Sum_Count_Sales = pump_sales_Sites.Sum_Count_Sales.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_Money = pump_sales_Sites.TotalSum_Sum_Money.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_Volume = pump_sales_Sites.TotalSum_Sum_Volume.GetTotalValue(isCalculate)
        Me.TotalSum_Count_Sales = pump_sales_Sites.TotalSum_Count_Sales.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = pump_sales_Sites.Report_TotalRecords.Value
        Me.Sum_Sum_Money = pump_sales_Sites.Sum_Sum_Money.Value
        Me.Sum_Sum_Volume = pump_sales_Sites.Sum_Sum_Volume.Value
        Me.Sum_Count_Sales = pump_sales_Sites.Sum_Count_Sales.Value
        Me.TotalSum_Sum_Money = pump_sales_Sites.TotalSum_Sum_Money.Value
        Me.TotalSum_Sum_Volume = pump_sales_Sites.TotalSum_Sum_Volume.Value
        Me.TotalSum_Count_Sales = pump_sales_Sites.TotalSum_Count_Sales.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End pump_sales_Sites clsReportGroup

'clspump_sales_SitesGroupsCollection @2-0351D6E3
Class clspump_sales_SitesGroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSale_DateCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSale_DateCurrentHeaderIndex = 2
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGrouppump_sales_Sites
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.Sale_DateTotalIndex = mSale_DateCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + pump_sales_Sites.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + pump_sales_Sites.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "Sale_Date" Then
            If PageSize > 0 And pump_sales_Sites.Sale_Date_Header.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Sale_Date_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + pump_sales_Sites.Sale_Date_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Sale_Date_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mSale_DateCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Sale_Date"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And pump_sales_Sites.Report_Footer.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + pump_sales_Sites.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And pump_sales_Sites.Sale_Date_Footer.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Sale_Date_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + pump_sales_Sites.Sale_Date_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSale_DateCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Sale_Date_Footer_OnCalculate", Me)
        pump_sales_Sites.Sum_Sum_Money.Reset()
        pump_sales_Sites.Sum_Sum_Volume.Reset()
        pump_sales_Sites.Sum_Count_Sales.Reset()
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Sale_Date"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        pump_sales_Sites.Report_TotalRecords.Value = pump_sales_Sites.Report_TotalRecords.InitialValue
        pump_sales_Sites.Sale_Date.Value = pump_sales_Sites.Sale_Date.InitialValue
        pump_sales_Sites.Site_Name.Value = pump_sales_Sites.Site_Name.InitialValue
        pump_sales_Sites.Sum_Money.Value = pump_sales_Sites.Sum_Money.InitialValue
        pump_sales_Sites.Sum_Volume.Value = pump_sales_Sites.Sum_Volume.InitialValue
        pump_sales_Sites.Count_Sales.Value = pump_sales_Sites.Count_Sales.InitialValue
        pump_sales_Sites.Sum_Sum_Money.Value = pump_sales_Sites.Sum_Sum_Money.InitialValue
        pump_sales_Sites.Sum_Sum_Volume.Value = pump_sales_Sites.Sum_Sum_Volume.InitialValue
        pump_sales_Sites.Sum_Count_Sales.Value = pump_sales_Sites.Sum_Count_Sales.InitialValue
        pump_sales_Sites.TotalSum_Sum_Money.Value = pump_sales_Sites.TotalSum_Sum_Money.InitialValue
        pump_sales_Sites.TotalSum_Sum_Volume.Value = pump_sales_Sites.TotalSum_Sum_Volume.InitialValue
        pump_sales_Sites.TotalSum_Count_Sales.Value = pump_sales_Sites.TotalSum_Count_Sales.InitialValue
        pump_sales_Sites.Report_CurrentDate.Value = pump_sales_Sites.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And pump_sales_Sites.Detail.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + pump_sales_Sites.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clspump_sales_SitesGroupsCollection

Class clsReportpump_sales_Sites 'pump_sales_Sites Class @2-C8B41E73

'pump_sales_Sites Variables @2-54572EB5

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public Sale_Date_HeaderBlock, Sale_Date_Header
    Public Sale_Date_FooterBlock, Sale_Date_Footer
    Public ActiveSorter, SortingDirection
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public Sale_Date_HeaderControls, Sale_Date_FooterControls
    Dim Report_TotalRecords
    Dim Sorter_Sum_Money
    Dim Sorter_Sum_Volume
    Dim Sorter_Count_Sales
    Dim Sale_Date
    Dim Site_Name
    Dim Sum_Money
    Dim Sum_Volume
    Dim Count_Sales
    Dim Separator
    Dim Sum_Sum_Money
    Dim Sum_Sum_Volume
    Dim Sum_Count_Sales
    Dim NoRecords
    Dim TotalSum_Sum_Money
    Dim TotalSum_Sum_Volume
    Dim TotalSum_Count_Sales
    Dim Report_CurrentDate
    Dim Navigator
'End pump_sales_Sites Variables

'pump_sales_Sites Class_Initialize Event @2-F1EF85B3
    Private Sub Class_Initialize()
        ComponentName = "pump_sales_Sites"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 1
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set Sale_Date_Footer = new clsSection
        Sale_Date_Footer.Visible = True
        Sale_Date_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Sale_Date_Footer.Height)
        Set Sale_Date_Header = new clsSection
        Sale_Date_Header.Visible = True
        Sale_Date_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Sale_Date_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clspump_sales_SitesDataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If
        ActiveSorter = CCGetParam("pump_sales_SitesOrder", Empty)
        SortingDirection = CCGetParam("pump_sales_SitesDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Sorter_Sum_Money = CCCreateSorter("Sorter_Sum_Money", Me, FileName)
        Set Sorter_Sum_Volume = CCCreateSorter("Sorter_Sum_Volume", Me, FileName)
        Set Sorter_Count_Sales = CCCreateSorter("Sorter_Count_Sales", Me, FileName)
        Set Sale_Date = CCCreateReportLabel( "Sale_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Sale_Date", ccsGet), "",  False, False,"")
        Set Site_Name = CCCreateReportLabel( "Site_Name", Empty, ccsText, Empty, CCGetRequestParam("Site_Name", ccsGet), "",  False, False,"")
        Set Sum_Money = CCCreateReportLabel( "Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Money", ccsGet), "",  False, False,"")
        Set Sum_Volume = CCCreateReportLabel( "Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Volume", ccsGet), "",  False, False,"")
        Set Count_Sales = CCCreateReportLabel( "Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Count_Sales", ccsGet), "",  False, False,"")
        Set Separator = CCCreatePanel("Separator")
        Set Sum_Sum_Money = CCCreateReportLabel( "Sum_Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Money", ccsGet), "Sum",  False, False,"")
        Set Sum_Sum_Volume = CCCreateReportLabel( "Sum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set Sum_Count_Sales = CCCreateReportLabel( "Sum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Sum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set TotalSum_Sum_Money = CCCreateReportLabel( "TotalSum_Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_Money", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Sum_Volume = CCCreateReportLabel( "TotalSum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Count_Sales = CCCreateReportLabel( "TotalSum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("TotalSum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End pump_sales_Sites Class_Initialize Event

'pump_sales_Sites Initialize Method @2-BD78A2B0
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.SetOrder ActiveSorter, SortingDirection
    End Sub
'End pump_sales_Sites Initialize Method

'pump_sales_Sites Class_Terminate Event @2-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End pump_sales_Sites Class_Terminate Event

'pump_sales_Sites Show Method @2-2FE3FE16
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("urls_Site_Name") = CCGetRequestParam("s_Site_Name", ccsGET)
            .Parameters("expr55") = sites_pump_sales.s_Sale_Date0.value
            .Parameters("expr56") = sites_pump_sales.s_Sale_Date.value
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set Sale_Date_HeaderBlock = TemplateBlock.Block("Section Sale_Date_Header")
        Set Sale_Date_FooterBlock = TemplateBlock.Block("Section Sale_Date_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(Site_Name, Sum_Money, Sum_Volume, Count_Sales, Separator))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords, TotalSum_Sum_Money, TotalSum_Sum_Volume, TotalSum_Count_Sales))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_CurrentDate, Navigator))
        Set Page_HeaderControls = CCCreateCollection(Page_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sorter_Sum_Money, Sorter_Sum_Volume, Sorter_Count_Sales))
        Set Sale_Date_HeaderControls = CCCreateCollection(Sale_Date_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sale_Date))
        Set Sale_Date_FooterControls = CCCreateCollection(Sale_Date_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sum_Sum_Money, Sum_Sum_Volume, Sum_Count_Sales))
        Dim Sale_DateKey
        Dim Groups
        Set Groups = New clspump_sales_SitesGroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report pump_sales_Sites", Errors)
        Else
            Do While Not Recordset.EOF
                Sale_Date.Value = Recordset.Fields("Sale_Date")
                Site_Name.Value = Recordset.Fields("Site_Name")
                Sum_Money.Value = Recordset.Fields("Sum_Money")
                Sum_Volume.Value = Recordset.Fields("Sum_Volume")
                Count_Sales.Value = Recordset.Fields("Count_Sales")
                Sum_Sum_Money.Value = Recordset.Fields("Sum_Sum_Money")
                Sum_Sum_Volume.Value = Recordset.Fields("Sum_Sum_Volume")
                Sum_Count_Sales.Value = Recordset.Fields("Sum_Count_Sales")
                TotalSum_Sum_Money.Value = Recordset.Fields("TotalSum_Sum_Money")
                TotalSum_Sum_Volume.Value = Recordset.Fields("TotalSum_Sum_Volume")
                TotalSum_Count_Sales.Value = Recordset.Fields("TotalSum_Count_Sales")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or Sale_DateKey <> Recordset.Fields("Sale_Date") Then
                    Groups.OpenGroup "Sale_Date"
                End If
                Groups.AddItem 
                Sale_DateKey = Recordset.Fields("Sale_Date")
                Recordset.MoveNext
                If Sale_DateKey <> Recordset.Fields("Sale_Date") Or Recordset.EOF Then
                    Groups.CloseGroup "Sale_Date"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        Site_Name.Value = items(i).Site_Name
                        Sum_Money.Value = items(i).Sum_Money
                        Sum_Volume.Value = items(i).Sum_Volume
                        Count_Sales.Value = items(i).Count_Sales
                        Separator.Visible = ("" = items(i+1).GroupType)
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        TotalSum_Sum_Money.Value = items(i).TotalSum_Sum_Money
                        TotalSum_Sum_Volume.Value = items(i).TotalSum_Sum_Volume
                        TotalSum_Count_Sales.Value = items(i).TotalSum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        Navigator.PageSize = PageSize
                        Navigator.PagesCount = Groups.TotalPages
                        Navigator.PageNumber = items(i).PageNumber
                        Navigator.Visible = ("Web" = ViewMode)
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderControls.Show
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "Sale_Date"
                        Sale_Date.Value = items(i).Sale_Date
                        Sum_Sum_Money.Value = items(i).Sum_Sum_Money
                        Sum_Sum_Volume.Value = items(i).Sum_Sum_Volume
                        Sum_Count_Sales.Value = items(i).Sum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Sale_Date_Header_BeforeShow", Me)
                            If Sale_Date_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Sale_Date_HeaderBlock, AttributePrefix
                                Sale_Date_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Sale_Date_Footer_BeforeShow", Me)
                            If Sale_Date_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Sale_Date_FooterBlock, AttributePrefix
                                Sale_Date_FooterControls.Show
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End pump_sales_Sites Show Method

'pump_sales_Sites PageSize Property Let @2-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End pump_sales_Sites PageSize Property Let

'pump_sales_Sites PageSize Property Get @2-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End pump_sales_Sites PageSize Property Get

End Class 'End pump_sales_Sites Class @2-A61BA892

Class clspump_sales_SitesDataSource 'pump_sales_SitesDataSource Class @2-E3E0CF15

'DataSource Variables @2-3ACA531E
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Sale_Date
    Public Site_Name
    Public Sum_Money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_Money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_Money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
'End DataSource Variables

'DataSource Class_Initialize Event @2-10098A98
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Sale_Date = CCCreateField("Sale_Date", "Sale_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Site_Name = CCCreateField("Site_Name", "Site_Name", ccsText, Empty, Recordset)
        Set Sum_Money = CCCreateField("Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set Sum_Volume = CCCreateField("Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Count_Sales = CCCreateField("Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set Sum_Sum_Money = CCCreateField("Sum_Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set Sum_Sum_Volume = CCCreateField("Sum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Sum_Count_Sales = CCCreateField("Sum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set TotalSum_Sum_Money = CCCreateField("TotalSum_Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set TotalSum_Sum_Volume = CCCreateField("TotalSum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set TotalSum_Count_Sales = CCCreateField("TotalSum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(Sale_Date,  Site_Name,  Sum_Money,  Sum_Volume,  Count_Sales,  Sum_Sum_Money,  Sum_Sum_Volume, _
             Sum_Count_Sales,  TotalSum_Sum_Money,  TotalSum_Sum_Volume,  TotalSum_Count_Sales)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_Sum_Money", "Sum_Money", ""), _
            Array("Sorter_Sum_Volume", "Sum_Volume", ""), _
            Array("Sorter_Count_Sales", "Count_Sales", ""))

        SQL = "SELECT DAY_DATE AS Sale_Date, SiteName AS Site_Name, sum(money) AS Sum_Money, Sum(volume) AS Sum_Volume, count(sale_id) AS Count_Sales  " & vbLf & _
        "FROM Sites INNER JOIN pump_sales ON " & vbLf & _
        "Sites.ss_id = pump_sales.ss_id {SQL_Where} " & vbLf & _
        "GROUP BY SiteName, DAY_DATE {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) FROM (SELECT  DAY_DATE AS Sale_Date, SiteName AS Site_Name, sum(money) AS Sum_Money, Sum(volume) AS Sum_Volume, count(sale_id) AS Count_Sales FROM Sites INNER JOIN pump_sales ON " & vbLf & _
        "Sites.ss_id = pump_sales.ss_id {SQL_Where} " & vbLf & _
        "GROUP BY SiteName, DAY_DATE) A"
        Where = ""
        Order = "SiteName"
        StaticOrder = "pump_sales.DAY_DATE asc"
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @2-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @2-8FB62491
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urls_Site_Name", ccsText, Empty, Empty, Empty, False
            .AddParameter 2, "expr55", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), dateadd("d", -10,date()), False
            .AddParameter 3, "expr56", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), date(), False
            .Criterion(1) = .Operation(opContains, False, "[SiteName]", .getParamByID(1))
            .Criterion(2) = .Operation(opGreaterThanOrEqual, False, "DAY_DATE", .getParamByID(2))
            .Criterion(3) = .Operation(opLessThanOrEqual, False, "DAY_DATE", .getParamByID(3))
            .AssembledWhere = .opAND(False, .opAND(False, .Criterion(1), .Criterion(2)), .Criterion(3))
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @2-2C6FAEE2
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End pump_sales_SitesDataSource Class @2-A61BA892

Class clsRecordSites_pump_sales 'Sites_pump_sales Class @11-63D92FBE

'Sites_pump_sales Variables @11-E2255166

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim ClearParameters
    Dim Button_DoSearch
    Dim s_Site_Name
    Dim s_Sale_Date
    Dim DatePicker_s_Sale_Date
    Dim s_Sale_Date0
    Dim DatePicker_s_Sale_Date0
'End Sites_pump_sales Variables

'Sites_pump_sales Class_Initialize Event @11-20AB3F76
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Sites_pump_sales")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Sites_pump_sales"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set s_Site_Name = CCCreateList(ccsListBox, "s_Site_Name", Empty, ccsText, CCGetRequestParam("s_Site_Name", Method), Empty)
        s_Site_Name.BoundColumn = "SiteName"
        s_Site_Name.TextColumn = "SiteName"
        Set s_Site_Name.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT SiteName  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_Sale_Date = CCCreateControl(ccsTextBox, "s_Sale_Date", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_Sale_Date", Method))
        Set DatePicker_s_Sale_Date = CCCreateDatePicker("DatePicker_s_Sale_Date", "Sites_pump_sales", "s_Sale_Date")
        Set s_Sale_Date0 = CCCreateControl(ccsTextBox, "s_Sale_Date0", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_Sale_Date0", Method))
        Set DatePicker_s_Sale_Date0 = CCCreateDatePicker("DatePicker_s_Sale_Date0", "Sites_pump_sales", "s_Sale_Date0")
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_Site_Name, s_Sale_Date, s_Sale_Date0)
    End Sub
'End Sites_pump_sales Class_Initialize Event

'Sites_pump_sales Class_Terminate Event @11-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites_pump_sales Class_Terminate Event

'Sites_pump_sales Validate Method @11-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Sites_pump_sales Validate Method

'Sites_pump_sales Operation Method @11-23ECD732
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "ReportSalesBySites.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "ReportSalesBySites.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Sites_pump_sales Operation Method

'Sites_pump_sales Show Method @11-555A86D7
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Sites_pump_sales" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_Site_Name, s_Sale_Date0, DatePicker_s_Sale_Date0, s_Sale_Date, DatePicker_s_Sale_Date, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("s_Site_Name", "s_Sale_Date", "s_Sale_Date0", "ccsForm"))
        ClearParameters.Page = "ReportSalesBySites.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_Site_Name.Errors
            Errors.AddErrors s_Sale_Date.Errors
            Errors.AddErrors s_Sale_Date0.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Sites_pump_sales" & ":"
            Controls.Show
        End If
    End Sub
'End Sites_pump_sales Show Method

End Class 'End Sites_pump_sales Class @11-A61BA892

'Include Page Implementation @48-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
