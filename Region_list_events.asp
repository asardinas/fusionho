<%
'BindEvents Method @1-BB784D73
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Region.Navigator.CCSEvents("BeforeShow") = GetRef("Region_Navigator_BeforeShow")
        Set Region1.CCSEvents("BeforeShow") = GetRef("Region1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Region_Navigator_BeforeShow(Sender) 'Region_Navigator_BeforeShow @22-EE7AAD62

'Hide-Show Component @23-BF0F4F88
    Dim TotalPages_23_1 : TotalPages_23_1 = CCSConverter.VBSConvert(ccsInteger, Region.DataSource.Recordset.PageCount)
    Dim Param2_23_2 : Param2_23_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_23_1) And Not IsEmpty(Param2_23_2) And TotalPages_23_1 < Param2_23_2) Then _
        Region.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Region_Navigator_BeforeShow @22-54C34B28

Function Region1_BeforeShow(Sender) 'Region1_BeforeShow @28-0563B08C

'Custom Code @37-73254650
' -------------------------
  	If Region1.Recordset.EOF Then
		Region1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Region1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Region1_BeforeShow @28-54C34B28


%>
