<%
'BindEvents Method @1-7C8C0B75
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeShow") = GetRef("Page_BeforeShow")
    Else
    End If
End Sub
'End BindEvents Method

Function Page_BeforeShow(Sender) 'Page_BeforeShow @1-A1547E8B

'Custom Code @2-73254650
' -------------------------
' Write your own code here.
' -------------------------
'End Custom Code

dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
Dim strXML

dim strCategories, strtank1, strtank2, strtank3, strtank4
dim i
dim var
dim var1
dim var2
dim var5

dim estacion
dim productos
dim tipo
dim fecha
dim fecha1


strGroups = ""
strXML = ""
Set FusionHO = Server.CreateObject("ADODB.Connection") 
FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
var = ""
estacion = CCGetRequestParam("site", ccsGet)
tipo = CCGetRequestParam("prod", ccsGet)
productos = CCGetRequestParam("grade", ccsGet)
var1 = CCGetRequestParam("date", ccsGet)
fecha = mid(var1,7,4)&"-"& mid(var1,1,2)&"-"& mid(var1,4,2)
fecha1 = mid(var1,17,4)&"-"& mid(var1,11,2)&"-"& mid(var1,14,2)

SQL = "SELECT DAY_DATE as dias, grade_name AS Grade_Name, round(Sum(money),0,1) AS Money, round(sum(volume),0,1) AS Volume, round(Count(sale_id),0,1) AS Sales FROM FusionHO.dbo.Sites INNER JOIN (FusionHO.dbo.pump_sales Inner JOIN FusionHO.dbo.Grades ON pump_sales.grade_id = Grades.grade_id) ON Sites.ss_id = pump_sales.ss_id where SiteName = '"&estacion&"' and grade_name = '"&productos&"' and DAY_DATE >= '"&fecha&"'and DAY_DATE <= '"&fecha1&"' GROUP BY SiteName, DAY_DATE, grade_name ORDER BY DAY_DATE"
dim algo																																																												

algo = datediff("d", fecha, fecha1)
dim arrData()
dim fila
dim col
fila = algo + 1
col = 5
ReDim arrData(fila,col)

dim variable
dim datefor	

For i=0 to UBound(arrData)-1  
variable = dateadd("d", i,fecha)
arrData(i,1) = mid(variable,1,2)&"/"& mid(variable,4,2)
arrData(i,2) = 0
arrData(i,3) = 0
arrData(i,4) = 0
next


'/////////// leo de la base, y guardo los datos en un array con las horas como indices

Set rsGroups = FusionHO.Execute(SQL)
While not rsGroups.EOF
	for i = 0 to UBound(arrData)-1
		variable = rsGroups("dias")
		if (arrData(i,1) = (mid(variable,1,2)&"/"& mid(variable,4,2))) then
		arrData(i,2) = rsGroups("Money")
		arrData(i,3) = rsGroups("Volume")
		arrData(i,4) = rsGroups("Sales")
		end if
	next
rsGroups.MoveNext

'Initialize <graph> element
strXML = "<graph caption='       Report By Sales in " & estacion & "' subcaption='"&fecha&" to "&fecha1&"                 ' hovercapbg='FFECAA' chartBottomMargin='0' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='2' showvalues='0' numdivlines='5' numVdivlines='0' yaxisminvalue='0' rotateNames='1' xAxisName='Hours' yAxisName='Values'>"

'Initialize <categories> element - necessary to generate a multi-series chart
strCategories = "<categories>"


	 
'Initiate <dataset> elements
strtank1 = "<dataset seriesName=' Money ($)' color='2AD62A' >"
strtank2 = "<dataset seriesName=' Volume (Lts)' color='F1683C' >"
strtank3 = "<dataset seriesName=' Sales (Count)' color='AFBBF8' >"

 
dim for2

'//////// armo el xml llenando los campos vacios hacia atras
 For i=0 to UBound(arrData)-1
	
		'Append <category name='...' /> to strCategories
		strCategories = strCategories & "<category name='" & arrData(i,1) & "' />"
		'Add <set value='...' /> to both the datasets

			strtank1 = strtank1 & "<set value='" & arrData(i,2) & "' />"

			strtank2 = strtank2 & "<set value='" & arrData(i,3) & "' />"	

			strtank3 = strtank3 & "<set value='" & arrData(i,4) & "' />"
		
   Next
 

  

Wend
   'Close <categories> element
   strCategories = strCategories & "</categories>"
   
      'Close <dataset> elements
   strtank1 = strtank1 & "</dataset>"
   strtank2 = strtank2 & "</dataset>"
   strtank3 = strtank3 & "</dataset>"


   'Assemble the entire XML now
'   ///////////// definir esto cuando capturo los datos del form ////////////

Select Case (tipo)
   Case "Money":
       'Sentencias 
       strXML = strXML & strCategories & strtank1 & "</graph>"'
   Case "Volume":
       'Sentencias 
       strXML = strXML & strCategories & strtank2 & "</graph>"'
   Case "Sales":
       'Sentencias 
       strXML = strXML & strCategories & strtank3 & "</graph>"'
   Case else:
       'Sentencias 
       strXML = strXML & strCategories & strtank1 & strtank2 & strtank3 & "</graph>"'
End Select

Response.ContentType = "text/xml"
'Just write out the XML data
'NOTE THAT THIS PAGE DOESN'T CONTAIN ANY HTML TAG, WHATSOEVER
 Response.Write(strXML)
'end if

End Function 'Close Page_BeforeShow @1-54C34B28


%>
