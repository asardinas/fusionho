<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-FF93BC24
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim MosoSearch
Dim Moso
Dim Header
Dim Footer
Dim Moso1
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "Moso_list.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "Moso_list.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Authenticate User @1-78185724
CCSecurityRedirect "1", Empty
'End Authenticate User

'Initialize Objects @1-D2FDFD62
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set MosoSearch = new clsRecordMosoSearch
Set Moso = New clsGridMoso
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set Footer = New clsFooter
Set Footer.Attributes = Attributes
Footer.Initialize "Footer", ""
Set Moso1 = new clsRecordMoso1
Moso.Initialize DBFusionHO
Moso1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Moso_list_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-630346C7
MosoSearch.Operation
Header.Operations
Footer.Operations
Moso1.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-F17FF6C2
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(MosoSearch, Moso, Header, Footer, Moso1))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-11BB90B3
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set MosoSearch = Nothing
    Set Moso = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Footer.UnloadPage
    Set Footer = Nothing
    Set Moso1 = Nothing
End Sub
'End UnloadPage Sub

Class clsRecordMosoSearch 'MosoSearch Class @2-C26C3387

'MosoSearch Variables @2-779C7885

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim s_keyword
    Dim Button_DoSearch1
'End MosoSearch Variables

'MosoSearch Class_Initialize Event @2-55844D23
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "MosoSearch")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "MosoSearch"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set s_keyword = CCCreateControl(ccsTextBox, "s_keyword", Empty, ccsText, Empty, CCGetRequestParam("s_keyword", Method))
        Set Button_DoSearch1 = CCCreateButton("Button_DoSearch1", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_keyword)
    End Sub
'End MosoSearch Class_Initialize Event

'MosoSearch Class_Terminate Event @2-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End MosoSearch Class_Terminate Event

'MosoSearch Validate Method @2-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End MosoSearch Validate Method

'MosoSearch Operation Method @2-C66E5251
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch1"
            If Button_DoSearch1.Pressed Then
                PressedButton = "Button_DoSearch1"
            End If
        End If
        Redirect = "Moso_list.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch1" Then
                If NOT Button_DoSearch1.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "Moso_list.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch1.x", "Button_DoSearch1.y", "Button_DoSearch1"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End MosoSearch Operation Method

'MosoSearch Show Method @2-0C01EAEF
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "MosoSearch" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_keyword, Button_DoSearch1))
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_keyword.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "MosoSearch" & ":"
            Controls.Show
        End If
    End Sub
'End MosoSearch Show Method

End Class 'End MosoSearch Class @2-A61BA892

Class clsGridMoso 'Moso Class @6-8A31FC74

'Moso Variables @6-90D5AB68

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Moso_Insert
    Dim Sorter_Moso_id
    Dim Sorter_Moso_name
    Dim Moso_id
    Dim Moso_name
    Dim Navigator
'End Moso Variables

'Moso Class_Initialize Event @6-8475BDF0
    Private Sub Class_Initialize()
        ComponentName = "Moso"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsMosoDataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 20 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("MosoOrder", Empty)
        SortingDirection = CCGetParam("MosoDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Moso_Insert = CCCreateControl(ccsLink, "Moso_Insert", Empty, ccsText, Empty, CCGetRequestParam("Moso_Insert", ccsGet))
        Set Sorter_Moso_id = CCCreateSorter("Sorter_Moso_id", Me, FileName)
        Set Sorter_Moso_name = CCCreateSorter("Sorter_Moso_name", Me, FileName)
        Set Moso_id = CCCreateControl(ccsLink, "Moso_id", Empty, ccsInteger, Empty, CCGetRequestParam("Moso_id", ccsGet))
        Set Moso_name = CCCreateControl(ccsLabel, "Moso_name", Empty, ccsText, Empty, CCGetRequestParam("Moso_name", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpSimple)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Moso Class_Initialize Event

'Moso Initialize Method @6-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Moso Initialize Method

'Moso Class_Terminate Event @6-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Moso Class_Terminate Event

'Moso Show Method @6-8E26FEBA
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urls_keyword") = CCGetRequestParam("s_keyword", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Moso_Insert, Sorter_Moso_id, Sorter_Moso_name, Navigator))
            
            Moso_Insert.Parameters = CCGetQueryString("QueryString", Array("Moso_id", "ccsForm"))
            Moso_Insert.Parameters = CCAddParam(Moso_Insert.Parameters, "var", 1)
            Moso_Insert.Page = "Moso_list.asp"
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(Moso_id, Moso_name))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Moso:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    Moso_id.Value = Recordset.Fields("Moso_id")
                    Moso_id.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
                    Moso_id.Parameters = CCAddParam(Moso_id.Parameters, "Moso_id", Recordset.Fields("Moso_id_param1"))
                    Moso_id.Page = "Moso_list.asp"
                    Moso_name.Value = Recordset.Fields("Moso_name")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Moso:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Moso:"
            StaticControls.Show
        End If

    End Sub
'End Moso Show Method

'Moso PageSize Property Let @6-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Moso PageSize Property Let

'Moso PageSize Property Get @6-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Moso PageSize Property Get

'Moso RowNumber Property Get @6-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Moso RowNumber Property Get

'Moso HasNextRow Function @6-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Moso HasNextRow Function

End Class 'End Moso Class @6-A61BA892

Class clsMosoDataSource 'MosoDataSource Class @6-6B5B95A6

'DataSource Variables @6-63918608
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Moso_id
    Public Moso_id_param1
    Public Moso_name
'End DataSource Variables

'DataSource Class_Initialize Event @6-5DCDBD41
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Moso_id = CCCreateField("Moso_id", "Moso_id", ccsInteger, Empty, Recordset)
        Set Moso_id_param1 = CCCreateField("Moso_id_param1", "Moso_id", ccsText, Empty, Recordset)
        Set Moso_name = CCCreateField("Moso_name", "Moso_name", ccsText, Empty, Recordset)
        Fields.AddFields Array(Moso_id, Moso_id_param1, Moso_name)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_Moso_id", "Moso_id", ""), _
            Array("Sorter_Moso_name", "Moso_name", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} Moso_id, Moso_name  " & vbLf & _
        "FROM Moso {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM Moso"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @6-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @6-98C87156
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urls_keyword", ccsText, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opContains, False, "[Moso_name]", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @6-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @6-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End MosoDataSource Class @6-A61BA892

Class clsRecordMoso1 'Moso1 Class @23-5808518B

'Moso1 Variables @23-315581E1

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim Moso_name
    Dim Button_Insert1
    Dim Button_Update
    Dim Button_Delete
    Dim Button_Cancel
'End Moso1 Variables

'Moso1 Class_Initialize Event @23-312B60C8
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsMoso1DataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Moso1")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Moso1"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set Moso_name = CCCreateControl(ccsTextBox, "Moso_name", "Name", ccsText, Empty, CCGetRequestParam("Moso_name", Method))
        Moso_name.Required = True
        Set Button_Insert1 = CCCreateButton("Button_Insert1", Method)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Delete = CCCreateButton("Button_Delete", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(Moso_name)
    End Sub
'End Moso1 Class_Initialize Event

'Moso1 Initialize Method @23-89DA22FB
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
            .Parameters("urlMoso_id") = CCGetRequestParam("Moso_id", ccsGET)
        End With
    End Sub
'End Moso1 Initialize Method

'Moso1 Class_Terminate Event @23-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Moso1 Class_Terminate Event

'Moso1 Validate Method @23-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Moso1 Validate Method

'Moso1 Operation Method @23-64DCA8D1
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Insert1")
            If Button_Insert1.Pressed Then
                PressedButton = "Button_Insert1"
            ElseIf Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Delete.Pressed Then
                PressedButton = "Button_Delete"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = "Moso_list.asp"
        If PressedButton = "Button_Delete" Then
            If NOT Button_Delete.OnClick OR NOT DeleteRow() Then
                Redirect = ""
            End If
        ElseIf PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Insert1" Then
                If NOT Button_Insert1.OnClick() OR NOT InsertRow() Then
                    Redirect = ""
                End If
            ElseIf PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Moso1 Operation Method

'Moso1 InsertRow Method @23-65757EA9
    Function InsertRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInsert", Me)
        If NOT InsertAllowed Then InsertRow = False : Exit Function
        DataSource.Moso_name.Value = Moso_name.Value
        DataSource.Insert(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInsert", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        InsertRow = (Errors.Count = 0)
    End Function
'End Moso1 InsertRow Method

'Moso1 UpdateRow Method @23-9BA08CFC
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        DataSource.Moso_name.Value = Moso_name.Value
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End Moso1 UpdateRow Method

'Moso1 DeleteRow Method @23-D5C1DF24
    Function DeleteRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeDelete", Me)
        If NOT DeleteAllowed Then DeleteRow = False : Exit Function
        DataSource.Delete(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterDelete", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        DeleteRow = (Errors.Count = 0)
    End Function
'End Moso1 DeleteRow Method

'Moso1 Show Method @23-AE987C6C
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Moso1" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Moso_name, Button_Insert1, Button_Update, Button_Delete, Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        Moso_name.Value = Recordset.Fields("Moso_name")
                    End If
                Else
                    EditMode = False
                End If
            End If
        End If
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors Moso_name.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Insert1.Visible = NOT EditMode AND InsertAllowed
        Button_Update.Visible = EditMode AND UpdateAllowed
        Button_Delete.Visible = EditMode AND DeleteAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Moso1" & ":"
            Controls.Show
        End If
    End Sub
'End Moso1 Show Method

End Class 'End Moso1 Class @23-A61BA892

Class clsMoso1DataSource 'Moso1DataSource Class @23-947483B0

'DataSource Variables @23-C9CAE6BE
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Moso_name
'End DataSource Variables

'DataSource Class_Initialize Event @23-EA8F8EEF
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Moso_name = CCCreateField("Moso_name", "Moso_name", ccsText, Empty, Recordset)
        Fields.AddFields Array(Moso_name)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "Moso_name", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "Moso_name", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT *  " & vbLf & _
        "FROM Moso {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @23-D7794366
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlMoso_id", ccsInteger, Empty, Empty, Empty, False
            AllParamsSet = .AllParamsSet
            .Criterion(1) = .Operation(opEqual, False, "[Moso_id]", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @23-48A2DA7D
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @23-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @23-A7FFD513
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM [Moso]" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @23-B987945A
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_Moso_name : IsDef_Moso_name = CCIsDefined("Moso_name", "Form")
        If Not UpdateOmitIfEmpty("Moso_name") Or IsDef_Moso_name Then Cmd.AddSQLStrings "[Moso_name]=" & Connection.ToSQL(Moso_name, Moso_name.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "[Moso]", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @23-FB76E0AD
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_Moso_name : IsDef_Moso_name = CCIsDefined("Moso_name", "Form")
        If Not InsertOmitIfEmpty("Moso_name") Or IsDef_Moso_name Then Cmd.AddSQLStrings "[Moso_name]", Connection.ToSQL(Moso_name, Moso_name.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "[Moso]", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End Moso1DataSource Class @23-A61BA892

'Include Page Implementation @20-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation

'Include Page Implementation @21-79649078
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Footer.asp" -->
<%
'End Include Page Implementation


%>
