<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" needGeneration="0" pasteActions="pasteActions">
	<Components>
		<Grid id="47" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="price_change_status, Sites" orderBy="price_change_id desc" name="Sites_price_change_status" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Sitesprice_change_status} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" activeCollection="TableParameters">
			<Components>
				<Sorter id="60" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="61" visible="True" name="Sorter_Aplication_Date" column="Aplication_Date" wizardCaption="{res:Aplication_Date}" wizardSortingType="SimpleDir" wizardControl="Aplication_Date" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_Aplication_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="62" visible="True" name="Sorter_File_Delivery_Date" column="File_Delivery_Date" wizardCaption="{res:File_Delivery_Date}" wizardSortingType="SimpleDir" wizardControl="File_Delivery_Date" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_File_Delivery_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="63" visible="True" name="Sorter_Reception_Date" column="Reception_Date" wizardCaption="{res:Reception_Date}" wizardSortingType="SimpleDir" wizardControl="Reception_Date" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_Reception_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="64" visible="True" name="Sorter_Change_Date" column="Change_Date" wizardCaption="{res:Change_Date}" wizardSortingType="SimpleDir" wizardControl="Change_Date" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_Change_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="65" visible="True" name="Sorter_price_change_id" column="price_change_id" wizardCaption="{res:price_change_id}" wizardSortingType="SimpleDir" wizardControl="price_change_id" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="66" visible="True" name="Sorter_Status" column="status_code" wizardCaption="{res:Status}" wizardSortingType="SimpleDir" wizardControl="Status" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_Status">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="67" visible="True" name="Sorter_Status_Description" column="status_description" wizardCaption="{res:Status_Description}" wizardSortingType="SimpleDir" wizardControl="Status_Description" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_Status_Description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="68" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusSiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="69" fieldSourceType="DBColumn" dataType="Text" html="False" name="Aplication_Date" fieldSource="Aplication_Date" wizardCaption="{res:Aplication_Date}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusAplication_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="70" fieldSourceType="DBColumn" dataType="Text" html="False" name="File_Delivery_Date" fieldSource="File_Delivery_Date" wizardCaption="{res:File_Delivery_Date}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusFile_Delivery_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="71" fieldSourceType="DBColumn" dataType="Text" html="False" name="Reception_Date" fieldSource="Reception_Date" wizardCaption="{res:Reception_Date}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusReception_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="72" fieldSourceType="DBColumn" dataType="Text" html="False" name="Change_Date" fieldSource="Change_Date" wizardCaption="{res:Change_Date}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusChange_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Link id="73" fieldSourceType="DBColumn" dataType="Integer" html="False" name="price_change_id" fieldSource="price_change_id" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Sites_price_change_statusprice_change_id" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="Price_send.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="167" sourceType="DataField" name="price_change_id" source="price_change_id"/>
					</LinkParameters>
				</Link>
				<Label id="74" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Status" fieldSource="Status" wizardCaption="{res:Status}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Sites_price_change_statusStatus">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="75" fieldSourceType="DBColumn" dataType="Text" html="False" name="Status_Description" fieldSource="Status_Description" wizardCaption="{res:Status_Description}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusStatus_Description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="76" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
				<Link id="169" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" PathID="Sites_price_change_statusLink1" hrefSource="Price_send.ccp" wizardUseTemplateBlock="False" removeParameters="price_change_id;aplication_date;aplication_time">
					<Components/>
					<Events/>
					<LinkParameters>
						<LinkParameter id="170" sourceType="Expression" format="yyyy-mm-dd" name="var" source="1"/>
					</LinkParameters>
					<Attributes/>
					<Features/>
				</Link>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="239"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="168" conditionType="Parameter" useIsNull="False" field="price_change_status.ss_id" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="ss_id"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="48" tableName="price_change_status" schemaName="dbo" posLeft="10" posTop="10" posWidth="140" posHeight="180"/>
				<JoinTable id="49" tableName="Sites" schemaName="dbo" posLeft="171" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="50" tableLeft="price_change_status" tableRight="Sites" fieldLeft="price_change_status.ss_id" fieldRight="Sites.ss_id" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="51" tableName="price_change_status" fieldName="price_change_id"/>
				<Field id="53" tableName="Sites" fieldName="SiteName"/>
				<Field id="54" fieldName="(Cast (aplication_date as varchar(10))  + '  ' + aplication_time) " isExpression="True" alias="Aplication_Date"/>
				<Field id="55" fieldName="(Cast (file_delivery_date as varchar(10))  + '  ' + file_delivery_time)" isExpression="True" alias="File_Delivery_Date"/>
				<Field id="56" fieldName="(Cast (reception_date as varchar(10))  + '  ' + reception_time)" isExpression="True" alias="Reception_Date"/>
				<Field id="57" fieldName="(Cast (change_date as varchar(10))  + '  ' +change_time)" isExpression="True" alias="Change_Date"/>
				<Field id="58" tableName="price_change_status" fieldName="status_code" alias="Status"/>
				<Field id="59" tableName="price_change_status" fieldName="status_description" alias="Status_Description"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<Grid id="77" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="Brand, Country, Local_type, Moso, Region, Service, Sites, Tm, City" name="Brand_City_Country_Local1" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:BrandCityCountryLocal_typeMosoRegionServiceSitesTm} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}">
			<Components>
				<Sorter id="119" visible="True" name="Sorter_ss_id" column="ss_id" wizardCaption="{res:ss_id}" wizardSortingType="SimpleDir" wizardControl="ss_id" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_ss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="120" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="121" visible="True" name="Sorter_Address" column="Address" wizardCaption="{res:Address}" wizardSortingType="SimpleDir" wizardControl="Address" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Address">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="122" visible="True" name="Sorter_Latitud" column="Latitud" wizardCaption="{res:Latitud}" wizardSortingType="SimpleDir" wizardControl="Latitud" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Latitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="123" visible="True" name="Sorter_Longitud" column="Longitud" wizardCaption="{res:Longitud}" wizardSortingType="SimpleDir" wizardControl="Longitud" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Longitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="124" visible="True" name="Sorter_Country" column="Country_name" wizardCaption="{res:Country}" wizardSortingType="SimpleDir" wizardControl="Country" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Country">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="125" visible="True" name="Sorter_Region" column="Region_name" wizardCaption="{res:Region}" wizardSortingType="SimpleDir" wizardControl="Region" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Region">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="126" visible="True" name="Sorter_City" column="City_name" wizardCaption="{res:City}" wizardSortingType="SimpleDir" wizardControl="City" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_City">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="127" visible="True" name="Sorter_Brand" column="Brand_name" wizardCaption="{res:Brand}" wizardSortingType="SimpleDir" wizardControl="Brand" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Brand">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="128" visible="True" name="Sorter_Zip" column="Zip" wizardCaption="{res:Zip}" wizardSortingType="SimpleDir" wizardControl="Zip" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Zip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="129" visible="True" name="Sorter_Phone" column="Phone" wizardCaption="{res:Phone}" wizardSortingType="SimpleDir" wizardControl="Phone" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Phone">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="130" visible="True" name="Sorter_Mail" column="Mail" wizardCaption="{res:Mail}" wizardSortingType="SimpleDir" wizardControl="Mail" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Mail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="131" visible="True" name="Sorter_Local_Type" column="Local_type_name" wizardCaption="{res:Local_Type}" wizardSortingType="SimpleDir" wizardControl="Local_Type" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Local_Type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="132" visible="True" name="Sorter_Service" column="Service_name" wizardCaption="{res:Service}" wizardSortingType="SimpleDir" wizardControl="Service" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Service">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="133" visible="True" name="Sorter_Tm" column="Tm_name" wizardCaption="{res:Tm}" wizardSortingType="SimpleDir" wizardControl="Tm" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Tm">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="134" visible="True" name="Sorter_Moso" column="Moso_name" wizardCaption="{res:Moso}" wizardSortingType="SimpleDir" wizardControl="Moso" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Moso">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="135" visible="True" name="Sorter_Sites_Status" column="Sites.Valid" wizardCaption="{res:Sites_Status}" wizardSortingType="SimpleDir" wizardControl="Sites_Status" wizardAddNbsp="False" PathID="Brand_City_Country_Local1Sorter_Sites_Status">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="136" fieldSourceType="DBColumn" dataType="Integer" html="False" name="ss_id" fieldSource="ss_id" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Brand_City_Country_Local1ss_id" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="Price_send.ccp" removeParameters="price_change_id;aplication_date;aplication_time;var">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="165" sourceType="Expression" name="site" source="1"/>
						<LinkParameter id="166" sourceType="DataField" name="ss_id" source="ss_id"/>
					</LinkParameters>
				</Link>
				<Label id="137" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="138" fieldSourceType="DBColumn" dataType="Text" html="False" name="Address" fieldSource="Address" wizardCaption="{res:Address}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Address">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="139" fieldSourceType="DBColumn" dataType="Float" html="False" name="Latitud" fieldSource="Latitud" wizardCaption="{res:Latitud}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Latitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="140" fieldSourceType="DBColumn" dataType="Float" html="False" name="Longitud" fieldSource="Longitud" wizardCaption="{res:Longitud}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Longitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="141" fieldSourceType="DBColumn" dataType="Text" html="False" name="Country" fieldSource="Country" wizardCaption="{res:Country}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Country">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="142" fieldSourceType="DBColumn" dataType="Text" html="False" name="Region" fieldSource="Region" wizardCaption="{res:Region}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Region">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="143" fieldSourceType="DBColumn" dataType="Text" html="False" name="City" fieldSource="City" wizardCaption="{res:City}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1City">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="144" fieldSourceType="DBColumn" dataType="Text" html="False" name="Brand" fieldSource="Brand" wizardCaption="{res:Brand}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Brand">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="145" fieldSourceType="DBColumn" dataType="Text" html="False" name="Zip" fieldSource="Zip" wizardCaption="{res:Zip}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Zip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="146" fieldSourceType="DBColumn" dataType="Text" html="False" name="Phone" fieldSource="Phone" wizardCaption="{res:Phone}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Phone">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="147" fieldSourceType="DBColumn" dataType="Text" html="False" name="Mail" fieldSource="Mail" wizardCaption="{res:Mail}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Mail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="148" fieldSourceType="DBColumn" dataType="Text" html="False" name="Local_Type" fieldSource="Local_Type" wizardCaption="{res:Local_Type}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Local_Type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="149" fieldSourceType="DBColumn" dataType="Text" html="False" name="Service" fieldSource="Service" wizardCaption="{res:Service}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Service">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="150" fieldSourceType="DBColumn" dataType="Text" html="False" name="Tm" fieldSource="Tm" wizardCaption="{res:Tm}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Tm">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="151" fieldSourceType="DBColumn" dataType="Text" html="False" name="Moso" fieldSource="Moso" wizardCaption="{res:Moso}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Moso">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="152" fieldSourceType="DBColumn" dataType="Boolean" html="False" name="Sites_Status" fieldSource="Sites_Status" wizardCaption="{res:Sites_Status}" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Brand_City_Country_Local1Sites_Status">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="153" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="118" conditionType="Parameter" useIsNull="False" field="Sites.SiteName" parameterSource="s_SiteName" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="78" tableName="Brand" schemaName="dbo" posLeft="10" posTop="10" posWidth="95" posHeight="88"/>
				<JoinTable id="79" tableName="Country" schemaName="dbo" posLeft="126" posTop="10" posWidth="95" posHeight="88"/>
				<JoinTable id="80" tableName="Local_type" schemaName="dbo" posLeft="242" posTop="10" posWidth="115" posHeight="88"/>
				<JoinTable id="81" tableName="Moso" schemaName="dbo" posLeft="378" posTop="10" posWidth="95" posHeight="88"/>
				<JoinTable id="82" tableName="Region" schemaName="dbo" posLeft="494" posTop="10" posWidth="95" posHeight="104"/>
				<JoinTable id="83" tableName="Service" schemaName="dbo" posLeft="610" posTop="10" posWidth="95" posHeight="88"/>
				<JoinTable id="84" tableName="Sites" schemaName="dbo" posLeft="726" posTop="10" posWidth="115" posHeight="180"/>
				<JoinTable id="85" tableName="Tm" schemaName="dbo" posLeft="862" posTop="10" posWidth="95" posHeight="120"/>
				<JoinTable id="101" tableName="City" schemaName="dbo" posLeft="21" posTop="108" posWidth="95" posHeight="104"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="86" tableLeft="Sites" tableRight="Brand" fieldLeft="Sites.Brand_id" fieldRight="Brand.Brand_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="87" tableLeft="Region" tableRight="Country" fieldLeft="Region.Country_id" fieldRight="Country.Country_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="88" tableLeft="Sites" tableRight="Country" fieldLeft="Sites.Country_id" fieldRight="Country.Country_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="89" tableLeft="Sites" tableRight="Local_type" fieldLeft="Sites.Local_type_id" fieldRight="Local_type.Local_type_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="90" tableLeft="Sites" tableRight="Moso" fieldLeft="Sites.Moso_id" fieldRight="Moso.Moso_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="91" tableLeft="Sites" tableRight="Region" fieldLeft="Sites.Region_id" fieldRight="Region.Region_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="92" tableLeft="Sites" tableRight="Service" fieldLeft="Sites.Service_id" fieldRight="Service.Service_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="93" tableLeft="Tm" tableRight="Sites" fieldLeft="Tm.Tm_id" fieldRight="Sites.Tm_id" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="102" tableLeft="Sites" tableRight="City" fieldLeft="Sites.City_id" fieldRight="City.City_id" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="94" tableName="Sites" fieldName="ss_id" isExpression="False"/>
				<Field id="95" tableName="Sites" fieldName="SiteName" isExpression="False"/>
				<Field id="96" tableName="Sites" fieldName="Address" isExpression="False"/>
				<Field id="97" tableName="Sites" fieldName="Latitud" isExpression="False"/>
				<Field id="98" tableName="Sites" fieldName="Longitud" isExpression="False"/>
				<Field id="99" tableName="Country" fieldName="Country_name" alias="Country"/>
				<Field id="100" tableName="Region" fieldName="Region_name" alias="Region"/>
				<Field id="104" tableName="City" fieldName="City_name" alias="City"/>
				<Field id="105" tableName="Sites" fieldName="Zip" isExpression="False"/>
				<Field id="106" tableName="Sites" fieldName="Phone" isExpression="False"/>
				<Field id="107" tableName="Sites" fieldName="Mail" isExpression="False"/>
				<Field id="108" tableName="Moso" fieldName="Moso_name" alias="Moso"/>
				<Field id="109" tableName="Tm" fieldName="Tm_name" alias="Tm"/>
				<Field id="110" tableName="Service" fieldName="Service_name" alias="Service"/>
				<Field id="111" tableName="Local_type" fieldName="Local_type_name" alias="Local_Type"/>
				<Field id="112" tableName="Brand" fieldName="Brand_name" alias="Brand"/>
				<Field id="113" tableName="Sites" fieldName="Sites.Valid" alias="Sites_Status"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<Record id="114" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Brand_City_Country_Local" wizardCaption="{res:CCS_SearchFormPrefix} {res:Brand_City_Country_Local} {res:CCS_SearchFormSuffix}" wizardOrientation="Vertical" wizardFormMethod="post" returnPage="Price_send.ccp" PathID="Brand_City_Country_Local">
			<Components>
				<Link id="115" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="Price_send.ccp" removeParameters="s_SiteName;site" wizardThemeItem="SorterLink" wizardDefaultValue="{res:CCS_Clear}" PathID="Brand_City_Country_LocalClearParameters">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="116" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}" PathID="Brand_City_Country_LocalButton_DoSearch">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<TextBox id="117" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" PathID="Brand_City_Country_Locals_SiteName" features="(assigned)">
					<Components/>
					<Events/>
					<Attributes/>
					<Features>
						<PTAutocomplete id="238" enabled="True" sourceType="Table" name="PTAutocomplete1" servicePage="services/Price_send_Brand_City_Country_Local_s_SiteName_PTAutocomplete1.ccp" category="Prototype" searchField="SiteName" connection="FusionHO" featureNameChanged="No" dataSource="Sites">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables/>
							<JoinLinks/>
							<Fields/>
							<Features/>
						</PTAutocomplete>
					</Features>
				</TextBox>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Record id="171" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="All" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="FusionHO" name="price_change_status" dataSource="price_change_status" errorSummator="Error" wizardCaption="{res:CCS_RecordFormPrefix} {res:price_change_status} {res:CCS_RecordFormSuffix}" wizardFormMethod="post" PathID="price_change_status" pasteAsReplace="pasteAsReplace" pasteActions="pasteActions">
			<Components>
				<Button id="172" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="{res:CCS_Insert}" PathID="price_change_statusButton_Insert">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="173" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="{res:CCS_Update}" PathID="price_change_statusButton_Update" removeParameters="price_change_id;aplication_date;aplication_time;var" returnPage="Price_send.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="174" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="{res:CCS_Delete}" PathID="price_change_statusButton_Delete" removeParameters="price_change_id;aplication_date;aplication_time;var" returnPage="Price_send.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="175" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="{res:CCS_Cancel}" PathID="price_change_statusButton_Cancel" removeParameters="price_change_id;aplication_date;aplication_time;var" returnPage="Price_send.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<TextBox id="176" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="aplication_date" fieldSource="aplication_date" required="False" caption="{res:aplication_date}" wizardCaption="{res:aplication_date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusaplication_date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="177" name="DatePicker_aplication_date" control="aplication_date" wizardSatellite="True" wizardControl="aplication_date" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css" PathID="price_change_statusDatePicker_aplication_date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="178" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="aplication_time" fieldSource="aplication_time" required="False" caption="{res:aplication_time}" wizardCaption="{res:aplication_time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusaplication_time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="179" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="ss_id" fieldSource="ss_id" required="True" caption="{res:ss_id}" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Hidden id="180" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="price_change_id" fieldSource="price_change_id" required="True" caption="{res:price_change_id}" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_change_statusprice_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="181"/>
					</Actions>
				</Event>
				<Event name="OnLoad" type="Client">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="182"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="183" conditionType="Parameter" useIsNull="False" field="price_change_id" parameterSource="price_change_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="184" tableName="price_change_status" posLeft="10" posTop="10" posWidth="140" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<EditableGrid id="185" urlType="Relative" secured="False" emptyRows="1" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" sourceType="Table" defaultPageSize="10" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="FusionHO" dataSource="price_send" name="price_send" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:price_send} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAltRecord="False" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" PathID="price_send" deleteControl="CheckBox_Delete" activeCollection="TableParameters" pasteActions="pasteActions">
			<Components>
				<Sorter id="186" visible="True" name="Sorter_price_change_id" column="price_change_id" wizardCaption="{res:price_change_id}" wizardSortingType="SimpleDir" wizardControl="price_change_id" PathID="price_sendSorter_price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="187" visible="True" name="Sorter_ss_id" column="ss_id" wizardCaption="{res:ss_id}" wizardSortingType="SimpleDir" wizardControl="ss_id" PathID="price_sendSorter_ss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="188" visible="True" name="Sorter_grade_id" column="grade_id" wizardCaption="{res:grade_id}" wizardSortingType="SimpleDir" wizardControl="grade_id" PathID="price_sendSorter_grade_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="189" visible="True" name="Sorter_levels" column="levels" wizardCaption="{res:levels}" wizardSortingType="SimpleDir" wizardControl="levels" PathID="price_sendSorter_levels">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="190" visible="True" name="Sorter_ppu_actual" column="ppu_actual" wizardCaption="{res:ppu_actual}" wizardSortingType="SimpleDir" wizardControl="ppu_actual" PathID="price_sendSorter_ppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="191" visible="True" name="Sorter_price_change_type" column="price_change_type" wizardCaption="{res:price_change_type}" wizardSortingType="SimpleDir" wizardControl="price_change_type" PathID="price_sendSorter_price_change_type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="192" visible="True" name="Sorter_new_price" column="new_price" wizardCaption="{res:new_price}" wizardSortingType="SimpleDir" wizardControl="new_price" PathID="price_sendSorter_new_price">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<TextBox id="193" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="price_change_id" fieldSource="price_change_id" required="False" caption="{res:price_change_id}" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendprice_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="194" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="ss_id" fieldSource="ss_id" required="True" caption="{res:ss_id}" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="195" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="grade_id" fieldSource="grade_id" required="True" caption="{res:grade_id}" wizardCaption="{res:grade_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendgrade_id" sourceType="Table" connection="FusionHO" dataSource="Grades" boundColumn="grade_id" textColumn="grade_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<ListBox id="196" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="levels" fieldSource="levels" required="True" caption="{res:levels}" wizardCaption="{res:levels}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendlevels" sourceType="ListOfValues" dataSource="1;1;2;2;3;3;4;4;5;5;6;6;7;7">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="197" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="ppu_actual" fieldSource="ppu_actual" required="False" caption="{res:ppu_actual}" wizardCaption="{res:ppu_actual}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="198" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="price_change_type" fieldSource="price_change_type" required="True" caption="{res:price_change_type}" wizardCaption="{res:price_change_type}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendprice_change_type" sourceType="Table" connection="FusionHO" dataSource="price_type" boundColumn="price_change_type" textColumn="price_change_description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="199" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="new_price" fieldSource="new_price" required="False" caption="{res:new_price}" wizardCaption="{res:new_price}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" PathID="price_sendnew_price" format="0.00">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<CheckBox id="200" visible="Dynamic" fieldSourceType="CodeExpression" dataType="Boolean" name="CheckBox_Delete" checkedValue="true" uncheckedValue="false" wizardCaption="{res:CCS_Delete}" wizardAddNbsp="True" PathID="price_sendCheckBox_Delete">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</CheckBox>
				<Navigator id="201" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
				<Button id="202" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Submit" operation="Submit" wizardCaption="{res:CCS_Update}" PathID="price_sendButton_Submit">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="203" urlType="Relative" enableValidation="False" isDefault="False" name="Cancel" operation="Cancel" wizardCaption="{res:CCS_Cancel}" PathID="price_sendCancel">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Label id="204" fieldSourceType="DBColumn" dataType="Text" html="False" editable="False" name="RowIDAttribute">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</Label>
				<Label id="205" fieldSourceType="DBColumn" dataType="Text" html="False" editable="False" name="RowNameAttribute">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="206" fieldSourceType="DBColumn" dataType="Text" html="True" editable="False" name="RowStyleAttribute">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="207" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="TextBox1" PathID="price_sendTextBox1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="208"/>
					</Actions>
				</Event>
				<Event name="BeforeShowRow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="209"/>
					</Actions>
				</Event>
				<Event name="OnLoad" type="Client">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="210"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="211" conditionType="Parameter" useIsNull="False" field="price_change_id" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="price_change_id"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="212" tableName="price_send" posLeft="10" posTop="10" posWidth="143" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<PKFields>
				<PKField id="213" tableName="price_send" fieldName="price_send_id" dataType="Integer"/>
				<PKField id="214" tableName="price_send" fieldName="ss_id" dataType="Integer"/>
				<PKField id="215" tableName="price_send" fieldName="grade_id" dataType="Integer"/>
				<PKField id="216" tableName="price_send" fieldName="levels" dataType="Integer"/>
				<PKField id="217" tableName="price_send" fieldName="price_change_type" dataType="Integer"/>
			</PKFields>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</EditableGrid>
		<Grid id="218" secured="False" sourceType="SQL" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="select p.price_change_id, s.SiteName, g.grade_name as Grade, p.levels, p.ppu_actual, t.price_change_description as change_type, p.new_price
from (((price_send as p inner join sites as s on p.ss_id = s.ss_id) inner join grades as g on p.grade_id = g.grade_id) inner join price_type as t on p.price_change_type = t.price_change_type)
where p.price_change_id = '{price_change_id}'" name="Grid3" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Grid3} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" activeCollection="SQLParameters" parameterTypeListName="ParameterTypeList">
			<Components>
				<Sorter id="219" visible="True" name="Sorter_price_change_id" column="price_change_id" wizardCaption="{res:price_change_id}" wizardSortingType="SimpleDir" wizardControl="price_change_id" wizardAddNbsp="False" PathID="Grid3Sorter_price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="220" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Grid3Sorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="103" visible="True" name="Sorter_Grade" column="Grade" wizardCaption="{res:Grade}" wizardSortingType="SimpleDir" wizardControl="Grade" wizardAddNbsp="False" PathID="Grid3Sorter_Grade">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="221" visible="True" name="Sorter_levels" column="levels" wizardCaption="{res:levels}" wizardSortingType="SimpleDir" wizardControl="levels" wizardAddNbsp="False" PathID="Grid3Sorter_levels">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="222" visible="True" name="Sorter_ppu_actual" column="ppu_actual" wizardCaption="{res:ppu_actual}" wizardSortingType="SimpleDir" wizardControl="ppu_actual" wizardAddNbsp="False" PathID="Grid3Sorter_ppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="223" visible="True" name="Sorter_change_type" column="change_type" wizardCaption="{res:change_type}" wizardSortingType="SimpleDir" wizardControl="change_type" wizardAddNbsp="False" PathID="Grid3Sorter_change_type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="224" visible="True" name="Sorter_new_price" column="new_price" wizardCaption="{res:new_price}" wizardSortingType="SimpleDir" wizardControl="new_price" wizardAddNbsp="False" PathID="Grid3Sorter_new_price">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="225" fieldSourceType="DBColumn" dataType="Integer" html="False" name="price_change_id" fieldSource="price_change_id" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="226" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid3SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="227" fieldSourceType="DBColumn" dataType="Text" html="False" name="Grade" fieldSource="Grade" wizardCaption="{res:Grade}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid3Grade">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="228" fieldSourceType="DBColumn" dataType="Integer" html="False" name="levels" fieldSource="levels" wizardCaption="{res:levels}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3levels">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="229" fieldSourceType="DBColumn" dataType="Float" html="False" name="ppu_actual" fieldSource="ppu_actual" wizardCaption="{res:ppu_actual}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3ppu_actual">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="230" fieldSourceType="DBColumn" dataType="Text" html="False" name="change_type" fieldSource="change_type" wizardCaption="{res:change_type}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid3change_type">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="231" fieldSourceType="DBColumn" dataType="Float" html="False" name="new_price" fieldSource="new_price" wizardCaption="{res:new_price}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid3new_price">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="232" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="233"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="234" variable="price_change_id" parameterType="URL" dataType="Text" parameterSource="price_change_id"/>
			</SQLParameters>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="2" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="Price_send.asp" forShow="True" url="Price_send.asp" comment="'" codePage="windows-1252"/>
		<CodeFile id="Events" language="ASPTemplates" name="Price_send_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
		<CodeFile id="PTAutocomplete238" language="ASPTemplates" name="Price_sendBrand_City_Country_Locals_SiteName_style.css" forShow="False" comment="/*" commentEnd="*/" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
