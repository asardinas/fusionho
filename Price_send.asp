<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-E643634F
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Sites_price_change_status
Dim Brand_City_Country_Local1
Dim Brand_City_Country_Local
Dim price_change_status
Dim price_send
Dim Grid3
Dim Header
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "Price_send.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "Price_send.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-D28FF993
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Sites_price_change_status = New clsGridSites_price_change_status
Set Brand_City_Country_Local1 = New clsGridBrand_City_Country_Local1
Set Brand_City_Country_Local = new clsRecordBrand_City_Country_Local
Set price_change_status = new clsRecordprice_change_status
Set price_send = new clsEditableGridprice_send
Set Grid3 = New clsGridGrid3
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Sites_price_change_status.Initialize DBFusionHO
Brand_City_Country_Local1.Initialize DBFusionHO
price_change_status.Initialize DBFusionHO
price_send.Initialize DBFusionHO
Grid3.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Price_send_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-7C82D453
Brand_City_Country_Local.Operation
price_change_status.Operation
price_send.ProcessOperations
Header.Operations
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-946921FD
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Sites_price_change_status, Brand_City_Country_Local1, Brand_City_Country_Local, price_change_status, price_send, Grid3, Header))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-74B4FB47
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set Sites_price_change_status = Nothing
    Set Brand_City_Country_Local1 = Nothing
    Set Brand_City_Country_Local = Nothing
    Set price_change_status = Nothing
    Set price_send = Nothing
    Set Grid3 = Nothing
    Header.UnloadPage
    Set Header = Nothing
End Sub
'End UnloadPage Sub



Class clsGridSites_price_change_status 'Sites_price_change_status Class @47-2C16F321

'Sites_price_change_status Variables @47-0A93849B

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_SiteName
    Dim Sorter_Aplication_Date
    Dim Sorter_File_Delivery_Date
    Dim Sorter_Reception_Date
    Dim Sorter_Change_Date
    Dim Sorter_price_change_id
    Dim Sorter_Status
    Dim Sorter_Status_Description
    Dim SiteName
    Dim Aplication_Date
    Dim File_Delivery_Date
    Dim Reception_Date
    Dim Change_Date
    Dim price_change_id
    Dim Status
    Dim Status_Description
    Dim Navigator
    Dim Link1
'End Sites_price_change_status Variables

'Sites_price_change_status Class_Initialize Event @47-04BE1D09
    Private Sub Class_Initialize()
        ComponentName = "Sites_price_change_status"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsSites_price_change_statusDataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Sites_price_change_statusOrder", Empty)
        SortingDirection = CCGetParam("Sites_price_change_statusDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Aplication_Date = CCCreateSorter("Sorter_Aplication_Date", Me, FileName)
        Set Sorter_File_Delivery_Date = CCCreateSorter("Sorter_File_Delivery_Date", Me, FileName)
        Set Sorter_Reception_Date = CCCreateSorter("Sorter_Reception_Date", Me, FileName)
        Set Sorter_Change_Date = CCCreateSorter("Sorter_Change_Date", Me, FileName)
        Set Sorter_price_change_id = CCCreateSorter("Sorter_price_change_id", Me, FileName)
        Set Sorter_Status = CCCreateSorter("Sorter_Status", Me, FileName)
        Set Sorter_Status_Description = CCCreateSorter("Sorter_Status_Description", Me, FileName)
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Aplication_Date = CCCreateControl(ccsLabel, "Aplication_Date", Empty, ccsText, Empty, CCGetRequestParam("Aplication_Date", ccsGet))
        Set File_Delivery_Date = CCCreateControl(ccsLabel, "File_Delivery_Date", Empty, ccsText, Empty, CCGetRequestParam("File_Delivery_Date", ccsGet))
        Set Reception_Date = CCCreateControl(ccsLabel, "Reception_Date", Empty, ccsText, Empty, CCGetRequestParam("Reception_Date", ccsGet))
        Set Change_Date = CCCreateControl(ccsLabel, "Change_Date", Empty, ccsText, Empty, CCGetRequestParam("Change_Date", ccsGet))
        Set price_change_id = CCCreateControl(ccsLink, "price_change_id", Empty, ccsInteger, Empty, CCGetRequestParam("price_change_id", ccsGet))
        Set Status = CCCreateControl(ccsLabel, "Status", Empty, ccsInteger, Empty, CCGetRequestParam("Status", ccsGet))
        Set Status_Description = CCCreateControl(ccsLabel, "Status_Description", Empty, ccsText, Empty, CCGetRequestParam("Status_Description", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        Set Link1 = CCCreateControl(ccsLink, "Link1", Empty, ccsText, Empty, CCGetRequestParam("Link1", ccsGet))
    IsDSEmpty = True
    End Sub
'End Sites_price_change_status Class_Initialize Event

'Sites_price_change_status Initialize Method @47-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Sites_price_change_status Initialize Method

'Sites_price_change_status Class_Terminate Event @47-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites_price_change_status Class_Terminate Event

'Sites_price_change_status Show Method @47-7BB07C76
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urlss_id") = CCGetRequestParam("ss_id", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_SiteName, Sorter_Aplication_Date, Sorter_File_Delivery_Date, Sorter_Reception_Date, Sorter_Change_Date, Sorter_price_change_id, Sorter_Status, Sorter_Status_Description, Navigator, Link1))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
            
            Link1.Parameters = CCGetQueryString("QueryString", Array("price_change_id", "aplication_date", "aplication_time", "ccsForm"))
            Link1.Parameters = CCAddParam(Link1.Parameters, "var", 1)
            Link1.Page = "Price_send.asp"
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(SiteName, Aplication_Date, File_Delivery_Date, Reception_Date, Change_Date, price_change_id, Status, Status_Description))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Sites_price_change_status:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    SiteName.Value = Recordset.Fields("SiteName")
                    Aplication_Date.Value = Recordset.Fields("Aplication_Date")
                    File_Delivery_Date.Value = Recordset.Fields("File_Delivery_Date")
                    Reception_Date.Value = Recordset.Fields("Reception_Date")
                    Change_Date.Value = Recordset.Fields("Change_Date")
                    price_change_id.Value = Recordset.Fields("price_change_id")
                    price_change_id.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
                    price_change_id.Parameters = CCAddParam(price_change_id.Parameters, "price_change_id", Recordset.Fields("price_change_id_param1"))
                    price_change_id.Page = "Price_send.asp"
                    Status.Value = Recordset.Fields("Status")
                    Status_Description.Value = Recordset.Fields("Status_Description")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Sites_price_change_status:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Sites_price_change_status:"
            StaticControls.Show
        End If

    End Sub
'End Sites_price_change_status Show Method

'Sites_price_change_status PageSize Property Let @47-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Sites_price_change_status PageSize Property Let

'Sites_price_change_status PageSize Property Get @47-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Sites_price_change_status PageSize Property Get

'Sites_price_change_status RowNumber Property Get @47-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Sites_price_change_status RowNumber Property Get

'Sites_price_change_status HasNextRow Function @47-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Sites_price_change_status HasNextRow Function

End Class 'End Sites_price_change_status Class @47-A61BA892

Class clsSites_price_change_statusDataSource 'Sites_price_change_statusDataSource Class @47-4855D41F

'DataSource Variables @47-087BD710
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public SiteName
    Public Aplication_Date
    Public File_Delivery_Date
    Public Reception_Date
    Public Change_Date
    Public price_change_id
    Public price_change_id_param1
    Public Status
    Public Status_Description
'End DataSource Variables

'DataSource Class_Initialize Event @47-B80BFCA6
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Aplication_Date = CCCreateField("Aplication_Date", "Aplication_Date", ccsText, Empty, Recordset)
        Set File_Delivery_Date = CCCreateField("File_Delivery_Date", "File_Delivery_Date", ccsText, Empty, Recordset)
        Set Reception_Date = CCCreateField("Reception_Date", "Reception_Date", ccsText, Empty, Recordset)
        Set Change_Date = CCCreateField("Change_Date", "Change_Date", ccsText, Empty, Recordset)
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Set price_change_id_param1 = CCCreateField("price_change_id_param1", "price_change_id", ccsText, Empty, Recordset)
        Set Status = CCCreateField("Status", "Status", ccsInteger, Empty, Recordset)
        Set Status_Description = CCCreateField("Status_Description", "Status_Description", ccsText, Empty, Recordset)
        Fields.AddFields Array(SiteName, Aplication_Date, File_Delivery_Date, Reception_Date, Change_Date, price_change_id, price_change_id_param1, Status, Status_Description)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Aplication_Date", "Aplication_Date", ""), _
            Array("Sorter_File_Delivery_Date", "File_Delivery_Date", ""), _
            Array("Sorter_Reception_Date", "Reception_Date", ""), _
            Array("Sorter_Change_Date", "Change_Date", ""), _
            Array("Sorter_price_change_id", "price_change_id", ""), _
            Array("Sorter_Status", "status_code", ""), _
            Array("Sorter_Status_Description", "status_description", ""))

        SQL = " " & vbLf & _
        "SELECT TOP {SqlParam_endRecord} price_change_id, SiteName, (Cast (aplication_date as varchar(10))  + '  ' + aplication_time)  AS Aplication_Date, (Cast (file_delivery_date as varchar(10))  + '  ' + file_delivery_time) AS File_Delivery_Date, " & vbLf & _
        "(Cast (reception_date as varchar(10))  + '  ' + reception_time) AS Reception_Date, (Cast (change_date as varchar(10))  + '  ' +change_time) AS Change_Date, " & vbLf & _
        "status_code AS Status, status_description AS Status_Description  " & vbLf & _
        "FROM price_change_status INNER JOIN Sites ON " & vbLf & _
        "price_change_status.ss_id = Sites.ss_id {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM price_change_status INNER JOIN Sites ON " & vbLf & _
        "price_change_status.ss_id = Sites.ss_id"
        Where = ""
        Order = "price_change_id desc"
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @47-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @47-5064D9D8
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlss_id", ccsInteger, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opEqual, False, "price_change_status.ss_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @47-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @47-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Sites_price_change_statusDataSource Class @47-A61BA892

Class clsGridBrand_City_Country_Local1 'Brand_City_Country_Local1 Class @77-9EF7F784

'Brand_City_Country_Local1 Variables @77-A0BD7878

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_ss_id
    Dim Sorter_SiteName
    Dim Sorter_Address
    Dim Sorter_Latitud
    Dim Sorter_Longitud
    Dim Sorter_Country
    Dim Sorter_Region
    Dim Sorter_City
    Dim Sorter_Brand
    Dim Sorter_Zip
    Dim Sorter_Phone
    Dim Sorter_Mail
    Dim Sorter_Local_Type
    Dim Sorter_Service
    Dim Sorter_Tm
    Dim Sorter_Moso
    Dim Sorter_Sites_Status
    Dim ss_id
    Dim SiteName
    Dim Address
    Dim Latitud
    Dim Longitud
    Dim Country
    Dim Region
    Dim City
    Dim Brand
    Dim Zip
    Dim Phone
    Dim Mail
    Dim Local_Type
    Dim Service
    Dim Tm
    Dim Moso
    Dim Sites_Status
    Dim Navigator
'End Brand_City_Country_Local1 Variables

'Brand_City_Country_Local1 Class_Initialize Event @77-C9976538
    Private Sub Class_Initialize()
        ComponentName = "Brand_City_Country_Local1"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsBrand_City_Country_Local1DataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Brand_City_Country_Local1Order", Empty)
        SortingDirection = CCGetParam("Brand_City_Country_Local1Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_ss_id = CCCreateSorter("Sorter_ss_id", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Address = CCCreateSorter("Sorter_Address", Me, FileName)
        Set Sorter_Latitud = CCCreateSorter("Sorter_Latitud", Me, FileName)
        Set Sorter_Longitud = CCCreateSorter("Sorter_Longitud", Me, FileName)
        Set Sorter_Country = CCCreateSorter("Sorter_Country", Me, FileName)
        Set Sorter_Region = CCCreateSorter("Sorter_Region", Me, FileName)
        Set Sorter_City = CCCreateSorter("Sorter_City", Me, FileName)
        Set Sorter_Brand = CCCreateSorter("Sorter_Brand", Me, FileName)
        Set Sorter_Zip = CCCreateSorter("Sorter_Zip", Me, FileName)
        Set Sorter_Phone = CCCreateSorter("Sorter_Phone", Me, FileName)
        Set Sorter_Mail = CCCreateSorter("Sorter_Mail", Me, FileName)
        Set Sorter_Local_Type = CCCreateSorter("Sorter_Local_Type", Me, FileName)
        Set Sorter_Service = CCCreateSorter("Sorter_Service", Me, FileName)
        Set Sorter_Tm = CCCreateSorter("Sorter_Tm", Me, FileName)
        Set Sorter_Moso = CCCreateSorter("Sorter_Moso", Me, FileName)
        Set Sorter_Sites_Status = CCCreateSorter("Sorter_Sites_Status", Me, FileName)
        Set ss_id = CCCreateControl(ccsLink, "ss_id", Empty, ccsInteger, Empty, CCGetRequestParam("ss_id", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Address = CCCreateControl(ccsLabel, "Address", Empty, ccsText, Empty, CCGetRequestParam("Address", ccsGet))
        Set Latitud = CCCreateControl(ccsLabel, "Latitud", Empty, ccsFloat, Empty, CCGetRequestParam("Latitud", ccsGet))
        Set Longitud = CCCreateControl(ccsLabel, "Longitud", Empty, ccsFloat, Empty, CCGetRequestParam("Longitud", ccsGet))
        Set Country = CCCreateControl(ccsLabel, "Country", Empty, ccsText, Empty, CCGetRequestParam("Country", ccsGet))
        Set Region = CCCreateControl(ccsLabel, "Region", Empty, ccsText, Empty, CCGetRequestParam("Region", ccsGet))
        Set City = CCCreateControl(ccsLabel, "City", Empty, ccsText, Empty, CCGetRequestParam("City", ccsGet))
        Set Brand = CCCreateControl(ccsLabel, "Brand", Empty, ccsText, Empty, CCGetRequestParam("Brand", ccsGet))
        Set Zip = CCCreateControl(ccsLabel, "Zip", Empty, ccsText, Empty, CCGetRequestParam("Zip", ccsGet))
        Set Phone = CCCreateControl(ccsLabel, "Phone", Empty, ccsText, Empty, CCGetRequestParam("Phone", ccsGet))
        Set Mail = CCCreateControl(ccsLabel, "Mail", Empty, ccsText, Empty, CCGetRequestParam("Mail", ccsGet))
        Set Local_Type = CCCreateControl(ccsLabel, "Local_Type", Empty, ccsText, Empty, CCGetRequestParam("Local_Type", ccsGet))
        Set Service = CCCreateControl(ccsLabel, "Service", Empty, ccsText, Empty, CCGetRequestParam("Service", ccsGet))
        Set Tm = CCCreateControl(ccsLabel, "Tm", Empty, ccsText, Empty, CCGetRequestParam("Tm", ccsGet))
        Set Moso = CCCreateControl(ccsLabel, "Moso", Empty, ccsText, Empty, CCGetRequestParam("Moso", ccsGet))
        Set Sites_Status = CCCreateControl(ccsLabel, "Sites_Status", Empty, ccsBoolean, DefaultBooleanFormat, CCGetRequestParam("Sites_Status", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Brand_City_Country_Local1 Class_Initialize Event

'Brand_City_Country_Local1 Initialize Method @77-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Brand_City_Country_Local1 Initialize Method

'Brand_City_Country_Local1 Class_Terminate Event @77-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Brand_City_Country_Local1 Class_Terminate Event

'Brand_City_Country_Local1 Show Method @77-06D71433
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urls_SiteName") = CCGetRequestParam("s_SiteName", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_ss_id, Sorter_SiteName, Sorter_Address, Sorter_Latitud, Sorter_Longitud, Sorter_Country, Sorter_Region, Sorter_City, Sorter_Brand, Sorter_Zip, Sorter_Phone, Sorter_Mail, Sorter_Local_Type, Sorter_Service, Sorter_Tm, Sorter_Moso, Sorter_Sites_Status, Navigator))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(ss_id, SiteName, Address, Latitud, Longitud, Country, Region, City, Brand, Zip, Phone, Mail, Local_Type, Service, Tm, Moso, Sites_Status))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Brand_City_Country_Local1:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    ss_id.Value = Recordset.Fields("ss_id")
                    ss_id.Parameters = CCGetQueryString("QueryString", Array("price_change_id", "aplication_date", "aplication_time", "var", "ccsForm"))
                    ss_id.Parameters = CCAddParam(ss_id.Parameters, "site", 1)
                    ss_id.Parameters = CCAddParam(ss_id.Parameters, "ss_id", Recordset.Fields("ss_id_param1"))
                    ss_id.Page = "Price_send.asp"
                    SiteName.Value = Recordset.Fields("SiteName")
                    Address.Value = Recordset.Fields("Address")
                    Latitud.Value = Recordset.Fields("Latitud")
                    Longitud.Value = Recordset.Fields("Longitud")
                    Country.Value = Recordset.Fields("Country")
                    Region.Value = Recordset.Fields("Region")
                    City.Value = Recordset.Fields("City")
                    Brand.Value = Recordset.Fields("Brand")
                    Zip.Value = Recordset.Fields("Zip")
                    Phone.Value = Recordset.Fields("Phone")
                    Mail.Value = Recordset.Fields("Mail")
                    Local_Type.Value = Recordset.Fields("Local_Type")
                    Service.Value = Recordset.Fields("Service")
                    Tm.Value = Recordset.Fields("Tm")
                    Moso.Value = Recordset.Fields("Moso")
                    Sites_Status.Value = Recordset.Fields("Sites_Status")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Brand_City_Country_Local1:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Brand_City_Country_Local1:"
            StaticControls.Show
        End If

    End Sub
'End Brand_City_Country_Local1 Show Method

'Brand_City_Country_Local1 PageSize Property Let @77-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Brand_City_Country_Local1 PageSize Property Let

'Brand_City_Country_Local1 PageSize Property Get @77-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Brand_City_Country_Local1 PageSize Property Get

'Brand_City_Country_Local1 RowNumber Property Get @77-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Brand_City_Country_Local1 RowNumber Property Get

'Brand_City_Country_Local1 HasNextRow Function @77-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Brand_City_Country_Local1 HasNextRow Function

End Class 'End Brand_City_Country_Local1 Class @77-A61BA892

Class clsBrand_City_Country_Local1DataSource 'Brand_City_Country_Local1DataSource Class @77-A2583FF0

'DataSource Variables @77-521ADF81
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public ss_id
    Public ss_id_param1
    Public ss_id_param2
    Public SiteName
    Public Address
    Public Latitud
    Public Longitud
    Public Country
    Public Region
    Public City
    Public Brand
    Public Zip
    Public Phone
    Public Mail
    Public Local_Type
    Public Service
    Public Tm
    Public Moso
    Public Sites_Status
'End DataSource Variables

'DataSource Class_Initialize Event @77-11C1FCE0
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set ss_id_param1 = CCCreateField("ss_id_param1", "ss_id", ccsText, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Address = CCCreateField("Address", "Address", ccsText, Empty, Recordset)
        Set Latitud = CCCreateField("Latitud", "Latitud", ccsFloat, Empty, Recordset)
        Set Longitud = CCCreateField("Longitud", "Longitud", ccsFloat, Empty, Recordset)
        Set Country = CCCreateField("Country", "Country", ccsText, Empty, Recordset)
        Set Region = CCCreateField("Region", "Region", ccsText, Empty, Recordset)
        Set City = CCCreateField("City", "City", ccsText, Empty, Recordset)
        Set Brand = CCCreateField("Brand", "Brand", ccsText, Empty, Recordset)
        Set Zip = CCCreateField("Zip", "Zip", ccsText, Empty, Recordset)
        Set Phone = CCCreateField("Phone", "Phone", ccsText, Empty, Recordset)
        Set Mail = CCCreateField("Mail", "Mail", ccsText, Empty, Recordset)
        Set Local_Type = CCCreateField("Local_Type", "Local_Type", ccsText, Empty, Recordset)
        Set Service = CCCreateField("Service", "Service", ccsText, Empty, Recordset)
        Set Tm = CCCreateField("Tm", "Tm", ccsText, Empty, Recordset)
        Set Moso = CCCreateField("Moso", "Moso", ccsText, Empty, Recordset)
        Set Sites_Status = CCCreateField("Sites_Status", "Sites_Status", ccsBoolean, Array(1, 0, Empty), Recordset)
        Fields.AddFields Array(ss_id,  ss_id_param1,  SiteName,  Address,  Latitud,  Longitud,  Country, _
             Region,  City,  Brand,  Zip,  Phone,  Mail,  Local_Type,  Service, _
             Tm,  Moso,  Sites_Status)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_ss_id", "ss_id", ""), _
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Address", "Address", ""), _
            Array("Sorter_Latitud", "Latitud", ""), _
            Array("Sorter_Longitud", "Longitud", ""), _
            Array("Sorter_Country", "Country_name", ""), _
            Array("Sorter_Region", "Region_name", ""), _
            Array("Sorter_City", "City_name", ""), _
            Array("Sorter_Brand", "Brand_name", ""), _
            Array("Sorter_Zip", "Zip", ""), _
            Array("Sorter_Phone", "Phone", ""), _
            Array("Sorter_Mail", "Mail", ""), _
            Array("Sorter_Local_Type", "Local_type_name", ""), _
            Array("Sorter_Service", "Service_name", ""), _
            Array("Sorter_Tm", "Tm_name", ""), _
            Array("Sorter_Moso", "Moso_name", ""), _
            Array("Sorter_Sites_Status", "Sites.Valid", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} ss_id, SiteName, Address, Latitud, Longitud, Country_name AS Country, Region_name AS Region, City_name AS City, Zip, Phone, " & vbLf & _
        "Mail, Moso_name AS Moso, Tm_name AS Tm, Service_name AS Service, Local_type_name AS Local_Type, Brand_name AS Brand, Sites.Valid AS Sites_Status  " & vbLf & _
        "FROM Tm INNER JOIN (((((((Sites INNER JOIN Brand ON " & vbLf & _
        "Sites.Brand_id = Brand.Brand_id) INNER JOIN Country ON " & vbLf & _
        "Sites.Country_id = Country.Country_id) INNER JOIN Local_type ON " & vbLf & _
        "Sites.Local_type_id = Local_type.Local_type_id) INNER JOIN Moso ON " & vbLf & _
        "Sites.Moso_id = Moso.Moso_id) INNER JOIN Region ON " & vbLf & _
        "Sites.Region_id = Region.Region_id) INNER JOIN Service ON " & vbLf & _
        "Sites.Service_id = Service.Service_id) INNER JOIN City ON " & vbLf & _
        "Sites.City_id = City.City_id) ON " & vbLf & _
        "Region.Country_id = Country.Country_id AND Tm.Tm_id = Sites.Tm_id {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM Tm INNER JOIN (((((((Sites INNER JOIN Brand ON " & vbLf & _
        "Sites.Brand_id = Brand.Brand_id) INNER JOIN Country ON " & vbLf & _
        "Sites.Country_id = Country.Country_id) INNER JOIN Local_type ON " & vbLf & _
        "Sites.Local_type_id = Local_type.Local_type_id) INNER JOIN Moso ON " & vbLf & _
        "Sites.Moso_id = Moso.Moso_id) INNER JOIN Region ON " & vbLf & _
        "Sites.Region_id = Region.Region_id) INNER JOIN Service ON " & vbLf & _
        "Sites.Service_id = Service.Service_id) INNER JOIN City ON " & vbLf & _
        "Sites.City_id = City.City_id) ON " & vbLf & _
        "Region.Country_id = Country.Country_id AND Tm.Tm_id = Sites.Tm_id"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @77-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @77-F60079E6
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urls_SiteName", ccsText, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opContains, False, "[Sites].[SiteName]", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @77-40984FC5
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @77-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Brand_City_Country_Local1DataSource Class @77-A61BA892

Class clsRecordBrand_City_Country_Local 'Brand_City_Country_Local Class @114-57669BD2

'Brand_City_Country_Local Variables @114-4A2E6393

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim ClearParameters
    Dim Button_DoSearch
    Dim s_SiteName
'End Brand_City_Country_Local Variables

'Brand_City_Country_Local Class_Initialize Event @114-04003704
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Brand_City_Country_Local")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Brand_City_Country_Local"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set s_SiteName = CCCreateControl(ccsTextBox, "s_SiteName", Empty, ccsText, Empty, CCGetRequestParam("s_SiteName", Method))
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_SiteName)
    End Sub
'End Brand_City_Country_Local Class_Initialize Event

'Brand_City_Country_Local Class_Terminate Event @114-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Brand_City_Country_Local Class_Terminate Event

'Brand_City_Country_Local Validate Method @114-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Brand_City_Country_Local Validate Method

'Brand_City_Country_Local Operation Method @114-FC54F8C5
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "Price_send.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "Price_send.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Brand_City_Country_Local Operation Method

'Brand_City_Country_Local Show Method @114-487390F1
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Brand_City_Country_Local" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_SiteName, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("s_SiteName", "site", "ccsForm"))
        ClearParameters.Page = "Price_send.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_SiteName.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Brand_City_Country_Local" & ":"
            Controls.Show
        End If
    End Sub
'End Brand_City_Country_Local Show Method

End Class 'End Brand_City_Country_Local Class @114-A61BA892

Class clsRecordprice_change_status 'price_change_status Class @171-5918E2A9

'price_change_status Variables @171-AA5C05F8

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim Button_Insert
    Dim Button_Update
    Dim Button_Delete
    Dim Button_Cancel
    Dim aplication_date
    Dim DatePicker_aplication_date
    Dim aplication_time
    Dim ss_id
    Dim price_change_id
'End price_change_status Variables

'price_change_status Class_Initialize Event @171-ADFBE18F
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsprice_change_statusDataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "price_change_status")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "price_change_status"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set Button_Insert = CCCreateButton("Button_Insert", Method)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Delete = CCCreateButton("Button_Delete", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set aplication_date = CCCreateControl(ccsTextBox, "aplication_date", CCSLocales.GetText("aplication_date", ""), ccsDate, DefaultDateFormat, CCGetRequestParam("aplication_date", Method))
        Set DatePicker_aplication_date = CCCreateDatePicker("DatePicker_aplication_date", "price_change_status", "aplication_date")
        Set aplication_time = CCCreateControl(ccsTextBox, "aplication_time", CCSLocales.GetText("aplication_time", ""), ccsText, Empty, CCGetRequestParam("aplication_time", Method))
        Set ss_id = CCCreateControl(ccsHidden, "ss_id", Empty, ccsInteger, Empty, CCGetRequestParam("ss_id", Method))
        ss_id.Required = True
        Set price_change_id = CCCreateControl(ccsHidden, "price_change_id", Empty, ccsInteger, Empty, CCGetRequestParam("price_change_id", Method))
        price_change_id.Required = True
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(aplication_date, aplication_time, ss_id, price_change_id)
    End Sub
'End price_change_status Class_Initialize Event

'price_change_status Initialize Method @171-BD48B8DF
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
            .Parameters("urlprice_change_id") = CCGetRequestParam("price_change_id", ccsGET)
        End With
    End Sub
'End price_change_status Initialize Method

'price_change_status Class_Terminate Event @171-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End price_change_status Class_Terminate Event

'price_change_status Validate Method @171-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End price_change_status Validate Method

'price_change_status Operation Method @171-8E00FE07
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Insert")
            If Button_Insert.Pressed Then
                PressedButton = "Button_Insert"
            ElseIf Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Delete.Pressed Then
                PressedButton = "Button_Delete"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = FileName & "?" & CCGetQueryString("All", Array("ccsForm", "Button_Insert.x", "Button_Insert.y", "Button_Insert", "Button_Update.x", "Button_Update.y", "Button_Update", "Button_Delete.x", "Button_Delete.y", "Button_Delete", "Button_Cancel.x", "Button_Cancel.y", "Button_Cancel"))
        If PressedButton = "Button_Delete" Then
            If NOT Button_Delete.OnClick OR NOT DeleteRow() Then
                Redirect = ""
            Else
                Redirect = "Price_send.asp?" & CCGetQueryString("QueryString", Array("ccsForm", "price_change_id", "aplication_date", "aplication_time", "var"))
            End If
        ElseIf PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            Else
                Redirect = "Price_send.asp?" & CCGetQueryString("QueryString", Array("ccsForm", "price_change_id", "aplication_date", "aplication_time", "var"))
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Insert" Then
                If NOT Button_Insert.OnClick() OR NOT InsertRow() Then
                    Redirect = ""
                End If
            ElseIf PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                Else
                    Redirect = "Price_send.asp?" & CCGetQueryString("QueryString", Array("ccsForm", "price_change_id", "aplication_date", "aplication_time", "var"))
                                
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End price_change_status Operation Method

'price_change_status InsertRow Method @171-55646BB7
    Function InsertRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInsert", Me)
        If NOT InsertAllowed Then InsertRow = False : Exit Function
        DataSource.aplication_date.Value = aplication_date.Value
        DataSource.aplication_time.Value = aplication_time.Value
        DataSource.ss_id.Value = ss_id.Value
        DataSource.price_change_id.Value = price_change_id.Value
        DataSource.Insert(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInsert", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        InsertRow = (Errors.Count = 0)
    End Function
'End price_change_status InsertRow Method

'price_change_status UpdateRow Method @171-AB08B581
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        DataSource.aplication_date.Value = aplication_date.Value
        DataSource.aplication_time.Value = aplication_time.Value
        DataSource.ss_id.Value = ss_id.Value
        DataSource.price_change_id.Value = price_change_id.Value
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End price_change_status UpdateRow Method

'price_change_status DeleteRow Method @171-D5C1DF24
    Function DeleteRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeDelete", Me)
        If NOT DeleteAllowed Then DeleteRow = False : Exit Function
        DataSource.Delete(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterDelete", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        DeleteRow = (Errors.Count = 0)
    End Function
'End price_change_status DeleteRow Method

'price_change_status Show Method @171-81B83C67
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        If CCGetFromGet("ccsForm", Empty) = "" Then
            HTMLFormAction = FileName & "?" & CCAddParam(CCGetQueryString("All", Array("ccsForm", "Button_Insert.x", "Button_Insert.y", "Button_Insert", "Button_Update.x", "Button_Update.y", "Button_Update", "Button_Delete.x", "Button_Delete.y", "Button_Delete", "Button_Cancel.x", "Button_Cancel.y", "Button_Cancel")), "ccsForm", "price_change_status" & IIf(EditMode, ":Edit", ""))
        Else
            HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "price_change_status" & IIf(EditMode, ":Edit", ""))
        End If
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(aplication_date, DatePicker_aplication_date, ss_id, price_change_id, aplication_time, Button_Insert, Button_Update, Button_Delete, Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        aplication_date.Value = Recordset.Fields("aplication_date")
                        aplication_time.Value = Recordset.Fields("aplication_time")
                        ss_id.Value = Recordset.Fields("ss_id")
                        price_change_id.Value = Recordset.Fields("price_change_id")
                    End If
                Else
                    EditMode = False
                End If
            End If
        End If
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors aplication_date.Errors
            Errors.AddErrors aplication_time.Errors
            Errors.AddErrors ss_id.Errors
            Errors.AddErrors price_change_id.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Insert.Visible = NOT EditMode AND InsertAllowed
        Button_Update.Visible = EditMode AND UpdateAllowed
        Button_Delete.Visible = EditMode AND DeleteAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "price_change_status" & ":"
            Controls.Show
        End If
    End Sub
'End price_change_status Show Method

End Class 'End price_change_status Class @171-A61BA892

Class clsprice_change_statusDataSource 'price_change_statusDataSource Class @171-CF166079

'DataSource Variables @171-9F6AA127
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public aplication_date
    Public aplication_time
    Public ss_id
    Public price_change_id
'End DataSource Variables

'DataSource Class_Initialize Event @171-864F638E
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set aplication_date = CCCreateField("aplication_date", "aplication_date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set aplication_time = CCCreateField("aplication_time", "aplication_time", ccsText, Empty, Recordset)
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(aplication_date, aplication_time, ss_id, price_change_id)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "aplication_date", True
        InsertOmitIfEmpty.Add "aplication_time", True
        InsertOmitIfEmpty.Add "ss_id", True
        InsertOmitIfEmpty.Add "price_change_id", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "aplication_date", True
        UpdateOmitIfEmpty.Add "aplication_time", True
        UpdateOmitIfEmpty.Add "ss_id", True
        UpdateOmitIfEmpty.Add "price_change_id", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT *  " & vbLf & _
        "FROM price_change_status {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @171-1574D3EC
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlprice_change_id", ccsInteger, Empty, Empty, Empty, False
            AllParamsSet = .AllParamsSet
            .Criterion(1) = .Operation(opEqual, False, "price_change_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @171-48A2DA7D
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @171-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @171-63C8EEEA
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM price_change_status" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @171-17B66192
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_aplication_date : IsDef_aplication_date = CCIsDefined("aplication_date", "Form")
        Dim IsDef_aplication_time : IsDef_aplication_time = CCIsDefined("aplication_time", "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id", "Form")
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id", "Form")
        If Not UpdateOmitIfEmpty("aplication_date") Or IsDef_aplication_date Then Cmd.AddSQLStrings "aplication_date=" & Connection.ToSQL(aplication_date, aplication_date.DataType), Empty
        If Not UpdateOmitIfEmpty("aplication_time") Or IsDef_aplication_time Then Cmd.AddSQLStrings "aplication_time=" & Connection.ToSQL(aplication_time, aplication_time.DataType), Empty
        If Not UpdateOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id=" & Connection.ToSQL(ss_id, ss_id.DataType), Empty
        If Not UpdateOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id=" & Connection.ToSQL(price_change_id, price_change_id.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "price_change_status", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @171-CD3AC7EE
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_aplication_date : IsDef_aplication_date = CCIsDefined("aplication_date", "Form")
        Dim IsDef_aplication_time : IsDef_aplication_time = CCIsDefined("aplication_time", "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id", "Form")
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id", "Form")
        If Not InsertOmitIfEmpty("aplication_date") Or IsDef_aplication_date Then Cmd.AddSQLStrings "aplication_date", Connection.ToSQL(aplication_date, aplication_date.DataType)
        If Not InsertOmitIfEmpty("aplication_time") Or IsDef_aplication_time Then Cmd.AddSQLStrings "aplication_time", Connection.ToSQL(aplication_time, aplication_time.DataType)
        If Not InsertOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id", Connection.ToSQL(ss_id, ss_id.DataType)
        If Not InsertOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id", Connection.ToSQL(price_change_id, price_change_id.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "price_change_status", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End price_change_statusDataSource Class @171-A61BA892

Class clsEditableGridprice_send 'price_send Class @185-F6179667

'price_send Variables @185-3DA20FE7

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public HTMLFormMethod
    Public PressedButton
    Public Errors
    Public IsFormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public PageNumber
    Public IsDSEmpty
    Public RowNumber
    Public CachedColumns
    Public CachedColumnsNames
    Public CachedColumnsNumber
    Public SubmittedRows
    Public EmptyRows
    Public ErrorMessages
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public ActiveSorter, SortingDirection
    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls
    Public NoRecordsControls
    Private MaxCachedValues
    Private CachedValuesNumber
    Private NewEmptyRows
    Private ErrorControls

    ' Class variables
    Dim Sorter_price_change_id
    Dim Sorter_ss_id
    Dim Sorter_grade_id
    Dim Sorter_levels
    Dim Sorter_ppu_actual
    Dim Sorter_price_change_type
    Dim Sorter_new_price
    Dim price_change_id
    Dim ss_id
    Dim grade_id
    Dim levels
    Dim ppu_actual
    Dim price_change_type
    Dim new_price
    Dim CheckBox_Delete
    Dim Navigator
    Dim Button_Submit
    Dim Cancel
    Dim RowIDAttribute
    Dim RowNameAttribute
    Dim RowStyleAttribute
    Dim TextBox1
    Public Row
'End price_send Variables

'price_send Class_Initialize Event @185-F562532E
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set ErrorControls = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsprice_sendDataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        ComponentName = "price_send"

        ActiveSorter = CCGetParam("price_sendOrder", Empty)
        SortingDirection = CCGetParam("price_sendDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CInt(CCGetParam(ComponentName & "Page", 1))

        If CCGetFromGet("ccsForm", Empty) = ComponentName Then
            IsFormSubmitted = True
            EditMode = True
        Else
            IsFormSubmitted = False
            EditMode = False
        End If
        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        Set Sorter_price_change_id = CCCreateSorter("Sorter_price_change_id", Me, FileName)
        Set Sorter_ss_id = CCCreateSorter("Sorter_ss_id", Me, FileName)
        Set Sorter_grade_id = CCCreateSorter("Sorter_grade_id", Me, FileName)
        Set Sorter_levels = CCCreateSorter("Sorter_levels", Me, FileName)
        Set Sorter_ppu_actual = CCCreateSorter("Sorter_ppu_actual", Me, FileName)
        Set Sorter_price_change_type = CCCreateSorter("Sorter_price_change_type", Me, FileName)
        Set Sorter_new_price = CCCreateSorter("Sorter_new_price", Me, FileName)
        Set price_change_id = CCCreateControl(ccsTextBox, "price_change_id", CCSLocales.GetText("price_change_id", ""), ccsInteger, Empty, CCGetRequestParam("price_change_id", Method))
        Set ss_id = CCCreateControl(ccsTextBox, "ss_id", CCSLocales.GetText("ss_id", ""), ccsInteger, Empty, CCGetRequestParam("ss_id", Method))
        ss_id.Required = True
        Set grade_id = CCCreateList(ccsListBox, "grade_id", CCSLocales.GetText("grade_id", ""), ccsInteger, CCGetRequestParam("grade_id", Method), Empty)
        grade_id.BoundColumn = "grade_id"
        grade_id.TextColumn = "grade_name"
        Set grade_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Grades {SQL_Where} {SQL_OrderBy}", "", ""))
        grade_id.Required = True
        Set levels = CCCreateList(ccsListBox, "levels", CCSLocales.GetText("levels", ""), ccsInteger, CCGetRequestParam("levels", Method), Empty)
        Set levels.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("1", "2", "3", "4", "5", "6", "7"), _
            Array("1", "2", "3", "4", "5", "6", "7")))
        levels.Required = True
        Set ppu_actual = CCCreateControl(ccsTextBox, "ppu_actual", CCSLocales.GetText("ppu_actual", ""), ccsFloat, Empty, CCGetRequestParam("ppu_actual", Method))
        Set price_change_type = CCCreateList(ccsListBox, "price_change_type", CCSLocales.GetText("price_change_type", ""), ccsInteger, CCGetRequestParam("price_change_type", Method), Empty)
        price_change_type.BoundColumn = "price_change_type"
        price_change_type.TextColumn = "price_change_description"
        Set price_change_type.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM price_type {SQL_Where} {SQL_OrderBy}", "", ""))
        price_change_type.Required = True
        Set new_price = CCCreateControl(ccsTextBox, "new_price", CCSLocales.GetText("new_price", ""), ccsFloat, Array(False, 2, True, False, False, "", "", 1, True, ""), CCGetRequestParam("new_price", Method))
        Set CheckBox_Delete = CCCreateControl(ccsCheckBox, "CheckBox_Delete", Empty, ccsBoolean, DefaultBooleanFormat, CCGetRequestParam("CheckBox_Delete", Method))
        CheckBox_Delete.CheckedValue = true
        CheckBox_Delete.UncheckedValue = false
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        Set Button_Submit = CCCreateButton("Button_Submit", Method)
        Set Cancel = CCCreateButton("Cancel", Method)
        Set RowIDAttribute = CCCreateControl(ccsLabel, "RowIDAttribute", Empty, ccsText, Empty, CCGetRequestParam("RowIDAttribute", Method))
        Set RowNameAttribute = CCCreateControl(ccsLabel, "RowNameAttribute", Empty, ccsText, Empty, CCGetRequestParam("RowNameAttribute", Method))
        Set RowStyleAttribute = CCCreateControl(ccsLabel, "RowStyleAttribute", Empty, ccsText, Empty, CCGetRequestParam("RowStyleAttribute", Method))
        RowStyleAttribute.HTML = True
        Set TextBox1 = CCCreateControl(ccsTextBox, "TextBox1", Empty, ccsText, Empty, CCGetRequestParam("TextBox1", Method))
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(price_change_id, ss_id, grade_id, levels, ppu_actual, price_change_type, new_price, TextBox1)

        SubmittedRows = 0
        NewEmptyRows = 0
        EmptyRows = 1

        InitCachedColumns()

        IsDSEmpty = True
    End Sub
'End price_send Class_Initialize Event

'price_send InitCachedColumns Method @185-52628C1A
    Sub InitCachedColumns()
        Dim RetrievedNumber, i
        CachedColumnsNumber = 5
        ReDim CachedColumnsNames(CachedColumnsNumber)
        CachedColumnsNames(0) = "price_send_id"
        CachedColumnsNames(1) = "ss_id"
        CachedColumnsNames(2) = "grade_id"
        CachedColumnsNames(3) = "levels"
        CachedColumnsNames(4) = "price_change_type"

        RetrievedNumber = 0
        CachedColumns = GetCachedColumns()

        If IsArray(CachedColumns) Then
            RetrievedNumber = UBound(CachedColumns)
            If RetrievedNumber > 0 Then
                MaxCachedValues = CInt(RetrievedNumber / CachedColumnsNumber)
                If (RetrievedNumber Mod CachedColumnsNumber) > 0 Then
                    MaxCachedValues = MaxCachedValues + 1
                End If
                CachedValuesNumber = MaxCachedValues
            End If
        End If

        If RetrievedNumber = 0 Then
            MaxCachedValues = 50
            ReDim CachedColumns(MaxCachedValues * CachedColumnsNumber)
            CachedValuesNumber = 0
        End If 

        If SubmittedRows > 0 Or NewEmptyRows > 0 Then
            EmptyRows = NewEmptyRows
        End If

        DataSource.CachedColumns = CachedColumns
        DataSource.CachedColumnsNumber = CachedColumnsNumber
        ReDim ErrorMessages(SubmittedRows + EmptyRows)
    End Sub
'End price_send InitCachedColumns Method

'price_send Initialize Method @185-3D281C70
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        With DataSource

            Set .Connection = objConnection
            .PageSize = PageSize
            .SetOrder ActiveSorter, SortingDirection
            .AbsolutePage = PageNumber
            .Parameters("urlprice_change_id") = CCGetRequestParam("price_change_id", ccsGET)
        End With
    End Sub
'End price_send Initialize Method

'price_send Class_Terminate Event @185-80BE16BA
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End price_send Class_Terminate Event

'price_send Validate Method @185-E51056EF
    Function Validate()
        Dim Validation
        Dim i, InsertedRows, Method, IsDeleted, IsEmptyRow, IsNewRow,EGErrors
        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        Validation = True

        If SubmittedRows > 0 Then
            Set EGErrors = New clsErrors
            EGErrors.AddErrors(Errors)
            Errors.Clear
            For i = 1 To SubmittedRows
                IsDeleted = (Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0)
                IsEmptyRow = (Len(CCGetRequestParam("price_change_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ss_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("grade_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("levels_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ppu_actual_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("price_change_type_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("new_price_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("TextBox1_" & CStr(i), Method)) = 0)

                If (Not IsDeleted) And (Not IsEmptyRow Or (i < SubmittedRows - EmptyRows + 1)) Then
                    price_change_id.Errors.Clear
                    price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), Method)
                    ss_id.Errors.Clear
                    ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), Method)
                    grade_id.Errors.Clear
                    grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), Method)
                    levels.Errors.Clear
                    levels.Text = CCGetRequestParam("levels_" & CStr(i), Method)
                    ppu_actual.Errors.Clear
                    ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), Method)
                    price_change_type.Errors.Clear
                    price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), Method)
                    new_price.Errors.Clear
                    new_price.Text = CCGetRequestParam("new_price_" & CStr(i), Method)
                    TextBox1.Errors.Clear
                    TextBox1.Text = CCGetRequestParam("TextBox1_" & CStr(i), Method)
                    ValidatingControls.Validate
                    CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidateRow", Me)
                    If Not ValidatingControls.IsValid() or Errors.Count >0 Then
                        Errors.AddErrors price_change_id.Errors
                        Errors.AddErrors ss_id.Errors
                        Errors.AddErrors grade_id.Errors
                        Errors.AddErrors levels.Errors
                        Errors.AddErrors ppu_actual.Errors
                        Errors.AddErrors price_change_type.Errors
                        Errors.AddErrors new_price.Errors
                        Errors.AddErrors CheckBox_Delete.Errors
                        Errors.AddErrors TextBox1.Errors
                        ErrorMessages(i) = Errors.ToString()
                        Validation = False
                        Errors.Clear
                    End If
                End If
            Next
            Errors.AddErrors(EGErrors)
            Set EGErrors = Nothing
        End If

        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = Validation And (Errors.Count = 0)
    End Function
'End price_send Validate Method

'price_send ProcessOperations Method @185-1D386A1C
    Sub ProcessOperations()
        Dim TmpWhere: TmpWhere = Datasource.Where

        If Not ( Visible And IsFormSubmitted ) Then Exit Sub

        If IsFormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Submit", "Button_Submit")
            If Button_Submit.Pressed Then
                PressedButton = "Button_Submit"
            ElseIf Cancel.Pressed Then
                PressedButton = "Cancel"
            End If
        End If
        Redirect = FileName & "?" & CCGetQueryString("QueryString",Array("ccsForm", "Button_Submit.x", "Button_Submit", "Cancel.x", "Cancel"))

        If PressedButton = "Cancel" Then
            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSubmit", Me)
            If NOT Cancel.OnClick Then
                Redirect = ""
            End If
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterSubmit", Me)
        ElseIf Validate() Then
            If PressedButton = "Button_Submit" Then
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSubmit", Me)
                If NOT Button_Submit.OnClick() OR (NOT InsertRows() And InsertAllowed) OR (NOT UpdateRows() And UpdateAllowed) OR (NOT DeleteRows() And DeleteAllowed) Then
                    Redirect = ""
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "AfterSubmit", Me)
            End If
        Else
            Redirect = ""
        End If

        Datasource.Where = TmpWhere
    End Sub
'End price_send ProcessOperations Method

'price_send InsertRows Method @185-2C3BACB6
    Function InsertRows()
        If NOT InsertAllowed Then InsertRows = False : Exit Function

        Dim i, InsertedRows, Method, IsDeleted, IsEmptyRow, HasErrors

        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)

        If SubmittedRows > 0 Then
            i = SubmittedRows - EmptyRows
            For i = (SubmittedRows - EmptyRows + 1) To SubmittedRows
                IsDeleted = (Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0)
                IsEmptyRow = True
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("price_change_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ss_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("grade_id_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("levels_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("ppu_actual_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("price_change_type_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("new_price_" & CStr(i), Method)) = 0)
                IsEmptyRow = IsEmptyRow And (Len(CCGetRequestParam("TextBox1_" & CStr(i), Method)) = 0)

                If (Not IsDeleted) And (Not IsEmptyRow) Then
                    price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), Method)
                    DataSource.price_change_id.Value = price_change_id.Value
                    ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), Method)
                    DataSource.ss_id.Value = ss_id.Value
                    grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), Method)
                    DataSource.grade_id.Value = grade_id.Value
                    levels.Text = CCGetRequestParam("levels_" & CStr(i), Method)
                    DataSource.levels.Value = levels.Value
                    ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), Method)
                    DataSource.ppu_actual.Value = ppu_actual.Value
                    price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), Method)
                    DataSource.price_change_type.Value = price_change_type.Value
                    new_price.Text = CCGetRequestParam("new_price_" & CStr(i), Method)
                    DataSource.new_price.Value = new_price.Value
                    DataSource.CurrentRow = i
                    DataSource.Insert(Command)


                    If DataSource.Errors.Count > 0 Then
                        HasErrors = True
                        ErrorMessages(i) = DataSource.Errors.ToString()
                        DataSource.Errors.Clear
                    End If
                End If
            Next
        End If

        InsertRows = Not(HasErrors)
    End Function
'End price_send InsertRows Method

'price_send UpdateRows Method @185-28F30A39
    Function UpdateRows()
        If NOT UpdateAllowed Then UpdateRows = False : Exit Function

        Dim i, InsertedRows, Method, IsDeleted, HasErrors
        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        If SubmittedRows > 0 Then
            For i = 1 To SubmittedRows - EmptyRows
                IsDeleted = (Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0)

                If Not IsDeleted Then
                    price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), Method)
                    DataSource.price_change_id.Value = price_change_id.Value
                    ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), Method)
                    DataSource.ss_id.Value = ss_id.Value
                    grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), Method)
                    DataSource.grade_id.Value = grade_id.Value
                    levels.Text = CCGetRequestParam("levels_" & CStr(i), Method)
                    DataSource.levels.Value = levels.Value
                    ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), Method)
                    DataSource.ppu_actual.Value = ppu_actual.Value
                    price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), Method)
                    DataSource.price_change_type.Value = price_change_type.Value
                    new_price.Text = CCGetRequestParam("new_price_" & CStr(i), Method)
                    DataSource.new_price.Value = new_price.Value
                    DataSource.CurrentRow = i
                    DataSource.Update(Command)


                    If DataSource.Errors.Count > 0 Then
                        HasErrors = True
                        ErrorMessages(i) = DataSource.Errors.ToString()
                        DataSource.Errors.Clear
                    End If
                End If
            Next
        End If
        UpdateRows = Not(HasErrors)
    End Function
'End price_send UpdateRows Method

'price_send DeleteRows Method @185-BF059AF4
    Function DeleteRows()
        Dim i, Method, HasErrors

        Method = IIf(IsFormSubmitted, ccsPost, ccsGet)
        If NOT DeleteAllowed Then DeleteRows = False : Exit Function


        If SubmittedRows > 0 Then
            For i = 1 To SubmittedRows - EmptyRows
                If Len(CCGetRequestParam("CheckBox_Delete_" & CStr(i), Method)) > 0 Then
                    DataSource.CurrentRow = i
                    DataSource.Delete(Command)


                    If DataSource.Errors.Count > 0 Then
                        HasErrors = True
                        ErrorMessages(i) = DataSource.Errors.ToString()
                        DataSource.Errors.Clear
                    End If
                End If
            Next
        End If

        DeleteRows = Not(HasErrors)
    End Function
'End price_send DeleteRows Method

'GetFormScript Method @185-C012DA2E
    Function GetFormScript(TotalRows)
        Dim script,i: script = ""
        script = script & vbNewLine & "<script language=""JavaScript"">" & vbNewLine & "<!--" & vbNewLine
        script = script & "var price_sendElements;" & vbNewLine
        script = script & "var price_sendEmptyRows = 1;" & vbNewLine
        script = script & "var " & ComponentName & "price_change_idID = 0;" & vbNewLine
        script = script & "var " & ComponentName & "ss_idID = 1;" & vbNewLine
        script = script & "var " & ComponentName & "grade_idID = 2;" & vbNewLine
        script = script & "var " & ComponentName & "levelsID = 3;" & vbNewLine
        script = script & "var " & ComponentName & "ppu_actualID = 4;" & vbNewLine
        script = script & "var " & ComponentName & "price_change_typeID = 5;" & vbNewLine
        script = script & "var " & ComponentName & "new_priceID = 6;" & vbNewLine
        script = script & "var " & ComponentName & "DeleteControl = 7;" & vbNewLine
        script = script & "var " & ComponentName & "TextBox1ID = 8;" & vbNewLine
        script = script & vbNewLine & "function initprice_sendElements() {" & vbNewLine
        script = script & vbTab & "var ED = document.forms[""price_send""];" & vbNewLine
        script = script & vbTab & "price_sendElements = new Array (" & vbNewLine
        For i = 1 To TotalRows
            script = script & vbTab & vbTab & "new Array(" & "ED.price_change_id_" & CStr(i) & ", " & "ED.ss_id_" & CStr(i) & ", " & "ED.grade_id_" & CStr(i) & ", " & "ED.levels_" & CStr(i) & ", " & "ED.ppu_actual_" & CStr(i) & ", " & "ED.price_change_type_" & CStr(i) & ", " & "ED.new_price_" & CStr(i) & ", " & "ED.CheckBox_Delete_" & CStr(i) & ", " & "ED.TextBox1_" & CStr(i) & ")"
            If(i <> TotalRows) Then script = script & "," & vbNewLine
        Next
        script = script & ");" & vbNewLine
        script = script & "}" & vbNewLine
        script = script & vbNewLine & "//-->" & vbNewLine & "</script>"
        GetFormScript = script
    End Function
'End GetFormScript Method

'GetFormState Method @185-8BEF9A95
    Function GetFormState
        Dim FormState, i, LastValueIndex, NewRows

        FormState = ""
        LastValueIndex = CachedValuesNumber * CachedColumnsNumber - 1

        If EditMode And LastValueIndex >= 0 Then

            For i = 1 To CachedColumnsNumber
                FormState = FormState & CachedColumnsNames(i-1)
                If i < CachedColumnsNumber Or LastValueIndex >= 0 Then FormState = FormState & ";"
            Next

            For i = 0 To LastValueIndex
                If IsNull(CachedColumns(i)) Then  CachedColumns(i) = ""
                FormState = FormState & CCToHTML(CCEscapeLOV(CachedColumns(i)))
                If i < LastValueIndex Then FormState = FormState & ";"
            Next
        End If

        NewRows = IIf(InsertAllowed, EmptyRows, 0)
        GetFormState = CStr(SubmittedRows - NewRows) & ";" & CStr(NewRows)
        If Len(FormState) > 0 Then GetFormState = GetFormState & ";" & FormState

    End Function
'End GetFormState Method

'GetCachedColumns Method @185-AA749F06
    Function GetCachedColumns
        Dim FormState, i, TotalValuesNumber, TempColumns, NewCachedColumns, TempValuesNumber
        Dim NewSubmittedRows : NewSubmittedRows = 0

        NewCachedColumns = Empty
        FormState = CCGetRequestParam("FormState", ccsPost)

        If CCGetFromGet("ccsForm", Empty) = ComponentName Then
            If Not IsNull(FormState) Then
                If InStr(FormState,"\;") > 0 Then _
                    FormState = Replace(FormState, "\;", "<!--semicolon-->")
                If InStr(FormState,";") > 0 Then 
                    TempColumns = Split(FormState,";")
                    If IsArray(TempColumns) Then 
                        TempValuesNumber = UBound(TempColumns) - 1
                        If TempValuesNumber >= 0 Then
                            NewSubmittedRows = TempColumns(0)
                            NewEmptyRows     = TempColumns(1)
                        End If
                        SubmittedRows = CLng(NewSubmittedRows) + CLng(NewEmptyRows)

                        If TempValuesNumber > 1 And TempValuesNumber >= CachedColumnsNumber Then
                            ReDim NewCachedColumns(TempValuesNumber - CachedColumnsNumber + 1)
                            For i = 0 To TempValuesNumber - CachedColumnsNumber - 1
                                NewCachedColumns(i) = Replace(CCUnEscapeLOV(TempColumns(i + CachedColumnsNumber + 2)),"<!--semicolon-->",";")
                            Next
                        End If
                    End If
                Else
                    SubmittedRows = FormState
                End If
            End If
        End If

        GetCachedColumns = NewCachedColumns
    End Function
'End GetCachedColumns Method

'price_send Show Method @185-2E84F1D9
    Sub Show(Tpl)
        Dim StaticControls,RowControls

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If Recordset.State = adStateOpen Then 
            EditMode = NOT Recordset.EOF 
        Else
            EditMode = False
        End If
        IsDSEmpty = NOT EditMode

        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "price_send")
        Set TemplateBlock = Tpl.Block("EditableGrid " & ComponentName)
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        TemplateBlock.Variable("HTMLFormProperties") = "action=""" & HTMLFormAction & """ method=""post"" " & "name=""" & ComponentName & """"
        TemplateBlock.Variable("HTMLFormEnctype") = "application/x-www-form-urlencoded"

        Dim NoRecordsBlock
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If

        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_price_change_id, Sorter_ss_id, Sorter_grade_id, Sorter_levels, Sorter_ppu_actual, Sorter_price_change_type, Sorter_new_price, _
                 Navigator, Button_Submit, Cancel))
        Set RowControls = CCCreateCollection(TemplateBlock.Block("Row"), Null, ccsParseAccumulate, _
            Array(price_change_id, ss_id, grade_id, levels, ppu_actual, price_change_type, new_price, _
                 CheckBox_Delete, RowIDAttribute, RowNameAttribute, RowStyleAttribute, TextBox1))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        If Not DeleteAllowed Then CheckBox_Delete.Visible = False

        Dim i, j
        i = 1 : j = 0
        If EditMode AND ReadAllowed Then
            If Recordset.Errors.Count > 0 Then
                With TemplateBlock.Block("Error")
                    .Variable("Error") = Recordset.Errors.ToString
                    .Parse False
                End With
            ElseIf Not Recordset.EOF Then
                While Not Recordset.EoF AND (i-1) < PageSize
                    RowNumber = i
                    Attributes("rowNumber") = i
                    
                    
                    

                    If Not IsFormSubmitted Then
                        price_change_id.Value = Recordset.Fields("price_change_id")
                    Else
                        price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        ss_id.Value = Recordset.Fields("ss_id")
                    Else
                        ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        grade_id.Value = Recordset.Fields("grade_id")
                    Else
                        grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        levels.Value = Recordset.Fields("levels")
                    Else
                        levels.Text = CCGetRequestParam("levels_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        ppu_actual.Value = Recordset.Fields("ppu_actual")
                    Else
                        ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        price_change_type.Value = Recordset.Fields("price_change_type")
                    Else
                        price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), ccsPost)
                    End If
                    If Not IsFormSubmitted Then
                        new_price.Value = Recordset.Fields("new_price")
                    Else
                        new_price.Text = CCGetRequestParam("new_price_" & CStr(i), ccsPost)
                    End If
                    If IsFormSubmitted Then 
                        CheckBox_Delete.Value = CCGetRequestParam("CheckBox_Delete_" & CStr(i), ccsPost)
                    End If
                    If IsFormSubmitted Then 
                        TextBox1.Text = CCGetRequestParam("TextBox1_" & CStr(i), ccsPost)
                    End If
                    price_change_id.ExternalName = "price_change_id_" & CStr(i)
                    ss_id.ExternalName = "ss_id_" & CStr(i)
                    grade_id.ExternalName = "grade_id_" & CStr(i)
                    levels.ExternalName = "levels_" & CStr(i)
                    ppu_actual.ExternalName = "ppu_actual_" & CStr(i)
                    price_change_type.ExternalName = "price_change_type_" & CStr(i)
                    new_price.ExternalName = "new_price_" & CStr(i)
                    CheckBox_Delete.ExternalName = "CheckBox_Delete_" & CStr(i)
                    RowIDAttribute.ExternalName = "RowIDAttribute_" & CStr(i)
                    RowNameAttribute.ExternalName = "RowNameAttribute_" & CStr(i)
                    RowStyleAttribute.ExternalName = "RowStyleAttribute_" & CStr(i)
                    TextBox1.ExternalName = "TextBox1_" & CStr(i)

                    If j >= MaxCachedValues Then
                            MaxCachedValues = MaxCachedValues + 50
                            ReDim Preserve CachedColumns(MaxCachedValues*CachedColumnsNumber)
                    End If
                    CachedColumns(j * CachedColumnsNumber) = Recordset.Recordset.Fields("price_send_id")
                    CachedColumns(j * CachedColumnsNumber + 1) = Recordset.Recordset.Fields("ss_id")
                    CachedColumns(j * CachedColumnsNumber + 2) = Recordset.Recordset.Fields("grade_id")
                    CachedColumns(j * CachedColumnsNumber + 3) = Recordset.Recordset.Fields("levels")
                    CachedColumns(j * CachedColumnsNumber + 4) = Recordset.Recordset.Fields("price_change_type")
                    CachedValuesNumber = i

                    If IsFormSubmitted Then
                        If Len(ErrorMessages(i)) > 0 Then
                            With TemplateBlock.Block("Row").Block("RowError")
                                .Variable("Error") = ErrorMessages(i)
                                .Parse False
                            End With
                        Else
                            TemplateBlock.Block("Row").Block("RowError").Visible = False
                        End If
                    End If

                    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                    Attributes.Show TemplateBlock.Block("Row"), "price_send:"
                    RowControls.Show
                    i = i + 1: j = j + 1

                    Recordset.MoveNext
                Wend
            End If
            Attributes.Show TemplateBlock, "price_send:"
            
            
            
        ElseIf Not EditMode And (Not InsertAllowed Or EmptyRows=0)Then
            NoRecordsBlock.Parse ccsParseOverwrite
        End If

        If Not InsertAllowed And Not UpdateAllowed And Not DeleteAllowed Then
            Button_Submit.Visible = False
        End If

        CheckBox_Delete.Visible = False
        price_change_id.Value = ""
        ss_id.Value = ""
        grade_id.Value = ""
        levels.Value = ""
        ppu_actual.Value = ""
        price_change_type.Value = ""
        new_price.Value = ""
        RowIDAttribute.Value = ""
        RowNameAttribute.Value = ""
        RowStyleAttribute.Value = ""
        TextBox1.Value = ""

        Dim NewRows
        NewRows = IIf(InsertAllowed, EmptyRows, 0)
        For i = i To i + NewRows - 1
            Attributes("rowNumber") = i
            price_change_id.ExternalName = "price_change_id_" & CStr(i)
            ss_id.ExternalName = "ss_id_" & CStr(i)
            grade_id.ExternalName = "grade_id_" & CStr(i)
            levels.ExternalName = "levels_" & CStr(i)
            ppu_actual.ExternalName = "ppu_actual_" & CStr(i)
            price_change_type.ExternalName = "price_change_type_" & CStr(i)
            new_price.ExternalName = "new_price_" & CStr(i)
            RowIDAttribute.ExternalName = "RowIDAttribute_" & CStr(i)
            RowNameAttribute.ExternalName = "RowNameAttribute_" & CStr(i)
            RowStyleAttribute.ExternalName = "RowStyleAttribute_" & CStr(i)
            TextBox1.ExternalName = "TextBox1_" & CStr(i)

            If IsFormSubmitted Then 
                CheckBox_Delete.Value = CCGetRequestParam("CheckBox_Delete_" & CStr(i), ccsPost)
            End If

            If IsFormSubmitted Then
                price_change_id.Text = CCGetRequestParam("price_change_id_" & CStr(i), ccsPost)
                ss_id.Text = CCGetRequestParam("ss_id_" & CStr(i), ccsPost)
                grade_id.Text = CCGetRequestParam("grade_id_" & CStr(i), ccsPost)
                levels.Text = CCGetRequestParam("levels_" & CStr(i), ccsPost)
                ppu_actual.Text = CCGetRequestParam("ppu_actual_" & CStr(i), ccsPost)
                price_change_type.Text = CCGetRequestParam("price_change_type_" & CStr(i), ccsPost)
                new_price.Text = CCGetRequestParam("new_price_" & CStr(i), ccsPost)
                RowIDAttribute.Text = CCGetRequestParam("RowIDAttribute_" & CStr(i), ccsPost)
                RowNameAttribute.Text = CCGetRequestParam("RowNameAttribute_" & CStr(i), ccsPost)
                RowStyleAttribute.Text = CCGetRequestParam("RowStyleAttribute_" & CStr(i), ccsPost)
                TextBox1.Text = CCGetRequestParam("TextBox1_" & CStr(i), ccsPost)

                If Len(ErrorMessages(i)) > 0 Then
                    With TemplateBlock.Block("Row").Block("RowError")
                        .Variable("Error") = ErrorMessages(i)
                        .Parse False
                    End With
                Else
                    TemplateBlock.Block("Row").Block("RowError").Visible = False
                End If
            End If

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
            Attributes.Show TemplateBlock.Block("Row"), "price_send:"
            RowControls.Show
        Next

        SubmittedRows = i - 1
        TemplateBlock.Variable("FormScript") = GetFormScript(i - 1)
        TemplateBlock.Variable("FormState")  = GetFormState()

        If IsFormSubmitted Then
            If Errors.Count > 0 Or DataSource.Errors.Count > 0 Then
                Errors.addErrors DataSource.Errors
                With TemplateBlock.Block("Error")
                    .Variable("Error") = Errors.ToString
                    .Parse False
                End With
            End If
        End If

        Navigator.PageSize = PageSize
        Navigator.SetDataSource Recordset
        StaticControls.Show

    End Sub
'End price_send Show Method

'price_send PageSize Property Let @185-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End price_send PageSize Property Let

'price_send PageSize Property Get @185-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End price_send PageSize Property Get

End Class 'End price_send Class @185-A61BA892

Class clsprice_sendDataSource 'price_sendDataSource Class @185-132A83E7

'DataSource Variables @185-6F5F8522
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CachedColumns
    Public CachedColumnsNumber
    Public CurrentRow
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public price_change_id
    Public ss_id
    Public grade_id
    Public levels
    Public ppu_actual
    Public price_change_type
    Public new_price
'End DataSource Variables

'DataSource Class_Initialize Event @185-E260E3EC
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Set ss_id = CCCreateField("ss_id", "ss_id", ccsInteger, Empty, Recordset)
        Set grade_id = CCCreateField("grade_id", "grade_id", ccsInteger, Empty, Recordset)
        Set levels = CCCreateField("levels", "levels", ccsInteger, Empty, Recordset)
        Set ppu_actual = CCCreateField("ppu_actual", "ppu_actual", ccsFloat, Empty, Recordset)
        Set price_change_type = CCCreateField("price_change_type", "price_change_type", ccsInteger, Empty, Recordset)
        Set new_price = CCCreateField("new_price", "new_price", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(price_change_id, ss_id, grade_id, levels, ppu_actual, price_change_type, new_price)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "price_change_id", True
        InsertOmitIfEmpty.Add "ss_id", True
        InsertOmitIfEmpty.Add "grade_id", True
        InsertOmitIfEmpty.Add "levels", True
        InsertOmitIfEmpty.Add "ppu_actual", True
        InsertOmitIfEmpty.Add "price_change_type", True
        InsertOmitIfEmpty.Add "new_price", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "price_change_id", True
        UpdateOmitIfEmpty.Add "ss_id", True
        UpdateOmitIfEmpty.Add "grade_id", True
        UpdateOmitIfEmpty.Add "levels", True
        UpdateOmitIfEmpty.Add "ppu_actual", True
        UpdateOmitIfEmpty.Add "price_change_type", True
        UpdateOmitIfEmpty.Add "new_price", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_price_change_id", "price_change_id", ""), _
            Array("Sorter_ss_id", "ss_id", ""), _
            Array("Sorter_grade_id", "grade_id", ""), _
            Array("Sorter_levels", "levels", ""), _
            Array("Sorter_ppu_actual", "ppu_actual", ""), _
            Array("Sorter_price_change_type", "price_change_type", ""), _
            Array("Sorter_new_price", "new_price", ""))

        SQL = "SELECT TOP {SqlParam_endRecord} *  " & vbLf & _
        "FROM price_send {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM price_send"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @185-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @185-5C2A6A53
    Public Sub BuildTableWhere()
        If CurrentRow > 0 Then
            Where = "price_send_id=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber), ccsInteger) & " AND ss_id=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 1), ccsInteger) & " AND grade_id=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 2), ccsInteger) & " AND levels=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 3), ccsInteger) & " AND price_change_type=" & Connection.ToSQL(CachedColumns((CurrentRow - 1) * CachedColumnsNumber + 4), ccsInteger)
        End If
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlprice_change_id", ccsInteger, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opEqual, False, "price_change_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @185-48D41146
    Function Open(Cmd)
        Errors.Clear
        CurrentRow = 0
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        Set WhereParameters = Nothing
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @185-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @185-48969D0E
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM price_send" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @185-2D74ACA3
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id_" & CurrentRow, "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id_" & CurrentRow, "Form")
        Dim IsDef_grade_id : IsDef_grade_id = CCIsDefined("grade_id_" & CurrentRow, "Form")
        Dim IsDef_levels : IsDef_levels = CCIsDefined("levels_" & CurrentRow, "Form")
        Dim IsDef_ppu_actual : IsDef_ppu_actual = CCIsDefined("ppu_actual_" & CurrentRow, "Form")
        Dim IsDef_price_change_type : IsDef_price_change_type = CCIsDefined("price_change_type_" & CurrentRow, "Form")
        Dim IsDef_new_price : IsDef_new_price = CCIsDefined("new_price_" & CurrentRow, "Form")
        If Not UpdateOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id=" & Connection.ToSQL(price_change_id, price_change_id.DataType), Empty
        If Not UpdateOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id=" & Connection.ToSQL(ss_id, ss_id.DataType), Empty
        If Not UpdateOmitIfEmpty("grade_id") Or IsDef_grade_id Then Cmd.AddSQLStrings "grade_id=" & Connection.ToSQL(grade_id, grade_id.DataType), Empty
        If Not UpdateOmitIfEmpty("levels") Or IsDef_levels Then Cmd.AddSQLStrings "levels=" & Connection.ToSQL(levels, levels.DataType), Empty
        If Not UpdateOmitIfEmpty("ppu_actual") Or IsDef_ppu_actual Then Cmd.AddSQLStrings "ppu_actual=" & Connection.ToSQL(ppu_actual, ppu_actual.DataType), Empty
        If Not UpdateOmitIfEmpty("price_change_type") Or IsDef_price_change_type Then Cmd.AddSQLStrings "price_change_type=" & Connection.ToSQL(price_change_type, price_change_type.DataType), Empty
        If Not UpdateOmitIfEmpty("new_price") Or IsDef_new_price Then Cmd.AddSQLStrings "new_price=" & Connection.ToSQL(new_price, new_price.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "price_send", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @185-07863575
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_price_change_id : IsDef_price_change_id = CCIsDefined("price_change_id_" & CurrentRow, "Form")
        Dim IsDef_ss_id : IsDef_ss_id = CCIsDefined("ss_id_" & CurrentRow, "Form")
        Dim IsDef_grade_id : IsDef_grade_id = CCIsDefined("grade_id_" & CurrentRow, "Form")
        Dim IsDef_levels : IsDef_levels = CCIsDefined("levels_" & CurrentRow, "Form")
        Dim IsDef_ppu_actual : IsDef_ppu_actual = CCIsDefined("ppu_actual_" & CurrentRow, "Form")
        Dim IsDef_price_change_type : IsDef_price_change_type = CCIsDefined("price_change_type_" & CurrentRow, "Form")
        Dim IsDef_new_price : IsDef_new_price = CCIsDefined("new_price_" & CurrentRow, "Form")
        If Not InsertOmitIfEmpty("price_change_id") Or IsDef_price_change_id Then Cmd.AddSQLStrings "price_change_id", Connection.ToSQL(price_change_id, price_change_id.DataType)
        If Not InsertOmitIfEmpty("ss_id") Or IsDef_ss_id Then Cmd.AddSQLStrings "ss_id", Connection.ToSQL(ss_id, ss_id.DataType)
        If Not InsertOmitIfEmpty("grade_id") Or IsDef_grade_id Then Cmd.AddSQLStrings "grade_id", Connection.ToSQL(grade_id, grade_id.DataType)
        If Not InsertOmitIfEmpty("levels") Or IsDef_levels Then Cmd.AddSQLStrings "levels", Connection.ToSQL(levels, levels.DataType)
        If Not InsertOmitIfEmpty("ppu_actual") Or IsDef_ppu_actual Then Cmd.AddSQLStrings "ppu_actual", Connection.ToSQL(ppu_actual, ppu_actual.DataType)
        If Not InsertOmitIfEmpty("price_change_type") Or IsDef_price_change_type Then Cmd.AddSQLStrings "price_change_type", Connection.ToSQL(price_change_type, price_change_type.DataType)
        If Not InsertOmitIfEmpty("new_price") Or IsDef_new_price Then Cmd.AddSQLStrings "new_price", Connection.ToSQL(new_price, new_price.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "price_send", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End price_sendDataSource Class @185-A61BA892

Class clsGridGrid3 'Grid3 Class @218-20B662CC

'Grid3 Variables @218-3B295878

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public ActiveSorter, SortingDirection
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Sorter_price_change_id
    Dim Sorter_SiteName
    Dim Sorter_Grade
    Dim Sorter_levels
    Dim Sorter_ppu_actual
    Dim Sorter_change_type
    Dim Sorter_new_price
    Dim price_change_id
    Dim SiteName
    Dim Grade
    Dim levels
    Dim ppu_actual
    Dim change_type
    Dim new_price
    Dim Navigator
'End Grid3 Variables

'Grid3 Class_Initialize Event @218-0E166C27
    Private Sub Class_Initialize()
        ComponentName = "Grid3"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsGrid3DataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If
        ActiveSorter = CCGetParam("Grid3Order", Empty)
        SortingDirection = CCGetParam("Grid3Dir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Sorter_price_change_id = CCCreateSorter("Sorter_price_change_id", Me, FileName)
        Set Sorter_SiteName = CCCreateSorter("Sorter_SiteName", Me, FileName)
        Set Sorter_Grade = CCCreateSorter("Sorter_Grade", Me, FileName)
        Set Sorter_levels = CCCreateSorter("Sorter_levels", Me, FileName)
        Set Sorter_ppu_actual = CCCreateSorter("Sorter_ppu_actual", Me, FileName)
        Set Sorter_change_type = CCCreateSorter("Sorter_change_type", Me, FileName)
        Set Sorter_new_price = CCCreateSorter("Sorter_new_price", Me, FileName)
        Set price_change_id = CCCreateControl(ccsLabel, "price_change_id", Empty, ccsInteger, Empty, CCGetRequestParam("price_change_id", ccsGet))
        Set SiteName = CCCreateControl(ccsLabel, "SiteName", Empty, ccsText, Empty, CCGetRequestParam("SiteName", ccsGet))
        Set Grade = CCCreateControl(ccsLabel, "Grade", Empty, ccsText, Empty, CCGetRequestParam("Grade", ccsGet))
        Set levels = CCCreateControl(ccsLabel, "levels", Empty, ccsInteger, Empty, CCGetRequestParam("levels", ccsGet))
        Set ppu_actual = CCCreateControl(ccsLabel, "ppu_actual", Empty, ccsFloat, Empty, CCGetRequestParam("ppu_actual", ccsGet))
        Set change_type = CCCreateControl(ccsLabel, "change_type", Empty, ccsText, Empty, CCGetRequestParam("change_type", ccsGet))
        Set new_price = CCCreateControl(ccsLabel, "new_price", Empty, ccsFloat, Empty, CCGetRequestParam("new_price", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Grid3 Class_Initialize Event

'Grid3 Initialize Method @218-2AEA3975
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.SetOrder ActiveSorter, SortingDirection
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Grid3 Initialize Method

'Grid3 Class_Terminate Event @218-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Grid3 Class_Terminate Event

'Grid3 Show Method @218-21F4C68B
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock

        With DataSource
            .Parameters("urlprice_change_id") = CCGetRequestParam("price_change_id", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Sorter_price_change_id, Sorter_SiteName, Sorter_Grade, Sorter_levels, Sorter_ppu_actual, Sorter_change_type, Sorter_new_price, Navigator))
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(price_change_id, SiteName, Grade, levels, ppu_actual, change_type, new_price))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Grid3:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    price_change_id.Value = Recordset.Fields("price_change_id")
                    SiteName.Value = Recordset.Fields("SiteName")
                    Grade.Value = Recordset.Fields("Grade")
                    levels.Value = Recordset.Fields("levels")
                    ppu_actual.Value = Recordset.Fields("ppu_actual")
                    change_type.Value = Recordset.Fields("change_type")
                    new_price.Value = Recordset.Fields("new_price")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Grid3:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Grid3:"
            StaticControls.Show
        End If

    End Sub
'End Grid3 Show Method

'Grid3 PageSize Property Let @218-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Grid3 PageSize Property Let

'Grid3 PageSize Property Get @218-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Grid3 PageSize Property Get

'Grid3 RowNumber Property Get @218-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Grid3 RowNumber Property Get

'Grid3 HasNextRow Function @218-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Grid3 HasNextRow Function

End Class 'End Grid3 Class @218-A61BA892

Class clsGrid3DataSource 'Grid3DataSource Class @218-85956FA2

'DataSource Variables @218-57953EF2
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public price_change_id
    Public SiteName
    Public Grade
    Public levels
    Public ppu_actual
    Public change_type
    Public new_price
'End DataSource Variables

'DataSource Class_Initialize Event @218-1A6117E1
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set price_change_id = CCCreateField("price_change_id", "price_change_id", ccsInteger, Empty, Recordset)
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Grade = CCCreateField("Grade", "Grade", ccsText, Empty, Recordset)
        Set levels = CCCreateField("levels", "levels", ccsInteger, Empty, Recordset)
        Set ppu_actual = CCCreateField("ppu_actual", "ppu_actual", ccsFloat, Empty, Recordset)
        Set change_type = CCCreateField("change_type", "change_type", ccsText, Empty, Recordset)
        Set new_price = CCCreateField("new_price", "new_price", ccsFloat, Empty, Recordset)
        Fields.AddFields Array(price_change_id, SiteName, Grade, levels, ppu_actual, change_type, new_price)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_price_change_id", "price_change_id", ""), _
            Array("Sorter_SiteName", "SiteName", ""), _
            Array("Sorter_Grade", "Grade", ""), _
            Array("Sorter_levels", "levels", ""), _
            Array("Sorter_ppu_actual", "ppu_actual", ""), _
            Array("Sorter_change_type", "change_type", ""), _
            Array("Sorter_new_price", "new_price", ""))

        SQL = "select p.price_change_id, s.SiteName, g.grade_name as Grade, p.levels, p.ppu_actual, t.price_change_description as change_type, p.new_price " & vbLf & _
        "from (((price_send as p inner join sites as s on p.ss_id = s.ss_id) inner join grades as g on p.grade_id = g.grade_id) inner join price_type as t on p.price_change_type = t.price_change_type) " & vbLf & _
        "where p.price_change_id = '{price_change_id}'"
        CountSQL = "SELECT COUNT(*) FROM (select p.price_change_id, s.SiteName, g.grade_name as Grade, p.levels, p.ppu_actual, t.price_change_description as change_type, p.new_price " & vbLf & _
        "from (((price_send as p inner join sites as s on p.ss_id = s.ss_id) inner join grades as g on p.grade_id = g.grade_id) inner join price_type as t on p.price_change_type = t.price_change_type) " & vbLf & _
        "where p.price_change_id = '{price_change_id}') cnt"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @218-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @218-83EA6348
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "price_change_id", "urlprice_change_id", ccsText, Empty, Empty, Empty, False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @218-CA87DA7C
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @218-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Grid3DataSource Class @218-A61BA892

'Include Page Implementation @2-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
