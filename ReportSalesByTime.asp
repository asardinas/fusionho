<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-A28C4F98
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Header
Dim pump_sales_Sites
Dim Sites_pump_sales
Dim Report_Print
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "ReportSalesByTime.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "ReportSalesByTime.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-C00A54EC
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Set pump_sales_Sites = New clsReportpump_sales_Sites
Set Sites_pump_sales = new clsRecordSites_pump_sales
Set Report_Print = CCCreateControl(ccsLink, "Report_Print", Empty, ccsText, Empty, CCGetRequestParam("Report_Print", ccsGet))

Report_Print.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
Report_Print.Parameters = CCAddParam(Report_Print.Parameters, "ViewMode", "Print")
Report_Print.Page = "ReportSalesByTime.asp"
pump_sales_Sites.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/ReportSalesByTime_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-C02D41A6
Header.Operations
Sites_pump_sales.Operation
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-121C1EBD
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Header, pump_sales_Sites, Sites_pump_sales, Report_Print))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-DC0192C8
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Header.UnloadPage
    Set Header = Nothing
    Set pump_sales_Sites = Nothing
    Set Sites_pump_sales = Nothing
End Sub
'End UnloadPage Sub

'pump_sales_Sites clsReportGroup @61-3F1465C9
Class clsReportGrouppump_sales_Sites
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public Site_Name
    Public Sale_Time
    Public Average_Money
    Public Average_Volume
    Public Sum_Money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_Money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_Money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public Site_NameTotalIndex

    Public Sub SetControls()
        Me.Site_Name = pump_sales_Sites.Site_Name.Value
        Me.Sale_Time = pump_sales_Sites.Sale_Time.Value
        Me.Average_Money = pump_sales_Sites.Average_Money.Value
        Me.Average_Volume = pump_sales_Sites.Average_Volume.Value
        Me.Sum_Money = pump_sales_Sites.Sum_Money.Value
        Me.Sum_Volume = pump_sales_Sites.Sum_Volume.Value
        Me.Count_Sales = pump_sales_Sites.Count_Sales.Value
        Me.Report_CurrentDate = pump_sales_Sites.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.Site_Name = HeaderGrp.Site_Name
        pump_sales_Sites.Site_Name.ChangeValue(Me.Site_Name)
        Me.Sale_Time = HeaderGrp.Sale_Time
        pump_sales_Sites.Sale_Time.ChangeValue(Me.Sale_Time)
        Me.Average_Money = HeaderGrp.Average_Money
        pump_sales_Sites.Average_Money.ChangeValue(Me.Average_Money)
        Me.Average_Volume = HeaderGrp.Average_Volume
        pump_sales_Sites.Average_Volume.ChangeValue(Me.Average_Volume)
        Me.Sum_Money = HeaderGrp.Sum_Money
        pump_sales_Sites.Sum_Money.ChangeValue(Me.Sum_Money)
        Me.Sum_Volume = HeaderGrp.Sum_Volume
        pump_sales_Sites.Sum_Volume.ChangeValue(Me.Sum_Volume)
        Me.Count_Sales = HeaderGrp.Count_Sales
        pump_sales_Sites.Count_Sales.ChangeValue(Me.Count_Sales)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        pump_sales_Sites.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = pump_sales_Sites.Report_TotalRecords.GetTotalValue(isCalculate)
        Me.Sum_Sum_Money = pump_sales_Sites.Sum_Sum_Money.GetTotalValue(isCalculate)
        Me.Sum_Sum_Volume = pump_sales_Sites.Sum_Sum_Volume.GetTotalValue(isCalculate)
        Me.Sum_Count_Sales = pump_sales_Sites.Sum_Count_Sales.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_Money = pump_sales_Sites.TotalSum_Sum_Money.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_Volume = pump_sales_Sites.TotalSum_Sum_Volume.GetTotalValue(isCalculate)
        Me.TotalSum_Count_Sales = pump_sales_Sites.TotalSum_Count_Sales.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = pump_sales_Sites.Report_TotalRecords.Value
        Me.Sum_Sum_Money = pump_sales_Sites.Sum_Sum_Money.Value
        Me.Sum_Sum_Volume = pump_sales_Sites.Sum_Sum_Volume.Value
        Me.Sum_Count_Sales = pump_sales_Sites.Sum_Count_Sales.Value
        Me.TotalSum_Sum_Money = pump_sales_Sites.TotalSum_Sum_Money.Value
        Me.TotalSum_Sum_Volume = pump_sales_Sites.TotalSum_Sum_Volume.Value
        Me.TotalSum_Count_Sales = pump_sales_Sites.TotalSum_Count_Sales.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End pump_sales_Sites clsReportGroup

'clspump_sales_SitesGroupsCollection @61-603E65E9
Class clspump_sales_SitesGroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSite_NameCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSite_NameCurrentHeaderIndex = 2
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGrouppump_sales_Sites
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.Site_NameTotalIndex = mSite_NameCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + pump_sales_Sites.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + pump_sales_Sites.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "Site_Name" Then
            If PageSize > 0 And pump_sales_Sites.Site_Name_Header.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Site_Name_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + pump_sales_Sites.Site_Name_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Site_Name_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mSite_NameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Site_Name"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And pump_sales_Sites.Report_Footer.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + pump_sales_Sites.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And pump_sales_Sites.Site_Name_Footer.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Site_Name_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + pump_sales_Sites.Site_Name_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSite_NameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Site_Name_Footer_OnCalculate", Me)
        pump_sales_Sites.Sum_Sum_Money.Reset()
        pump_sales_Sites.Sum_Sum_Volume.Reset()
        pump_sales_Sites.Sum_Count_Sales.Reset()
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Site_Name"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        pump_sales_Sites.Report_TotalRecords.Value = pump_sales_Sites.Report_TotalRecords.InitialValue
        pump_sales_Sites.Site_Name.Value = pump_sales_Sites.Site_Name.InitialValue
        pump_sales_Sites.Sale_Time.Value = pump_sales_Sites.Sale_Time.InitialValue
        pump_sales_Sites.Average_Money.Value = pump_sales_Sites.Average_Money.InitialValue
        pump_sales_Sites.Average_Volume.Value = pump_sales_Sites.Average_Volume.InitialValue
        pump_sales_Sites.Sum_Money.Value = pump_sales_Sites.Sum_Money.InitialValue
        pump_sales_Sites.Sum_Volume.Value = pump_sales_Sites.Sum_Volume.InitialValue
        pump_sales_Sites.Count_Sales.Value = pump_sales_Sites.Count_Sales.InitialValue
        pump_sales_Sites.Sum_Sum_Money.Value = pump_sales_Sites.Sum_Sum_Money.InitialValue
        pump_sales_Sites.Sum_Sum_Volume.Value = pump_sales_Sites.Sum_Sum_Volume.InitialValue
        pump_sales_Sites.Sum_Count_Sales.Value = pump_sales_Sites.Sum_Count_Sales.InitialValue
        pump_sales_Sites.TotalSum_Sum_Money.Value = pump_sales_Sites.TotalSum_Sum_Money.InitialValue
        pump_sales_Sites.TotalSum_Sum_Volume.Value = pump_sales_Sites.TotalSum_Sum_Volume.InitialValue
        pump_sales_Sites.TotalSum_Count_Sales.Value = pump_sales_Sites.TotalSum_Count_Sales.InitialValue
        pump_sales_Sites.Report_CurrentDate.Value = pump_sales_Sites.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And pump_sales_Sites.Detail.Visible And CurrentPageSize + pump_sales_Sites.Page_Footer.Height + pump_sales_Sites.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + pump_sales_Sites.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(pump_sales_Sites.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clspump_sales_SitesGroupsCollection

Class clsReportpump_sales_Sites 'pump_sales_Sites Class @61-C8B41E73

'pump_sales_Sites Variables @61-7867CC2B

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public Site_Name_HeaderBlock, Site_Name_Header
    Public Site_Name_FooterBlock, Site_Name_Footer
    Public ActiveSorter, SortingDirection
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public Site_Name_HeaderControls, Site_Name_FooterControls
    Dim Report_TotalRecords
    Dim Sorter_Sale_Time
    Dim Sorter_Average_Money
    Dim Sorter_Average_Volume
    Dim Sorter_Sum_Volume
    Dim Sorter_Count_Sales
    Dim Sorter_Sum_Money
    Dim Site_Name
    Dim Sale_Time
    Dim Average_Money
    Dim Average_Volume
    Dim Sum_Money
    Dim Sum_Volume
    Dim Count_Sales
    Dim Sum_Sum_Money
    Dim Sum_Sum_Volume
    Dim Sum_Count_Sales
    Dim NoRecords
    Dim TotalSum_Sum_Money
    Dim TotalSum_Sum_Volume
    Dim TotalSum_Count_Sales
    Dim PageBreak
    Dim Report_CurrentDate
    Dim Navigator
'End pump_sales_Sites Variables

'pump_sales_Sites Class_Initialize Event @61-BAAAB3B2
    Private Sub Class_Initialize()
        ComponentName = "pump_sales_Sites"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 2
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set Site_Name_Footer = new clsSection
        Site_Name_Footer.Visible = True
        Site_Name_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Footer.Height)
        Set Site_Name_Header = new clsSection
        Site_Name_Header.Visible = True
        Site_Name_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clspump_sales_SitesDataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If
        ActiveSorter = CCGetParam("pump_sales_SitesOrder", Empty)
        SortingDirection = CCGetParam("pump_sales_SitesDir", Empty)
        If NOT(SortingDirection = "ASC" OR SortingDirection = "DESC") Then _
            SortingDirection = Empty

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Sorter_Sale_Time = CCCreateSorter("Sorter_Sale_Time", Me, FileName)
        Set Sorter_Average_Money = CCCreateSorter("Sorter_Average_Money", Me, FileName)
        Set Sorter_Average_Volume = CCCreateSorter("Sorter_Average_Volume", Me, FileName)
        Set Sorter_Sum_Volume = CCCreateSorter("Sorter_Sum_Volume", Me, FileName)
        Set Sorter_Count_Sales = CCCreateSorter("Sorter_Count_Sales", Me, FileName)
        Set Sorter_Sum_Money = CCCreateSorter("Sorter_Sum_Money", Me, FileName)
        Set Site_Name = CCCreateReportLabel( "Site_Name", Empty, ccsText, Empty, CCGetRequestParam("Site_Name", ccsGet), "",  False, False,"")
        Set Sale_Time = CCCreateReportLabel( "Sale_Time", Empty, ccsInteger, Empty, CCGetRequestParam("Sale_Time", ccsGet), "",  False, False,"")
        Set Average_Money = CCCreateReportLabel( "Average_Money", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Average_Money", ccsGet), "",  False, False,"")
        Set Average_Volume = CCCreateReportLabel( "Average_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Average_Volume", ccsGet), "",  False, False,"")
        Set Sum_Money = CCCreateReportLabel( "Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Money", ccsGet), "",  False, False,"")
        Set Sum_Volume = CCCreateReportLabel( "Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Volume", ccsGet), "",  False, False,"")
        Set Count_Sales = CCCreateReportLabel( "Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Count_Sales", ccsGet), "",  False, False,"")
        Set Sum_Sum_Money = CCCreateReportLabel( "Sum_Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Money", ccsGet), "Sum",  False, False,"")
        Set Sum_Sum_Volume = CCCreateReportLabel( "Sum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set Sum_Count_Sales = CCCreateReportLabel( "Sum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Sum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set TotalSum_Sum_Money = CCCreateReportLabel( "TotalSum_Sum_Money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_Money", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Sum_Volume = CCCreateReportLabel( "TotalSum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Count_Sales = CCCreateReportLabel( "TotalSum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("TotalSum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set PageBreak = CCCreatePanel("PageBreak")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End pump_sales_Sites Class_Initialize Event

'pump_sales_Sites Initialize Method @61-BD78A2B0
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.SetOrder ActiveSorter, SortingDirection
    End Sub
'End pump_sales_Sites Initialize Method

'pump_sales_Sites Class_Terminate Event @61-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End pump_sales_Sites Class_Terminate Event

'pump_sales_Sites Show Method @61-D75C6265
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("urls_Site_Name") = CCGetRequestParam("s_Site_Name", ccsGET)
            .Parameters("expr103") = Sites_pump_sales.s_Sale_Date0.value
            .Parameters("expr119") = Sites_pump_sales.s_Sale_Date.value
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set Site_Name_HeaderBlock = TemplateBlock.Block("Section Site_Name_Header")
        Set Site_Name_FooterBlock = TemplateBlock.Block("Section Site_Name_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(Sale_Time, Average_Money, Average_Volume, Sum_Money, Sum_Volume, Count_Sales))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords, TotalSum_Sum_Money, TotalSum_Sum_Volume, TotalSum_Count_Sales))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(PageBreak, Report_CurrentDate, Navigator))
        Set Page_HeaderControls = CCCreateCollection(Page_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sorter_Sale_Time, Sorter_Average_Money, Sorter_Average_Volume, Sorter_Sum_Volume, Sorter_Count_Sales, Sorter_Sum_Money))
        Set Site_Name_HeaderControls = CCCreateCollection(Site_Name_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Site_Name))
        Set Site_Name_FooterControls = CCCreateCollection(Site_Name_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sum_Sum_Money, Sum_Sum_Volume, Sum_Count_Sales))
        Dim Site_NameKey
        Dim Groups
        Set Groups = New clspump_sales_SitesGroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report pump_sales_Sites", Errors)
        Else
            Do While Not Recordset.EOF
                Site_Name.Value = Recordset.Fields("Site_Name")
                Sale_Time.Value = Recordset.Fields("Sale_Time")
                Average_Money.Value = Recordset.Fields("Average_Money")
                Average_Volume.Value = Recordset.Fields("Average_Volume")
                Sum_Money.Value = Recordset.Fields("Sum_Money")
                Sum_Volume.Value = Recordset.Fields("Sum_Volume")
                Count_Sales.Value = Recordset.Fields("Count_Sales")
                Sum_Sum_Money.Value = Recordset.Fields("Sum_Sum_Money")
                Sum_Sum_Volume.Value = Recordset.Fields("Sum_Sum_Volume")
                Sum_Count_Sales.Value = Recordset.Fields("Sum_Count_Sales")
                TotalSum_Sum_Money.Value = Recordset.Fields("TotalSum_Sum_Money")
                TotalSum_Sum_Volume.Value = Recordset.Fields("TotalSum_Sum_Volume")
                TotalSum_Count_Sales.Value = Recordset.Fields("TotalSum_Count_Sales")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or Site_NameKey <> Recordset.Fields("Site_Name") Then
                    Groups.OpenGroup "Site_Name"
                End If
                Groups.AddItem 
                Site_NameKey = Recordset.Fields("Site_Name")
                Recordset.MoveNext
                If Site_NameKey <> Recordset.Fields("Site_Name") Or Recordset.EOF Then
                    Groups.CloseGroup "Site_Name"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        Sale_Time.Value = items(i).Sale_Time
                        Average_Money.Value = items(i).Average_Money
                        Average_Volume.Value = items(i).Average_Volume
                        Sum_Money.Value = items(i).Sum_Money
                        Sum_Volume.Value = items(i).Sum_Volume
                        Count_Sales.Value = items(i).Count_Sales
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        TotalSum_Sum_Money.Value = items(i).TotalSum_Sum_Money
                        TotalSum_Sum_Volume.Value = items(i).TotalSum_Sum_Volume
                        TotalSum_Count_Sales.Value = items(i).TotalSum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                PageBreak.Visible = (i < EndItem-1 And ViewMode <> "Web")
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        Navigator.PageSize = PageSize
                        Navigator.PagesCount = Groups.TotalPages
                        Navigator.PageNumber = items(i).PageNumber
                        Navigator.Visible = ("Web" = ViewMode)
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderControls.Show
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "Site_Name"
                        Site_Name.Value = items(i).Site_Name
                        Sum_Sum_Money.Value = items(i).Sum_Sum_Money
                        Sum_Sum_Volume.Value = items(i).Sum_Sum_Volume
                        Sum_Count_Sales.Value = items(i).Sum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Header_BeforeShow", Me)
                            If Site_Name_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_HeaderBlock, AttributePrefix
                                Site_Name_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Footer_BeforeShow", Me)
                            If Site_Name_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_FooterBlock, AttributePrefix
                                Site_Name_FooterControls.Show
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End pump_sales_Sites Show Method

'pump_sales_Sites PageSize Property Let @61-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End pump_sales_Sites PageSize Property Let

'pump_sales_Sites PageSize Property Get @61-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End pump_sales_Sites PageSize Property Get

End Class 'End pump_sales_Sites Class @61-A61BA892

Class clspump_sales_SitesDataSource 'pump_sales_SitesDataSource Class @61-E3E0CF15

'DataSource Variables @61-E6B7DC56
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Site_Name
    Public Sale_Time
    Public Average_Money
    Public Average_Volume
    Public Sum_Money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_Money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_Money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
'End DataSource Variables

'DataSource Class_Initialize Event @61-6603AD0E
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Site_Name = CCCreateField("Site_Name", "Site_Name", ccsText, Empty, Recordset)
        Set Sale_Time = CCCreateField("Sale_Time", "Sale_Time", ccsInteger, Empty, Recordset)
        Set Average_Money = CCCreateField("Average_Money", "Average_Money", ccsFloat, Empty, Recordset)
        Set Average_Volume = CCCreateField("Average_Volume", "Average_Volume", ccsFloat, Empty, Recordset)
        Set Sum_Money = CCCreateField("Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set Sum_Volume = CCCreateField("Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Count_Sales = CCCreateField("Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set Sum_Sum_Money = CCCreateField("Sum_Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set Sum_Sum_Volume = CCCreateField("Sum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Sum_Count_Sales = CCCreateField("Sum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set TotalSum_Sum_Money = CCCreateField("TotalSum_Sum_Money", "Sum_Money", ccsFloat, Empty, Recordset)
        Set TotalSum_Sum_Volume = CCCreateField("TotalSum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set TotalSum_Count_Sales = CCCreateField("TotalSum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(Site_Name,  Sale_Time,  Average_Money,  Average_Volume,  Sum_Money,  Sum_Volume,  Count_Sales, _
             Sum_Sum_Money,  Sum_Sum_Volume,  Sum_Count_Sales,  TotalSum_Sum_Money,  TotalSum_Sum_Volume,  TotalSum_Count_Sales)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing
        Orders = Array( _ 
            Array("Sorter_Sale_Time", "DAY_TIME", ""), _
            Array("Sorter_Average_Money", "Average_Money", ""), _
            Array("Sorter_Average_Volume", "Average_Volume", ""), _
            Array("Sorter_Sum_Volume", "Sum_Volume", ""), _
            Array("Sorter_Count_Sales", "Count_Sales", ""), _
            Array("Sorter_Sum_Money", "Sum_Money", ""))

        SQL = "SELECT SiteName AS Site_Name, DAY_TIME AS Sale_Time, Avg(money) AS Average_Money, avg(volume) AS Average_Volume, Sum(money) AS Sum_Money, " & vbLf & _
        "sum(volume) AS Sum_Volume, Count(sale_id) AS Count_Sales  " & vbLf & _
        "FROM Sites INNER JOIN pump_sales ON " & vbLf & _
        "Sites.ss_id = pump_sales.ss_id {SQL_Where} " & vbLf & _
        "GROUP BY SiteName, DAY_TIME {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) FROM (SELECT  SiteName AS Site_Name, DAY_TIME AS Sale_Time, Avg(money) AS Average_Money, avg(volume) AS Average_Volume, Sum(money) AS Sum_Money, " & vbLf & _
        "sum(volume) AS Sum_Volume, Count(sale_id) AS Count_Sales FROM Sites INNER JOIN pump_sales ON " & vbLf & _
        "Sites.ss_id = pump_sales.ss_id {SQL_Where} " & vbLf & _
        "GROUP BY SiteName, DAY_TIME) A"
        Where = ""
        Order = "DAY_TIME"
        StaticOrder = "Sites.SiteName asc"
    End Sub
'End DataSource Class_Initialize Event

'SetOrder Method @61-68FC9576
    Sub SetOrder(Column, Direction)
        Order = Recordset.GetOrder(Order, Column, Direction, Orders)
    End Sub
'End SetOrder Method

'BuildTableWhere Method @61-C0153C72
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urls_Site_Name", ccsText, Empty, Empty, Empty, False
            .AddParameter 2, "expr103", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), dateadd("d", -10,date()), False
            .AddParameter 3, "expr119", ccsDate, Array("mm", "/", "dd", "/", "yyyy"), Array("mm", "/", "dd", "/", "yyyy"), date(), False
            .Criterion(1) = .Operation(opContains, False, "[SiteName]", .getParamByID(1))
            .Criterion(2) = .Operation(opGreaterThanOrEqual, False, "DAY_DATE", .getParamByID(2))
            .Criterion(3) = .Operation(opLessThanOrEqual, False, "DAY_DATE", .getParamByID(3))
            .AssembledWhere = .opAND(False, .opAND(False, .Criterion(1), .Criterion(2)), .Criterion(3))
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @61-2C6FAEE2
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @61-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End pump_sales_SitesDataSource Class @61-A61BA892

Class clsRecordSites_pump_sales 'Sites_pump_sales Class @72-63D92FBE

'Sites_pump_sales Variables @72-06CCA0DA

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim ClearParameters
    Dim Button_DoSearch
    Dim s_Site_Name
    Dim s_Sale_Date0
    Dim DatePicker_s_Sale_Date1
    Dim s_Sale_Date
    Dim DatePicker_s_Sale_Date2
'End Sites_pump_sales Variables

'Sites_pump_sales Class_Initialize Event @72-8B9A361E
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        InsertAllowed = False
        UpdateAllowed = False
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Sites_pump_sales")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Sites_pump_sales"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set ClearParameters = CCCreateControl(ccsLink, "ClearParameters", Empty, ccsText, Empty, CCGetRequestParam("ClearParameters", Method))
        Set Button_DoSearch = CCCreateButton("Button_DoSearch", Method)
        Set s_Site_Name = CCCreateList(ccsListBox, "s_Site_Name", Empty, ccsText, CCGetRequestParam("s_Site_Name", Method), Empty)
        s_Site_Name.BoundColumn = "SiteName"
        s_Site_Name.TextColumn = "SiteName"
        Set s_Site_Name.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT SiteName  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
        Set s_Sale_Date0 = CCCreateControl(ccsTextBox, "s_Sale_Date0", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_Sale_Date0", Method))
        Set DatePicker_s_Sale_Date1 = CCCreateDatePicker("DatePicker_s_Sale_Date1", "Sites_pump_sales", "s_Sale_Date0")
        Set s_Sale_Date = CCCreateControl(ccsTextBox, "s_Sale_Date", Empty, ccsDate, Array("mm", "/", "dd", "/", "yyyy"), CCGetRequestParam("s_Sale_Date", Method))
        Set DatePicker_s_Sale_Date2 = CCCreateDatePicker("DatePicker_s_Sale_Date2", "Sites_pump_sales", "s_Sale_Date")
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(s_Site_Name, s_Sale_Date0, s_Sale_Date)
    End Sub
'End Sites_pump_sales Class_Initialize Event

'Sites_pump_sales Class_Terminate Event @72-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites_pump_sales Class_Terminate Event

'Sites_pump_sales Validate Method @72-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Sites_pump_sales Validate Method

'Sites_pump_sales Operation Method @72-E54DF4AA
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = "Button_DoSearch"
            If Button_DoSearch.Pressed Then
                PressedButton = "Button_DoSearch"
            End If
        End If
        Redirect = "ReportSalesByTime.asp"
        If Validate() Then
            If PressedButton = "Button_DoSearch" Then
                If NOT Button_DoSearch.OnClick() Then
                    Redirect = ""
                Else
                    Redirect = "ReportSalesByTime.asp?" & CCGetQueryString("Form", Array(PressedButton, "ccsForm", "Button_DoSearch.x", "Button_DoSearch.y", "Button_DoSearch"))
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Sites_pump_sales Operation Method

'Sites_pump_sales Show Method @72-EBFA22E7
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        EditMode = False
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Sites_pump_sales" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(s_Site_Name, s_Sale_Date0, DatePicker_s_Sale_Date1, s_Sale_Date, DatePicker_s_Sale_Date2, ClearParameters, Button_DoSearch))
        ClearParameters.Parameters = CCGetQueryString("QueryString", Array("s_Site_Name", "s_Sale_Date", "s_Sale_Date0", "ccsForm"))
        ClearParameters.Page = "ReportSalesByTime.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors s_Site_Name.Errors
            Errors.AddErrors s_Sale_Date0.Errors
            Errors.AddErrors s_Sale_Date.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Sites_pump_sales" & ":"
            Controls.Show
        End If
    End Sub
'End Sites_pump_sales Show Method

End Class 'End Sites_pump_sales Class @72-A61BA892



'Include Page Implementation @60-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
