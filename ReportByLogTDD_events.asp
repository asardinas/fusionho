<%
'BindEvents Method @1-22E683A8
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Report1.CCSEvents("Detail_BeforeShow") = GetRef("Report1_Detail_BeforeShow")
        Set Report1.Navigator.CCSEvents("BeforeShow") = GetRef("Report1_Page_Footer_Navigator_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Report1_Detail_BeforeShow(Sender) 'Report1_Detail_BeforeShow @7-CA962371

'Custom Code @46-73254650
' -------------------------
dim fecha
dim fecha2
dim diferencia
fecha=report1.File_Date.Value
'fecha2="09/09/2011"
fecha2=date()

diferencia = DateDiff("D", fecha, fecha2)
  If (diferencia >= 5) Then
    report1.ReportLabel1.Value = "Rowold"
  Else
    	report1.ReportLabel1.Value = "Rownew"
  End if

' -------------------------
'End Custom Code

End Function 'Close Report1_Detail_BeforeShow @7-54C34B28

Function Report1_Page_Footer_Navigator_BeforeShow(Sender) 'Report1_Page_Footer_Navigator_BeforeShow @15-0AA8A152

'Hide-Show Component @16-537B31AC
    Dim TotalPages_16_1 : TotalPages_16_1 = CCSConverter.VBSConvert(ccsInteger, Report1.DataSource.Recordset.PageCount)
    Dim Param2_16_2 : Param2_16_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_16_1) And Not IsEmpty(Param2_16_2) And TotalPages_16_1 < Param2_16_2) Then _
        Report1.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Report1_Page_Footer_Navigator_BeforeShow @15-54C34B28


%>
