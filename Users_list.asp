<%@ CodePage=1252 %>
<%
'Include Common Files @1-568FBDBB
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-2511A4AB
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Users
Dim Users1
Dim Header
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "Users_list.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "Users_list.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-561B94AA
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Users = New clsGridUsers
Set Users1 = new clsRecordUsers1
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Users.Initialize DBFusionHO
Users1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Users_list_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-17C67797
Users1.Operation
Header.Operations
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-D01E23A0
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Users, Users1, Header))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-84476E2C
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set Users = Nothing
    Set Users1 = Nothing
    Header.UnloadPage
    Set Header = Nothing
End Sub
'End UnloadPage Sub

Class clsGridUsers 'Users Class @2-E174CC68

'Users Variables @2-F3CDDB2C

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public DataSource
    Public PageNumber
    Public Command
    Public TemplateBlock
    Public IsDSEmpty
    Public ForceIteration
    Public Attributes
    Private ShownRecords
    Public Recordset

    Private CCSEventResult

    ' Grid Controls
    Public StaticControls, RowControls, NoRecordsControls
    Dim Users_Insert
    Dim User_id
    Dim Name
    Dim User_name
    Dim Password
    Dim Email
    Dim Departament
    Dim Telephone
    Dim Group_id
    Dim Navigator
'End Users Variables

'Users Class_Initialize Event @2-423DC1E1
    Private Sub Class_Initialize()
        ComponentName = "Users"
        Visible = True
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set Errors = New clsErrors
        Set DataSource = New clsUsersDataSource
        Set Command = New clsCommand
        PageSize = CCGetParam(ComponentName & "PageSize", Empty)
        If IsNumeric(PageSize) And Len(PageSize) > 0 Then
            If PageSize <= 0 Then Errors.AddError(CCSLocales.GetText("CCS_GridPageSizeError", Empty))
            If PageSize > 100 Then PageSize = 100
        End If
        If NOT IsNumeric(PageSize) OR IsEmpty(PageSize) Then _
            PageSize = 10 _
        Else _
            PageSize = CInt(PageSize)
        PageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(PageNumber) And Len(PageNumber) > 0 Then
            Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
            PageNumber = 1
        ElseIf Len(PageNumber) > 0 Then
            If PageNumber > 0 Then
                PageNumber = CLng(PageNumber)
            Else
                Errors.AddError(CCSLocales.GetText("CCS_GridPageNumberError", Empty))
                PageNumber = 1
            End If
        Else
            PageNumber = 1
        End If

        Set Users_Insert = CCCreateControl(ccsLink, "Users_Insert", Empty, ccsText, Empty, CCGetRequestParam("Users_Insert", ccsGet))
        Set User_id = CCCreateControl(ccsLink, "User_id", Empty, ccsInteger, Empty, CCGetRequestParam("User_id", ccsGet))
        Set Name = CCCreateControl(ccsLabel, "Name", Empty, ccsText, Empty, CCGetRequestParam("Name", ccsGet))
        Set User_name = CCCreateControl(ccsLabel, "User_name", Empty, ccsText, Empty, CCGetRequestParam("User_name", ccsGet))
        Set Password = CCCreateControl(ccsLabel, "Password", Empty, ccsText, Empty, CCGetRequestParam("Password", ccsGet))
        Set Email = CCCreateControl(ccsLabel, "Email", Empty, ccsText, Empty, CCGetRequestParam("Email", ccsGet))
        Set Departament = CCCreateControl(ccsLabel, "Departament", Empty, ccsText, Empty, CCGetRequestParam("Departament", ccsGet))
        Set Telephone = CCCreateControl(ccsLabel, "Telephone", Empty, ccsText, Empty, CCGetRequestParam("Telephone", ccsGet))
        Set Group_id = CCCreateControl(ccsLabel, "Group_id", Empty, ccsInteger, Empty, CCGetRequestParam("Group_id", ccsGet))
        Set Navigator = CCCreateNavigator(ComponentName, "Navigator", FileName, 10, tpCentered)
        Navigator.PageSizes = Array("1", "5", "10", "25", "50")
    IsDSEmpty = True
    End Sub
'End Users Class_Initialize Event

'Users Initialize Method @2-57CE6952
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
        DataSource.PageSize = PageSize
        DataSource.AbsolutePage = PageNumber
    End Sub
'End Users Initialize Method

'Users Class_Terminate Event @2-B97CC660
    Private Sub Class_Terminate()
        Set CCSEvents = Nothing
        Set DataSource = Nothing
        Set Command = Nothing
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Users Class_Terminate Event

'Users Show Method @2-0B4B17BB
    Sub Show(Tpl)
        Dim HasNext
        If NOT Visible Then Exit Sub

        Dim RowBlock, NoRecordsBlock


        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        If DataSource.Errors.Count = 0 Then IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Grid " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        Set RowBlock = TemplateBlock.Block("Row")
        Set NoRecordsBlock = TemplateBlock.Block("NoRecords")
        Set StaticControls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Users_Insert, Navigator))
            
            Users_Insert.Parameters = CCGetQueryString("QueryString", Array("User_id", "ccsForm"))
            Users_Insert.Parameters = CCAddParam(Users_Insert.Parameters, "var", 1)
            Users_Insert.Page = "Users_list.asp"
            Navigator.PageSize = PageSize
            Navigator.SetDataSource Recordset
        Set RowControls = CCCreateCollection(RowBlock, Null, ccsParseAccumulate, _
            Array(User_id, Name, User_name, Password, Email, Departament, Telephone, Group_id))

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If NOT Visible Then Exit Sub

        RowControls.PreserveControlsVisible
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Grid " & ComponentName, Errors)
        Else

            ' Show NoRecords block if no records are found
            If Recordset.EOF Then
                Attributes.Show TemplateBlock, "Users:"
                TemplateBlock.Block("NoRecords").Parse ccsParseOverwrite
            End If
            HasNext = HasNextRow()
            ForceIteration = False
            Do While ForceIteration Or HasNext
                Attributes("rowNumber") = ShownRecords + 1
                If HasNext Then
                    User_id.Value = Recordset.Fields("User_id")
                    User_id.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
                    User_id.Parameters = CCAddParam(User_id.Parameters, "User_id", Recordset.Fields("User_id_param1"))
                    User_id.Page = "Users_list.asp"
                    Name.Value = Recordset.Fields("Name")
                    User_name.Value = Recordset.Fields("User_name")
                    Password.Value = Recordset.Fields("Password")
                    Email.Value = Recordset.Fields("Email")
                    Departament.Value = Recordset.Fields("Departament")
                    Telephone.Value = Recordset.Fields("Telephone")
                    Group_id.Value = Recordset.Fields("Group_id")
                End If
                CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShowRow", Me)
                Attributes.Show TemplateBlock.Block("Row"), "Users:"
                RowControls.Show
                If HasNext Then Recordset.MoveNext
                ShownRecords = ShownRecords + 1
                HasNext = HasNextRow()
            Loop
            Attributes.Show TemplateBlock, "Users:"
            StaticControls.Show
        End If

    End Sub
'End Users Show Method

'Users PageSize Property Let @2-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Users PageSize Property Let

'Users PageSize Property Get @2-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Users PageSize Property Get

'Users RowNumber Property Get @2-F32EE2C6
    Public Property Get RowNumber()
        RowNumber = ShownRecords + 1
    End Property
'End Users RowNumber Property Get

'Users HasNextRow Function @2-9BECE27A
    Public Function HasNextRow()
        HasNextRow = NOT Recordset.EOF AND ShownRecords < PageSize
    End Function
'End Users HasNextRow Function

End Class 'End Users Class @2-A61BA892

Class clsUsersDataSource 'UsersDataSource Class @2-5BA94802

'DataSource Variables @2-10A38681
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public User_id
    Public User_id_param1
    Public Name
    Public User_name
    Public Password
    Public Email
    Public Departament
    Public Telephone
    Public Group_id
'End DataSource Variables

'DataSource Class_Initialize Event @2-9D71BEF5
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set User_id = CCCreateField("User_id", "User_id", ccsInteger, Empty, Recordset)
        Set User_id_param1 = CCCreateField("User_id_param1", "User_id", ccsText, Empty, Recordset)
        Set Name = CCCreateField("Name", "Name", ccsText, Empty, Recordset)
        Set User_name = CCCreateField("User_name", "User_name", ccsText, Empty, Recordset)
        Set Password = CCCreateField("Password", "Password", ccsText, Empty, Recordset)
        Set Email = CCCreateField("Email", "Email", ccsText, Empty, Recordset)
        Set Departament = CCCreateField("Departament", "Departament", ccsText, Empty, Recordset)
        Set Telephone = CCCreateField("Telephone", "Telephone", ccsText, Empty, Recordset)
        Set Group_id = CCCreateField("Group_id", "Group_id", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(User_id, User_id_param1, Name, User_name, Password, Email, Departament, Telephone, Group_id)

        SQL = "SELECT TOP {SqlParam_endRecord} User_id, Name, User_name, Password, Email, Departament, Telephone, Group_id  " & vbLf & _
        "FROM Users {SQL_Where} {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) " & vbLf & _
        "FROM Users"
        Where = ""
        Order = "User_id"
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @2-98E5A92F
    Public Sub BuildTableWhere()
    End Sub
'End BuildTableWhere Method

'Open Method @2-6EA306C4
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL = CountSQL
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End UsersDataSource Class @2-A61BA892

Class clsRecordUsers1 'Users1 Class @22-10B852DC

'Users1 Variables @22-CE0C8EBA

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim Button_Insert
    Dim Button_Update
    Dim Button_Delete
    Dim Button_Cancel
    Dim User_name
    Dim Password
    Dim Name
    Dim Email
    Dim Departament
    Dim Telephone
    Dim Group_id
'End Users1 Variables

'Users1 Class_Initialize Event @22-1B9B840E
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsUsers1DataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Users1")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Users1"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set Button_Insert = CCCreateButton("Button_Insert", Method)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Delete = CCCreateButton("Button_Delete", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set User_name = CCCreateControl(ccsTextBox, "User_name", CCSLocales.GetText("User_name", ""), ccsText, Empty, CCGetRequestParam("User_name", Method))
        User_name.Required = True
        Set Password = CCCreateControl(ccsTextBox, "Password", CCSLocales.GetText("Password", ""), ccsText, Empty, CCGetRequestParam("Password", Method))
        Password.Required = True
        Set Name = CCCreateControl(ccsTextBox, "Name", CCSLocales.GetText("Name", ""), ccsText, Empty, CCGetRequestParam("Name", Method))
        Name.Required = True
        Set Email = CCCreateControl(ccsTextBox, "Email", CCSLocales.GetText("Email", ""), ccsText, Empty, CCGetRequestParam("Email", Method))
        Email.Required = True
        Email.InputMask = "^[\w\.-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$"
        Set Departament = CCCreateControl(ccsTextBox, "Departament", CCSLocales.GetText("Departament", ""), ccsText, Empty, CCGetRequestParam("Departament", Method))
        Departament.Required = True
        Set Telephone = CCCreateControl(ccsTextBox, "Telephone", CCSLocales.GetText("Telephone", ""), ccsText, Empty, CCGetRequestParam("Telephone", Method))
        Telephone.Required = True
        Telephone.InputMask = "^[0-9]{2,5}-? ?[0-9]{4,8}$"
        Set Group_id = CCCreateList(ccsListBox, "Group_id", CCSLocales.GetText("Group_id", ""), ccsInteger, CCGetRequestParam("Group_id", Method), Empty)
        Set Group_id.DataSource = CCCreateDataSource(dsListOfValues, Empty, Array( _
            Array("1", "2", "3"), _
            Array("User", "Manager", "Administrator")))
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(User_name, Password, Name, Email, Departament, Telephone, Group_id)
    End Sub
'End Users1 Class_Initialize Event

'Users1 Initialize Method @22-B87DFEC2
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
            .Parameters("urlUser_id") = CCGetRequestParam("User_id", ccsGET)
        End With
    End Sub
'End Users1 Initialize Method

'Users1 Class_Terminate Event @22-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Users1 Class_Terminate Event

'Users1 Validate Method @22-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Users1 Validate Method

'Users1 Operation Method @22-EF931C34
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Insert")
            If Button_Insert.Pressed Then
                PressedButton = "Button_Insert"
            ElseIf Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Delete.Pressed Then
                PressedButton = "Button_Delete"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = "Users_list.asp"
        If PressedButton = "Button_Delete" Then
            If NOT Button_Delete.OnClick OR NOT DeleteRow() Then
                Redirect = ""
            End If
        ElseIf PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Insert" Then
                If NOT Button_Insert.OnClick() OR NOT InsertRow() Then
                    Redirect = ""
                End If
            ElseIf PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Users1 Operation Method

'Users1 InsertRow Method @22-94EC59AB
    Function InsertRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInsert", Me)
        If NOT InsertAllowed Then InsertRow = False : Exit Function
        DataSource.User_name.Value = User_name.Value
        DataSource.Password.Value = Password.Value
        DataSource.Name.Value = Name.Value
        DataSource.Email.Value = Email.Value
        DataSource.Departament.Value = Departament.Value
        DataSource.Telephone.Value = Telephone.Value
        DataSource.Group_id.Value = Group_id.Value
        DataSource.Insert(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInsert", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        InsertRow = (Errors.Count = 0)
    End Function
'End Users1 InsertRow Method

'Users1 UpdateRow Method @22-A22739C1
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        DataSource.User_name.Value = User_name.Value
        DataSource.Password.Value = Password.Value
        DataSource.Name.Value = Name.Value
        DataSource.Email.Value = Email.Value
        DataSource.Departament.Value = Departament.Value
        DataSource.Telephone.Value = Telephone.Value
        DataSource.Group_id.Value = Group_id.Value
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End Users1 UpdateRow Method

'Users1 DeleteRow Method @22-D5C1DF24
    Function DeleteRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeDelete", Me)
        If NOT DeleteAllowed Then DeleteRow = False : Exit Function
        DataSource.Delete(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterDelete", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        DeleteRow = (Errors.Count = 0)
    End Function
'End Users1 DeleteRow Method

'Users1 Show Method @22-B0992817
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Users1" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Name,  User_name,  Password,  Email,  Departament,  Telephone,  Group_id, _
                 Button_Insert,  Button_Update,  Button_Delete,  Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        User_name.Value = Recordset.Fields("User_name")
                        Password.Value = Recordset.Fields("Password")
                        Name.Value = Recordset.Fields("Name")
                        Email.Value = Recordset.Fields("Email")
                        Departament.Value = Recordset.Fields("Departament")
                        Telephone.Value = Recordset.Fields("Telephone")
                        Group_id.Value = Recordset.Fields("Group_id")
                    End If
                Else
                    EditMode = False
                End If
            End If
        End If
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors User_name.Errors
            Errors.AddErrors Password.Errors
            Errors.AddErrors Name.Errors
            Errors.AddErrors Email.Errors
            Errors.AddErrors Departament.Errors
            Errors.AddErrors Telephone.Errors
            Errors.AddErrors Group_id.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Insert.Visible = NOT EditMode AND InsertAllowed
        Button_Update.Visible = EditMode AND UpdateAllowed
        Button_Delete.Visible = EditMode AND DeleteAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Users1" & ":"
            Controls.Show
        End If
    End Sub
'End Users1 Show Method

End Class 'End Users1 Class @22-A61BA892

Class clsUsers1DataSource 'Users1DataSource Class @22-45FF169C

'DataSource Variables @22-DC69D88F
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public User_name
    Public Password
    Public Name
    Public Email
    Public Departament
    Public Telephone
    Public Group_id
'End DataSource Variables

'DataSource Class_Initialize Event @22-19AC2FE4
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set User_name = CCCreateField("User_name", "User_name", ccsText, Empty, Recordset)
        Set Password = CCCreateField("Password", "Password", ccsText, Empty, Recordset)
        Set Name = CCCreateField("Name", "Name", ccsText, Empty, Recordset)
        Set Email = CCCreateField("Email", "Email", ccsText, Empty, Recordset)
        Set Departament = CCCreateField("Departament", "Departament", ccsText, Empty, Recordset)
        Set Telephone = CCCreateField("Telephone", "Telephone", ccsText, Empty, Recordset)
        Set Group_id = CCCreateField("Group_id", "Group_id", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(User_name, Password, Name, Email, Departament, Telephone, Group_id)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "User_name", True
        InsertOmitIfEmpty.Add "Password", True
        InsertOmitIfEmpty.Add "Name", True
        InsertOmitIfEmpty.Add "Email", True
        InsertOmitIfEmpty.Add "Departament", True
        InsertOmitIfEmpty.Add "Telephone", True
        InsertOmitIfEmpty.Add "Group_id", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "User_name", True
        UpdateOmitIfEmpty.Add "Password", True
        UpdateOmitIfEmpty.Add "Name", True
        UpdateOmitIfEmpty.Add "Email", True
        UpdateOmitIfEmpty.Add "Departament", True
        UpdateOmitIfEmpty.Add "Telephone", True
        UpdateOmitIfEmpty.Add "Group_id", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT *  " & vbLf & _
        "FROM Users {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @22-6153766C
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlUser_id", ccsInteger, Empty, Empty, Empty, False
            AllParamsSet = .AllParamsSet
            .Criterion(1) = .Operation(opEqual, False, "[User_id]", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @22-48A2DA7D
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @22-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @22-4796C466
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Cmd.SQL = "DELETE FROM [Users]" & IIf(Len(Where) > 0, " WHERE " & Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @22-EDD03071
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_User_name : IsDef_User_name = CCIsDefined("User_name", "Form")
        Dim IsDef_Password : IsDef_Password = CCIsDefined("Password", "Form")
        Dim IsDef_Name : IsDef_Name = CCIsDefined("Name", "Form")
        Dim IsDef_Email : IsDef_Email = CCIsDefined("Email", "Form")
        Dim IsDef_Departament : IsDef_Departament = CCIsDefined("Departament", "Form")
        Dim IsDef_Telephone : IsDef_Telephone = CCIsDefined("Telephone", "Form")
        Dim IsDef_Group_id : IsDef_Group_id = CCIsDefined("Group_id", "Form")
        If Not UpdateOmitIfEmpty("User_name") Or IsDef_User_name Then Cmd.AddSQLStrings "[User_name]=" & Connection.ToSQL(User_name, User_name.DataType), Empty
        If Not UpdateOmitIfEmpty("Password") Or IsDef_Password Then Cmd.AddSQLStrings "[Password]=" & Connection.ToSQL(Password, Password.DataType), Empty
        If Not UpdateOmitIfEmpty("Name") Or IsDef_Name Then Cmd.AddSQLStrings "[Name]=" & Connection.ToSQL(Name, Name.DataType), Empty
        If Not UpdateOmitIfEmpty("Email") Or IsDef_Email Then Cmd.AddSQLStrings "[Email]=" & Connection.ToSQL(Email, Email.DataType), Empty
        If Not UpdateOmitIfEmpty("Departament") Or IsDef_Departament Then Cmd.AddSQLStrings "[Departament]=" & Connection.ToSQL(Departament, Departament.DataType), Empty
        If Not UpdateOmitIfEmpty("Telephone") Or IsDef_Telephone Then Cmd.AddSQLStrings "[Telephone]=" & Connection.ToSQL(Telephone, Telephone.DataType), Empty
        If Not UpdateOmitIfEmpty("Group_id") Or IsDef_Group_id Then Cmd.AddSQLStrings "[Group_id]=" & Connection.ToSQL(Group_id, Group_id.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "[Users]", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @22-15D259C4
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_User_name : IsDef_User_name = CCIsDefined("User_name", "Form")
        Dim IsDef_Password : IsDef_Password = CCIsDefined("Password", "Form")
        Dim IsDef_Name : IsDef_Name = CCIsDefined("Name", "Form")
        Dim IsDef_Email : IsDef_Email = CCIsDefined("Email", "Form")
        Dim IsDef_Departament : IsDef_Departament = CCIsDefined("Departament", "Form")
        Dim IsDef_Telephone : IsDef_Telephone = CCIsDefined("Telephone", "Form")
        Dim IsDef_Group_id : IsDef_Group_id = CCIsDefined("Group_id", "Form")
        If Not InsertOmitIfEmpty("User_name") Or IsDef_User_name Then Cmd.AddSQLStrings "[User_name]", Connection.ToSQL(User_name, User_name.DataType)
        If Not InsertOmitIfEmpty("Password") Or IsDef_Password Then Cmd.AddSQLStrings "[Password]", Connection.ToSQL(Password, Password.DataType)
        If Not InsertOmitIfEmpty("Name") Or IsDef_Name Then Cmd.AddSQLStrings "[Name]", Connection.ToSQL(Name, Name.DataType)
        If Not InsertOmitIfEmpty("Email") Or IsDef_Email Then Cmd.AddSQLStrings "[Email]", Connection.ToSQL(Email, Email.DataType)
        If Not InsertOmitIfEmpty("Departament") Or IsDef_Departament Then Cmd.AddSQLStrings "[Departament]", Connection.ToSQL(Departament, Departament.DataType)
        If Not InsertOmitIfEmpty("Telephone") Or IsDef_Telephone Then Cmd.AddSQLStrings "[Telephone]", Connection.ToSQL(Telephone, Telephone.DataType)
        If Not InsertOmitIfEmpty("Group_id") Or IsDef_Group_id Then Cmd.AddSQLStrings "[Group_id]", Connection.ToSQL(Group_id, Group_id.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "[Users]", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End Users1DataSource Class @22-A61BA892

'Include Page Implementation @38-4175E89F
%>
<!-- #INCLUDE VIRTUAL="/Alpheton_HO/Header.asp" -->
<%
'End Include Page Implementation


%>
